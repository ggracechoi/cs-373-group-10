@startuml

left to right direction
!define TABLE_ENTITY(x) class x << (T, lightBlue) >>
!define PRIMARY_KEY(x) <u>x</u>
!define FOREIGN_KEY(x) <color:royalBlue>x</color>

' Define tables
TABLE_ENTITY(affordable_housing) {
  +PRIMARY_KEY(id)
  --
  name : text
  address : text
  students_only : text
  community_disabled : text
  waitlist : text
  website : text
  street_view_url : text
  google_maps_url : text
  google_maps_embed_url : text
  latitude : double
  longitude : double
}

TABLE_ENTITY(housing_pantry_association) {
  +FOREIGN_KEY(housing_id)
  +FOREIGN_KEY(pantry_id)
  --
  distance : float
}

TABLE_ENTITY(food_pantries) {
  +PRIMARY_KEY(id)
  --
  name : text
  address : text
  opening_hours : text
  website : text
  rating : double
  number_of_ratings : int
  street_view_url : text
  google_maps_url : text
  google_maps_embed_url : text
  latitude : double
  longitude : double
}


TABLE_ENTITY(pantry_thrift_association) {
  +FOREIGN_KEY(pantry_id)
  +FOREIGN_KEY(thrift_id)
  --
  distance : float
}


TABLE_ENTITY(thrift_stores) {
  +PRIMARY_KEY(id)
  --
  name : text
  address : text
  phone_number : text
  website : text
  rating : double
  number_of_ratings : int
  image_url : text
  google_maps_url : text
  google_maps_embed_url : text
  latitude : double
  longitude : double
}


TABLE_ENTITY(thrift_housing_association) {
  +FOREIGN_KEY(thrift_id)
  +FOREIGN_KEY(housing_id)
  --
  distance : float
}

' Many many relationship
affordable_housing "1"--"*" housing_pantry_association
housing_pantry_association "*" -- "1" food_pantries

food_pantries "1" -- "*" pantry_thrift_association
pantry_thrift_association "*" -- "1" thrift_stores

thrift_stores "1" -- "*" thrift_housing_association
thrift_housing_association "*" -- "1" affordable_housing

' Cleaner look
affordable_housing -[hidden]- housing_pantry_association
housing_pantry_association -[hidden]- food_pantries
food_pantries -[hidden]- pantry_thrift_association
pantry_thrift_association -[hidden]- thrift_stores
thrift_stores -[hidden]- thrift_housing_association


@enduml