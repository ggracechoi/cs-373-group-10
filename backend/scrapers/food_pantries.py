import requests
import json
import time
from urllib.parse import quote
import re

# Replace 'YOUR_API_KEY' with your actual Google API key
google_api_key = "AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE"
google_base_url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
street_view_base_url = "https://maps.googleapis.com/maps/api/streetview"

# Set the location to Austin
location = "Austin, TX"

# Set the search queries to find food-related places
keywords = ["food pantry", "food bank", "charity kitchen", "community kitchen"]

# Set a limit for the number of results
results_limit = 150

# Create and open a text file for writing
with open("food_pantries.txt", "w") as file:
    unique_places = set()
    count = 0
    # Perform multiple requests to handle pagination
    for keyword in keywords:
        google_params = {
            "query": keyword,
            "location": location,
            "key": google_api_key,
            "radius": 50000,
        }

        while True:
            # Make the Google Places API request
            google_response = requests.get(google_base_url, params=google_params)
            google_data = google_response.json()

            # Check if the Google Places API request was successful
            if google_data["status"] == "OK":
                # Iterate through the results
                for place in google_data["results"]:
                    # Check if the place is in Austin and is unique
                    if (
                        location.lower() in place["formatted_address"].lower()
                        and place["place_id"] not in unique_places
                    ):
                        unique_places.add(place["place_id"])
                        count += 1

                        # Fetch details for each place using Place Details API
                        details_url = (
                            "https://maps.googleapis.com/maps/api/place/details/json"
                        )
                        details_params = {
                            "place_id": place["place_id"],
                            "key": google_api_key,
                        }
                        details_response = requests.get(
                            details_url, params=details_params
                        )
                        details_data = details_response.json()

                        # Extract relevant information
                        name = place.get("name", "N/A")
                        address = details_data["result"].get("formatted_address", "N/A")
                        opening_hours = (
                            details_data["result"]
                            .get("opening_hours", {})
                            .get("weekday_text", "N/A")
                        )

                        if opening_hours != "N/A":
                            opening_hours = ", ".join(opening_hours)
                            opening_hours = opening_hours.replace("–", "-")
                            opening_hours = re.sub(r"[^\x00-\x7F]", " ", opening_hours)

                        website = details_data["result"].get("website", "N/A")
                        rating = details_data["result"].get("rating", "N/A")
                        num_ratings = details_data["result"].get(
                            "user_ratings_total", "N/A"
                        )
                        location_coords = details_data["result"]["geometry"]["location"]
                        latitude = location_coords["lat"]
                        longitude = location_coords["lng"]

                        # Constructing Google Maps URL using the place name
                        maps_url = f"https://www.google.com/maps/search/?api=1&query={quote(name)}"

                        # Construct Google Maps Embed URL
                        if latitude and longitude:
                            embedded_maps_url = f"https://www.google.com/maps/embed/v1/view?center={latitude},{longitude}&zoom=15&key={google_api_key}"
                        else:
                            embedded_maps_url = "N/A"

                        location_coords = details_data["result"]["geometry"]["location"]
                        latitude = location_coords["lat"]
                        longitude = location_coords["lng"]

                        # Constructing street view URL
                        street_view_url = f"{street_view_base_url}?location={latitude},{longitude}&key={google_api_key}&size=200x200&fov=90"

                        # Write the information to the file, including street view URL
                        file.write(
                            f"Place {count} - Name: {name}\nAddress: {address}\nOpening Hours: {opening_hours}\nWebsite: {website}\nRating: {rating}\nNumber of Ratings: {num_ratings}\nStreet View URL: {street_view_url}\nGoogle Maps URL: {maps_url}\nGoogle Maps Embed URL: {embedded_maps_url}\n"
                        )

                        file.write("\n")
            else:
                break

            # Check if there is a next page
            if "next_page_token" in google_data:
                next_page_token = google_data["next_page_token"]
                # Wait for a few seconds before making the next request
                time.sleep(20)
                google_params["pagetoken"] = next_page_token
            else:
                break

print(f"Information written to 'food_pantries_austin.txt'")
