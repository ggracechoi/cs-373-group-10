import requests
from urllib.parse import quote

google_api_key = "AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90"
street_view_base_url = "https://maps.googleapis.com/maps/api/streetview"

# Affordable housing in Austin API endpoint
gov_api_url = "https://data.austintexas.gov/resource/4syj-z4ky.json"

# Request data from the affordable housing website
response = requests.get(gov_api_url)
if response.status_code == 200:
    housing_data = response.json()
else:
    print(f"Failed to fetch data: HTTP {response.status_code}")
    housing_data = []

# File to store the housing data
filename = "affordable_housing.txt"

# Write data to file
with open(filename, "w") as file:
    for index, housing in enumerate(housing_data, start=1):
        property_name = housing.get("property_name", "N/A")
        address = housing.get("address", "N/A")
        city = housing.get("city", "Austin")
        state = housing.get("state", "TX")
        zip_code = housing.get("zip_code", "N/A")
        students_only = housing.get("students_only", "N/A")
        community_disabled = housing.get("community_disabled", "N/A")
        has_waitlist = housing.get("has_waitlist", "N/A")
        website = housing.get("website", "N/A")
        latitude = housing.get("latitude")
        longitude = housing.get("longitude")

        # Construct Google Maps URL
        maps_query = quote(f"{address}, {city}, {state} {zip_code}")
        maps_url = f"https://www.google.com/maps/search/?api=1&query={maps_query}"

        # Constructing street view URL
        if latitude and longitude:
            street_view_url = f"{street_view_base_url}?location={latitude},{longitude}&key={google_api_key}&size=200x200&fov=90"
        else:
            street_view_url = "N/A"

        # Construct Google Maps Embed URL
        if latitude and longitude:
            embedded_maps_url = f"https://www.google.com/maps/embed/v1/view?center={latitude},{longitude}&zoom=15&key={google_api_key}"
        else:
            embedded_maps_url = "N/A"

        file.write(
            f"Property {index} - Name: {property_name}\n"
            f"Address: {address}, {city}, {state} {zip_code}\n"
            f"Students Only: {students_only}\n"
            f"Community Disabled: {community_disabled}\n"
            f"Waitlist: {has_waitlist}\n"
            f"Website: {website}\n"
            f"Street View URL: {street_view_url}\n"
            f"Google Maps URL: {maps_url}\n"
            f"Google Maps Embed URL: {embedded_maps_url}\n\n"
        )
