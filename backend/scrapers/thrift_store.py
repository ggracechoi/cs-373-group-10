from __future__ import print_function
import time
import requests
import sys

import time
import argparse
import json
import pprint
import requests
import sys
import urllib

try:
    from urllib.error import HTTPError
    from urllib.parse import quote
    from urllib.parse import urlencode
except ImportError:
    from urllib2 import HTTPError
    from urllib import quote
    from urllib import urlencode

# Constants
THRIFT_STORE_TERM = "thrift stores"
AUSTIN_LOCATION = "Austin, TX"
TOTAL_STORES = 90
SEARCH_LIMIT = 3
API_HOST = "https://api.yelp.com"
SEARCH_PATH = "/v3/businesses/search"


def request(host, path, api_key, url_params=None):
    url_params = url_params or {}
    url = "{0}{1}".format(host, quote(path.encode("utf8")))
    headers = {"Authorization": "Bearer %s" % api_key}
    response = requests.request("GET", url, headers=headers, params=url_params)
    return response.json()


def search(api_key, term, location, offset=0):
    url_params = {
        "term": term.replace(" ", "+"),
        "location": location.replace(" ", "+"),
        "limit": SEARCH_LIMIT,
        "offset": offset,
    }
    return request(API_HOST, SEARCH_PATH, api_key, url_params=url_params)


def get_all_thrift_stores(api_key):
    all_stores = []
    offset = 0
    while len(all_stores) < TOTAL_STORES:
        response = search(api_key, THRIFT_STORE_TERM, AUSTIN_LOCATION, offset)
        stores = response.get("businesses")
        if not stores:
            break
        all_stores.extend(stores)
        offset += SEARCH_LIMIT
        time.sleep(1)  # Avoid hitting the API rate limit
    return all_stores[:TOTAL_STORES]


def query_api(api_key):
    try:
        thrift_stores = get_all_thrift_stores(api_key)
        print(f"Retrieved {len(thrift_stores)} thrift stores.")
        save_to_txt(thrift_stores)
    except HTTPError as error:
        sys.exit(
            "Encountered HTTP error {0} on {1}:\n {2}\nAbort program.".format(
                error.code,
                error.url,
                error.read(),
            )
        )


def save_to_txt(stores):
    with open("thrift_stores.txt", "w") as file:
        index = 1
        for store in stores:
            store_info = format_store_info(store, index)
            file.write(store_info + "\n\n")
            index += 1


def format_store_info(store, index):
    info = f"Thrift store {index}\n"
    info += f"ID: {store.get('id')}\n"
    info += f"Name: {store.get('name')}\n"
    location = store.get("location", {})
    address = f"{location.get('address1', '')}, {location.get('city', '')}, {location.get('state', '')} {location.get('zip_code', '')}"
    info += f"Address: {address}\n"
    info += f"Phone Number: {store.get('display_phone')}\n"
    info += f"Website: {store.get('url')}\n"
    info += f"Rating: {store.get('rating')}\n"
    info += f"Number of Reviews: {store.get('review_count')}\n"
    info += f"Image URL: {store.get('image_url')}\n"
    google_maps_url = (
        f"https://www.google.com/maps/search/?api=1&query={quote(address)}"
    )
    info += f"Google Maps URL: {google_maps_url}\n"
    coordinates = store.get("coordinates", {})
    latitude = coordinates.get("latitude")
    longitude = coordinates.get("longitude")
    if latitude and longitude:
        embedded_maps_url = f"https://www.google.com/maps/embed/v1/view?center={latitude},{longitude}&zoom=15&key={google_api_key}"
        info += f"Google Maps Embed URL: {embedded_maps_url}\n"
    else:
        info += "N/A\n"
    return info


if __name__ == "__main__":
    API_KEY = "rtTKHqEj55rTTO22Y-RE3mo0O91HYLFH85Hd04PKmJBdNbxkqkgNhYMrTy2_3vAQ2Szk6u2MNNl8sFBxjh2SbcyS6eNkIu8fD9Ya-Z6JG237Kj_comkWO3iyeunHZXYx"  # Set your API Key here
    google_api_key = "AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90"
    query_api(API_KEY)
