from math import radians, cos, sin, sqrt, atan2
import pandas as pd
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker


def calculate_distance(lat1, lon1, lat2, lon2):
    # Convert latitude and longitude from degrees to radians
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    r = 6371
    dist = r * c

    return dist


# Initialize the database connection
engine = create_engine(
    "mysql+mysqlconnector://admin:Group10SWE2024!@database-1.cvkkumc2gffe.us-east-1.rds.amazonaws.com/backend"
)
print("done with engine")
metadata = MetaData()

thrift_housing = Table("thrift_housing_association", metadata, autoload_with=engine)
housing_pantry = Table("housing_pantry_association", metadata, autoload_with=engine)
pantry_thrift = Table("pantry_thrift_association", metadata, autoload_with=engine)
thrift_stores = Table("thrift_stores", metadata, autoload_with=engine)
affordable_housing = Table("affordable_housing", metadata, autoload_with=engine)
food_pantry = Table("food_pantries", metadata, autoload_with=engine)
print("existing tables")

Session = sessionmaker(bind=engine)
session = Session()
print("made session")

thrift_stores_data = session.query(thrift_stores).all()
affordable_housing_data = session.query(affordable_housing).all()
food_pantry_data = session.query(food_pantry).all()

# run the following for loops individually so that if there are errors you dont have to keep going through everything again, so jsut comment out whatever tables you dont want to fill

for thrift_store in thrift_stores_data:
    print("thrift store: " + str(thrift_store.id))
    for housing in affordable_housing_data:
        print("housing: " + str(housing.id))
        distance = calculate_distance(
            thrift_store.Latitude,
            thrift_store.Longitude,
            housing.Latitude,
            housing.Longitude,
        )
        # Insert into association table
        insert_stmt = thrift_housing.insert().values(
            thrift_id=thrift_store.id, housing_id=housing.id, distance=distance
        )
        session.execute(insert_stmt)

for housing in affordable_housing_data:
    print("housing: " + str(housing.id))
    for pantry in food_pantry_data:
        distance = calculate_distance(
            housing.Latitude, housing.Longitude, pantry.Latitude, pantry.Longitude
        )
        # Insert into association table
        insert_stmt = housing_pantry.insert().values(
            housing_id=housing.id, pantry_id=pantry.id, distance=distance
        )
        session.execute(insert_stmt)

for pantry in food_pantry_data:
    print("pantry: " + str(pantry.id))
    for thrift in thrift_stores_data:
        print("thrift: " + str(thrift.id))
        distance = calculate_distance(
            pantry.Latitude, pantry.Longitude, thrift.Latitude, thrift.Longitude
        )
        # Insert into association table
        insert_stmt = pantry_thrift.insert().values(
            pantry_id=pantry.id, thrift_id=thrift.id, distance=distance
        )
        session.execute(insert_stmt)


print("done inserting")
session.commit()
session.close()
