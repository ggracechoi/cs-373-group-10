import geopandas as gpd
import json

# Load world countries shapefile
world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

# Extract country names and centroid coordinates
country_coordinates = {}
for index, row in world.iterrows():
    country_name = row['name']
    centroid = row['geometry'].centroid
    country_coordinates[country_name] = [centroid.x, centroid.y]

# Export the data as a JavaScript object
with open('../frontend/country_coordinates.js', 'w') as file:
    file.write('const countryCoordinates = ')
    json.dump(country_coordinates, file)
    file.write(';')
