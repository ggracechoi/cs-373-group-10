from flask import Flask, request, jsonify
from sqlalchemy import create_engine, text
from flask_cors import CORS
from typing import Dict, List

app = Flask(__name__)

engine = create_engine(
    "mysql+mysqlconnector://admin:Group10SWE2024!@database-1.cvkkumc2gffe.us-east-1.rds.amazonaws.com/backend"
)

thrift_store_key_map = {
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Phone Number": "phoneNumber",
    "Website": "website",
    "Rating": "rating",
    "Number of Reviews": "numRatings",
    "Image URL": "imageURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
}

affordable_housing_key_map = {
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Students Only": "studentsOnly",
    "Community Disabled": "communityDisabled",
    "Waitlist": "waitlist",
    "Website": "website",
    "Street View URL": "streetViewURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
}

food_pantries_key_map = {
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Opening Hours": "openingHours",
    "Website": "website",
    "Rating": "rating",
    "Number of Ratings": "numRatings",
    "Street View URL": "streetViewURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
}

# Doesn't work with belowpython 3.9
# def remap_dict_keys(dict_in: dict, key_map: dict[str, str]):
#     return {key_map[name]: val for name, val in dict_in.items()}

# def remap_dict_list(list_in: list[dict], key_map: dict[str, str]):
#     return [remap_dict_keys(row, key_map) for row in list_in]


def remap_dict_keys(dict_in: Dict[str, any], key_map: Dict[str, str]) -> Dict[str, any]:
    return {key_map.get(name, name): val for name, val in dict_in.items()}


def remap_dict_list(
    list_in: List[Dict[str, any]], key_map: Dict[str, str]
) -> List[Dict[str, any]]:
    return [remap_dict_keys(row, key_map) for row in list_in]


@app.route("/thrift-stores/search", methods=["GET"])
def search_thrift_stores():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM thrift_stores 
                WHERE `Name` LIKE :query 
                OR `Address` LIKE :query
                OR `Phone Number` LIKE :query
                OR `Website` LIKE :query
                OR CAST(`Rating` AS CHAR) LIKE :query
                OR `Number of Reviews` LIKE :query
            """
            )
            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, thrift_store_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Phone Number` LIKE :full_input",
                    "`Website` LIKE :full_input",
                    "CAST(`Rating` AS CHAR) LIKE :full_input",
                    "`Number of Reviews` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Phone Number` LIKE :word_{i} OR `Website` LIKE :word_{i} OR CAST(`Rating` AS CHAR) LIKE :word_{i} OR `Number of Reviews` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM thrift_stores
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "phoneNumber",
                "website",
                "rating",
                "numRatings",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, thrift_store_key_map)
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


@app.route("/affordable-housing/search", methods=["GET"])
def search_affordable_housing():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM affordable_housing
                WHERE `Name` LIKE :query 
                OR `Address` LIKE :query
                OR `Students Only` LIKE :query
                OR `Community Disabled` LIKE :query
                OR `Waitlist` LIKE :query
                OR `Website` LIKE :query
            """
            )
            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, affordable_housing_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Students Only` LIKE :full_input",
                    "`Community Disabled` LIKE :full_input",
                    "`Waitlist` LIKE :full_input",
                    "`Website` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Students Only` LIKE :word_{i} OR `Community Disabled` LIKE :word_{i} OR `Waitlist` LIKE :word_{i} OR `Website` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM affordable_housing
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "studentsOnly",
                "communityDisabled",
                "waitlist",
                "website",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, affordable_housing_key_map)
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


@app.route("/food-pantries/search", methods=["GET"])
def search_food_pantries():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM food_pantries
                WHERE `Name` LIKE :query
                OR `Address` LIKE :query
                OR `Opening Hours` LIKE :query
                OR CAST(`Rating` AS CHAR) LIKE :query
                OR `Number of Ratings` LIKE :query
                OR `Website` LIKE :query
            """
            )

            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, food_pantries_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Opening Hours` LIKE :full_input",
                    "CAST(`Rating` AS CHAR) LIKE :full_input",
                    "`Number of Ratings` LIKE :full_input",
                    "`Website` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Opening Hours` LIKE :word_{i} OR CAST(`Rating` AS CHAR) LIKE :word_{i} OR `Number of Ratings` LIKE :word_{i} OR `Website` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM food_pantries
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "openingHours",
                "website",
                "rating",
                "numRatings",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, food_pantries_key_map)
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


if __name__ == "__main__":
    with app.test_client() as client:
<<<<<<< HEAD
        # response = client.get('/thrift-stores/search?query=Thrift Store')
        # print(response.get_json())
        #response = client.get('/affordable-housing/search?query=Villas on')
        #print(response.get_json())
        response = client.get('/food-pantries/search?query=pantry')
        print(response.get_json())
=======
        response = client.get("/thrift-stores/search?query=Thrift Store")
        print(response.get_json())
        # response = client.get('/affordable-housing/search?query=Villas on')
        # print(response.get_json())
        # response = client.get('/food-pantries/search?query=Texas Food')
        # print(response.get_json())
>>>>>>> refs/remotes/origin/main
