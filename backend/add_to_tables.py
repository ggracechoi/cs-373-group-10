import pymysql

# MySQL database connection parameters
host = "database-1.cvkkumc2gffe.us-east-1.rds.amazonaws.com"
user = "admin"
password = "Group10SWE2024!"
database = "backend"

# Connect to the database
connection = pymysql.connect(host=host, user=user, password=password, database=database)

try:
    with connection.cursor() as cursor:
        # SQL query to update the column with the correct API key
        update_query = """
        UPDATE affordable_housing 
        SET `Google Maps Embed URL` = REPLACE(`Google Maps Embed URL`, 'AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90', 'AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE')
        """
        
        # Execute the update query
        cursor.execute(update_query)
        
    # Commit the changes
    connection.commit()
    print("API key updated successfully!")
    
finally:
    # Close the connection
    connection.close()