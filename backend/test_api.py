import requests
import unittest


class Tests(unittest.TestCase):
    base_url = "https://api.atxassist.me/"

    def make_api_request(self, endpoint, data=None):
        url = self.base_url + endpoint
        response = requests.get(url, json=data)
        return response.json()

    def test_all_thrift_stores(self):
        response_json = self.make_api_request("get-all-thrifts")
        # print(response_json)

    def test_single_thrift_store(self):
        data = {"id": 4}
        response_json = self.make_api_request("get-thrift-id", data)
        # print(response_json)

    def test_all_aff_houses(self):
        response_json = self.make_api_request("get-all-affordable-housing")
        # print(response_json)

    def test_single_aff_house(self):
        data = {"id": 4}
        response_json = self.make_api_request("get-affordable-housing-id", data)
        # print(response_json)

    def test_all_food_pantries(self):
        response_json = self.make_api_request("get-all-food-pantries")
        # print(response_json)

    def test_single_food_pantry(self):
        data = {"id": 4}
        response_json = self.make_api_request("get-food-pantry-id", data)
        # print(response_json)

    def test_search_thrift_stores(self):
        data = {"query": "store"}
        response_json = self.make_api_request("thrift-stores/search", data)
        # print(response_json)

    def test_filter_and_sort_thrift_stores(self):
        data = {"zipCode": "12345", "sortRating": "true"}
        response_json = self.make_api_request("thrift-stores/filtered", data)
        # print(response_json)

    def test_search_affordable_housing(self):
        data = {"query": "housing"}
        response_json = self.make_api_request("affordable-housing/search", data)
        # print(response_json)

    def test_filter_and_sort_affordable_housing(self):
        data = {"waitlistStatus": "true", "sortAlphabetically": "true"}
        response_json = self.make_api_request("affordable-housing/filtered", data)
        # print(response_json)

    def test_search_food_pantries(self):
        data = {"query": "pantry"}
        response_json = self.make_api_request("food-pantries/search", data)
        print(response_json)

    def test_filter_and_sort_food_pantries(self):
        data = {"rating": "4.0", "sortRatings": "true"}
        response_json = self.make_api_request("food-pantries/filtered", data)
        # print(response_json)


if __name__ == "__main__":
    unittest.main()
