from flask import Flask
from flask_cors import CORS
from sqlalchemy import create_engine, text
from flask import request, jsonify

app = Flask(__name__)
CORS(app)

engine = create_engine(
    "mysql+pymysql://admin:Group10SWE2024!@database-1.cvkkumc2gffe.us-east-1.rds.amazonaws.com/backend"
)

# Map MySQL column names to their associated JSON property names
thrift_store_key_map = {
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Phone Number": "phoneNumber",
    "Website": "website",
    "Rating": "rating",
    "Number of Reviews": "numRatings",
    "Image URL": "imageURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
}
affordable_housing_key_map = {
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Students Only": "studentsOnly",
    "Community Disabled": "communityDisabled",
    "Waitlist": "hasWaitlist",
    "Website": "website",
    "Street View URL": "streetViewURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
}
food_pantry_key_map = {
<<<<<<< HEAD
    'id': 'id',
    'Name': 'name',
    'Address': 'address',
    'Opening Hours': 'openingHours',
    'Website': 'website',
    'Rating': 'rating',
    'Number of Ratings': 'numRatings',
    'Street View URL': 'streetViewURL',
    'Google Maps URL': 'googleMapsURL',
    'Google Maps Embed URL': 'embedURL',
    'Latitude': 'latitude',
    'Longitude': 'longitude',
=======
    "id": "id",
    "Name": "name",
    "Address": "address",
    "Opening Hours": "hours",
    "Website": "website",
    "Rating": "rating",
    "Number of Ratings": "numRatings",
    "Street View URL": "streetViewURL",
    "Google Maps URL": "googleMapsURL",
    "Google Maps Embed URL": "embedURL",
    "Latitude": "latitude",
    "Longitude": "longitude",
>>>>>>> refs/remotes/origin/main
}
associations_key_map = {
    "thrift_id": "thriftID",
    "housing_id": "housingID",
    "pantry_id": "pantryID",
    "distance": "distance",
}


def remap_dict_keys(dict_in: dict, key_map: dict[str, str]):
    return {key_map[name]: val for name, val in dict_in.items()}


def remap_dict_list(list_in: list[dict], key_map: dict[str, str]):
    return [remap_dict_keys(row, key_map) for row in list_in]


# Routes


@app.route("/thrift-stores", methods=["GET"])
def get_all_thrifts():
    with engine.connect() as connection:
        sql = text("SELECT * FROM thrift_stores;")
        results = connection.execute(sql).mappings().fetchall()

        results = remap_dict_list(results, thrift_store_key_map)

        return results


@app.route("/affordable-housing", methods=["GET"])
def get_all_affordable_housing():
    with engine.connect() as connection:
        sql = text("SELECT * FROM affordable_housing;")
        results = connection.execute(sql).mappings().fetchall()

        results = remap_dict_list(results, affordable_housing_key_map)

        return results


@app.route("/food-pantries", methods=["GET"])
def get_all_food_pantries():
    with engine.connect() as connection:
        sql = text("SELECT * FROM food_pantries;")
        results = connection.execute(sql).mappings().fetchall()

        results = remap_dict_list(results, food_pantry_key_map)

        return results


@app.route("/thrift-stores/<id>", methods=["GET"])
def get_thrift_id(id: int):
    with engine.connect() as connection:
        sql = text("SELECT * FROM thrift_stores WHERE id = :id")
        result = connection.execute(sql, {"id": id}).mappings().fetchone()

        result = remap_dict_keys(result, thrift_store_key_map)

        return result


@app.route("/affordable-housing/<id>", methods=["GET"])
def get_affordable_housing_id(id: int):
    with engine.connect() as connection:
        sql = text("SELECT * FROM affordable_housing WHERE id = :id")
        result = connection.execute(sql, {"id": id}).mappings().fetchone()

        result = remap_dict_keys(result, affordable_housing_key_map)

        return result


@app.route("/food-pantries/<id>", methods=["GET"])
def get_food_pantries_id(id: int):
    with engine.connect() as connection:
        sql = text("SELECT * FROM food_pantries WHERE id = :id")
        result = connection.execute(sql, {"id": id}).mappings().one()

        result = remap_dict_keys(result, food_pantry_key_map)

        return dict(result)


@app.route("/thrift-stores/<id>/nearby-resources", methods=["GET"])
def get_thrift_nearby_resources(id: int):
    with engine.connect() as connection:
        # Fetch nearby housing
        housing_sql = text(
            "SELECT * FROM thrift_housing_association WHERE thrift_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        housing = connection.execute(housing_sql, {"id": id}).mappings().fetchall()

        # Fetch nearby pantries
        pantry_sql = text(
            "SELECT * FROM pantry_thrift_association WHERE thrift_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        pantries = connection.execute(pantry_sql, {"id": id}).mappings().fetchall()

        # Format
        results = {
            "nearbyHousing": remap_dict_list(housing, associations_key_map),
            "nearbyPantries": remap_dict_list(pantries, associations_key_map),
        }

        return results


@app.route("/affordable-housing/<id>/nearby-resources", methods=["GET"])
def get_housing_nearby_resources(id):
    with engine.connect() as connection:
        # Fetch nearby thrift stores
        thrift_sql = text(
            "SELECT * FROM thrift_housing_association WHERE housing_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        thrifts = connection.execute(thrift_sql, {"id": id}).mappings().fetchall()

        # Fetch nearby pantries
        pantry_sql = text(
            "SELECT * FROM housing_pantry_association WHERE housing_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        pantries = connection.execute(pantry_sql, {"id": id}).mappings().fetchall()

        # Format
        results = {
            "nearbyThrifts": remap_dict_list(thrifts, associations_key_map),
            "nearbyPantries": remap_dict_list(pantries, associations_key_map),
        }

        return results


@app.route("/food-pantries/<id>/nearby-resources", methods=["GET"])
def get_pantry_nearby_resources(id):
    with engine.connect() as connection:
        # Fetch nearby thrift stores
        thrift_sql = text(
            "SELECT * FROM pantry_thrift_association WHERE pantry_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        thrifts = connection.execute(thrift_sql, {"id": id}).mappings().fetchall()

        # Fetch nearby housing
        housing_sql = text(
            "SELECT * FROM housing_pantry_association WHERE pantry_id = :id ORDER BY distance ASC LIMIT 3;"
        )
        housing = connection.execute(housing_sql, {"id": id}).mappings().fetchall()

        # Format
        results = {
            "nearbyThrifts": remap_dict_list(thrifts, associations_key_map),
            "nearbyHousing": remap_dict_list(housing, associations_key_map),
        }

        return results


def filter_and_sort_locations(
    locations,
    zip_code=None,
    area_code=None,
    sort_by_rating=False,
    sort_by_num_ratings=False,
    sort_alphabetically=False,
):
    # Filter by zip code if provided
    if zip_code is not None:
        locations = [loc for loc in locations if zip_code in loc["address"]]
    if area_code is not None:
        locations = [loc for loc in locations if area_code == loc["phoneNumber"][1:4]]
    if sort_alphabetically:
        locations.sort(key=lambda x: x["name"].lower())
    if sort_by_num_ratings:
        locations.sort(key=lambda x: x["numRatings"], reverse=True)
    if sort_by_rating:
        locations.sort(key=lambda x: x["rating"], reverse=True)
    return locations


@app.route("/thrift-stores/filtered", methods=["GET"])
def get_filtered_and_sorted_thrift_stores():
    # Fetch query parameters
    zip_code = request.args.get("zipCode")
    area_code = request.args.get("areaCode")
    sort_by_rating = request.args.get("sortRating", "false").lower() == "true"
    sort_by_num_ratings = request.args.get("sortNumRatings", "false").lower() == "true"
    sort_alphabetically = (
        request.args.get("sortAlphabetically", "false").lower() == "true"
    )

    with engine.connect() as connection:
        sql = text("SELECT * FROM thrift_stores;")
        results = connection.execute(sql).mappings().fetchall()

        results = remap_dict_list(results, thrift_store_key_map)

    filtered_sorted_locations = filter_and_sort_locations(
        results,
        zip_code=zip_code,
        area_code=area_code,
        sort_by_rating=sort_by_rating,
        sort_by_num_ratings=sort_by_num_ratings,
        sort_alphabetically=sort_alphabetically,
    )

    return jsonify(filtered_sorted_locations)


def filter_and_sort_affordable_housing(
    housing_list,
    waitlist_status=None,
    community_disabled_status=None,
    students_only_status=None,
    zip_code=None,
    sort_alphabetically=False,
):
    filtered_list = housing_list
    if waitlist_status is not None:
        filtered_list = [
            item
            for item in filtered_list
            if match_str_bool(item["hasWaitlist"], waitlist_status)
        ]
    if community_disabled_status is not None:
        filtered_list = [
            item
            for item in filtered_list
            if match_str_bool(item["communityDisabled"], community_disabled_status)
        ]
    if students_only_status is not None:
        filtered_list = [
            item
            for item in filtered_list
            if match_str_bool(item["studentsOnly"], students_only_status)
        ]
    if zip_code is not None:
        filtered_list = [item for item in filtered_list if zip_code in item["address"]]
    if sort_alphabetically:
        filtered_list = sorted(filtered_list, key=lambda x: x["name"])

    return filtered_list


def match_str_bool(str, b):
    if b:
        return str.lower() == "true"
    return str.lower() == "false"


@app.route("/affordable-housing/filtered", methods=["GET"])
def get_filtered_and_sorted_affordable_housing():
    # Fetch query parameters
    waitlist_status = request.args.get("waitlistStatus")
    community_disabled_status = request.args.get("communityDisabledStatus")
    students_only_status = request.args.get("studentsOnlyStatus")
    zip_code = request.args.get("zipCode")
    sort_alphabetically = (
        request.args.get("sortAlphabetically", "false").lower() == "true"
    )

    # Convert string query parameters to boolean where necessary
    if waitlist_status is not None:
        waitlist_status = waitlist_status.lower() in ["true", "1", "t", "y", "yes"]
    if community_disabled_status is not None:
        community_disabled_status = community_disabled_status.lower() in [
            "true",
            "1",
            "t",
            "y",
            "yes",
        ]
    if students_only_status is not None:
        students_only_status = students_only_status.lower() in [
            "true",
            "1",
            "t",
            "y",
            "yes",
        ]

    with engine.connect() as connection:
        sql = text("SELECT * FROM affordable_housing;")
        results = connection.execute(sql).mappings().fetchall()
        results = remap_dict_list(results, affordable_housing_key_map)

    filtered_sorted_results = filter_and_sort_affordable_housing(
        results,
        waitlist_status=waitlist_status,
        community_disabled_status=community_disabled_status,
        students_only_status=students_only_status,
        zip_code=zip_code,
        sort_alphabetically=sort_alphabetically,
    )

    return jsonify(filtered_sorted_results)


from datetime import datetime
import re


def parse_time(time_str):
    """Convert a time string into a datetime.time object, defaulting to PM if not specified."""
    try:
        return datetime.strptime(time_str, "%I:%M %p").time()
    except ValueError:
        # If parsing fails, try appending "PM" and parsing again
        try:
            return datetime.strptime(time_str + " PM", "%I:%M %p").time()
        except ValueError:
            # If it still fails, raise the original error
            raise ValueError(f"time data '{time_str}' does not match format '%I:%M %p'")


def is_time_within_range(start_time, end_time, query_start, query_end):
    """Check if the query time range overlaps with the start and end times."""
    return not (end_time < query_start or start_time > query_end)


def is_open_on_day(hours_str, day, time_range=None):
    day_hours_match = re.search(rf"{day}: (.*?)(,|$)", hours_str, re.IGNORECASE)
    if not day_hours_match:
        return False  # Day not found in the hours string

    day_hours = day_hours_match.group(1).strip()
    if "closed" in day_hours.lower():
        return False  # Closed all day

    if time_range:
        query_start_str, query_end_str = time_range.split(" - ")
        query_start = parse_time(query_start_str)
        query_end = parse_time(query_end_str)
        # Split the hours by commas to handle multiple opening periods in one day
        periods = day_hours.split(",")
        for period in periods:
            # Assuming periods are formatted as "Start - End"
            if "-" in period:
                start_str, end_str = period.split(" - ")
                start_time = parse_time(start_str.strip())
                end_time = parse_time(end_str.strip())

                if is_time_within_range(start_time, end_time, query_start, query_end):
                    return True  # The pantry is open within the queried time range
        return False  # Not open within the queried time range
    else:
        return True


def filter_and_sort_food_pantries(
    pantries_list,
    day=None,
    time_range=None,
    rating=None,
    zip_code=None,
    sort_by_ratings=False,
    sort_alphabetically=False,
):
    filtered_list = pantries_list
    if day is not None:
        filtered_list = [
            item
            for item in filtered_list
            if is_open_on_day(item["hours"], day, time_range)
        ]
    if rating is not None:
        filtered_list = [item for item in filtered_list if item["rating"] >= rating]
    if zip_code is not None:
        filtered_list = [item for item in filtered_list if zip_code in item["address"]]
    if sort_by_ratings:
        filtered_list = sorted(
            filtered_list, key=lambda x: x["numRatings"], reverse=True
        )
    if sort_alphabetically:
        filtered_list = sorted(filtered_list, key=lambda x: x["name"])
    return filtered_list


@app.route("/food-pantries/filtered", methods=["GET"])
def get_filtered_and_sorted_food_pantries():
    # Fetch query parameters
    day = request.args.get("day")
    time_range = request.args.get("timeRange")  # Expected format: "4:00 PM - 6:00 PM"
    rating = request.args.get("rating")
    zip_code = request.args.get("zipCode")
    sort_by_ratings = request.args.get("sortRatings", "false").lower() == "true"
    sort_alphabetically = (
        request.args.get("sortAlphabetically", "false").lower() == "true"
    )

    # Convert rating to float if provided
    if rating is not None:
        try:
            rating = float(rating)
        except ValueError:
            return (
                jsonify(
                    {"error": "Invalid rating format. Please provide a numeric value."}
                ),
                400,
            )

    with engine.connect() as connection:
        sql = text("SELECT * FROM food_pantries;")
        results = connection.execute(sql).mappings().fetchall()
        results = remap_dict_list(results, food_pantry_key_map)

    filtered_sorted_results = filter_and_sort_food_pantries(
        results,
        day=day,
        time_range=time_range,
        rating=rating,
        zip_code=zip_code,
        sort_by_ratings=sort_by_ratings,
        sort_alphabetically=sort_alphabetically,
    )

    return jsonify(filtered_sorted_results)


@app.route("/thrift-stores/search", methods=["GET"])
def search_thrift_stores():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM thrift_stores 
                WHERE `Name` LIKE :query 
                OR `Address` LIKE :query
                OR `Phone Number` LIKE :query
                OR `Website` LIKE :query
                OR CAST(`Rating` AS CHAR) LIKE :query
                OR `Number of Reviews` LIKE :query
            """
            )
            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, thrift_store_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Phone Number` LIKE :full_input",
                    "`Website` LIKE :full_input",
                    "CAST(`Rating` AS CHAR) LIKE :full_input",
                    "`Number of Reviews` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Phone Number` LIKE :word_{i} OR `Website` LIKE :word_{i} OR CAST(`Rating` AS CHAR) LIKE :word_{i} OR `Number of Reviews` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM thrift_stores
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "phoneNumber",
                "website",
                "rating",
                "numRatings",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, thrift_store_key_map)
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


@app.route("/affordable-housing/search", methods=["GET"])
def search_affordable_housing():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM affordable_housing
                WHERE `Name` LIKE :query 
                OR `Address` LIKE :query
                OR `Students Only` LIKE :query
                OR `Community Disabled` LIKE :query
                OR `Waitlist` LIKE :query
                OR `Website` LIKE :query
            """
            )
            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, affordable_housing_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Students Only` LIKE :full_input",
                    "`Community Disabled` LIKE :full_input",
                    "`Waitlist` LIKE :full_input",
                    "`Website` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Students Only` LIKE :word_{i} OR `Community Disabled` LIKE :word_{i} OR `Waitlist` LIKE :word_{i} OR `Website` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM affordable_housing
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "studentsOnly",
                "communityDisabled",
                "hasWaitlist",
                "website",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, affordable_housing_key_map)
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


@app.route("/food-pantries/search", methods=["GET"])
def search_food_pantries():
    search_query = request.args.get("query", "")
    full_input = f"%{search_query}%"
    if full_input.count(" ") == 0:
        with engine.connect() as connection:
            sql = text(
                """
                SELECT * FROM food_pantries
                WHERE `Name` LIKE :query
                OR `Address` LIKE :query
                OR `Opening Hours` LIKE :query
                OR CAST(`Rating` AS CHAR) LIKE :query
                OR `Number of Ratings` LIKE :query
                OR `Website` LIKE :query
            """
            )

            params = {"query": f"%{search_query}%"}
            results = connection.execute(sql, params).mappings().fetchall()
            results = remap_dict_list(results, food_pantry_key_map)
            return jsonify(results)
    else:
        words = search_query.split()
        word_queries = [f"%{word}%" for word in words]

        with engine.connect() as connection:
            # Fetch all potential matches based on the full input or any of the individual words
            dynamic_where_clause = " OR ".join(
                [
                    "`Name` LIKE :full_input",
                    "`Address` LIKE :full_input",
                    "`Opening Hours` LIKE :full_input",
                    "CAST(`Rating` AS CHAR) LIKE :full_input",
                    "`Number of Ratings` LIKE :full_input",
                    "`Website` LIKE :full_input",
                ]
                + [
                    f"`Name` LIKE :word_{i} OR `Address` LIKE :word_{i} OR `Opening Hours` LIKE :word_{i} OR CAST(`Rating` AS CHAR) LIKE :word_{i} OR `Number of Ratings` LIKE :word_{i} OR `Website` LIKE :word_{i}"
                    for i, _ in enumerate(word_queries)
                ]
            )

            sql = f"""
                SELECT * FROM food_pantries
                WHERE {dynamic_where_clause}
            """

            params = {"full_input": full_input}
            for i, word in enumerate(word_queries):
                params[f"word_{i}"] = word

            results = connection.execute(text(sql), params).mappings().fetchall()

        # Calculate relevance score in Python
        def calculate_relevance(record):
            score = 0
            for field in [
                "name",
                "address",
                "hours",
                "website",
                "rating",
                "numRatings",
            ]:
                if record[field] and search_query.lower() in str(record[field]).lower():
                    score += 3  # Score for full match
                for word in words:
                    if record[field] and word.lower() in str(record[field]).lower():
                        score += 1  # Score for individual word match
            return score

        # Assuming 'remap_dict_list' function is correctly defined as in your original code
        remapped_results = remap_dict_list(results, food_pantry_key_map)
<<<<<<< HEAD
        scored_results = [(record, calculate_relevance(record)) for record in remapped_results]
=======
        scored_results = [
            (record, calculate_relevance(record)) for record in remapped_results
        ]
>>>>>>> refs/remotes/origin/main
        scored_sorted_results = sorted(scored_results, key=lambda x: x[1], reverse=True)

        # Convert to desired format, if necessary. For now, returning just the records sorted by relevance.
        final_results = [record for record, _ in scored_sorted_results]

        return jsonify(final_results)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3003)
