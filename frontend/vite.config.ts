import { resolve } from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    watch: {
        usePolling: true
    },
    host: true,
    port: process.env.PORT ? +process.env.PORT : 5173,
    proxy: {
      // Proxying API requests to avoid issues with CORS and wrong responses
      '/api': {
        target: 'https://api.supportsouthsudan.me',  // Change to your API's base URL
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      }
    },
  }
})
