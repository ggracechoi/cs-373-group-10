from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest

URL = "https://www.atxassist.me/"

class AcceptanceTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(URL)

    @classmethod
    def closeClass(self):
        self.driver.close()
    
    def test_about_page(self):
        self.driver.find_element(By.LINK_TEXT, "About").click()
        self.assertEqual(self.driver.current_url, URL + "about/")
    
    def test_pantry_page(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        self.assertEqual(self.driver.current_url, URL + "foodpantries/")
    
    def test_thrift_page(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        self.assertEqual(self.driver.current_url, URL + "thriftstores/")

    def test_housing_page(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        self.assertEqual(self.driver.current_url, URL + "affordablehousing/")

    def test_pantry_next(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[11]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[11]/a'))).click()
        self.assertEqual(prev_link, URL + "foodpantries/?page="+str((int(self.driver.current_url[-1]) - 1)))
    
    def test_housing_next(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[33]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[33]/a'))).click()
        self.assertEqual(prev_link, URL + "affordablehousing/?page="+str((int(self.driver.current_url[-1]) - 1)))

    def test_thrift_next(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[9]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[9]/a'))).click()
        self.assertEqual(prev_link, URL + "thriftstores/?page="+str((int(self.driver.current_url[-1]) - 1)))

    def test_pantry_prev(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[11]/a'))).click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[11]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[2]/a'))).click()
        self.assertEqual(prev_link, URL + "foodpantries/?page="+str((int(self.driver.current_url[-1]) + 1)))
    
    def test_housing_prev(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[33]/a'))).click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[33]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[2]/a'))).click()
        self.assertEqual(prev_link, URL + "affordablehousing/?page="+str((int(self.driver.current_url[-1]) + 1)))

    def test_thrift_prev(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[9]/a'))).click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[9]/a'))).click()
        prev_link = self.driver.current_url
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/ul/li[2]/a'))).click()
        self.assertEqual(prev_link, URL + "thriftstores/?page="+str((int(self.driver.current_url[-1]) + 1)))

    def test_general_search_button(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="navbarSupportedContent"]/form/button'))).click()
        self.assertEqual(self.driver.current_url, URL + "search?query=")

    def test_pantry_search_button(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/form/button'))).click()
        self.assertEqual(self.driver.current_url, URL + "foodpantries/?query=")

    def test_thrift_search_button(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/form/button'))).click()
        self.assertEqual(self.driver.current_url, URL + "thriftstores/?query=")

    def test_housing_search_button(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/form/button'))).click()
        self.assertEqual(self.driver.current_url, URL + "affordablehousing/?query=")

    def test_pantry_learn_more(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div[2]/a'))).click()
        self.assertEqual(self.driver.current_url, URL + "foodpantries/1")

    def test_thrift_learn_more(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div[2]/a'))).click()
        self.assertEqual(self.driver.current_url, URL + "thriftstores/1")

    def test_housing_learn_more(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/div[2]/a'))).click()
        self.assertEqual(self.driver.current_url, URL + "affordablehousing/1")

    def test_pantry_filtering_popup(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[3]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/button'))).click()
        popup = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[3]/div/div")))
        self.assertTrue(popup.is_displayed())

    def test_thrift_filtering_popup(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/button'))).click()
        popup = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[3]/div/div")))
        self.assertTrue(popup.is_displayed())

    def test_housing_filtering_popup(self):
        self.driver.find_element(By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a').click()
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div/button'))).click()
        popup = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[3]/div/div")))
        self.assertTrue(popup.is_displayed())

if __name__ == "__main__":
    unittest.main()
