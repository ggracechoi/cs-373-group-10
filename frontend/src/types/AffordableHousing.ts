export interface AffordableHousing {
  id: number;
  name: string;
  address: string;
  studentsOnly: string;
  communityDisabled: string;
  hasWaitlist: string;
  website: string;
  streetViewURL: string;
  googleMapsURL: string;
  embedURL: string;
  latitude: number;
  longitude: number;
}
