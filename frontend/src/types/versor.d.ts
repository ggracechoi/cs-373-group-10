// versor.d.ts
declare module 'versor' {
    export function versor(): any;
    export function delta(v0: any, v1: any): any;
    export function multiply(q0: any, q1: any): any;
    export function rotate(r: any): any;
    export function rotation(q: any): any;
    export function cartesian([lambda, phi]: [number, number]): any;
  }
  