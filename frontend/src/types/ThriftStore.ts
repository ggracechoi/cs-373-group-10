export interface ThriftStore {
  id: number;
  name: string;
  address: string;
  phoneNumber: string;
  website: string;
  rating: number;
  numRatings: number;
  imageURL: string;
  googleMapsURL: string;
  embedURL: string;
  latitude: number;
  longitude: number;
}
