export interface FoodPantry {
  id: number;
  name: string;
  address: string;
  hours: string;
  website: string;
  rating: number;
  numRatings: number;
  streetViewURL: string;
  googleMapsURL: string;
  embedURL: string;
  latitude: number;
  longitude: number;
}
