export interface HousingPantryAssociation {
  distance: number;
  housingID: number;
  pantryID: number;
}

export interface PantryThriftAssociation {
  distance: number;
  pantryID: number;
  thriftID: number;
}

export interface ThriftHousingAssociation {
  distance: number;
  thriftID: number;
  housingID: number;
}