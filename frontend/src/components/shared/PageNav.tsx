import { useEffect } from "react";
import { Pagination } from "react-bootstrap";

interface PageNavProps {
  numPages: number;
  page: number;
  setPage: (n: number) => void;
}

function PageNav(props: PageNavProps) {
  const { page, numPages, setPage } = props;

  // Validate page number
  useEffect(() => {
    if (!Number.isInteger(page) || 1 > page || page > props.numPages) {
      setPage(1);
    }
  }, [page, numPages]);

  // Disable the previous or next buttons if on first or last page respectively
  let prevClass = "";
  let nextClass = "";
  if (page == 1) {
    prevClass += " disabled";
  }
  if (page == props.numPages) {
    nextClass += " disabled";
  }

  return (
    <Pagination style={{ justifyContent: "center" }}>
      <Pagination.Item className={prevClass} onClick={() => setPage(1)}>
        <span aria-hidden="true">&laquo;</span>
      </Pagination.Item>
      <Pagination.Item className={prevClass} onClick={() => setPage(page - 1)}>
        <span aria-hidden="true">&lsaquo;</span>
      </Pagination.Item>
      {[...Array(props.numPages)].map((_n, i) => {
        let n = i + 1; // one-indexed
        let isDisabled: string = (n == page) ? "disabled" : "";
        return (
          <Pagination.Item className={isDisabled} onClick={() => setPage(n)} key={i}>
            {n}
          </Pagination.Item>
        );
      })}
      <Pagination.Item className={nextClass} onClick={() => setPage(page + 1)}>
        <span aria-hidden="true">&rsaquo;</span>
      </Pagination.Item>
      <Pagination.Item className={nextClass} onClick={() => setPage(numPages)}>
        <span aria-hidden="true">&raquo;</span>
      </Pagination.Item>
    </Pagination>
  );
}

export default PageNav;
