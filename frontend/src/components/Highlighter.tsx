import Highlighter from "react-highlight-words";

function highlightText (text: string, query: string | undefined) {
  if (query === undefined) return <>{text}</>;
  const queries = query.split(/\s+/);
  return (
    <Highlighter 
        highlightStyle={{
            padding: '0',
            backgroundColor: '#fff187',
        }}
        searchWords={queries}
        textToHighlight={text}
        autoEscape={true}
    />
  )
}

export default highlightText;
