import { Col, Container, Image, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import FoodPantryInfoCard from "./FoodPantryInfoCard";
import FoodPantryHoursCard from "./FoodPantryHoursCard";
import { useEffect, useState } from "react";
import { FoodPantry } from "../../types/FoodPantry";
import { ThriftStore } from "../../types/ThriftStore";
import { AffordableHousing } from "../../types/AffordableHousing";
import AffordableHousingCard from "../AffordableHousingCard";
import ThriftStoresCard from "../ThriftStoresCard";
import { HousingPantryAssociation, PantryThriftAssociation } from "../../types/Associations";

interface NearbyResources {
  nearbyHousing: Array<HousingPantryAssociation>;
  nearbyThrifts: Array<PantryThriftAssociation>;
}

function FoodPantryInstance () {
  
  const { id: id_str } = useParams();
  const [pantry, setPantry] = useState<FoodPantry>();
  const [nearbyResources, setNearbyResources] = useState<NearbyResources>();
  const [nearbyHousing, setNearbyHousing] = useState<Array<AffordableHousing>>();
  const [nearbyThrifts, setNearbyThrifts] = useState<Array<ThriftStore>>();

  // Validate id
  const id: number = Number(id_str);
  if (!Number.isInteger(id)) {
    return "404";
  }

  // Load data from API
  useEffect(() => {
    fetch(`https://api.atxassist.me/food-pantries/${id}`)
      .then((res) => res.json())
      .then((json) => setPantry(json));
    fetch(`https://api.atxassist.me/food-pantries/${id}/nearby-resources`)
      .then((res) => res.json())
      .then((json) => setNearbyResources(json));
  }, []);

  // Load data for nearby instances
  useEffect(() => {
    if (nearbyResources === undefined) {
      return;
    }

    // Get nearby housing data
    const housingFetches = nearbyResources.nearbyHousing.map((elem) => {
      return fetch(`https://api.atxassist.me/affordable-housing/${elem.housingID}`)
        .then((res) => res.json());
    });
    Promise.all(housingFetches).then((result) => setNearbyHousing(result));

    // Get nearby thrift data
    const thriftFetches = nearbyResources.nearbyThrifts.map((elem) => {
      return fetch(`https://api.atxassist.me/thrift-stores/${elem.thriftID}`)
        .then((res) => res.json());
    });
    Promise.all(thriftFetches).then((result) => setNearbyThrifts(result));
  }, [nearbyResources]);

  if (pantry === undefined) {
    return <>Loading...</>;
  }

  return (
    <>
      <Container className="bg-white">
        <div className="d-flex flex-row align-items-end">
          <div className="p-2">
            <Image src={pantry.streetViewURL} thumbnail />
          </div>
          <div className="p-2">
            <h1 className="mb-0">{pantry.name}</h1>
          </div>
        </div>

        <hr />

        <Row xs={2}>
          <Col>
            <FoodPantryHoursCard pantry={pantry}/>
          </Col>
          <Col>
            <FoodPantryInfoCard pantry={pantry} />
          </Col>
          <Col>
            <iframe
              src={pantry.embedURL}
              width="600"
              height="450"
              allowFullScreen={false}
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            />
          </Col>
        </Row>

        <hr />

        <h1>Nearby Affordable Housing</h1>
        <Row xs={2}>
          {nearbyHousing?.map((e, i) => {
            return <Col><AffordableHousingCard housing={e} key={i} /></Col>
          })}
        </Row>

        <hr />
        
        <h1>Nearby Thrift Stores</h1>
        <Row xs={2}>
          {nearbyThrifts?.map((e, i) => {
            return <Col><ThriftStoresCard store={e} key={i} /></Col>
          })}
        </Row>
      </Container>
    </>
  );
}

export default FoodPantryInstance;
