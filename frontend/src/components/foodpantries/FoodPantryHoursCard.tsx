import { Card, ListGroup, ListGroupItem } from "react-bootstrap";

function FoodPantryHoursCard (props: {pantry: any}) {

  // Parse opening hours
  const hoursData = props.pantry.hours.split(", ").map((e: any) => e.split(": "));

  // TODO Properly implement this for Phase 2
  // let display: boolean = true;
  // if (hoursData.length != 7)
  //   display = false;
  // // Ensure each element of hoursData has length 2
  // if (!hoursData.reduce((a, b) => { return a && b.length == 2 }, true))
  //   display = false;

  return (
    <>
      <Card className="text-center" style={{width: "18rem"}}>
        <Card.Header>
          <h5 className="card-title mb-0">Opening Hours</h5>
        </Card.Header>
        <ListGroup className="list-group-flush">
          {hoursData.map((item: any, index: number) => (
            <ListGroupItem key={index}>
              <b>{item[0]}:</b> {item[1]}
            </ListGroupItem>
          ))}
        </ListGroup>
      </Card>
    </>
  );
}

export default FoodPantryHoursCard;
