import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { FoodPantry } from "../../types/FoodPantry";

function FoodPantryInfoCard (props: {pantry: FoodPantry}) {
  const pantry: FoodPantry = props.pantry;
  
  return (
    <Card style={{width: "18rem"}}>
      <Card.Header>
        <h5 className="card-title mb-0">Information</h5>
      </Card.Header>
      <ListGroup className="list-group-flush">
        <ListGroupItem><b>Address:</b> {pantry.address}</ListGroupItem>
        <ListGroupItem>
          <b>Rating:</b> {pantry.rating}/5 
        </ListGroupItem>
        <ListGroupItem>
          <b>Number of Ratings:</b> {pantry.numRatings}
        </ListGroupItem>
      </ListGroup>
      <Card.Body>
        <a href={pantry.website} className="btn btn-primary mb-3">Open Website</a>
        <a href={pantry.googleMapsURL} className="btn btn-primary">Open in Google Maps</a>
      </Card.Body>
    </Card>
  );
}

export default FoodPantryInfoCard;
