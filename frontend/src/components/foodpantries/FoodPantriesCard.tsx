import { FoodPantry } from "../../types/FoodPantry";
import "../Card.css";
import highlightText from "../Highlighter";

function FoodPantries(props: { pantry: FoodPantry, query?: string }) {
  const {pantry, query} = props;
  
  return (
    <div className="Card">
      <img
        className="card-image"
        src={pantry.streetViewURL}
        alt="instance image"
        id="CardThumbnail"
      ></img>
      <h2 className="card-title">{highlightText(pantry.name, query)}</h2>
      <p className="card-text">
        - Address: {highlightText(pantry.address, query)}
        {/* <br></br> Open Hours: {pantry.opening_hours} // very big */}
        <br></br> - Rating: {highlightText((pantry.rating).toString(), query)} / 5
        <br></br> - Number of Ratings: {highlightText((pantry.numRatings).toString(), query)}
      </p>
      <a href={`/foodpantries/${pantry.id}`} className="btn btn-info btn-block">Learn More</a>
    </div>
  );
}

export default FoodPantries;
