import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { ThriftStore } from "../../types/ThriftStore";

function ThriftStoreInfoCard (props: {store: ThriftStore}) {
  const thrift: ThriftStore = props.store;
  
  return (
    <Card style={{width: "18rem"}}>
      <Card.Header>
        <h5 className="card-title mb-0">Information</h5>
      </Card.Header>
      <ListGroup className="list-group-flush">
        <ListGroupItem><b>Phone number:</b> {thrift.phoneNumber}</ListGroupItem>
        <ListGroupItem><b>Address:</b> {thrift.address}</ListGroupItem>
        <ListGroupItem>
          <b>Rating:</b> {thrift.rating}/5
        </ListGroupItem>
        <ListGroupItem>
          <b>Number of Ratings:</b> {thrift.numRatings}
        </ListGroupItem>
      </ListGroup>
      <Card.Body>
        <a href={thrift.website} className="btn btn-primary">Open Website</a>
      </Card.Body>
    </Card>
  );
}

export default ThriftStoreInfoCard;
