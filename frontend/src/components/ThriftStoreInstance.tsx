import { Col, Container, Image, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import ThriftStoreInfoCard from "./thriftstores/ThriftStoreInfoCard";
import { useEffect, useState } from "react";
import { ThriftStore } from "../types/ThriftStore";
import { FoodPantry } from "../types/FoodPantry";
import { AffordableHousing } from "../types/AffordableHousing";
import { PantryThriftAssociation, ThriftHousingAssociation } from "../types/Associations";
import FoodPantries from "./foodpantries/FoodPantriesCard";
import AffordableHousingCard from "./AffordableHousingCard";

interface NearbyResources {
  nearbyPantries: Array<PantryThriftAssociation>;
  nearbyHousing: Array<ThriftHousingAssociation>;
}

function ThriftStoreInstance () {

  const { id: id_str } = useParams();
  const [thrift, setThrift] = useState<ThriftStore>();
  const [nearbyResources, setNearbyResources] = useState<NearbyResources>();
  const [nearbyPantries, setNearbyPantries] = useState<Array<FoodPantry>>();
  const [nearbyHousing, setNearbyHousing] = useState<Array<AffordableHousing>>();

  // GYC: 
  // const [filters, setFilters] = useState<FilterObject>({

  // })

  // const sortAndFilter = (filterObj: FilterObject) =>

  // }

  // Validate id
  const id: number = Number(id_str);
  if (!Number.isInteger(id)) {
    return "404";
  }

  // Load data from API
  useEffect(() => {
    fetch(`https://api.atxassist.me/thrift-stores/${id}`)
      .then((res) => res.json())
      .then((json) => setThrift(json));
    fetch(`https://api.atxassist.me/thrift-stores/${id}/nearby-resources`)
      .then((res) => res.json())
      .then((json) => setNearbyResources(json));
  }, [id]);

  // Load data for nearby resources
  useEffect(() => {
    if (nearbyResources === undefined) {
      return;
    }

    // Get nearby pantry data
    const pantryFetches = nearbyResources.nearbyPantries.map((e) => {
      return fetch(`https://api.atxassist.me/food-pantries/${e.pantryID}`)
        .then((res) => res.json());
    });
    Promise.all(pantryFetches).then((results) => setNearbyPantries(results));

    // Get nearby housing data
    const housingFetches = nearbyResources.nearbyHousing.map((e) => {
      return fetch(`https://api.atxassist.me/affordable-housing/${e.housingID}`)
        .then((res) => res.json());
    });
    Promise.all(housingFetches).then((results) => setNearbyHousing(results));
  }, [nearbyResources]);

  if (thrift === undefined) {
    return <>Loading...</>;
  }
  
  return (
    <Container className="bg-white">
      <div className="d-flex flex-row align-items-end">
        <div className="p-2">
          <Image src={thrift.imageURL} id="CardThumbnail" thumbnail />
        </div>
        <div className="p-2">
          <h1 className="mb-0">{thrift.name}</h1>
        </div>
      </div>

      <hr />

      <ThriftStoreInfoCard store={thrift} />

      <iframe
        src={thrift.embedURL}
        width="600"
        height="450"
        allowFullScreen={false}
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      />

      <hr />

      <h1>Nearby Food Pantries</h1>

      <Row xs={2}>
        {nearbyPantries?.map((e, i) => {
          return <Col><FoodPantries pantry={e} key={i} /></Col>
        })}
      </Row>

      <hr />

      <h1>Nearby Affordable Housing</h1>
      <Row xs={2}>
        {nearbyHousing?.map((e, i) => {
          return <Col><AffordableHousingCard housing={e} key={i} /></Col>
        })}
      </Row>
    </Container>
  );
}

export default ThriftStoreInstance;
