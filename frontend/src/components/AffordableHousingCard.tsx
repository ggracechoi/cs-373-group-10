import { AffordableHousing } from "../types/AffordableHousing";
import "./Card.css";
import highlightText from "./Highlighter";

function AffordableHousingCard(props: { housing: AffordableHousing, query?: string }) {
  const {housing, query} = props;
  
  return (
    <div className="Card">
      <img
        className="card-image"
        src={housing.streetViewURL}
        alt="instance image"
        id="CardThumbnail"
      />
      <h2 className="card-title">{highlightText(housing.name, query)}</h2>
      <p className="card-text">
        - Address: {highlightText(housing.address, query)}
        <br /> - Students Only: {highlightText(housing.studentsOnly, query)}
        <br /> - Disabled Community: {highlightText(housing.communityDisabled, query)}
        <br /> - Waitlist: {highlightText(housing.hasWaitlist, query)}
      </p>
      <a href={`/affordablehousing/${housing.id}`} className="btn btn-info btn-block">Learn More</a>
    </div>
  );
}

export default AffordableHousingCard;
