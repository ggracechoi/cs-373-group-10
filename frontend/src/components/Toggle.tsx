import "./Toggle.css";

export const Toggle = (props: { handleChange: () => void, isChecked: boolean }) => {
    return (
        <div className="toggle-container">
            <input 
                type="checkbox"
                id="check"
                className="toggle"
                onChange={props.handleChange}
                checked={props.isChecked}
            />
            <label htmlFor="check" className={props.isChecked ? 'dark-mode-lable' : ''}>Dark Mode</label>
        </div>
    );
};