import { ThriftStore } from "../types/ThriftStore";
import "./Card.css";
import highlightText from "./Highlighter";

function ThriftStoresCard(props: { store: ThriftStore, query?: string }) {
  const {store, query} = props;
  
  return (
    <>
      <div className="Card">
        <img
          className="card-image"
          src={store.imageURL}
          alt="instance picture"
          id="CardThumbnail"
        ></img>
        <h2 className="card-title">{highlightText(store.name, query)}</h2>
        <p className="card-text">
          - Address: {highlightText(store.address, query)}
          <br></br> - Phone Number: {highlightText((store.phoneNumber).toString(), query)}
          <br></br> - Rating: {store.rating} / 5
          <br></br> - Number of Ratings: {highlightText((store.numRatings).toString(), query)}
        </p>
        <a href={`/thriftstores/${store.id}`} className="btn btn-info btn-block">Learn More</a>
      </div>
    </>
  );
}

export default ThriftStoresCard;
