import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { AffordableHousing } from "../../types/AffordableHousing";

function HousingInfoCard (props: {housing: AffordableHousing}) {
  const housing: AffordableHousing = props.housing;
  
  return (
    <Card style={{width: "18rem"}}>
      <Card.Header>
        <h5 className="card-title mb-0">Information</h5>
      </Card.Header>
      <ListGroup className="list-group-flush">
        <ListGroupItem><b>Address:</b> {housing.address}</ListGroupItem>
        <ListGroupItem><b>Students only:</b> {housing.studentsOnly}</ListGroupItem>
        <ListGroupItem><b>Community disabled:</b> {housing.communityDisabled}</ListGroupItem>
        <ListGroupItem><b>Waitlist:</b> {housing.hasWaitlist}</ListGroupItem>
      </ListGroup>
      <Card.Body>
        <a href={housing.website} className="btn btn-primary">Open Website</a>
      </Card.Body>
    </Card>
  );
}

export default HousingInfoCard;
