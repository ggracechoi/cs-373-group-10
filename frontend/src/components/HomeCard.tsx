import "./HomeCard.css";

function HomeCard(props: { name: string, description: string, imagePath: string }) {
  return (
    <div className="Card">
      <img
        className="card-image"
        src={props.imagePath}
        alt="instance image"
      ></img>
      <h2 className="card-title">{props.name}</h2>
      <p className="card-text">
        {props.description}
      </p>
      <button type="button" className="btn btn-info btn-block">
        Learn More
      </button>
    </div>
  );
}

export default HomeCard;
