import { Col, Container, Image, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import HousingInfoCard from "./affordablehousing/HousingInfoCard";
import { useEffect, useState } from "react";
import { AffordableHousing } from "../types/AffordableHousing";
import { HousingPantryAssociation, ThriftHousingAssociation } from "../types/Associations";
import { FoodPantry } from "../types/FoodPantry";
import { ThriftStore } from "../types/ThriftStore";
import FoodPantries from "./foodpantries/FoodPantriesCard";
import ThriftStoresCard from "./ThriftStoresCard";

interface NearbyResources {
  nearbyPantries: Array<HousingPantryAssociation>;
  nearbyThrifts: Array<ThriftHousingAssociation>;
}

function AffordableHousingInstance () {
  
  const { id: id_str } = useParams();
  const [housing, setHousing] = useState<AffordableHousing>();
  const [nearbyResources, setNearbyResources] = useState<NearbyResources>();
  const [nearbyPantries, setNearbyPantries] = useState<Array<FoodPantry>>();
  const [nearbyThrifts, setNearbyThrifts] = useState<Array<ThriftStore>>();

  // Validate id
  const id: number = Number(id_str);
  if (!Number.isInteger(id)) {
    return "404";
  }

  // Load data from API
  useEffect(() => {
    fetch(`https://api.atxassist.me/affordable-housing/${id}`)
      .then((res) => res.json())
      .then((json) => setHousing(json));
    fetch(`https://api.atxassist.me/affordable-housing/${id}/nearby-resources`)
      .then((res) => res.json())
      .then((json) => setNearbyResources(json));
  }, [id]);

  // Load data for nearby resources
  useEffect(() => {
    if (nearbyResources === undefined) {
      return;
    }

    // Get nearby pantry data
    const pantryFetches = nearbyResources.nearbyPantries.map((e) => {
      return fetch(`https://api.atxassist.me/food-pantries/${e.pantryID}`)
        .then((res) => res.json());
    });
    Promise.all(pantryFetches).then((result) => setNearbyPantries(result));
    
    // Get nearby thrift data
    const thriftFetches = nearbyResources.nearbyThrifts.map((e) => {
      return fetch(`https://api.atxassist.me/thrift-stores/${e.thriftID}`)
        .then((res) => res.json())
    });
    Promise.all(thriftFetches).then((result) => setNearbyThrifts(result));
  }, [nearbyResources]);
  
  if (housing === undefined) {
    return <>Loading...</>;
  }

  return (
    <Container className="bg-white">
      <div className="d-flex flex-row align-items-end">
        <div className="p-2">
          <Image src={housing.streetViewURL} id="CardThumbnail" thumbnail />
        </div>
        <div className="p-2">
          <h1 className="mb-0">{housing.name}</h1>
        </div>
      </div>

      <hr />

      <HousingInfoCard housing={housing} />

      <iframe
        src={housing.embedURL}
        width="600"
        height="450"
        allowFullScreen={false}
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      />
      
      <hr />

      <h1>Nearby Food Pantries</h1>

      <Row xs={2}>
        {nearbyPantries?.map((e, i) => {
          return <Col><FoodPantries pantry={e} key={i} /></Col>
        })}
      </Row>

      <hr />

      <h1>Nearby Thrift Stores</h1>

      <Row xs={2}>
        {nearbyThrifts?.map((e, i) => {
          return <Col><ThriftStoresCard store={e} key={i} /></Col>
        })}
      </Row>
    </Container>
  );
}

export default AffordableHousingInstance;
