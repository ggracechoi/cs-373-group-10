import { useState } from "react";
import "../App.css";
import imagePath from "../assets/logo.png";

interface NavItem {
  name: string;
  path: string;
}

const pages: NavItem[] = [
  {
    name: "Home",
    path: "/",
  },
  {
    name: "About",
    path: "/about/",
  },
  {
    name: "Food Pantries",
    path: "/foodpantries/",
  },
  {
    name: "Thrift Stores",
    path: "/thriftstores/",
  },
  {
    name: "Affordable Housing",
    path: "/affordablehousing/",
  },
];

function NavBar() {
  const [selectedIndex, setSelectedIndex] = useState(-1);
  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-dark shadow" style={{ backgroundColor: '#3A405A' }}>
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            <img
              src={imagePath}
              width="131"
              height="30"
              className="d-inline-block align-top"
              alt=""
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse 
            navbar-collapse 
            align-items-center
            flex-column
            flex-md-row"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav ml-auto mb-2 mb-lg-1">
              {pages.map((item, index) => (
                <li
                  key={item.name}
                  className="nav-item"
                  onClick={() => setSelectedIndex(index)}
                >
                  <a
                    className={
                      selectedIndex == index
                        ? "nav-link active fw-bold"
                        : "nav-link"
                    }
                    href={item.path}
                  >
                    {item.name}
                  </a>
                </li>
              ))}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Visualizations
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a className="dropdown-item" href="/visualizations/">Visualizations</a>
                  <a className="dropdown-item" href="/devvisualizations/">Developer Visualizations</a>
                </div>
              </li>
            </ul>
            <form className="d-flex" role="search" method="get" action="/search">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                name="query"
              />
              <button className="btn btn-outline-success">Search</button>
            </form>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default NavBar;
