import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NavBar from "./components/NavBar";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import Home from "./Pages/Home";
import About from "./Pages/About";
import FoodPantries from "./Pages/FoodPantries";
import ThriftStores from "./Pages/ThriftStores";
import AffordableHousing from "./Pages/AffordableHousing";
import FoodPantryInstance from "./components/foodpantries/FoodPantryInstance";
import ThriftStoreInstance from "./components/ThriftStoreInstance";
import AffordableHousingInstance from "./components/AffordableHousingInstance";
import Visualizations from "./Pages/Visualizations";
import DevVisualizations from "./Pages/DevVisualizations";
import Search from "./Pages/Search";

function App() {
  return (
    <div className="cursor">
      <NavBar />
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/foodpantries" element={<FoodPantries />} />
          <Route path="/foodpantries/:id" element={<FoodPantryInstance />} />
          <Route path="/thriftstores" element={<ThriftStores />} />
          <Route path="/thriftstores/:id" element={<ThriftStoreInstance />} />
          <Route path="/affordablehousing" element={<AffordableHousing />} />
          <Route path="/affordablehousing/:id" element={<AffordableHousingInstance />} />
          <Route path="/visualizations" element={<Visualizations />} />
          <Route path="/devvisualizations" element={<DevVisualizations />} />
          <Route path="/search" element={<Search />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
