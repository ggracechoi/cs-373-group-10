/**
 * @jest-environment jsdom
 */

import { render, screen } from '@testing-library/react';
import Home from './Pages/Home';
import About from './Pages/About';
import NavBar from './components/NavBar';
import '@testing-library/jest-dom';
import { BrowserRouter as Router } from "react-router-dom";
import FoodPantries from './Pages/FoodPantries';
// import fetchMock from 'jest-fetch-mock';
import { act } from 'react-dom/test-utils';
import PantriesMock from './js_objects/stubs/PantryDatum';
import AffordableHousing from './Pages/AffordableHousing';
import HousingMock from './js_objects/stubs/HousingDatum';
import ThriftStores from './Pages/ThriftStores';
import ThriftStoresMock from './js_objects/stubs/ThriftStoreDatum';
import PageNav from './components/shared/PageNav';
import Search from './Pages/Search';

// Mock Fetch
beforeEach(() => {
  global.fetch = jest.fn((url) => {
    // console.log(url);

    // Food pantries
    if (url.match(/https?:\/\/api\.atxassist\.me\/food-pantries\/(?:filtered|search).*/)) {
      return Promise.resolve({
        json: () => Promise.resolve(PantriesMock)
      });
    }

    // Affordable housing
    if (url.match(/https?:\/\/api\.atxassist\.me\/affordable-housing\/(?:filtered|search).*/)) {
      return Promise.resolve({
        json: () => Promise.resolve(HousingMock)
      });
    }

    // Thrift stores
    if (url.match(/https?:\/\/api\.atxassist\.me\/thrift-stores\/(?:filtered|search).*/)) {
      return Promise.resolve({
        json: () => Promise.resolve(ThriftStoresMock)
      });
    }

    return Promise.resolve({
      json: () => Promise.resolve({ "msg": "Unknown API call!" })
    });
  }) as jest.Mock;
});

test('renders Home page', () => {
  render(<Home />);
  const headerElement = screen.getByText(/Essential Living, Endless Possibilities/i);
  expect(headerElement).toBeInTheDocument();
});

test('renders Search page', async () => {
  await act(async () => render(<Router><Search /></Router>));
  const headerElement1 = screen.getByText(/Food Pantries:/i);
  expect(headerElement1).toBeInTheDocument();
  const headerElement2 = screen.getByText(/Thrift Stores:/i);
  expect(headerElement2).toBeInTheDocument();
  const headerElement3 = screen.getByText(/Affordable Housing:/i);
  expect(headerElement3).toBeInTheDocument();
});
test('renders Home page food pantries card', () => {
  render(<Home />);
  const headerElement = screen.getByText(/Get connected with food pantries near you!/i);
  expect(headerElement).toBeInTheDocument();
});

test('renders Home page thrift stores card', () => {
  render(<Home />);
  const headerElement = screen.getByText(/Discover local thrift stores!/i);
  expect(headerElement).toBeInTheDocument();
});

test('renders Home page affordable housing card', () => {
  render(<Home />);
  const headerElement = screen.getByText(/Find your home in Austin, TX!/i);
  expect(headerElement).toBeInTheDocument();
});

test('renders navbar', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toBeInTheDocument();
});

test('makes sure navbar has link to Home page', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toHaveTextContent("Home");
});

test('makes sure navbar has link to About page', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toHaveTextContent("About");
});

test('makes sure navbar has link to Food Pantries page', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toHaveTextContent("Food Pantries");
});

test('makes sure navbar has link to Thrift Stores page', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toHaveTextContent("Thrift Stores");
});

test('makes sure navbar has link to Affordable Housing page', () => {
  render(<NavBar />);
  const navElement = screen.getByRole('navigation');
  expect(navElement).toHaveTextContent("Affordable Housing");
});


test('renders about page', () => {
  render(<About />);
  expect(screen.getByText(/About ATX Assist/)).toBeInTheDocument();
  expect(screen.getByText(/Our Team/)).toBeInTheDocument();
});

test('renders food pantry model page', async () => {
  await act(async () => render(<Router><FoodPantries /></Router>));
  expect(screen.getByText(/Learn about all the food pantries available in the Austin area./)).toBeInTheDocument();
});

test('renders affordable housing model page', async () => {
  await act(async () => render(<Router><AffordableHousing /></Router>));
  const headerElement = screen.getByText(/Learn about all the affordable housing available in the Austin area./);
  expect(headerElement).toBeInTheDocument();
});

test('renders thrift stores model page', async () => {
  await act(async () => render(<Router><ThriftStores /></Router>));
  const headerElement = screen.getByText(/Learn about all the thrift stores available in the Austin area./);
  expect(headerElement).toBeInTheDocument();
});

describe('PageNav', () => {
  test('calls setPage with non-int', async () => {
    const setPageMock = jest.fn();
    await act(async () => render(<PageNav page={1.4} numPages={4} setPage={setPageMock} />));
    expect(setPageMock).toHaveBeenCalled();
    return;
  });

  test('calls setPage with negative number', async () => {
    const setPageMock = jest.fn();
    await act(async () => render(<PageNav page={-3} numPages={4} setPage={setPageMock} />));
    expect(setPageMock).toHaveBeenCalled();
    return;
  });

  test('calls setPage with page = 0', async () => {
    const setPageMock = jest.fn();
    await act(async () => render(<PageNav page={0} numPages={4} setPage={setPageMock} />));
    expect(setPageMock).toHaveBeenCalled();
    return;
  });

  test('calls setPage with page > pageNum', async () => {
    const setPageMock = jest.fn();
    await act(async () => render(<PageNav page={15} numPages={4} setPage={setPageMock} />));
    expect(setPageMock).toHaveBeenCalled();
    return;
  });

  test('standard test 1', async () => {
    let called = false;
    const setPageMock = jest.fn(() => called = true);
    await act(async () => render(<PageNav page={1} numPages={5} setPage={setPageMock} />));
    const item = screen.getByText(/5/);
    expect(item).toBeInTheDocument();
    expect(called).toBe(false);
    return;
  });

  test('standard test 2', async () => {
    let called = false;
    const setPageMock = jest.fn(() => called = true);
    await act(async () => render(<PageNav page={7} numPages={11} setPage={setPageMock} />));
    const item = screen.getByText(/5/);
    expect(item).toBeInTheDocument();
    expect(called).toBe(false);
    return;
  });

  return;
});


// describe('FoodPantryHoursCard', () => {
//   test('renders food pantry hours card for an instance', () => {
//     const pantry = {
//       name: 'Test Pantry',
//       hours: 'Monday: 9:00 AM - 5:00 PM, Tuesday: 9:00 AM - 5:00 PM, Wednesday: 9:00 AM - 5:00 PM',
//     };

//     const { getByTestId } = render(<FoodPantryHoursCard pantry={pantry} />);

//     const hoursCard = getByTestId('food-pantry-hours-card');
//     expect(hoursCard).toBeInTheDocument();
//   });

//   test('displays correct hours', () => {
//     const pantry = {
//       name: 'Test Pantry',
//       hours: 'Monday: 9:00 AM - 5:00 PM, Tuesday: 9:00 AM - 5:00 PM, Wednesday: 9:00 AM - 5:00 PM',
//     };
  
//     const { getByTestId } = render(<FoodPantryHoursCard pantry={pantry} />);
  
//     const mondayHours = getByTestId('hours-item-0').textContent;
//     const tuesdayHours = getByTestId('hours-item-1').textContent;
//     const wednesdayHours = getByTestId('hours-item-2').textContent;
  
//     const displayedHours = `${mondayHours}, ${tuesdayHours}, ${wednesdayHours}`;
  
//     expect(displayedHours).toEqual(pantry.hours);
//   });
// });

// describe('HousingInfoCard', () => {
//   const mockHousing = {
//     "address": "810 E Slaughter Lane, Austin, TX 78744",
//     "communityDisabled": "False",
//     "embedURL": "https://www.google.com/maps/embed/v1/view?center=30.15999985,-97.77619934&zoom=15&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90",
//     "googleMapsURL": "https://www.google.com/maps/search/?api=1&query=810%20E%20Slaughter%20Lane%2C%20Austin%2C%20TX%2078744",
//     "hasWaitlist": "True",
//     "id": 1,
//     "latitude": 30.15999985,
//     "longitude": -97.77619934,
//     "name": "River Valley Apartments",
//     "streetViewURL": "https://maps.googleapis.com/maps/api/streetview?location=30.15999985,-97.77619934&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90",
//     "studentsOnly": "False",
//     "website": "https://www.rivervalleyapt.com/"
//   };

//   test('renders housing information correctly', () => {
//     render(<HousingInfoCard housing={mockHousing} />);
    
//     const addressElement1 = screen.getByText(`Address:`);
//     const addressElement2 = screen.getByText(`${mockHousing.address}`);

//     expect(addressElement1).toBeInTheDocument();
//     expect(addressElement2).toBeInTheDocument();
//   });
// });

// describe('ThriftStoreInfoCard', () => {
//   const mockThriftStore = {
//     "address": "1904 Guadalupe St, Austin, TX 78705",
//     "embedURL": "https://www.google.com/maps/embed/v1/view?center=30.282327,-97.742439&zoom=15&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90",
//     "googleMapsURL": "https://www.google.com/maps/search/?api=1&query=1904%20Guadalupe%20St%2C%20Austin%2C%20TX%2078705",
//     "id": 1,
//     "imageURL": "https://s3-media4.fl.yelpcdn.com/bphoto/D6srGxW2KClhoKnrmIgT1Q/o.jpg",
//     "latitude": 30.282327,
//     "longitude": -97.742439,
//     "name": "Monkies Vintage and Thrift",
//     "numRatings": 53,
//     "phoneNumber": "(512) 520-4595",
//     "rating": 4.3,
//     "website": "https://www.yelp.com/biz/monkies-vintage-and-thrift-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA"
//   };

//   test('renders thrift store information correctly', () => {
//     render(<ThriftStoreInfoCard store={mockThriftStore} />);
    
//     const phoneElement1 = screen.getByText(`Phone number:`);
//     const phoneElement2 = screen.getByText(`${mockThriftStore.phoneNumber}`);

//     expect(phoneElement1).toBeInTheDocument();
//     expect(phoneElement2).toBeInTheDocument();
//   });
// });
