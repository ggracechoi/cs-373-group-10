export interface CountryData {
  name: string;
  refugees: number;
}
