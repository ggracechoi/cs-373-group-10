export const pantry_data = [
    {
      "name": "The Olivet Helping Hands Center-Food Pantry",
      "address": "1161 San Bernard St, Austin, TX 78702, USA",
      "opening_hours": "Monday: Open 24 hours, Tuesday: Open 24 hours, Wednesday: Open 24 hours, Thursday: Open 24 hours, Friday: Open 24 hours, Saturday: Open 24 hours, Sunday: Open 24 hours",
      "website": "http://www.obcaus.org/ministry-that-ministers/helping-hands.html",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2696613,-97.72564229999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=The%20Olivet%20Helping%20Hands%20Center-Food%20Pantry"
    },
    {
      "name": "UT Outpost",
      "address": "UA9 Building, The University of Texas at Austin, 2609 University Ave, Austin, TX 78712, USA",
      "opening_hours": "Monday: Closed, Tuesday: 2:00\u2009\u2013\u20095:00\u202fPM, Wednesday: 2:00\u2009\u2013\u20096:00\u202fPM, Thursday: 2:00\u2009\u2013\u20096:00\u202fPM, Friday: 10:00\u202fAM\u2009\u2013\u20092:00\u202fPM, Saturday: 12:00\u2009\u2013\u20094:00\u202fPM, Sunday: Closed",
      "website": "https://deanofstudents.utexas.edu/emergency/utoutpost.php",
      "rating": 4.6,
      "num_of_ratings": 12,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFha8GXqL0F55YKciyhNgOJ8bo3UYK-6crpn50nP5XXg6SCkM9KTOFt4JqdYd9GUtMWvFeGnfV9D6n5LYSy54j7jVL73IJLcKuukuOsqjCJP-AZoLGledi_tL-Iw_-YRpvNIJWUTxyCSBlyEAWel5WZokOIiSXucRprIS7iMjMSkiWYl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjiskTGQPZlGYZNIGgYckykqeFL8y9VpYlZL7mJA0P80PsT28kEL5oMwCsi8vp-QusnEvrLGCxYcZyXOMHNN41cuWbPAj6ndL5Fhwi0Udf43TSHLK3Sfp_M6GJRTU_uKGURHOO_ljlAQcnoX9cDJRkntfxvrk3fCmuDUWcv3vriCPgM&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgsI0z0-86J-SxU0hKbqL3Mm0WY4qDUy_DFvr5crWmCvbxm7PIuoNfMQiDUfk0oCOf0n1KamxAIrvtyYTrdzDkl3FbT1PpTEpt3xG45YBFsrOhThEvmoC3OPH97OFbXGXEhSVpYUTdiOra8jsebnLk1yv8zS9TXr_5dKP6VPhX3ILII&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiklUXzwm780LmJKvKU9J-XzHwcSE630oP3YjIJH8nQAlh4cWsqb7etHmNQ30a_Lm244Y0CwzkGCtentklJBWWfQquhbAbP1QEhIWI2FWrpWhJW2bPBkrkIsZMYqSSgMJgHwOL8IqJ7TJXkDYEFEimaBPp4PlBFfU6PeGdzpUzjzYwN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiLr6e3uO5vXm3h0IcfZIT50SArzk_69tOJ19hW3urD53upUPRd77QcH4WFoeGalEgZnctgFVQBMQVpZyGM-FBI1t8WySgMLD3549ExfYU9m3sSFuOHpGzjJBC9cI1W3MG_fUHhScEGKd7rArrz497uXrl2wN5zWgfitxyHRBJCa9Ox&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjChOcDyOvDCDU3FDOCMrNTuqP5jSktIfqKazGLbQ7rgLI4aNKtEwGi8B7XdYBd7fg7pM5-3Xl2nrsK5sdhPakTwVZ25c4M1ML5tOD5ceRQ1ayDLCmPgG8y44JxouwSeOTYIt6V0zo_1bpjrpDQHvnOt4o7UUG1ciAh_65xK8nH1s_a&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjxIRxrnTnwlHdTb0H97LY0rwbRA9bS1UsoP00YQPt8bpgkvg4vciuCGM86yR8E5CLgVnb9pXfCqBjFmtqrWpESq-L8fQezwbAuRAe_PCx1htrsmCzi7QtYOjhzLPw1ksbpP_6FDfr5j6wksKp4i7AKb0PnTFIiN2hLqPHRmb16irWt&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgmMeENZKg6SwgjPHC3yMtGI0r4YU9BORqbNrTn8BBJUZP6MpgnU0hJuyZnzz2K6HSx4fUJ57jD3HBpqk3D-seRybsDOFar838Al8gVu9oziFG2rjnLAJKWO0NM6_2NMsKEhZZrZMP-9_atzuOYqxPhjqLG-ff01il947r-plw9GGgh&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiYdT39hjwQKLk-b4mQdHA1tWcykXo0Fht9l11Dsk2fKg2cktgqMvnn9BZR71YYhabY3XhO3FgOKwsNGAk4R2XDG0cijE_ll4020YlFcqwJ8L6yZla_wloqquNFEDnI_FUuS1nh3457iLhF-w0-jHNPlHauWmvLFyK-0lgMABbQdEIX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj4CR_LFOrxye1o-jjDDRM6VmwwDFj1JPeISAOcPw1KPVsBEhar7PvJ6f5oUu533c1fK7pO87MbWS9V7CFlRYB28L0N_UbJozQxs5njFFtNsPPirVQnYEZiME_u85ZA60IG165sGdpwNpggUa6oJypfdIoX7-dThhlRunEvP-8S897T&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=UT%20Outpost"
    },
    {
      "name": "Micah 6 of Austin",
      "address": "2203 San Antonio St, Austin, TX 78705, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: 5:30\u2009\u2013\u20097:00\u202fPM, Friday: Closed, Saturday: 9:30\u2009\u2013\u200911:00\u202fAM, Sunday: Closed",
      "website": "https://micah6austin.org/",
      "rating": 4.0,
      "num_of_ratings": 2,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2855286,-97.74247659999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Micah%206%20of%20Austin"
    },
    {
      "name": "Hope Food Pantry Austin",
      "address": "4001 Speedway, Austin, TX 78751, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: 9:00\u2009\u2013\u200910:30\u202fAM, Friday: 9:00\u2009\u2013\u200910:30\u202fAM, Saturday: Closed, Sunday: Closed",
      "website": "http://hopefoodpantryaustin.org/",
      "rating": 4.8,
      "num_of_ratings": 54,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgQkbwc18J2TxTUFyuzcN_mtC40RVFM8UAPEJZQ02fSFZZqVoGUXapeFznQEpORhOXhlbBi3GzSw_sjJ8FeI8-aZGWmG5gtAIFzdF8hCW00HaRqsGiXMfryytvx_5zJvWHjaJ7ibZczUMNQEEBkGZexcDOzLBjPQFUDtm3nV_d7wgOf&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhuR26fbX_qiLhutap2r1w3Gdyc84LXLu3i4r5EyjoHz0GCdQMDUcNZtg5e1ztBOHuVPbfQ2dLtyZ7RTASpppvs4DJDFRxNw6fPuuv8K16CopiFXKpgDrSibKaFVTlR5c6VHvY8akkP-GLxCY-G4tqsqmb700Bx92aRuzr8nTa--n8F&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFifCOcCKZTrLDNWgNbKXiLwed8TwivhLju559-sYTvKBbMxAXsVHCW-lOvZ7t89IvOUjHs0gBZFjd3OVDU9KY9Le9hLwA6D0cpm78hgttnjP_HiUfj2jrND8hc5RFLKC9F6gdAbR-NUmFJU59pGuJZih-bmceqzSZdC9_pBmn3NllZ5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh1HM_TK7fkRrsTRzsUTh9EyY-5jHxujDLBO866UpQn903rB0QSAX5aOpI1ZVyDsrgV9FPjEkVUEfEIc5_2ikXZBCs4ZdTRziKnv4hLDmoGbBlKVfAAywiTV1i2tiOWWjVx05v2G5zhonV6g6UIcCCDR6d_3Ufww_OMEvGHrybsL-od&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjZPTca-sQSEl98aQH2AOqs2MnafhgAgLMqbILrn8n2QWuvC7x2cx8rqNhp2r6_OQOZVsdkYaGhTMPr5uiEFVaqAYSOYUeob3ZM8KYxX421DCGR38X7Y6yeFcYXfp6WsCfjsg91VXv0_xSf5bDf5zLJHSAAbxGYqwkIxfNr9cTtAxIy&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFihXgn-4o2xa3rS8KNca3JWBF9qI1vmupH8wLa_9g7b8MKEISp9gToHA3_5EN-6mUWfnE3nJPrJ7RGAxGwi_OTymgaTiweSc6rdz_IWduA4UkFKJNMg6t2yDaej16EOOC65TiYGqbbkqIX6jeSa89yE3TTaEegitoo589xmoXPo6u6W&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi4kW354oD2Cb8jp-zdlPhefYq-UURd9ccigHSAVYJPUgNa2ahSak1bprPbz8K3svfCS8jqGz8p9JVhnJE3loZbZTVxsYkqa66CcMfdjYAAjr46NKbG5zQGaaBp-_q6Ubfeur2GFamXI-98rtY_EwipvlXj1dYykqDCV0HySsseVo7s&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdSj8AcHfgJ4JLSnvE_66FTGaNeD10sfe_ca5fvvMYkrwUXxNbD2xg_ZPoycRc8DI1i0VYcPn86mubF7hYLnX-tsC27PT4uaqch3uUkyeUD7rXm440tnVxfTYADsJwBIRweZeBxaPyVmN9lwJ5VFplZO8UmeM-Fbii7vTwQKVZ98YA&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhwPK_ViRZkDLYFLMUVZkCM89dcMB5WCySL8TxBYJDEm0YS0KRn4LiTrKEO7QVDbqp6BsU-Wb7eV1yTBkqjc0tDfWm4DnlWVSUfDyaBxuwD3y33DlRBZmBhshf8PSXJjdSudiHzCFmSHR4t2rgXi7-emFRFO1eEN8JjyU8lpXWznom2&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Hope%20Food%20Pantry%20Austin"
    },
    {
      "name": "Community Partnership For The Homeless - Food Distribution Center",
      "address": "Parking lot, Austin, TX 78701, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2699702,-97.7398934&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Community%20Partnership%20For%20The%20Homeless%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Hyde Park Baptist Church - Food Pantry",
      "address": "3810 Speedway, Austin, TX 78751, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 1:00\u2009\u2013\u20093:00\u202fPM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://hpbc.org/foodpantry#foodpantry",
      "rating": 5.0,
      "num_of_ratings": 2,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3013154,-97.73337509999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Hyde%20Park%20Baptist%20Church%20-%20Food%20Pantry"
    },
    {
      "name": "English At Work - Food Distribution Center",
      "address": "3710 Cedar St, Austin, TX 78705, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.300591,-97.7352766&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=English%20At%20Work%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Green Corn Project - Food Distribution Center",
      "address": "Austin, TX 78705, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.304094,-97.7437525&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Green%20Corn%20Project%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Universal Living Wage - Food Distribution Center",
      "address": "Parking lot, Austin, TX 78701, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2699702,-97.7398934&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Universal%20Living%20Wage%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Personal Connections Healthcare Services/hiv Programs, Inc - Food Distribution Center",
      "address": "1704 E 12th St, Austin, TX 78702, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2737518,-97.7215085&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Personal%20Connections%20Healthcare%20Services/hiv%20Programs%2C%20Inc%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Our Lady Of Guadalupe Food Pantry - Food Distribution Center",
      "address": "1206 E 9th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: Closed, Friday: 11:30\u202fAM\u2009\u2013\u20091:30\u202fPM, Saturday: 9:00\u2009\u2013\u200911:00\u202fAM, Sunday: Closed",
      "website": "http://www.olgaustin.org/ministries.shtml",
      "rating": 4.3,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.267083,-97.7273171&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Our%20Lady%20Of%20Guadalupe%20Food%20Pantry%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Texas Department Of Human Services - Food Distribution Center",
      "address": "Parking lot, Austin, TX 78701, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2699702,-97.7398934&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Texas%20Department%20Of%20Human%20Services%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Southwest Educational Development Laboratory - Food Distribution Center",
      "address": "211 E 7th St, Austin, TX 78701, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2682943,-97.74043929999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Southwest%20Educational%20Development%20Laboratory%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "East Austin Neighborhood Center - Food Distribution Center",
      "address": "211 Comal St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 7:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 7:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 7:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 7:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u200912:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.austintexas.gov/department/east-austin-neighborhood-center",
      "rating": 4.2,
      "num_of_ratings": 5,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2595679,-97.72755740000001&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=East%20Austin%20Neighborhood%20Center%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Ceden Family Resource Center - Food Distribution Center",
      "address": "1203 E 7th St, Austin, TX 78702, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 3.0,
      "num_of_ratings": 2,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2650247,-97.72985229999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Ceden%20Family%20Resource%20Center%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Travis County Community Center Central Austin-Food Pantry",
      "address": "5325 Airport Blvd Suite 1100, Austin, TX 78751, USA",
      "opening_hours": "Monday: 8:30\u202fAM\u2009\u2013\u20094:00\u202fPM, Tuesday: 8:30\u202fAM\u2009\u2013\u20094:00\u202fPM, Wednesday: 1:00\u2009\u2013\u20094:00\u202fPM, Thursday: 8:30\u202fAM\u2009\u2013\u20094:00\u202fPM, Friday: 8:30\u202fAM\u2009\u2013\u20094:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.traviscountytx.gov/health-human-services/community-centers",
      "rating": 3.4,
      "num_of_ratings": 7,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3152136,-97.7141122&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Travis%20County%20Community%20Center%20Central%20Austin-Food%20Pantry"
    },
    {
      "name": "Department Of Human Services - Food Distribution Center",
      "address": "Austin, TX 78703, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2974509,-97.7670172&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Department%20Of%20Human%20Services%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Urban Roots - Food Distribution Center",
      "address": "2921 E 17th St, Austin, TX 78702, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2786748,-97.7100451&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Urban%20Roots%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Foundation Communities-Vintage Creek - Food Pantry",
      "address": "7224 Northeast Dr, Austin, TX 78723, USA",
      "opening_hours": "N, /, A",
      "website": "https://www.centraltexasfoodbank.org/location/foundation-communities-vintage-creek",
      "rating": 1.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3212179,-97.6808957&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Foundation%20Communities-Vintage%20Creek%20-%20Food%20Pantry"
    },
    {
      "name": "The Center For Economic Justice - Food Distribution Center",
      "address": "1701A S 2nd St, Austin, TX 78704, USA",
      "opening_hours": "N, /, A",
      "website": "http://www.cej-online.org/",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.24846699999999,-97.75669719999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=The%20Center%20For%20Economic%20Justice%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Bethany Faith Food Pantry(2nd Wed and 4th Tues)",
      "address": "3507 E 12th St, Austin, TX 78721, USA",
      "opening_hours": "Monday: Closed, Tuesday: 6:00\u2009\u2013\u20098:00\u202fPM, Wednesday: 10:00\u202fAM\u2009\u2013\u20092:00\u202fPM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "http://www.bccdisciplesofchrist.com/bethany_faith_food_pantry",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2771349,-97.6950567&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Bethany%20Faith%20Food%20Pantry%282nd%20Wed%20and%204th%20Tues%29"
    },
    {
      "name": "Equal Justice Center - Food Distribution Center",
      "address": "510 S Congress Ave, Austin, TX 78704, USA",
      "opening_hours": "N, /, A",
      "website": "http://www.equaljusticecenter.org/",
      "rating": 5.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2562427,-97.7476995&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Equal%20Justice%20Center%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Montopolis Recreation Center - Food Distribution Center",
      "address": "1200 Montopolis Dr, Austin, TX 78741, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2322294,-97.6998885&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Montopolis%20Recreation%20Center%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Greater Love Baptist church - Food Distribution Center",
      "address": "6601 Manor Rd, Austin, TX 78723, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 3.7,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3083997,-97.6686811&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Greater%20Love%20Baptist%20church%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Saint Ignatius Martyr Church Food Pantry",
      "address": "2303 Euclid Ave, Austin, TX 78704, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fPM, Thursday: 9:00\u2009\u2013\u200911:00\u202fAM, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://st-ignatius.org/food-pantry",
      "rating": 4.1,
      "num_of_ratings": 15,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgSauUPQDyQZjx6bhpd_Q_9OfOsqnxVFp83r0KNBT2qKIKAPm3SVjB3_z_Knik4Ee5lhnD_7Nk_hPdOgwY6uECECmaW9ccjCxIfSogCndMR9H1a-0p5PQqY-vsjm_BAGeSdcmXjqHoVy0Qch1nvh_sLOcpTRyFfhLvm0cLCKQpiSzMW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi6UOzHsvIJWhhlV9sxVdVR742MQkJd0gnTw0dKF4lB21vO4FD8iX9D3FvfJrnBpHoem-HyU-45II7UeMDYiX4fyI_6b8-e6josyBMZ6dSBKjBtc3WybE0V7CJkgC6mvMUDcHOXMjcMCP-k0s4QH3xgl7vWYBAC0g7H06BemCJs4_ZY&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Saint%20Ignatius%20Martyr%20Church%20Food%20Pantry"
    },
    {
      "name": "Saint Louis Food Pantry",
      "address": "2118 St Joseph Blvd, Austin, TX 78757, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: 8:30\u2009\u2013\u200910:30\u202fAM, Sunday: Closed",
      "website": "",
      "rating": 5.0,
      "num_of_ratings": 5,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3514119,-97.7309228&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Saint%20Louis%20Food%20Pantry"
    },
    {
      "name": "4hcapital-americorps - Food Distribution Center",
      "address": "1600 Smith Rd, Austin, TX 78721, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 5.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2583143,-97.67834839999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=4hcapital-americorps%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Workers Defense Project/proyecto Defensa Laboral - Food Distribution Center",
      "address": "5604 Manor Rd, Austin, TX 78723, USA",
      "opening_hours": "N, /, A",
      "website": "http://www.workersdefense.org/",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3038983,-97.6825145&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Workers%20Defense%20Project/proyecto%20Defensa%20Laboral%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "WSM-Wellness",
      "address": "1912 E William Cannon Dr, Austin, TX 78744, USA",
      "opening_hours": "Monday: Open 24 hours, Tuesday: 4:00\u202fPM\u2009\u2013\u20092:00\u202fAM, Wednesday: 4:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Thursday: Open 24 hours, Friday: Open 24 hours, Saturday: Open 24 hours, Sunday: Open 24 hours",
      "website": "http://wsm-wellness.com/",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFieY5XwlRI17yqBROWPbTGNQXHH8acBZxACrqOZnHWrJA62DEMHXrb0TBCGgpX-fYmBJ6kXSGxhsfnUrCOO3ew3B5YASUYJWV727zy3gZR8PmU0MprCHMeTTjFa__DFu5SJ64qOTrNqV9aE8dEtv3XHkqggJjhiDNyddn-QEPoJIKQI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhCIewb04S-UnqSGj_R3RNdqZmNPibQDrnCA-pTiorIAauNUesBP2mSOv8p2abPNIHb7XyTQIR0KoTUwiViZJqMdVHisCckbpeaLPD1mY_TrgUJGz2_OAbHvsfFEHOz8eVAZE8oxwo8UanCUe6EUAF5I8Fb085Tx57WlplzQhnjonzR&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=WSM-Wellness"
    },
    {
      "name": "Mobile Loaves & Fishes - Food Distribution Center",
      "address": "903 S Capital of Texas Hwy, Austin, TX 78746, USA",
      "opening_hours": "N, /, A",
      "website": "https://mlf.org/",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2890683,-97.8261112&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Mobile%20Loaves%20%26%20Fishes%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Foundation Communities-Sierra Vista - Food Pantry",
      "address": "4320 S Congress Ave, Austin, TX 78745, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: 4:30\u2009\u2013\u20096:30\u202fPM, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "",
      "rating": 5.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.21926700000001,-97.7672862&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Foundation%20Communities-Sierra%20Vista%20-%20Food%20Pantry"
    },
    {
      "name": "Central Texas Food Bank",
      "address": "6500 Metropolis Dr, Austin, TX 78744, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.centraltexasfoodbank.org/",
      "rating": 4.7,
      "num_of_ratings": 399,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiFeyICeGupy_NzV0eR_V8xgPFpdfIaAdjmyGkTIMeGFG2zSLIeLCH4TizGyU9pj9m5UdcLEC6nDXEt9lWtM3g0vEX2TQaXQHBYYKJEsB3lPi-eRKvvcy9bCUnM8VLQ7wWM6d7PEVIASQttab8HlIL6L59tPXVSDFvAkLhICF5_tStS&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjS4Bzpuc16RWOdAIeogrwWqLZuk5OYRJMW0BW1r15ZYcYEKXelWEXahTkYmdFSKVpwJIgqzTVcE4oBuX0OYgxlta1VCQzTOGzuuhaJtdZVzxM2w-B9I9CNv0ldpJR1XgF169ZbB-xI6c4bidAM1BFXKNc_wmgOxqRMSesmZE4YNCrz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiHnjYU9r3ApJZpSQqkjogadc2aAHDfYIiYO4NjyT7qJvo9wXFJyqcZnGD44fU-529GBHXSqcDM8II-dKgEbTalqDMC8MxqLQZhLns98kNk54OCfCbunScS4ZhOqz5rfWaFSisLpbcnM8S9oBnZrBRPtXkIYM4dClhil1Gm2Kl2hemJ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjLsZ5R-aQ-apESNwKqVwJ2hgt-1_4lAW_5XhNavxs9aSjxSC0X4uvWPg23zxH_hKqnFPnZbkvlIreP2MPZerQvfnHfXXzAnSH5a0OJQCtRzu6DKNk_qnhQjqWOyiXrL9lk_2wbZzmWzrBZWD6z6vjGK41fTDqYc8wkttpo3FS_E8q-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi0VKCk0AIUCvz6d7pPGMrql9MsW0bkkBvanGIa6dV0I6MejYlr1DpoqfJeLnBC3x68gK6YGPhANM0okd68Txr65RBqDDtWAhBGr6-vbb2ApxuvtuyG-mBUkpIOaQe_XmEeF7Gng1XYiJ1qx761lczzEPK8fKBGAOjqORT1BcNi-Rs5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiRME3YRZYXd61640IG62dqkHm8A4dzYEjGstGS1fc9ALF7wagpl6_SaKJxE6KQoKllgXHLwlt5asgL9Hh4U5blxxCMiRasSjI3-DxOKhSpRPrLXKta5O5pfk_Bg0dMR1d8xypNjuVm5sxqDVaCjVo9STK7mun3CSaasrzMLyVI_kKB&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjgziqTSudFd5QTO8hFoBupuDgubxX8mX4LgykN0TvG-xoB0NGHKRtNMEG_RiUh0tU4cxgUuLHycMW06rwuvoY6eSJY_B2n_k0ey49G8G6oUJF6BxvHdA8TsVCP-RKlZ9zQtFVzyLP8HgkLJzzm7VjkFct4o0KoX7qEkccD9dSyZ8T3&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiBSZ4b7Bo89qcDjoGVouJFNNRxaI_iNvYprmIAkmpdTA_h1PmUvglWkr4-w3A6KrABMiJUkLftHAeldd7J83QXvRHqYrrSYMvFEMM583Oxl_28WqqK_jjfFmdY_gFlawdhGEU9i9d35-TqIa5N2GjMpoUMm9nSH2MsOnTK_fC6_lut&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjP1bnhHYowNxC0rYgC55iK9f-q4oNvMd5YjiO95DpTIJaYfESVPs4-IFp5J5pJ_XRVpL73iItvijjf0YUm2Jjp0c8GsJ9kCgX1V-291A3LnQDhbW002HO4Mvmnk6zg0TOqTpHtqQKeByUI6Aa6PJ9KIvyLakyI9m2f3TUIhRxCBhj2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjZiRkSLnpKh7_T6HSJqFpBfCAf_3bHpsk0hEkDY1eL9VpLYEerfNxc7xph53ioO0yjl717NEFmwxHp5fOZqWZNtQMaU7fQTpOcdd935Cr6oLFP7Cy8nPK2EM5mFtpD_UaOqqgY9c9Zj3RDUvMWLHtQI6gygj0cGZr4bloPcogsCngT&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Central%20Texas%20Food%20Bank"
    },
    {
      "name": "Foundation Communities-Sierra Ridge - Food Pantry",
      "address": "201 W St Elmo Rd, Austin, TX 78745, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: 4:30\u2009\u2013\u20096:30\u202fPM, Friday: 10:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://foundcom.org/housing/our-austin-",
      "rating": 4.5,
      "num_of_ratings": 4,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgt-WSVR7zQcSnhR49K1AF9ccBd2L0VlpFEVsvvX7-SfFvgRQuidfMexuGOJ97bh0Qy81s_gQ2ZfseP1GkeTMBOU9ehwTXzxoWmRQveHMZ4TXjjaD9mNTKde5cr6Y2ge8E43_AS124bsMx6S8RqVVQzJ2n4yZsyTCK1GvIiwQibj7_e&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Foundation%20Communities-Sierra%20Ridge%20-%20Food%20Pantry"
    },
    {
      "name": "Principe De Paz - Food Pantry",
      "address": "1204 Payton Gin Rd, Austin, TX 78758, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 5:30\u2009\u2013\u20097:00\u202fPM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "http://www.pdpaustin.org/",
      "rating": 3.8,
      "num_of_ratings": 4,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3620419,-97.7083643&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Principe%20De%20Paz%20-%20Food%20Pantry"
    },
    {
      "name": "Austin Spanish Seventh Day Adventist Church - Food Pantry",
      "address": "100 W Rundberg Ln, Austin, TX 78753, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 12:00\u2009\u2013\u20095:00\u202fPM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "",
      "rating": 4.1,
      "num_of_ratings": 8,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjZUeUWQsWZtY8QiBTatRYWrl_sLIpzlrHSOf_mZ9FqX9ApFrYipX6m8aUocYPgzoxreiiDR3Hqf_nf4SCNcsZj9Kl2G_-CES0qtfKdqeoV36jKnx2T7yFbshxrUC6apy6ErYTc_mhp_FosC_5ipfY0mwhGqOpgXkDUPwbmLgCHFXeK&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Austin%20Spanish%20Seventh%20Day%20Adventist%20Church%20-%20Food%20Pantry"
    },
    {
      "name": "Foundation Communities-Homestead Oaks - Food Pantry",
      "address": "3226 W Slaughter Ln, Austin, TX 78748, USA",
      "opening_hours": "N, /, A",
      "website": "https://foundcom.org/2022/02/retiree-neighbors-up-with-healthy-food-pantries/",
      "rating": 5.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.1833749,-97.844083&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Foundation%20Communities-Homestead%20Oaks%20-%20Food%20Pantry"
    },
    {
      "name": "Heaven's Harvest - Food Distribution Center",
      "address": "1734 Rutland Dr, Austin, TX 78758, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: Closed, Friday: 12:00\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "",
      "rating": 3.7,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3753892,-97.7100869&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Heaven%27s%20Harvest%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Foundation Communities-Southwest Trails - Food Pantry",
      "address": "8405 Old Bee Caves Rd, Austin, TX 78735, USA",
      "opening_hours": "N, /, A",
      "website": "https://foundcom.org/housing/our-austin-communities/southwest-trails-apartments/",
      "rating": 5.0,
      "num_of_ratings": 2,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiJrtYJArvuXw-y1OtehrkjyWfP7Jj03ORvLbWNwQCYFlh0Xj2RxvFCEVLErAJddo1qjN7dmvdiesJvMRSYlHoCBEAbGh63DyFl7Wffa5UXh42GG2SRkW7znOgjeCp7W9pMgz7NUrHrTciIIPMtr1tj3uAkiqDLcswY5te0diTtm_yX&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Foundation%20Communities-Southwest%20Trails%20-%20Food%20Pantry"
    },
    {
      "name": "El Buen Samaritano - Food Pantry",
      "address": "7000 Woodhue Dr, Austin, TX 78745, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: Closed, Thursday: 8:00\u2009\u2013\u200910:00\u202fAM, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://elbuen.org/wellness/food-pantry/",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.1992941,-97.8021703&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=El%20Buen%20Samaritano%20-%20Food%20Pantry"
    },
    {
      "name": "Manos De Cristo",
      "address": "4911 Harmon Ave, Austin, TX 78751, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20097:00\u202fPM, Tuesday: 7:00\u202fAM\u2009\u2013\u20097:00\u202fPM, Wednesday: 7:00\u202fAM\u2009\u2013\u20097:00\u202fPM, Thursday: 7:00\u202fAM\u2009\u2013\u20097:00\u202fPM, Friday: 7:00\u202fAM\u2009\u2013\u20097:00\u202fPM, Saturday: 8:00\u202fAM\u2009\u2013\u20092:00\u202fPM, Sunday: Closed",
      "website": "http://www.manosdecristo.org/",
      "rating": 4.2,
      "num_of_ratings": 218,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgUtG1UlVm3hcre4bTW7_wnQsmlnUchBC7z3kC4nHq-cEmaTQTIj-Ors3gVLJ6ZKK7MMPYDBvQqI9sFJK69X4UQeofr0wM-NoPoNHevVDg-01hyjijbsejyhka38TUmwQvIrHqOE2I2piOvzqJlm9TkE63Eb6E3QPsKVziCXZmHAA1K&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjTH6qlyD9WeR3xaidBxw5FhHQjLl0Mh01EjxfcXWzB8PWRHWsdcAqs3xcJ7yQ1B00RSkAoct504frzlXkdjpL9Q4ZzYRXsmh3eUlqvXdTsRMcmKhNaTnjVrLbH76gZk3bPPhMibVgueQtkC8rBG8m_yUFjJ2jYVN1uubWhmv8VnzF5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgBjI6yKZ3Kq9OZ771WYvqZJtSu12v3dOzmp_gUZAf3NfTu3Ck2dMAWzUeFj3vSBsQA2CddMHh9kc1AZN_BPZ2kyufhMcS68bO1Awv82PqfG4zx8UQheZGiXQsnjTyOUPJsnxeHNBj4Q-9nX9NegYc4E-qjqJoktxf-MLhygFO0w5nR&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi0QZYO8D-1Bh8ht-KGNcBkoXEUOk-L4qNwZOLxPJkxD8KT7clbhPttg8YoZObz1IQS1yiG1oBKl4_tR0GB8uHdSLuaSYsaalisZHpIiSDuXAfiTK_62Nfu20_VUxIL0RerWA_r1GVPdO6nv2HnsAiFLKR0Xh8nLO-yfbAVBvmE2NRH&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Manos%20De%20Cristo"
    },
    {
      "name": "Abiding Love Food Pantry",
      "address": "7210 Brush Country Rd, Austin, TX 78749, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20093:00\u202fPM, Tuesday: Closed, Wednesday: Closed, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://www.abidinglove.org/connect/outreach/food-pantry.html",
      "rating": 5.0,
      "num_of_ratings": 5,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2177411,-97.8458474&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Abiding%20Love%20Food%20Pantry"
    },
    {
      "name": "Covenant United Methodist Church - Food Pantry",
      "address": "4410 Duval Rd #6808, Austin, TX 78727, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 3:30\u2009\u2013\u20095:30\u202fPM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://www.covenantaustin.org/food-pantry",
      "rating": 4.4,
      "num_of_ratings": 5,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.4136293,-97.73204559999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Covenant%20United%20Methodist%20Church%20-%20Food%20Pantry"
    },
    {
      "name": "Feed the Community (Gateway Church) - Food Distribution Center",
      "address": "7104 McNeil Dr, Austin, TX 78729, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 9:00\u2009\u2013\u200911:00\u202fAM, Thursday: Closed, Friday: Closed, Saturday: Closed, Sunday: Closed",
      "website": "https://www.gatewaychurch.com/feed-the-community/",
      "rating": 2.5,
      "num_of_ratings": 2,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.4378482,-97.76447759999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Feed%20the%20Community%20%28Gateway%20Church%29%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Texas Center For Women's Business Enterprise - Food Distribution Center",
      "address": "Austin, TX 78734, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 4.0,
      "num_of_ratings": 1,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVMSjnAb0IV6Ur-0VoEWl4KlITnCnsdqHpbX9cYWeSrmB6GEAKm5kE4cW3kLjpe3zc2vgqmJYLOL5WqmSzVKyVlCPkz-8AYJOe3nMd0ZmyEcXC5uvXUn-e7GBtpxesD2TPj_mF2o7lcXPvxuw8TBCWKS7Bq2t5XtFi8hjbSGbia7Y&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Texas%20Center%20For%20Women%27s%20Business%20Enterprise%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Covenant United Methodist Church",
      "address": "4410 Duval Rd #6808, Austin, TX 78727, USA",
      "opening_hours": "N, /, A",
      "website": "http://www.covenantaustin.org/",
      "rating": 4.4,
      "num_of_ratings": 16,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFizCSelInrtuPfQsFAi0eCNuV2kEuxY8mgig0YimNnFBli9TjLMmsSqu4L27osmClunXP7yYsTpSMfOoCy3JtufpK2ZcjEqnonWGJRKYTOR6MiFwupgA36MmhjcG0xJTE5tGLQYg4qCZw_kksmpzHzSkIWtJM-XOpldAAtSPhGvO-76&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiAPvTxE9NYlfDMnJn-J9mVZSiJcWCL70xjwUUm-XX6rzgbYufzUK2ev09A9imqDws8AG3sf3SUknuVcpyVQ2_ttDDItuZRUwwdgGc-xXcChMAcCo951qY_XuygApLIQhQJn49JoQGJTQeQwRRf_qDcXUIDLLQDR_ZRpMO4Pwx3WDny&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiFpFvw6r1bBVVlbOo1seQSouyqZBJDdtw8NJQYvfjqDtA2qsxmc_si4vRikdErGqcQx2Hn7ltI6vNnlGuPaqyozSwDjpjXMKMbafxUG2pX9JjfEV6cO05AKAVaAoi_OhDQ-MhwZU_DeLVxm3Pkmi1aF3zPduEsNknKuVLcdIeHiFMV&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh1pR6bmmKFEkAg9VYy_7ZMe_KwugAILDQPb-tE454uQF9BQeK4EfnKnDI8yfjEVaYMkZH1tFTsJG5Iy6I7wxqIhoU9IWZHpUDGx6A5RtFLV6A-ygPMxaLWDF088NYw-qd6BknJW1JqyXeCvkZzCM05tKz8f3muHjRtlABO-8DF6cGe&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi7p50i0X1MyfKqxdTa367ngN2yS44XGYVRcajmbRf_2UUm5VHyLbnSZzwBTHdvuJUBxI9BTN4WhYdP1eV2sHTYwY91Q1if0IeKDvNYPTqjkwc-2HEjtY-5T96-ss4S84UPTBDwSRjcd0D7zZ6JoP8SHlbSgV8J1-7WkiIh2uiHLvCg&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg9w8yro5WWZhnLpIpljWxGagZ4Jxvlg8TKXeYe95ojRqBABtnLHDxV1sBG4enOBfg-RXXyUhEl41VtEOIKRLqGqRmCbg5oJPqN3FNB9Xv6T1zFIXfG2zzV1ekZ7vgIMNbu8uIllyeKE4aK3TfuSJ-RU6nM0sceUhctW4SJHGu7VTOt&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh-GX36Md6XnD-6SXC5ZcuuXFY-ROgHXCA7ewQyKIIHGC75hAG7H576SMQyUUN2HKI2fXgU6GlPXhGgD7kqOJWpc1G52aBbfWrWqzvnTUeMgeVjA92zZPzvtx4X6G9kLOOHxalBqgahSVx9dHf1-6i4R5n4pZ9es7N7F7Z0JZOCnomA&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjt2hpI-boQcvlH410CjqInb76BtwaPo-K4kbMyAKlL-0ko4rvVnNgCHYVTUmMV7NfGNURPUQaKH42dmMHxNzR56oUtlFzEbSedpx4-T-Ghg5BBFt0H5WXMuEnbExmMXNy6U1ikC_QnX6ZtPQGJlZbI0z6Id9vIE_StxIoqG6jPM_f6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhxS4fpQcaoDXJr7zbcIEnY6yJvJMwbVY-i5SdowXfwTrxevAiWe9fcNBfCAT2crsnf3NX_NzhRJUFfJyRtV6iWSS-mmwtgNZFLLYdLAxIrPXrKyRsxH5RxlSDT01jCUp1rB94Ax2mpuRvbdkSQjoRjSeTWVjuyeP5OoR9-GWVELmND&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiCbgSC6ZuQ7FTCze1bFvBUpvpZFuQYtRGfNaaW43NcYjw_xHCxXumLPoYLqrUgj_A0U2lJCl4gri1sg7SfNO_OBkhd-nPI9ahtP7LlXHIfAjXYSIL2XeKh2eysHRzuCf6bvPq7TIHodv48jQEIc_5HjGwbuK6JE5B8wel3vtmJOa13&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Covenant%20United%20Methodist%20Church"
    },
    {
      "name": "Texans Care For Children - Food Distribution Center",
      "address": "SAN JACINTO BUILDING, 814 San Jacinto Blvd, Austin, TX 78701, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 0.0,
      "num_of_ratings": 0,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.2700752,-97.73956419999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Texans%20Care%20For%20Children%20-%20Food%20Distribution%20Center"
    },
    {
      "name": "Austin Baptist Chapel- Angel House Soup Kitchen",
      "address": "908 E Cesar Chavez St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Tuesday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Wednesday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Thursday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Friday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Saturday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM, Sunday: 9:30\u202fAM\u2009\u2013\u200912:30\u202fPM",
      "website": "http://austinbaptistchapel.com/",
      "rating": 4.6,
      "num_of_ratings": 196,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgHb51m9JIcZMw_yBuRIyY6N79WDOTqe91PDiCzsmfYk3mGSDjaOgi0fe5eQw22BMzSC1tfqh82SM4ad9LG11egQAwRWjRGJsZhJwXm851gu3WE7wX8BhQZa3ivq79eaTKy1j4yy-OE2OH1RjJkDR8pSL-SrzBOlc0fHAvVxAXzGC7k&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiqSQW80N7Ph7xBPKNUGEqBOcsoTmYnBGjnQUGb8SmY-xpxHmX6bOLsKqkL9loKgW8LEa6TNBipwIGvWcn1g4vm3JsZoaV-x6JKSwUcwRV-VuRMIYoOmv_RPpJ0KD9_qj0eZ2iodPzetWewkn5SWAMVHMvfj9KojWiV1qe4aQVjaud2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFirhmHNfHnk1BAMByJpg026V8f1YWZkbxlLi8MbVyHPwgl2I8NL0S7JOc3v-poJVRUhaEHA8IB1e7rVmD-q9VOLYKGRcWizB5o5Dvn81HSI5qibb1YN6yJqX0hz-zWugbDmTj4LI-DcVDGlinQufy0h224BFQDBCcWt5for9lN6YH8m&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgFTQcelODDj5ZOuCn9tWsbZOcVuXpr1LJ16IWCYGtd4V4xXtc6gc-U9cjWSvOqqV_fJy-OrhFM1JNkF3OSI8-T2EAxS7H4PAiczSxsvXgzzZt-mrflqNczl5uzXUczhiNemAgxh1RtD17jHXu5duxQ6OsZ_QYA9Pw9VdzUPK4hVKbj&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjmE0uSZOl-8e4EQqc5cB0FgMsOYI_SG2ITnpljqbUSef0HLzPxRwMYor1blGXxarcectASuEKa8w1YZzdoo7rGlS5Gbr9W5s8p7tpYU0g8u1OhQG5Fd721yjrcwNdXFhGr21IPjTn27d-UBOhMTstOI2I3OogTmf8LhekTMQWsRMp2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjTdzJ7peuj0U3m27VmLrPsUaY7P9wkzoj8Dqx7lGzEiDssvIP7_D2keS7GshlmTFRq4AbQGoe46iFlni0ZV4Mih0zbMJ6Jl88KEtunQ5wO_UDcVjq0fwZoJcuxjpAdIYvBDDsEk-MhpeZWuQU0jtlitlUerZ_IlL3BYAR8cqkYCi4T&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjaXhIQfA9K76y_E-toS3dnn0AKCFn8s4tf62Dx4dW5AndZaWMQF-Vg6b-1VlX60B1tCivxGSH1rfUi2uKFNZexQaDsYUYsA64PtarDOm20CvbMh3Tx4cQUJ4hc4xJsy3q0TVCqqrNblQfcniwBs8XbSwqt8v6n3rf_HU4T-toSj83Z&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjuOK7Q0jiA9hii_X5FICEcBS_zSMm26Njq06J_LJdKh-RzirWocZSqUj_vMr0_08o36PUzlafr1Ei4X-vA5TXW3VvwnLEziBJH0G0K6_kPJMYOWaXuYXBJcpTF8vuvN4cF2DFRwGBwi0xFiPCMcCEDV6OsOR2aluUObm8Ihx_rSmWD&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi6gaU7e4eVHmZZ5kyfPRW1qSr26i_NbypzuOcT27uBdsd3BzCEXKX5Qak5qljwajTLSLL-XESiIgG84UirtE-zcT-Sm5aNAviSmpGiGGoshaJpQuLiV5yt-DNOBYg5Kf8AaMJfCouLo6RK8UoZ7BBmiRcSrpj52n08KuSqjgVZIj-T&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg4tasO2vQ5bHfIMzfBB5XZXLuSI6xSiAMdhTj0RgTqaXIeUNbVs1XTNDRgjZeFbHZzhJA6GXO_q4RADL1GzMspJuGJcjGADJD2H0oSk8rmyGn31eHFsefNmDgNE-F3VP4pxHSAk108oHewm7VaLvC8nPA45yB2xRyx8CadBk6zww9f&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Austin%20Baptist%20Chapel-%20Angel%20House%20Soup%20Kitchen"
    },
    {
      "name": "Community Vegan",
      "address": "1124 E 11th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 12:00\u2009\u2013\u20092:00\u202fPM, Tuesday: Closed, Wednesday: Closed, Thursday: 12:00\u2009\u2013\u20097:00\u202fPM, Friday: 12:00\u2009\u2013\u20097:00\u202fPM, Saturday: 12:00\u2009\u2013\u20097:00\u202fPM, Sunday: 10:00\u202fAM\u2009\u2013\u20092:00\u202fPM",
      "website": "https://www.communityveganatx.com/",
      "rating": 4.7,
      "num_of_ratings": 109,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgvwWQYUPnMm1lbhd1Swo4TPB0CpRrdNlj4kdmkBaLgm8as-HcrWdidJXu4bFyMp2CXSCNXrFel22Bva4BXgh5LiqgwiaJ2f7NH_0OhI6eH3dG1ExJKgTFrDXxJVCrFw1ilGxAIt5C7smQZXplmNWmrdQPGdsQOMt6VskwwL6dL-31r&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjF5pSayGvvGCm5XLEEaQUo-uoezFURQFy2m3lkJxxPRoV5wUephOfp_wNGal1Lck6Plc-fcK0v738m4hKOE4NPJKw1qG8WU8sG3YAtBO_CTk1VsaWuhmfm4lbA9wu3UNYtBznrljrBU9MRVboOI9qjIjg_ohx92a8U6JbadH5r4VuY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjPSELWm-9dk9J_Y-6cgQL0hyRSQ1g4dFnMSSp8CqDPYsyQO8modxXUz9gQAYN-emvt1TeJDFIwJsVNfJAm3F2nU1ItftxAhKWdhDtfJppogWknBfDUDrcaIjUapWkNkPJ-l6QBK0jaF5buU9HDMsKgbHPNTi-5Y-fJ_W03sdAbAfqC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh_DnHRPnl2v7I_YFmkkok04v47hrxZVXsqn4dqYCh5diQExqU6aSADiwajo5EAr7dL-UR7JrFTCKOl5Jz_HlsQTAn5iu_Kv7i_21wUX2O8FEIas-gXVt20rym6PXmyUdy9xZkdqYxCfQFbxwCpO4dB-83Ss40MI9evfCwF8ZGayZpV&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjJSBvX0CiTYJe3TaX_g-y-pQPqmNwdF1A434ZCLOJGVGyfVC3e_ul_D6TqLBUFXbqRZzV96hP8wlVtmqKYVYzC1rELrZiIOxFC0vux0hxMrhFoNZk9tXBo98lBMiLdT9Jj0Vqf15TunvModlgpLbKxTdtoSjfsd1VOHFcvQMLK0I3T&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgCpmOW2aBLP9BMN7uMItwbyIg30cB3OJZqk4jSTC-EBUDeYZiYvc5JylUX56sCOPsdbrDLzS-me15q2IDZZuXtWIm3-4_UXM0VfaAotxPQX-H7bGUzkZ6T9A50uZQed4AZbB-1fiz6RGe42ZP0t34YlgPsW2b4bisBYEUlxowbA7E_&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjt9B_M7zQjCRG4RAo_t0JTdPjcFqCI47e7zijoFpatT8yKpYLGCJfyV7olLYHIi7QiZrtQwWRPJnf7hqss8ELQDFapAzqAQbra9E31REf8Q7Fo8hlz0uSgLvG656Gq9Y8SuFsGN4nNRMY1LMC_Jc2vNyYNH0fA4itklXDxZiCxQvDY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhkU9iHm0UjJjCe_IQ_SGCgUNT5WPMlgibwaUCRTnfwMn9t7XHnAOT8qrpsm6IjK4rmPFd167Zn1VEiClMfDPm8cLQIdkdk3Q5Qu7MNQ_-GcnfmUkspjz92UVoeR-kmiQlbCqQHwOEEThTijq3H-_atSRzcRskPOxlqjIYtAf49HqXn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhospCJJ3Mt9kbbBAsGggkBS2xF-jxg9I3D7x5DLCto11q-zK1kGUW2eog7NKj1Kn9TrzTnCrAZy73iSglmrxS8R3sw-bNTQjC8m6HKo0UogqTdeFIMN_QsJ4DtvUt0roYEu3lVXAI3AO-7-tnUVNEiepqguCbPdzxD6e3ZXMkOLXni&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjRzlZ-MX0aEFqRmKfD-FQ1ZX8EGdHF7K9mvTQ0k073K8fW-DE_tL5DOdRBCi_8FASid3Hxd8FZAzxbxW3Q2LuXjOaX6IKD8kkYMpaln3g4-49ljC0b_zqTJFS4uztvQDG0bDccrKnA7xAStJvKvA5H60ebF5CHr75zFhYQ1Tw02Se0&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Community%20Vegan"
    },
    {
      "name": "Meals on Wheels Central Texas",
      "address": "3227 E 5th St., Austin, TX 78702, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20093:30\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.mealsonwheelscentraltexas.org/",
      "rating": 4.3,
      "num_of_ratings": 91,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjV3ZSQAltrwIO9mFB2WyykSKHWe0pYq9IX-xotbO7i_XsGTWX4domzwoKKcPhjBncA6gn_bTZub3HUfw6DNg-1C6LPvkccVc4D_K2xeHAXULxI3_2q4dVodXe9cQcc5Bes44RWLzpr2Nj_1iQEamD0uMdQRMuF6Wq3XtYLo6aV1YDR&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhy6AHdx8-ozELjGawDs8GgmGf9rPlwM2XaOxv7Sd8TEUgwibdDqIad2kIJd6n5Yu_th6ZWtINAcoef4xgkQv2_zIfTHsTEl5gwOFuAA_DlPpnFSUsrE7JI69-TuyHVi5ABDNltXWncH3p2srj78MxLPXQjz7EL5K8ZMakeCQKCIbHm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjZfA0SWZhlRxMhgaAmroziZ16-gV7FSVWoZA4O5SQQh6apaHk1LO8QZ2YFFNpN6zRgC11OsQlXj0QewrvkPkosAvEM01-euba330PjZQqRY_RKePvTCxui00CmeG8Urcmmlc0d5GiYclFQsMJDojvNfdr41H8CwNNG5CTLgekohBB4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiQ2wkjVYLrNZfQe_MVPKkLJ-BEeoY5t2CmYe-whSVEyFOYZZi_uQIZTX06D0MyGcAHVt3Ksuu76KcvSTBS7JWq-djX90LR4Y4pLNtYYydnvZHiiO_AOivC714lzG9HAKX-z_0s0LxLZi56W5FXIpy7VFCC1_KaPmQntkKsCr9qrv0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjThCKY6XU7-AKHOHYMwLmnV-Pk-mT9SW9zdc5BiC4tqhOwygFiEKnyvxMkGroj1F1GLq8IuXmUqVnsp3tQb2CRADPdaa-xHKcc73lyu7Ydu7YGJicBlEwCWWNK1fkNO5A-y_p060X1elIaBnkgym93l1d5noQOC2KLSZtEJhQjLuHS&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhnYa6fg3APibcbr8Zsedv8fkXkquBp6n5-R588RGxGDbEaxNeznzmSGLA4h2nu79JDmNaujGcuYXab2yTiMlfrGBjPi5ysCFIRIFULySR6_tfvFCmMcWSvEMlt2dGO1FfgRjn7Akcc2Orp5TnZ3TafyFTq8sCI6nGUwjVKNExyWiak&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhxXQjXOQ2u8iy6ndHl9sXtfNzy40LamyfdlWiqFnaekze56oNCwXELxsFYDCy23jkNM2S-BF6dbXUL1aGyMypR4q5XIXJfhdOTUTIsgryi9uL6weM9DhL_av5GLuGcKHI9o-paapYyFwZSWI0usyxz_pP2IrqBVU8crlygPX_0Q5Un&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiY_1x1gT9KnIGNLO-Fs41KDlnlQ6-KUdEj5V348Bev60-5-H2KAdPmhwH5d3ttEq_P4SkX8lvxQI5vTRe6_phQD9kaHMZ6ioqn75xtsLKiU8FPfIC-Il4Wl4QyCLBsqKPIN1OG_O1rViQgK2D7-zT880x719SGMpWsioJ0drFj8Y8B&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg-yXaJg3_ITYV9qfmfqyZJ0207ZYqUj1vFXU_ScUmJoL-_mLijv7JrftJqfYjS5Eo-5Zf0-5ouk4yYcNa2wKX7uDxwCU-XkITO6BNJoauwVRiPQbqAUFheHulv02VkUWb5sz3l7I9ZaPCDYMbwsBUyDblFJIsnBsG8vupDfV_kcNBp&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgEGvuCaiBw9FuJDJtHtvaO3sLU6NOXNlC5TxXcNEicVWXJFXYNCCsuSFBoru3hP_F8rODHVv4FLlTpNGgORQyfscZHYsq9WFzjGeHCDQcZu2hSBr29P6Aa2aFqIZc0zjaw6WBw6bL2Y_DcA5LmFhBVhcw_1WZl2iN0m8ObosQn_ykw&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Meals%20on%20Wheels%20Central%20Texas"
    },
    {
      "name": "Hoover's Cooking",
      "address": "2002 Manor Rd, Austin, TX 78722, USA",
      "opening_hours": "Monday: Closed, Tuesday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 4:00\u2009\u2013\u20098:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 4:00\u2009\u2013\u20098:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 4:00\u2009\u2013\u20098:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 4:00\u2009\u2013\u20098:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://hooverscooking.com/",
      "rating": 4.2,
      "num_of_ratings": 2544,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgA6gWotNiqQk_FFs2vaa7NsN81p4MqJzdCBKq6rydj-rWVX0NHGGIU7RxSUITyVse3-lLOAlXEqtTVIXMozHwDBijaJWn0lwropDNyZjDu5MIMO72bbzVVJmrvdPGS_AijcOvfI30HSvFjH4uc8DFRuSuR6hKFZR9KkdiRJDhib6m4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgBjltEJ2xPz_j4Ld_Z35KfrHb9u1-73aFq0rKMgA0zG7gNrMpokgWP3Q6SHNI5EVMUttyQfZ1nwEi0A8ZaxeNbEUHWgk25EqZkZjXTCnZ07Vkrx1dIMefH0tdMb5bS4-KyUDJWybFXhhwmkDrXNK1LyzOEljdfIDhPItux0tPQCQRe&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhoVmLQYs0R6mVQW7uEg34cAUWuGvMfbZnrvnwcMH6HdwxdMJ83j3gZEO0D2aHB9997W8nFqhsrJaxc06b8x5ox_am44jRUY2QBR3J-WrJ9iFt8bT-tL0lXKmUwTdYS6F-v0g_H-8aKx75DMSu0GDkVFTPueAsk9SANIB4gnvB2qgXG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgaBgJNNtEMMIOZbHT-jpzrrgsOYpjmCgT-Xzk8FVn0KEYzIyUYReWaCNBrkyycpB9-FwVC1cqGs7i1hyBnQIVF2eIwLMes4J0LAgotzjtYpdTBEW7RiBGixplNEInYlqxqMuB96H2JjRQWAGNEPydxAMzQFAOFu-qBhXUuWsFEPRoc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgfkwRSG_KESp695okkReZ-K5jjva5BfjfcKgVKdTNiuUcKdSErg-8Es2jY4UWMB_q-qUPAgflqU_IyqZhhMI4ziR8k_CpxkMHswrLewhYIMs1ApM2yIYRMCoZIWao0UhwHE4J9Nc-EGvaZieOambwBAmvy03tp6jyyl8wxx0OIDF0S&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiW5eWVtJS4qY75s5k3cmEbV_s7WCkABpGHez_z4Q_yvSChd2JUT2TFV_etFLaTWGDToQK4cJxA1Ias_HGlZlzhSp01dzLvajBiPK2Sa4LO9F4qv9BM8ynBN813PVHdZfp6PnJnTB7J8h9ZVrHBT1P52H4_mmAyjxvWVgi49MKOwoAn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi3EKQS3i0x-oCN7NWXdQIcBbciW5T56Ttq-eQgn2sityGFW2pqhelfy_thb-QocE6VqiUIGvWWFi4PLZF79Bi7v3iyT6Oy1WgzSn6a6RnAxgz--BRB_xC1ykrCa5Isr0F1Cg8BBILiuMCM24qxKZ4Brh_wbaL8Gl7YFn_INCWKAo-c&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhI81B0zmKYfxwdfeMyvtLPPzjQqQSc9uA7-cdXvV4FMfMyNbH4ZEFLjNM2C63mmkTjeT8EEwyVYVSwfKMVfLk2n1UYGCmlvEzgmdW_QYn4xQOtsG3mvwABV9yG7j_e-ZxaBOcS2b-uc30JQLha8yC_TjHWDsfebEfWvj2drOU_GQ0F&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjWxzmo3ncgeY1dwe2QIZY9jcj1VILVoz-_cnMGtyLwcKszbpeuMfxc_Y0Lbcw1EuKZreK6Xo9R_-788b4DvAgyli3ANaLp6IYSGWYaWJJ_vVHY-5xhKEvFwM2kxYRgh0ojYjR8xcrQoYXpInE75BnEaJDCXJTZ7ufbr4d4HnQHqmPo&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhNwpJzkPThXJpC2r0cpBw8qUo2AcSrYVR3fgY_JioNF6ehK_cT0jekjLJ_7pWHgJqJHVxtwbkmbdgMsh122kn8qMeRqWUddRV3Bk-vVMo3pXamXCnJ1ypxuiZQLF7TIDney9Ny8fnch5LCp2VYTqUMWqfc-5U4ttVmvkLW3QNXIPAq&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Hoover%27s%20Cooking"
    },
    {
      "name": "Caritas of Austin",
      "address": "611 Neches St, Austin, TX 78701, USA",
      "opening_hours": "Monday: 8:30\u202fAM\u2009\u2013\u20094:30\u202fPM, Tuesday: 8:30\u202fAM\u2009\u2013\u20094:30\u202fPM, Wednesday: 8:30\u202fAM\u2009\u2013\u20094:30\u202fPM, Thursday: 8:30\u202fAM\u2009\u2013\u20094:30\u202fPM, Friday: 8:30\u202fAM\u2009\u2013\u200912:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://caritasofaustin.org/",
      "rating": 4.3,
      "num_of_ratings": 156,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFihq45ujrXDgv_EVhev--hLvZH0KLP_vLDqm1N9mHyNFyjjzSZ90C7FPgKuhPm8ciKetO26Hddvqt6Mbjlq0Xgd38SeE2GTmXW7Vpyv_-tOS9dXq9lwbSe0SHMrPawras4cmZ8-gCA_sN-02_dmpPG3MwqaSRIHQcLNBk9qeZI_XSWm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiVEXdGusGWssD6aZ08NVOg_FOXTtC3dZD9mtPENDDrIWdQiZD7TaO4SQqEQophkq5s13tli63uGvYNHCTkdpILptMvcDfaWsY8F72X93M9yQ7JKSjYL8xalccEfvGBgL2MF89_Rj3WcU6i6ari1f5DTqSri7G5NUswzirdM0O5i7Q-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjoT1P30n72bIp7s8JotvPRBuvft7IPwJoxFNpHHftuxAAVn9da0xp-riDTVWb4l6jhnNgrtasfxAkJ096Gp_WeUZU39nJifHU1G_xOFD7eQAFq_zSvwj8BQ-l994r39_glJALcVjZzJGpWG3jvdRA5T2LhPL1_XYW2eyeZ64NraAUY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiS7BYi2_UbzlCO5MbfjwYO3McWmMNVOIib42GBQXHxwKnVWnar3hffHokjSZcn2ugekkPXW_zvCwX0Mp5wfyGdQT8rRFEK9Sud3JVnk8M_BjvRxCiKxjqo0NrEh3FawCaYy-_c9qEg46v-qUjtbH_D5qWGmjez_2JWcJDZOD_Q5PpF&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFidXgakdCIZjnARoVCJ7IuRGoC5Q-O2crwT1AkA_tSO0zcMaTnDFAQ_7Jo60gD_csXO2SNSuiuN029YC7c16rub5mkD6qjk3RfVOAmbnILMEs64a7iRu09FHCHgXtmCCQUyGSr5x3NaH1AMGzqtTrj8hElev2zq1GJ6fKLGmlD9YgMk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhY0SKsEQp4Jqbkqej6l-B_S27natIOuNOsNKHBhjCDoTBjzM7WaTYpRP0_731UMNY5drtix5MW7n11s16qRJeIQSm8tACVYkkWBuDEU1qMFwxztlKaicShy8C57Rx__NETEvrsfC2PgkJD8RNfI67KFErkkV4xa4IROmrNl88xi9-6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj21M8IJ5aC__aI4MwtuvubxPhoDMl9p4gF6w19BD0bnZl7RaxjWwjdw1VLbX3y3RvuVa4y5A3-MZbITMlZNbH2XFDViP1UWN7rohhvs8c2ApSFXbvby_gbjJ1uDK0XHOhjM4JNbZhfJJu5UgGGhDsdIX9XDzxy_9sBOz843L1tIeoc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFimSAX7Eh4VLDWwjJzrsXsKJ4FGDg5FIGv7ROOStye6KFCey7SLpMbc1qAuuvLUWrwtYXjkRTTrI3cE84O6YSC0fkRDcW-8xYKZpkg3VN1h8h93R7IANNZyQj182d-duqtOBH6Cw04IVLQwLWmve_0cUxvQn_kwHX5EAv85Mk7o43Rl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi7hheTJl4dW0YcRvDWTn8Tl5TFbvjXcsuxTnh3bjl2L7YVKFwlunbOjeYuGM4RCDTLuggUQPlHclPcW5_Sp7MksCbACbyLfrAPUCFbEF5yV5c4dn1OtRFu2ei5v876miNaNidYyBFcFk-KHZbcwvsV9gs0Brf7eiNpc020dCwwHtWC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgHnLxi-X1bc1KyBrquBQYrXjNl_NizBxc8PzC-u8Pd7QgoNCLAyWK4Z0X2CZS2mzLUIFmv48iqYmpV7ERuEAYrKp70oFjZXTOFp4FHercNDJ1_Z6b6YvxN4EZ0qkwiDAzYlWScgXHHd0kTAtXHkvKbVV8HN6n-jKvsZjFZrh-QY8V5&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Caritas%20of%20Austin"
    },
    {
      "name": "Colleen's Kitchen",
      "address": "1911 Aldrich St STE 100, Austin, TX 78723, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 10:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 10:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://www.colleensaustin.com/",
      "rating": 4.3,
      "num_of_ratings": 1087,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFidhHoC5TzQH7q4NaETPHnznrdDwUa9ChcW8gmXfKC1mobfKyQ9xwdhMGAgP9273cw8yC0BpCDLpHokumP8Voa_v73GQaE7e2MuJHvg5Ha4u_XnexBlvB_C76TXm1W_z05yEA5QmP-xM_YSk8IwVapoziygwW_arkecbwLR4iSAPPMc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFisSLcUxMXMdasWLOdRuunyCuMg6v-v4-ALKrxKw8OvVqIDmzukmlD0nTT68bsDyJrr4XVs4rP0R_xkhAHT7UCae9o5v5E29qIY2xihdvU4dzKmuHLNwkM7xLweLBmZRaQ5J84RULbweykCJ30gCFulEACWVqpBaTaZiR3BtxLzs_Sk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjrAhecfGwykl1LQU_f_QzX-ePpMB0sWWyHk0byPJYgSql0_eTfl68zx2bOsNOw59CkZau53q5jRcPfYdSsw5K1pP_ZUWwCPVcPstBTukJxlVyAdB9Le4_yMKRBVzjywA4AAJ_UbUkZknEEUi3LDSeAAnjEfGq9tkXOtk3PQp0KzcyM&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhjS7w_EIbUqAt86WZBdC8PvAzaVPbVFxYPfDMEa8jmYD4nn_-8jrgWKCn-lTxWvE9h5RGD0hvcoh6OHRju9ERY0SLMF0o4kAqfVUvZ5mcBOEyzpa15Fx0uckix6CuLo9AtIply1erfLzWXirD2MRM4ipM9CJElXL_SOIkupM1aOyKx&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj559qsoH_IIywYHbXh7j7Pg8hZYS_LiBPIt_roDZWAMDmf7vcCZy9Tc3EJawQsU9Mndtl40GtXGLfEeZiMmUkoPOIbu3q74X6SSqV_OcfUjg0FYM3B0clUBTeJnNXoNDcY1QPOwmtsCsa8jdOYVukX5vKQMCE57WDPr2fDxdpfSStW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhQ50Fcisdqnd_zjMOB5bqkQ5f3tislDCyOvChfc621QR9i4yUMw8aRd0-b5uAJ3BECq2xBa_8Zx2VJZvTCUwAqwf-NDYSruTdPlBGTtept_hlqDedb7EkEt8bsRdVeRwR4yNR6rehnZRtYUNGFO1zmYNH9SAwjNw7oHiOjK1z-JxCt&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhuxB3fRBhCz7Ktj5nJjYEAx5sMKTkj9bbIXunafBG5cvCostxjEVVdbpHLhTxawW69PNrDrpIz8pk2veyCZIRDRXtoaU30XhUS-wkRFC3ONWVLduwz-xqLfsqsKCaAW-Nte_qRpo5BMm8w65HXbw1uWayBk9eeHZZPQdoAg9lynS-O&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhXDEOfcnP903HDK4-9U-r_1Z7N6UXeKtXJdo-eCiArjmXJjUfn9L5EMmlkCxLbpTFl9ff0AsKENM51F_uwfLYxEgqJxgz-fzUDNMCNp7XPRGHiDB0335GvIYD8XXj2XIEOGvbGSUGOAIGabtgnb-3dScEAYvms3dy6OgsUQhF0HHg7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjX25DwmKz7urPohYcrQGgTDudEDLxn2giFZBpiaVaBi0ZtzeqKUfSaIL3ANbUCvSXpAAM2csIDZgLkYWYTqBF5sah9w5gFB04kw3Q79lXzooLn5CeVwZnx6L1qY8inYtP_4jjaLE5ZkqSM4ejTn810a7_rRg6eUkga7OtRcaaR4SGb&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiEwHGcrM6_9WbJNwlikHwpln_1ATa_8hZ7IVmRbwppcAaSYE5ya_CnAaX89--pJV7YrYSQOKNBGuSVmqGg5jwaeSmOtiGOTUiDORNieKCQ5LDCjFNrXZliqoJ7SpoVJVo7x7LbPLOJl2ussvoMyoIUwVyF14LroG3-ZVs_Pv01bEig&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Colleen%27s%20Kitchen"
    },
    {
      "name": "Mama\u2019z Soul Kitchen",
      "address": "5903 Wellington Dr, Austin, TX 78723, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 1:00\u2009\u2013\u20097:00\u202fPM, Thursday: 1:00\u2009\u2013\u20097:00\u202fPM, Friday: 1:00\u2009\u2013\u20097:00\u202fPM, Saturday: 1:00\u2009\u2013\u20097:00\u202fPM, Sunday: 1:00\u2009\u2013\u20097:00\u202fPM",
      "website": "http://www.mamassoulkitchen.com/",
      "rating": 4.8,
      "num_of_ratings": 86,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgG3zP_alz16jcTdqKwnYj-zcxXy6b0KcbVnj2g5Y6HLd_wkxNhzazGuDU6BsAUnAbmd_WFMZT4F3yJ2YQtWhbYNW4e5ByN4_rPbi_voGdfElQQqQtFPD3oAqUzVrRNNSV7iURujOk9WxsqOXekHM7jri_-uSxoYQ6NuQiq-dqWVxDm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg1bkakVJ2whmKawtPVs_M1pEcUlydH4BMTWu1b3RCIMiXY8w8oSa4F9iFOXwJXoh-uEQINCsgwZJP4tjw9orRXbNsyFKZVYwc_cLGuuvZuS46vJ6i5hCGoBpH3eTNQwlyccbBqPcV84TAVf5VIgir_8WdH3tLEtEV7xRLdMIKFFHkZ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiQT5vsNvIS_ZHo_Lf_IGdzLl_NXDoXCbI36LKptNG9izlMNHYnDIor6pdyTLzwUXjBM2zgB5fsfy24srLYz7KU9StqGCsA6EoFjV0gkaDfgEtCcJSnCT_yV_I1LA8yKekDTeXrEzQ74JcpefaRb5pavwe0fC6hc7WWp2qSzuT1IjCD&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFijan7kLOOXaCDniMM2OV3xHV588Di4quKPp5VkmGAQz4xXUtZarrP0RnAj5kGlC7WPpse6pfutGyrextKROJu_HY8EfsyLN5WW-TcEOb5hBhWqSOaZH4SRRBQisROV8u7azhrnnJOLU102K-Vq-fx8ZnpdhLm7KUwwzfSeh-q0ORom&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhlWI8i1pkbM8vg0zS0x-tLhCBLkCcM1jh83TpfssMbtwN8XjC4eSokyDdX_d02C9zmNNotqxk5OVFUoFT_MUOTuSfppKK7TtTqNRBGtYZk_rNdxBE7FIdGj_bP1RsX0UwKoiLWljbyVK87PbRsvLO_lYCJ0ChZ_1QWVtkQPB6kgF4Y&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh6OEIxbbiYVJ0ierjrQgXxsMVn9eyir5vgqzTN3F9awsraeC502lQTZJqlgpe6O7lVmJXye1YN1uYXQMlPfKRjpLoLMq8uQLipridTZ_8_IpjP7LdXR8H84oSgi2Tr6vzvmKvF0QSlI88KZ20Itw7iFedcQ7O14ATDV9knSBotRW9w&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiYSSOfiyfGuelePXT_7rRyyTO5PZ0FIMiJs6jgiXNYC8MAmcCm7d23LBsku2zoslKV3VmIEiHFY6nbsaRiV_HnY92KWMU9nRYeY5udtHhoYmxERrbY0hRVvLNCexggOZ_QYb2z_lDzQ31vWk7lItptdbmXE-DCKa3vwpHu3BJl0KHW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhJvwyuSpjN46pg1IdF-jN5a5gL-dN7Y_YxOj-KcEIZEc5n58dqESHuWHqgKp9-ujus4v0vv0gzwY0ypDtd9Hjg5yi0IawA2H8liMPI3lAHqZYoEUBcO47PB152o1CcD0TfrFlRmw0UtVbVktVcZv6q2XTOg54EJ1OeKKuRhtT7Oi1T&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhnbSs-e6oR_f0mUayBTdOCHufiRWVpKyGO8YVAMJRCCiFbkvgeeB7yGikle_s5elTw9BAokv-V5B9epLWDm1nmdRI-Gc52N1YbkIuNfL3CWZDO-Zx003sSZ_muDAl3HHfkB3FeBHnu9sxEu-a1wltA3PR7Wfd7li_KuJfHS7FSRzBh&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhrO3xupqvsLr6vZzqJjV0IemObA-058NLw9hwoLm4mopwMp8QQ3Zrmb8KEOLmN31cz9so8R-A8CJdmJgiLFw7hj5OV0WDs-NGgSe84316z7mPddC6_HF_ZZ8Hkh76YKt0mjBnX1HYf2bpaSqFqfTQg0qygaOrwQ24RcwGsMMmzCK9C&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Mama%E2%80%99z%20Soul%20Kitchen"
    },
    {
      "name": "Moody's Kitchen and Bar",
      "address": "2530 Guadalupe St, Austin, TX 78705, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u200912:00\u202fAM, Tuesday: 11:00\u202fAM\u2009\u2013\u200912:00\u202fAM, Wednesday: 11:00\u202fAM\u2009\u2013\u200912:00\u202fAM, Thursday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fAM, Friday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fAM, Saturday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fAM, Sunday: 11:00\u202fAM\u2009\u2013\u200912:00\u202fAM",
      "website": "http://www.moodysaustin.com/?y_source=1_MTQ0NDg0NTktNzE1LWxvY2F0aW9uLndlYnNpdGU=",
      "rating": 3.8,
      "num_of_ratings": 108,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjGvRA4KerDDAMxrFKaYy1FXXtMlOI_UFGEkjpBNa5Yf7j9tyrFaHNcUvFf2vFsAKasbLGjOUXAZTg1aAeNLj_XiGibDqHgNQfTIlxfMALr6O-cFRgjRSBkjHHYR9T2GfE6yeAh-fAPALPB99oVVjYOsM5pBNSLpmldgCofxLoHSWAV&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh1QTPlW6lpDI8Lh81PsW4aZg_67To2BmQuns1u7LZmsgh4JPRlRIAwsIhdc45ccXtAjFIj26S0vzA10DC10W-fV7lPW57b_DPb_4fgERjqtingHz0XUHPRrRADgVlxmDLsMJAIm4GMq_O9YS3chmhDTKxfN2mwQtpauUfFD1fr6Viv&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi5YFeNtV9hJu9cvm-2sbXYIz_UOoz4O6aShyKNIDDutv_IRThljD9766aut07NOSYLJdm8N0A-jf1Suo8Rq4GNrbz_fbgUd6PHpCQtGWJp0zUVUPkzRRqdBPZQku6drsiM3UWmAgxdBHavLMJF0Dit-h7PBSd6vRJTrlr1QbCLmX4p&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgRsLJNJ5APCyOVW_iUxtM1RLBSju3MFxkIKeZDM23Avjtv4zys7oAz25g3Lj69Fcz-rEqlEqySFBA41xblAShT5SHsPKKQII3bbM64uhuhRKK3KuuuyjED8fBisEeJU8lJmcSZ7ZOk5kAehaV8FQGjPPZIEYNVZmGKDa2dSRtTnR6G&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh0UTvUfbTci0DtPrA0S91Qcq10zsHHsecG-kRjHyCzKhQkPPuxu-6siAlCj0872JjFJGP8hbxoQxdvG-RYiD8ctcZtqabCAy9hfFx8prLUclI035ItyBSgrgPGuEdDNaZL7kxJgRbB6dsxfgjS1gxv8HY80X2kuZqfNsRi8LbtcUnh&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiJEA2MjMxUr7KS3rQLXymc5SIuboH84WI3If32uqDHdsYIl945z_HDpRGC10yKfwG8PVrnOOItFOkTSTQlvPb-4YmfrpDkG_0PGk45Ezk-hPj2pzs02BTDs1iTxjjA2m_ThcRM_yzh1hRzv7NQ4_LUTN8qpiNzIZs_ugzkfw31PQms&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg_Y4iXaI8qNNj_Q1tmG9LDHvhxpMBdJBhlOe-3H93iaTYwcxIACoE7Ddfw0WsME5iAf2a6pRhWrq9jua8kQbUhR42W6IJ-AbWrLpn3lGvMYd-wzehqZM3Mm-pjSpxghfhn_E8Lo7vhV7OtIop6jJtLaueeS5Z_mANGBweoe1dnOKYw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhPAF92CWlI5Q3YMw7TgDwHSTiejMFVKpC5fLoorXLb0qnrcY16KorSagwCc-hgqBPNpp9pJkYOHB4PdhVYDo2m5PAyFLOI4Yi7hOlEN3LyarGRR2YvA6zTKGHt8uWiA_rVW8a0_AyKY4GVXEh1ilfZUVVu4HC1R6AleP0c0LF8nLxi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjpfkFQ1RqX4BM3doVMxbDnsXjV5Hb3x-jm0ndmyIytgdRXWi9oU9-FxRtQCVmEMsgVDIEDbQmpyP7BD3lLA1UYqC5zzxVqLcLWHvMlVVT9j00svxpZbRy2wHZfBOqJWsUvSEAKqC3Amuilm-ZDagd4YMduBAYPLZ7O4RW6CR6Egb7-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjko8yLHteEyCKSXfosfg6M98vU36xcdXf2g2SO6WU9yYj0Z9ZcAm5_SQVD-cd3qos_eLzWe7KA1p4Z8fH27CoIw2OTF4DB-I2_OMbi0npy9N1BzhDSFWghLdPvef2nfiYpn_hZFHTcC6gUvBGI7stoNFLi5xI_btxL8f47PG73eaGL&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Moody%27s%20Kitchen%20and%20Bar"
    },
    {
      "name": "Sustainable Food Center",
      "address": "Building C, 2921 E 17th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "http://www.sustainablefoodcenter.org/",
      "rating": 4.8,
      "num_of_ratings": 60,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgeg6uylgMbiyc7xt2JibaOODCR9DsedjQVfpEpWlw8Mro3bwaADamo9S211YHnksM-hRBDUCEJgjwuPecrkItM4jhvGwU2uyBT4J2CzIV4kssKOdcRgOdseh9nUDWIzkRGKWgwNGCIArwBFEG0h1nqXkvQCzPWkzJX13sR0cHyp-gX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjguUFZKmbx1ILy7WFhOTDDeIYUOGzKtt9szE8mHw0IWxOj_ZRfZVeP0_J3rhI8GtcHdAf28PnreEeIlmuMLPVs8fgW7nRVfX6qLUrhu7eVD_muHb0JCsO-NRsvYlwFmHRER8MzfDLb_rHjEXgtmmm8YscPRPUxgmyOVnJIgZg_vANN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhnvHuqFSGQSN7h01qLre1RkOX6YT_mfZ6t52fw4SZ9i11gozo-Uc0xGWrt2HFm2e1QQAFkiiUMVbHsBvKYieCbibyWpsaI3zuqhoK0dUQvO5RblRHvPxUwEQimVVn--XX4eVTLUwll4-gzS3jQJVslux4EV_cQSQPPqFwbakPygzFI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjNSzUd3hj_Rjv4y12F2QpiwZTFHrVpmPymEafmrpPdO_8d3_mJox4F1bo35nfqkVUFF9hZ9UhHIINgajzMTdB5SrhGdbscsVTtxQiAx0Jtr4LoJKmp-rVjovw7ftfB24W4UZIS88QcW07czQeOmqkyG3HZ8Gcn9Gqedwsn5Ogy7777&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhJvgFf2VYPsIgi4ffzFIcx7s2mtfwo-aeI8EM-OzjqGTsCpzW7DyASjDgIbQ2IKII0l5NMVPHQ2Uvr7VGeHNP6sCTgoU8dkSW_P2z1thpoF8lgWGr2UCv4Bynkec6vovBqS8eJTjA1TFGpxJGT2ZfSn7z19-9CAhVShiWrrpE6vJeC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVpenC4SXrL3VAsb8lBzDl87YUtDaxOPhJgx7P4SmFPuL9ZOf36w5Durle_6IeJt-GuxzUHkjCCOHS6eSGVXYuAkz-c_Tuf0abbxJnYApAUSfS90yalDYW2K7SHbEkQlO9FopFmtb7a0iB5muNcN1IfQ1H9yJVc-OlpTyQXvFURu2V&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgAEMXTd_ZmaGopOL7guXYiU4QX713iKR9qBaeL3nUmOI76o8La-IaRoh4kJ3zsxZNwm42KVwLqhUkDcVgs4CM4eL5piShnYn9niMRvxcNZdyzCRw772Ci1D4diG6k5H6X52Ir-nF2g-sGUiiH5oCcDAzglhvnX74NHD1QyGeb76QQC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFieMZN8R5QUvXsfTjNsJqwjT6SUqHxv55kwAQnD1Ui0YwCjXe5c8e6FjkGBFstecnkeYH_zlk_Yhfz7YBgA7rjmRxI0vYA1EjvcyOd-TgYoR_wtiZu5eNG4S6FrHqD347HvytoLjA8_6KXz_wsP7riaGusnrvj4pLvQ8o4dy2LORZ8i&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFirn_OtkAHS-Rd1Ifve38UabN7zxZg-s0y0PQGuGOjIn7FNOSgdzxoAojq2jptdTZVv3iq4qaEWaBNN30VP5fH26l2dZUNcyFJ0_ierzqo9lBQGuJqsTO_X--AGuWRzbp9YrLXPsqWrk66FtTG65qHG_WxQ2r2yNeqpbwKnWv-SkhLz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdeOliAD-vrbCWSqnxsRuvndPJVSNbgp2vEGF-N97EgM3Lk1TNa7H2ykHUDMP76XcJuD5EhKV6qsQrBucyaULlimsL3bMj3IYGzdYJi7gmOOFrE5JHmWDyv9rMe3vrRwr7UsdVSpfY3CIE7Ye_2TiTs2hEM5QeaVqlM-EUIDxJrcgF&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Sustainable%20Food%20Center"
    },
    {
      "name": "PREP ATX Commercial Kitchens",
      "address": "1300 E Anderson Ln Building D, Austin, TX 78752, USA",
      "opening_hours": "Monday: Open 24 hours, Tuesday: Open 24 hours, Wednesday: Open 24 hours, Thursday: Open 24 hours, Friday: Open 24 hours, Saturday: Open 24 hours, Sunday: Open 24 hours",
      "website": "https://www.prepatx.com/",
      "rating": 4.0,
      "num_of_ratings": 20,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhW1Zjxh2lG_j_VlZwzubH0Usr7GMTd5h6cz3zWVTrw4TWHHWJgJcFiDSuTTZp6CPxUpP4AvD7dP3K432Zm9B5ADqjIsoY-mn1ddW0yU-6e4eL4SnrLMpCcmB0GGGwU6vqeif3qUwDiTdFrXS05gbQF5NA59xqZPYKSkOEm1ntbNwqo&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg4tsAZQfXJIBF0LvDgL4DqOUdQQy1llOKH7Y6APxBVI5GcJk0KLYULDJPGUcImlZbw8PkVasfsZx6TWLJs65iV0W7_RUSmnZ-lOwqgPo7v9hxZUiKg91Yd5RFaCWfHRAQt6DoyrI4RBv-2qq76iHQhwsVZ2xFQmN4UpfV6pC5gNUXY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh-ZxE0YqqIVAV9oHuewTrHGnNgoM6mUZWqy_uoOdYsqsGUJGgVFNSyH4lCGR06x0y9yVrroHSceW--L88kKsLKMEd9uIhi1ul8pCl5e5akxef1CwYmiojbNbBJhwpeDmB5n_HMm6ESghSfun3hZ_Vl6JLwGXI5Yolem0qX4qjyP98E&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgF8cNehFYce2LoWFiBQjPXdTFcLi6WgCDmdgEpvHdhraov5CXvxwz4HOM7F-eK4P0zh8msV5F9sFG6CmWA9P7329uMcNQLYoCmC6hl8ISeZB7UgGtPJuuZXs0w3zafBfCDUaNzIBrhyj2_NT3ICSoFyVbArgujKUS-sekErxm2udLU&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgxm_bnbKc7ArIyqig5SpCVwTiVtmU26dwGY5VzJDcdCetpgl2cqIm0SxlUmC95WQRN44nxw5nMfHvmxLu1VpFF9MUgR7XHKQUow3C-4ZashG2TMChaMsLcFau8_qF1fIW2Tq26a9Uafffnn42mqVHSw5w509kzLu5R-oixMkl5jCuL&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgrczwdP9edpwC0llAByEdenGsJvKqmUEUyrdF1wucdJJ7Sc6YdNBaeLo7rwfQjAISPKbeACbZoacfS9202ZyMolI5VVlc-Jtd9EZ9-curICSZV7LepGKXsdTdcRDFmrTT_GQjCjHH26fESvvFyV3BEybZ_lwSTA42INUyHGeEW8Kbc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiBPwQotuo7zroL3o8KwVbuSYIpY8zcx41PeAB3Lm8aQ6QI6edcbTFEJvpW-BCnEdyuHxDtSDcX7_em_40dwD5QwxWWn4E5mdGq9eN_4HxAJWzRJ47qrKX_DzAk-_-qBp_8jVkj-cLju2GYSj2-iSQdYYEb_rJgGqXhqmwNN3oqpPg0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFipIY7p4-3dZXXcPdWrF_sVw3xKhNoxVPgqcq2ozCU7y_qp-HlaNrBMg6DkBm-4WrADnMrwNuUQtkqh9G6tLZym-5O__nJP2xWCvlIlQE3C8b38eiJs1And33e3Cz1mxlcTNQtys9uFHk62YWM-wc0JJiEoG398F8G1tcFlL05G7x3X&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjvTSglyfZ9BggiiWCDY7NrD5DRS1GOO_DHUl8BheHM8j9x3-DDGgvSHthTV_caCOgiVNZ-Slyjk1y9sz0sqZyruwhIWYd3lRbtnG1JuQd_sW6XQATeB_2CHgmilzJi6GGKQwtFi3ZK0v7O9QIEj7ax7-jaZHfGhXiep4-Jk62bJWhX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgobX-A1dni0yHWcdUDU41InBQ1QkR9m6dHSfH8OFY-DAnipijosJ0t9BHY3yLImlCOMmNTqiNfDPfSOHjmgOa0DGXOOjh2qS2zU6G0z2uIcn20L9yY5Car_J9wzDzK58qZa2QMzGGyOOcjZpiH5Iprvv__chcOoD7_Ldh_kcnqQuvV&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=PREP%20ATX%20Commercial%20Kitchens"
    },
    {
      "name": "Community First! Village",
      "address": "9301 Hog Eye Rd Suite 950, Austin, TX 78724, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://mlf.org/community-first/",
      "rating": 4.7,
      "num_of_ratings": 575,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiJ3Bl5wOsZ0aDLo5rPM49-WTd9N22XOQmXazHsVfwGRpAgDUzDkD04udQgPFSs6OvkF323bJ2Ez6uiM3ig_4B64ipV5OKr18rnbLNuZfvpDBZ7m1dfXM-_mdRfpjG8dNRai28KdpVXru2vLShAWf4lBYgclmHIv6oz0LU3QLHEzRUe&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVg3nuIowaAfATSq_b6ODgFi_Jdvl2irolSdV1TOHeOb1xjUbbGvO0ILXfcgKLFE4Q-HfyYcHneB2lEIqbcbfUb6DqexCGAGagMwVyaE3ilr9AiNVbuX9K9x-hTyo9s0RP_E_VoUb5kO4LJ9Pd8jlKjgUk7GkRa3AyDl7vhBg97HVW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjUrFxPxsIF7a_pJFYZG7Q7eTNeMRs3ZcAg5uyBLgKQZIZSjp26yXRC_RpYMrzuV16dqI7W2PTurIJ4BI11WojXXfFK9BbVrTUUFZsGKR_ugGigCd1XlQGKJAmlT5lX246rMODlhTAR3fpgmQJHI9skBRe0d54Sl5n3KxrEhKa8mp63&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgJXmtFqadepLmORUBtjCkOol97DhnBnNx11dq-1WkEnCyPVP1r5J8Fj8I17a-0g_UBo6rHX8wGRR1IV-6f6aBsLv5rV2430zmcGzbbcz4gBqpC1gSKbD0ZitXPEJUpw9R2nLmJBZCnE6tSqIvWtfToU9lAaO8j_b4_5WbE_W3OHK_F&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgm6MY67IKwztnINNsUdYwis4PREFROxfeQKcNjrDK4VHeKOFaPJEikZnQ1h5UdeCe1Jc88tLawEFqguWZA38hfGBmWG6i5c2C_NnfLhg_ppYEHd3vBnv1uQV4wKNi1pfoFJjDDes-OZzOVwLftpc3d5l8ZYjFSdaCTPkain9FyC96e&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgeQNtPsDQpFcN2vAqyx7qfsEz6havEOwDlVU2hw6ReEylwiFTnpY0b6w7KbyvCcEYf1rleMlIQo-QxScaxjMEfyFjRZAnzBCmSsGhPvsEY_e5rc10vsZ-AiFi3C7LEwVG2dLhoDxAFrx-RIVCs_iV6AOkAsn5uNWE1WZczPOSdYNea&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgUf_tRFw83YZBbVnD8Whed3yET3QKipZgbEzSHAaoRwIE4WgbHwFnu6bygKqykscYShJWjQ82dTEsP9cZiauyogk9qTfa1sIu-FAPRttC_YSvEx_KV4j0cAQ91WC7WVRbNL2NRZ1MZZXmUj2aWkJHOl9i5iQhIM6af0gtwjoutbpkI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg51cdFx6yhOteioc87s28ODqgrnA0gJdzpJ38sXOLyDk_WS33Un70MXz24pUyqjJeuw8DFWv5DeDmnsLhH__dB0UPQmF58etdeYEOEI_LtsFoqtPPBKBug-iWnbf6mIlri1UqsPoTSYXDPMenUR6MMTMIG_PIL-FObd_YXiu3XlzF3&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgzvYsML_7AApUGHmG3eynjZhdIuePHrk5s0V9uDPZAPDQvlCC3YvVJuZhm-5ynL8llfPCIpRxzHVe55NfbC2Q3Y57h71qvgeYjUlu5ez83OCBjafwf1qwQtEPXQUPQt2lwEiIcaKiSmczTLp4ZOWJhiBLuTjwMbxjAXULhWoCDb01Z&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjXv9xDG3cY_U74mgnB8p0_wm5nYtW7eqqdpcoPYbDQWfnUhqbZkUaeXE1xO4icRBaZ23hj3UW7UJH1H1DNGD0vmcv9MW3UzKxdi8Xih22h_VWrhEYYjf5u-TrEOuy8Sttcrx68-aDMr6cwSJdqu263B54qEQkD8bGLjNWeEqqSeDec&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Community%20First%21%20Village"
    },
    {
      "name": "GhostLine Kitchens",
      "address": "3400 Comsouth Dr, Austin, TX 78744, USA",
      "opening_hours": "Monday: Open 24 hours, Tuesday: Open 24 hours, Wednesday: Open 24 hours, Thursday: Open 24 hours, Friday: Open 24 hours, Saturday: Open 24 hours, Sunday: Open 24 hours",
      "website": "https://ghostline.kitchen/",
      "rating": 3.4,
      "num_of_ratings": 31,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjX49Wsy_jEopJdQGYMXV-lvEJ-7SLvun0zGC5esg5ZLfJT6q6OOkQPJvBxryMeXzulL7vRmSIq0U3NVQV-ICzuT_XaatpknCoXfzzrzYNe2aht-aQUUqYzfdRdHQc6P6Y_hLmpwae8JUQNRqsivrm9VFCerqL7R8LG2gFNeoOGOhoF&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgaZJwBrhToY90I6CnZeg8EyZdWHvL_If_ne8foiseLFJaFvkyxsGx1vpepS6t8qo5dLfmU76VD6XUEz5BjKlDq-8WLXu-o2tjP16lip0DT7cduEGH7VsQLNfxrgD43x9d4aF7DT_PtKVC3g2CckhKkjwNZZbJQ8sqc7q1f8S8deB6B&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFinZ5CVED6by2-LE-otMtu7DjLXWqbfOHomhWL6GS0CbJ2KRxXaEdvHKVyaekiS4pLTUk4C08QeHNGdoMhL-FJe3i99WPk4vsp8Vjie3cAff82dR7wBapcKfZ7BBSNqfPzlj_NaWlv297UlzCWGKO5PVL4JnY4hv-ZzMOaTJESxNMeQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiwyQQFO4LphOzNbB3KlZ5BnT_EGJDKp_7UsJQovD14muCMkKQEivd2GiKfmSGWz0ZIczxtN9klK_WkckOJjpQ89Rq0ZSwrib9lsVmlcM4Afg6ArStMlckJPFB3v5fpPtm3HcU7zrw2kvQk-NzPlOAUe0c03gx2Nf7W-Z1fAEHJHX2o&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiOET9fV1svpftGsVBDIZEMSGIDDJUdhapKkzI5S-jPQ9fikDfvsys3LaYo8CeZ8pxYBd1WxPOCX_gLjUh4NKseHB5O61x_tOmJhB_WvOblRfdTVe17ZvaIYMvZke19gM0GsncA47-lGQ3vSVez5jEW8SzVoBhtr4BQTVP2dEWRBbHL&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiZnjgEe-ajACb2rF1Qrg77HdZHYlvLArtrfbPvSCP2c9dD0v0yYN8YXn8v9PSylpHOjgYYoWHhdF9sjT2DchzSQIwz6P1H0Gl_FY94RNzX2jhr6F163vkLR50R1kV1nn6ogB4MBwNDk6BZsepVLh6Xkqt-5xntLKBnB9l6jhU6FrI1&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi2pB4SJA3XNmKqGIt8eumeva_OlSXQUxUzn2o4d3sOTfN2gg_w_BJYOjSiCo-6tx2VittMmHpjePO9FTcgXlqVQtQecWdEmmLW7-DL8eVn7XCpzlyWYbouI0NOoORHGWa_eis6CwADUSVvTvdRCRYlGU8N4J0B5MKoJ_TQ8LfcJlcl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhI6gA9ZzJWwTVxQwYtNdU5xLmwJDlznKNx2g2HN7_gMRZKRPqdQTOHGrP0NYBciQALbbEKsS6GgI3Xh1tM2uAHv5ChqUv4bAbeR1DlMZn79kdJ2EEfyLJTYUP1Yfis8TLl_WnHMEoRin3q85ykhRuq-CtWtpYRsJu8NdoRCSk1yfl2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjgMQ3pv0dUuciDdt8PqoJzRHHW3-0XgHul3yVJbbIdSgk1iyDhj8kQrpRJN8vgFwuw9FyPE2w_MmfQ_L6JOXqS8W9xILb7zu2xaSNKkDfEQ31Hd4Ae6NoVWels1PoQeGrgxrZGkk2GrJ7uHxowB4ztJWkwCrZ5Jy6wrUSE6UIAYHy6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhsqf6QLFn6JeuXpHXSexltHvcdvyu0oPtSoO9OAd2sITK0s4QvAft13RsNj5os5CczxvY1m6CR6sA5kAmpPaHM0Mm5rRVih1wQkUR1nsou7tvXGBhkJRkTX3w_Cy5OuhiNMlf2az0IujmViBKjlBFuVDGOsIEnBoBNYtZ16lKtorMf&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=GhostLine%20Kitchens"
    },
    {
      "name": "Caritas of Austin North",
      "address": "9027 Northgate Blvd, Austin, TX 78758, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.caritasofaustin.org/",
      "rating": 4.4,
      "num_of_ratings": 38,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3702218,-97.7164238&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Caritas%20of%20Austin%20North"
    },
    {
      "name": "Communities In Schools of Central Texas",
      "address": "3000 S I-35 Frontage Rd Suite 200, Austin, TX 78704, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://ciscentraltexas.org/",
      "rating": 4.3,
      "num_of_ratings": 18,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgaQ5lCA-7gavkE6HvYj2h5YWvStDNj0tyNwfbfG95pTNVidSyPLjFzPjtrrZwjuRvazy6GLeAUGAeyhHlfq1J-RUdllzdqWSKfBad3FUVX5RvnFsd7pqXkeEXzbry0itV9cUH65jEZo030bO0VJB1KD9QaTyVDifpaHt0oZGNDLv1J&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhWL5fFof8s86t66ZANpPPR_xTa8mJWMZVEFOcpUZrBYIm44wz4q7bAPjUyEsnT4Sa9Mxy5EIdMJuyskDpwwjdem6GYdQ4diLTRDd7-Prnc32prLHM9ppN2ahBvXnp9wRMAxCH-p_CxFCnPOHwUouSk_83lnXTl9I6Ex4wesGYUuqXA&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhfla6wF7RmtdNMuTEqgGPQwpuu8wQRAwoCTP6wo6lFJVYqWUw5ccYrGbwSWP4R-hcucDEB2GVri9vJDCImfO6LrRAjJ_iIsgZ82kJe2z8Qu1rmgzBsCeaC8Q9vMMcth_EhbWDAOHhxk_wTgp_dXO0VvF1QueQzjURakA-b9wXpLruk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiwbNzgJ03kO018_NRNLbRpfPa8cgrv2GM4S3RUeYn_r9daq7OvYEYMv5xf2hdvXqyDaaJX4ipinqgm2CXWWQFdFavA2Bghibevr0bo8pgguWj-1IOqmAX31Ub24ECCqJoV3j_BxIeWa3cz-Ph1Nl63GRtBlfkhZFVddsS6LUqoVmuY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhlZX2mfapv8hsSAyjueU0yz-IMVWt6eLvpIvBJY0B6Fy8BBkbcLm_oVh7Yvf05ALXNu01XPT-J5MfaxD3_1Doodf7wU-thia8TwpDd-sxKQHO3V0_ZmdzWAK-2gT6IMPNe_YhO_DJzopBBLEzGAx5HRmESrACU1H86DFh9d_kCw0-B&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Communities%20In%20Schools%20of%20Central%20Texas"
    },
    {
      "name": "Generation SERVE",
      "address": "5555 N Lamar Blvd c123, Austin, TX 78751, USA",
      "opening_hours": "N, /, A",
      "website": "http://generationserve.org/",
      "rating": 5.0,
      "num_of_ratings": 3,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/streetview?location=30.3238811,-97.72544429999999&key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&size=200x200&fov=90"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Generation%20SERVE"
    },
    {
      "name": "Community Impact Center",
      "address": "5330 Bluffstone Ln, Austin, TX 78759, USA",
      "opening_hours": "Monday: 8:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 8:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 8:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 8:30\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 8:30\u202fAM\u2009\u2013\u20093:30\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.jlaustin.org/support/capital-campaign/",
      "rating": 4.7,
      "num_of_ratings": 6,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj1jDRTeU7xU2yUYa-B7F6_QQOl7ixHZuEf8TnqilI0Kfl33Ud7UIX63oeY07uhoXkKDnyWF_t3Lb1nkrkG5HN5lkS7t9br0dSkVOU-ZVWKP9vlQ37XU2avSDZMq1ojYk43_maVaTUiVtF17QVQDBrDisMeCnL0O-zB5J_kPAicTYll&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg2gQnSmvXBXb6pHWnVG2GFSBzNybGsIYYKu23C5OSrNt2uhfaTfjK66u2qFb3jV5QRhfrGRz_KK_4M80ek1cRUlND299Xetg4-o3UYWfXb7Tf3fUDDbm1vGI-qhn-8wjbzDxEzjPjmvdHTgxTKdcJSW71D9qwOpumJtNEEUbFdDnhP&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiov7h9j5aWmQXAtrZnyDFMAh_g9OFEag7UTpZtM5eIaIfJFjP4Aa9kp2j-38l3ZGE2a5agD7u5ekL3k3DaKkgGmydzFfMBTIthGLYy0PIXhY-a_29_UCoYMoke0PcCuCRAPSj6cX67gPf7Z1cixWtYD3unuCYV1if7QfeR1g0YUF_l&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhq8GMMmKPysMhw1MepzVPk4meQcdnZYC3zI1jqIPK3x9BwRwMnd3nACFQjQ59Dv_nxb9IcN7qrrxyiX9c41XpN3rnN934VDbDBaG4kp5QEJgcmGJ9tZeASVJAlDFdYoQoHM08AFjwuVWBoVfZiSvv2xxBw5eYZ_V7FBhImh71Hx_91&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhEg7b4ym7VOt5djQzwQNi0gS3lrLeRQcpYwecZ4vLE69fN4s5xZ41alf4gRGysc-rvLoIIwBNQdiM7ta20Bl-c3EMbxubuvXXeijNgNBhUM_SDtRH1cQ2PbeUqHCDNd6KNN8hZuzLqZ6035jKQEkjG92rgJLFGbDLrbHK79FEesuN2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhMyRbZ6eiPe2X_mIHH9hhoFxZhcQJcedQdyawkeqDFvE7ZXGFK4j3mJWO0CmHB5zTaS4rhPkq3gLBHoRmUTy7voC2oe6rzqjShBgK5NEAG2LYHN-muYqmA0WWeD-SqRCxsKICH0R0uvsr8EhpPS3cxnd2ugB6KzGEsjodH4rtBd8mX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi7FZgxOpwi6BQ7zEEdd-A6uk6R2ADkSk2LGsS8WYKodSPZsKTpCmbZxhAutwnDh1NeapSJlxYs7uBO47qPBjokYc7D6ps_4M4aqLi0oO7Qvp_GEW1ntCvz9Gmu4wPawfzhexzlus6TXgmc7muxhVqT2uZwP7ntYvbCzw5WxHZOtEzB&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdvYG6AOb37TR59jRccVsJ8sCI3o7mhP6vHsg3Iw6AeFBtfkeZxigjILCRiqwMJExpvcKYwxsvwDzP0WWT2qRDYYGrIscDO_ixjCZ-TNQb9pO8Q57qY1OwZukJLgdwabP5etZkAFh1eHyb5i5G5OoYUX-VJAcybE0pB0LRuADXAvM4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh5Z6RgqAuiThDwkEkzZ0tEEQx7Hc6i2RLX0tNEI4bMXgHNRiE_XCrIrts66BeVjALvFzi88sdHuHkfKcKP4OcqzGexOssfln6Jc1sMbO8cNmI-e029zYqI62qlMkvHLv2X0Gb7UCPqD_KzLqXhKBLd0CUWy_t2BRU4ejkrDbKJD-fW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhru8Ve6h2Vxy_NiyG_y3_uAPnIolkeBy_3PuC7XPnMS8aWZy9Sf0f_wyNyorntaaBN_D5cW2SNSfgBaThxv_N166SyzupJVCbrrBAjkPc4-HZEHbkSHEWORL0LS1krQd1tzYOOEbEerc-hfbn2t6ncEaiLdgbSj2Fkx8y5a50G99Rw&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Community%20Impact%20Center"
    },
    {
      "name": "Juliet Italian Kitchen",
      "address": "1500 Barton Springs Rd, Austin, TX 78704, USA",
      "opening_hours": "Monday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 11:30\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "https://www.juliet-austin.com/",
      "rating": 4.3,
      "num_of_ratings": 1800,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFje2R5qy3tpsLJNKxH_TLEQdSy5hIuor0VkqebPyc1dcVhF0bBNiFqjmuHPBf29-HK2fWY2CyPk1G_hr5heJllHTHs4-14aq_x2SPtaicmhYrXd19SsIrpgimKwXm3GC2wXVDl8nsBAn8IDVKoF2DtiKASPjo2zRIuALVCC_88qSiwO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj84sv6PXKsPKqc8_sh9mDYe0Xzf8jtmulrRF0RmaV9-lmPNSlB4yscDM18CFFLQh7ivEQGu4FaWtk4T3uuoPraDlT3GFG3IuSPnrjh_Z5sk6nRLqU747BjcQw8wLB1UlwgjqMJa9huMT0AKsZPBG4q6Gf2FXYrNpEQLZLSZP6zTKpj&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgB0o_JTyPe_M90kcDhLGMyV5jXK9gx_7aeOXsupy3lqk_HipKfB8gqRHLaCIsf59_46iQ2iNfm_icrYjGll_7-kdLi8FWPyG_vuHAbUXzvNKtUDEeokSKPeqaaw1Kgr4FZj1iNf3D4bUoi_fvweawF7_W28bgd8kyqOhTWPB0obJes&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgyGQnNKB3Gyd-G1sSLr8g2gINcmpXMKlWV9uyBIS2EVI8zUW62Fs5lHtpLYHagmeeRBMMTO_H7MYs05Mq_yMjknSGp2O9Wn_-hxJGPxrNTjUm7p4m21KLjJEVfYUeFWQtm0frwW067eZOs_lbICii0mx8KuCYQz8xi_wtcwk4B8Ikz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhw01t6COg_xwyplfHTYs0XqMO9_0ny5cdvnODMtXeo1oY7KNFNeD_Dqrk9L_9Y_dRkFleDqiIOZe5GBkUAvhtV8YjeQaSoiHWjZFZ4dGijE3H67AHIhEXSEF3krYdABh-1u2ZBaEWsEKl_F8194pJXy1qJeTJK6q1yp15Ufr80a38w&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiOxKYP52G3KwFg63pIHDPnIRFawvGB9PoBvISJt7UNnl0-giPZCaBCVFAQ7jdJtz9NNPJpCnojzXv90Sd0EUECi0E0MqDBNDDwX69BclfJ3ns1KArY1GsP5YRZ0aOh_t_P8GKzVN2tuouBiTnw3gP3xGICqOIcb1mCE8afB2sAqyDa&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFipQ9bLVodLFn3yzvfiOMYoq1utntBWsxHstB2h1OL0P_Q6gT9uziids58DRxMNNu5hvSlMri2OZfMlfl80JtYySpYSVwbLgXd0uQMTyMiN7HBt3b6N0QUUZ3VaiTudq2CMp85kWy7cpD9mLc0xpFxARZCFR4mVF_vlZxVk7RE3K6vi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgYAai1HmZL-YEOApcui9vRbj-plTZt0F_RJHeW3KMTV60Qc-SK62BdMUfZsCDYJ5y0liTkpQwBaTq8a9tMkGjg6Qo6UFwulsCMAoP0e_1eB5c_UWvaY2YdxCwqG-x5nfAjJNrYIVV-3wxzpU9XRM2LGsSuV9hcPuN6D_66W2FKK8OF&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiBtF2_1M288C7S4av1eD5EBM2_oI20ZpmYNKWssmRb4hzrtjferEscl5HabDJ9IId_zBVJxRR8-jyIa5ID10xIpbAivAzehh3NBo0JH-L4Nbf9QXAHJ5yFEBdoYcdaosrfrTnb-qAJOl0CQqS0k_M3e3_CpqTM3WBfwPXMiz3maJrt&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjYW1IlJTGmDFFvghii7_kXNWg0z7D21KDFfoy8nnt43xgSiOPiPSyahyMw7yvnjR80hNj-Brqxb0Q6ClB85Io2dhnbpa72ae7QBLPchYpsgZKMBz83scRa8XYr20YQLJGFFg2T1109smetvKyKtnjgMeEoiRXFitmUsx5nve5Lxhnp&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Juliet%20Italian%20Kitchen"
    },
    {
      "name": "Casa de Luz Village",
      "address": "1701 Toomey Rd, Austin, TX 78704, USA",
      "opening_hours": "Monday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Tuesday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Wednesday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Thursday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Friday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Saturday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM, Sunday: 7:00\u202fAM\u2009\u2013\u20098:30\u202fPM",
      "website": "https://www.casadeluz.org/",
      "rating": 4.7,
      "num_of_ratings": 572,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiGdVsMJdZHL5hq5rOivFuzSibnMX0H3DWkBCnOwCoszqf9GJjGJMGlFNuXlfpYPQd76KAT3M1RF3FzO90jhnFcs7AdCweo7oku7g_Kvum95NSoA_wTCiHrb8mNDn2vDshvwfcVyoVym2t0eHA3Z46DzH1qAmKDshZSKprKRDf-EGLp&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgkXNMAZeDvC1Z4JilsCramMxIk01wSfPq5A_XnyobhvypASsx03Z_SMqTqq6NmC04LGOHpZDMEbxoHM8U3fBB4HIhSIOLVkPibysYfTib7wNSksMPDuZ0rh3EXlgzxT162iW6e4fM0u-75OXKZqY5lXhCr-nHHZiBuEtAEonKIxMgs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjzAMtFZIZIEgyyMmdZkx0m0DeAQew1-F1f9CdgzKA6abfljcS2PSSURkihoT2tMQlRsf7k2U13LUvENKklZ_Kmn9gqrgSxexh4sf_kZcmRtUPBQdV3MgJS63ZWJmDN1wd22uX_OSE4dEY85h8_zy5CDQjioPghR19VY-Tr6GdMEzXY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgJGi8dSJtgxwqh-8mL2DymxbFiW4halAxozg3AVS4TVAKsthgT_jVL9_QjIyItr7TNOJXczawcPs9gyzeCIDunEH_bNF9l6U0ArbL1g5q6fQjhZ3It9ME_memolLQnAlxGpzk5VqWNC-AY7tolUZ2OSzVQGEszE-neCaw7xbMYneMB&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh7Wgq5ACsVEnXAIUXrQrCVbdp52Zp2qY6yCrPZ8lLHTwRpzOYOnFsi8zjwDYE-D_KYMcAXhS3p0YnGR62hEPto197g4PEOgYxAMN8JjeKDZOl8wzY2MKEvClViobfCPidbxTdJI5ZTQ3VM33gUF1lQVj-VxtkezgrVXFohv5IwUbn-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFicufrPkX-zz-wGc5ArYWLIzuAaU5zXFlBMkmxMt9UlX-RLxzY-X7o6XHZJQ6Q12cj1wjJkPs3z-tYI8w8COZj347tTVa_dKrW6Wa-dbxj42tMflAM2n-1WOG8N056urZ7Zg8ZGQEoH3KN14LH5OC-bYthUH9Bd2RQtyNgBc5wLc4VK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgh7fxhjXi2FDjkj_YDWFPZzc0sYjy0gWSC6dJrdEP_-aWhrVp3rjqbPn66ShZ4BC4tffK6xAdYobokvi8kyt-TtVtG8oRyJFIwmIMIWGN-kr5sc7_MKzBJzEGv-QhcYYkkSPyHiCeUisyk26v_pPa6Ot9xuShfp6bOKJERv7E-I0z-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgOUqwOowzDaFIEaI4LO88L5jqLE-1VaD5QnOYpmr5B08dbEYbMRxLP3sJ29rYKJwDJdTRKVgrmlAmESCgdnTIJf3ZqgkuNTpmApQll9aLiwyiXOA9nxCsOjmlaXTj0hIljTahPecZdTXwyG_HqQu6Bj_PqnkEoAbOtxUX3teqhg_KP&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhabdFBRLHbW4P3Ar_QdgdLvXimG-_t72VvHDqpUjPOgC12mAp0Ywn3ZaN8o6d8GxKK-jgbfeCO6UO5qPT-PQCDnpOGT9344_EeLKcrKvigIsVaCfPHW2SCFA0rbtFSGCUYOTHOYjDmZzYTAL06s7BIYFXmnN59P2SVY354zymhJcOf&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjRillleP9q1JCg43G2VKR3gu1Upm3Tt-YzSshxfXIzCZuHQZKjKzcnB8IZssPrKHfb6tM1y_I1V_d7i3Aogi-pTdeN-yWQYWaJwfbHuKZq2SOs40IWNn2mBc_WKHHwPBbq1u-p_VtkbSVwnDqaev25E-xJ6qIJKdM4FVVt2QW4mOwy&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Casa%20de%20Luz%20Village"
    },
    {
      "name": "Krab Kingz Seafood Austin",
      "address": "1108 E 12th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Saturday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM, Sunday: 11:30\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "https://www.krabkingzatx.com/",
      "rating": 4.1,
      "num_of_ratings": 107,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgkablGzk-WjIQCqEaKgpHM1Kty33BgjLjkyjwVwhutECoJtCHmnt2MhMzKDO500iDNLeSZEPxgpEjV5lJ-1JnT_v1mvYFanFdfPcLwXWGyRwQzOhmBK6Li66ZHOPlc9LL6bKomh89aWPzFez54LvuEYyRas6GhImR016p7Ryv5Rfu0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjOqwUTVG2FITIZIeUGIke0cBtWp5CENb23yszzWdhSpGqLgNtITPzLfyA8-NKbH6-9uBf_83-0WhlQcs062MPbtY-n2XioFFwTMbZWag7lpleHG-w4xsayWELkFP3qvLat4hLZHdNopScTNir9J75ar93GFLKFIJfUtqY1q_kTxf1C&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj5DNmTjabxHaFWLi5jFCxFiL7L1JiIb8BcEFI-a1myU15Lr6a1JbN98ON6Q5BNfrj5KCooG_dkHVNGukKvIuw3Do9AqRbjWeoYitap9hgHVNgJtItW2iwZ8pXpa9o1lv7vIJ0T_f08feljMyAuaiVHqKwCY-WVYz8iK5Uj7GpHfIYX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFghfdCjhT4Zde7_7N5m4n8FgRe99iJDo4fP86he_Mv0jKmzW2w_qTE_wLTj00uOwKJm7wTU0yPk8e6CYBv6c9a9o203i4XeBB_vHT08h_PbuTE8Um7gJ1DvfMbXgrt9OcX39dXfmm84krLAGfJ95VkFaejHx5ji7mG-dtmqB4yYPoaV&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgW-mJ1X8hMMf0B4smuvfrYu9uvNNIZfB23DZMHEc7_gWfJUJ-Ypj1X_F3kgYr5E7EfyLehDxpEF6v8q1MBwF3ZMGEiALUC5oAINidclPAZEyDtDAKkRHx9_UeiNFZxhOl1auZU-BMry8d91626h2q7Z-88_i8HjGF-Dg9QYUYNC1eX&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg3gq8F2Q-2EuDKDuKe4jkReLaYeD0lWf5r_XxuN75Z0wkPfGGbDrORBRkm_10NWojW4F-sq0DT0SAwnpEZGLNHJQ5ldrAwmxwQiPiQeU6HFrqALF4uiSBB1HVDDKKIZjCjIY08O86ZLqPH6Cfm1qBT_IicqMucMFbkz-kG05azcGAT&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjt1M4dlRd3s4S8oECCOwOovlH_gYSWjuS4_4KPiq0Q1Fxp3SIP7J3VsKOmMg235zeJdxxMu69UFgdRkcrIJES1WldS_Baoo7TMnB-bpkt_juofTsaRC_duxvf824voiWja2pwBHYVzsRLgcnSjQE8v7pEgBGAb7mzmCJmKpWDeKjUg&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi-VrQ5-eiH0pfpTpmmihENAq-1hFy_B7Wmgu2xrwIgUzQCaYJMhfvtjzHxk1ZUD1Ox7Jl5aoNURDQPtzBtd067I9HoolBVG1no74N2IlTfJelVKQzUBzztuyneGiaNb4Wf38Dv61RuCAzQQdmUIGtfd_rDgmAtKB52fbPPZpwqkDlm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhBMKC8kT0AWZkQWPb3ht_mpkNVzxKh2qOLRT6jVwIkd0BlYOAvCY8eR2QbIhS2Vv8SC-PUIyl_x9SjTugIGWY2R5KWd5y_Nu5m1GkVP6f5bpIKib-3oGJqryKTgKVD-gHSJ56X5SUhzPMFOnIAnvVC4jhxPKaQhtpNHCSk8Sw4akxT&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiBEEnUc7u3l-mRHX1vbcZs1FKsaCYXlDYAd7ncpKgBRkU91zXxpikrYl_GOkVX64-rFDqbw3ArlioYShL-_yd2-ZbRo5RDihG0ElHpDdA1lvftTefsLjpdYlFuhNJQftjoePtHDJKJU3YhsopHavQWfxmdrVyblO9i0M5qKe-6QfGY&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Krab%20Kingz%20Seafood%20Austin"
    },
    {
      "name": "MezzeMe",
      "address": "The Triangle, 4700 W Guadalupe St Suite#9, Austin, TX 78751, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://www.mezzeme.com/?y_source=1_OTMxODc4My03MTUtbG9jYXRpb24ud2Vic2l0ZQ%3D%3D",
      "rating": 4.6,
      "num_of_ratings": 1357,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFglD-CNGw16n6mxENV-1iHCkcY8P97f7_QgVYUodBE_ELO6V1smQr-LZdI1l6QOQBDDl9DZZ2fYHCtRmN6RVd6Q85-xuNhAT84kjMQrXSYjjRoszoL-7DTxQgfGcU-7W0NSoOnT8U69sR8QuHDgGjgwSOdRttAGiA7VbFDW3pha9nvR&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj7xTw-KMtylAATRtOd5KFUjR9IL8I9hryToUDXABpm3oWdNXjxrXQx0mCWmibZt-ak1nShqwcacKxVhkFM-z2p0Jk3-4i7R1C2RgD0h-cWZEFHMMQJnQUGDUkigDrTnKObJlosgc35IMJsOSm3y81sg_AnrpB31QRUzrLWpThOe1Ee&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj9_0v47Nn37AYK1JgBV0f3acEUeUb5btAzI5ODBq0hdih6_kEV8H_0ZnFoaJnIe_XYQOzHN_9EWhaUfaUboO1bmLVMoslkZbvticEKiejTMlrX7wM0lxBcb3LARZNLGkAzwGnRllBXg2jyFlCTamlBeEX7dFeIxsX7YiilksOvJPIy&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjMjgr6QpTyQddve5JomcBCMGVO07mBgnpuAQrxUmobwZLDQub0XgTRl3PlD-duQpDqIH-mPGJ0ynfXX5pUFnLZwBgED4J27mtqx7kvkuNIVAd2dhcs1rj_36x10fAbMLIfbcUKHv0Nj8YzS3OwEesO6FJHXuavcE9-Pa3yYKcVQthW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFim4R-qy1icq2WrDI3MxypwuDBH2JpfocYwZ5vUWomvc2oWS8nWQIYodWf3qBIaVdRh2baZDw3UR4u-SXYrgW_3ivTwhA9_x9IWdZQZQ0woLQkEE5Ys17VQ1j8K5NDKCvWWKS4xw7TQtZSwKSXqAexHdZdvDnzGcBILTFXyaQEJFZZ4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhuJe7EaVk2eEpCk2FEfsFLeqUkhj_ke-KKH11_Q1dOm6R2eywCgdinbBOoPoPw4GhjhFIFS54giFOZLr2FfYnIQhgkCNX1CUHsaYoUJ8xpiXzxBzxppwAwT3ecozNpm8yDC7BcurpCG1tf9jhlcReeus7Lx9FO-sU_sWD3leZYZSGY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhkletLlGxbcu5Z339-nEYp85vP0FVa_IeD-kb9FCTO_ZJldvyfLknFovry-jostHBv05VkA5SCRitXDtRt9Fwjf1aQqpJqeelYrPR8kGQt1PKcMBsOtqg0p_Jj8_cxvsUnRLKyqAmP55kpkU4DZFbxlO6vs2Jm8xnrdQdEaEpSSQBQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhk49Tl5AjUNh3liZ8RvErUUA_DLYAV_uxU6mCDHusZFt_f2eS_aUUjlpgm3g2qK_gC4bhSvDut1HpVmEL352W_-JqOTnQx9RI8DGMl0JCZKjt-_v4bSrGJp4URkbBLeeeMJQnxHEGfpn-02RQ42BmI_SkasrjhnBWG3akgi3G-XkpE&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjBJFVPLCklc0OjLCHlZNDPImjjzEKaETS9oGo9kX4YZ3lKF9daF7Q88aqmLp14YBvd-i2_U98WrpKeJwD3sNeMhcukvSyt0GO-BO9KEfqB7B85_0UBXhznMQNh1vZkrP9oxmudhdwp7cL44GFvGLuSJ2BDFG-9UBxnqDzoNd-7CMtc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFitQR2et0wDbo_CRV8to_ua3EPvyx64Z1Q92NZ61KscH8aE_UrfBFl_8cKzbYN26vkdpZHf8OMY5bG1g_gilTC3D6NfWYab33lx6TE3el-VjiMuOBZ484CDoQ1eS1cokqlS3Yxi_zCbHhi_fSS3-wOhcVbHkb1rtarQcpuXAF7ZOBHq&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=MezzeMe"
    },
    {
      "name": "Galaxy Cafe",
      "address": "1000 W Lynn St, Austin, TX 78703, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Saturday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Sunday: 8:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://www.galaxycafeaustin.com/",
      "rating": 4.4,
      "num_of_ratings": 886,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjwg1IM4X47RHaoMrxAeKbE7DHkbRAlEXG1HxF82AMDJIr1LiKCt7h4BVTFxZQD9EFdcwzoxpoZiRkyFeHgqo1t2L-EuytAkEKz-xEHGnH5E2etUI1L-WyIyz_DH9-XbxfJcC6XMfN6-g8EvvzSL7GykKBSoLkPFUepC5x9VC1Z58QZ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhE3qF2DZ51we9h6c9qGR8N7AJRfXiix9MQQSfffUHZI9JX8H__vhHS1Duw-gzftHl9YjSX6mpvO4UPT5T1NNrQADSaQfh18-kEMHzfBWwI3KCuSXjn7bwGfJBSSyH9Cn7FWdMVNRBlpR-dMc1Z7WfyMVnLCRbov3QQUuIZR2roefa_&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiv3YuLMp5deR0IMcZWBcNs4m3XzqQc-28aAXXY7FymKiQ1sNUE6ZxlhxrWaqX8xS5h1USRtSwqtDjX4C5xGAng2rtlqcSy8jEvgBl2ylb_3NMwSMhC-bX9UV3l_TQWkI0xH9Wvaz0haH5udopq2bsECDf8K58G1VZIr2cM777b9xQ6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgjSQcD-8d3S8cC-d2NZsBYBtdnI_4F_YM8ak5UEmq63H2IBacp0iZaaD6-ULOb3vphUHnCZLWb5uW13KL5L_TJ4BMVNdkuZ2UptE6NeRkGwhTRD8K1zUKyo5Iy_0mDdR6ShWgoxj4BudoO_HzeNwB0mjT9tZY6M8GPRyhVhLNAiY9g&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhA_3HZ3xkN8S4bsYYbeiPXyjhVhW9GxhEvihgfSL5JCEmqbA22n7IY1EaFl0V_zF6WwWK6o4VXqpF4RXNRov85SgGsFMrAsRRZT_sO-UkNgxN26aafIMN4wt2KyBlBG42XMGYqSIV5r4QvtC5o-ZNxZ6UaKt8HainbAtQlzvlBB-Mh&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjlr8Oss0ShH7ObS8qbC9ogVA3APUSOC6bIwGnMFCWbwyCtvMTBBqVm4Tky34T1pOsyyQu7NMktz9o8N6nkibftBnoRPcXzqupkiEkkInxKtPTAEElLGKugQ_PIhky52aaNA1ZVw4E69AaX7AYnirZ4hGISN_iU1IygLZ9_MehjwmcO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgNoQrRDFoJVCb-HhqpOD3Zd4ZGr1O0_p48lgcPW96IkQ-RvQEpeE-cPZc-QwK-VJuY-B7QySqsvVBU4rTc7Tjx46cnrFdmY0ujl29hbsDP_yyaeu5HJi9ncJtCyRn5O001OFpuvBJ1OAvTFxznJoxXFsesJfFaX0gYDCJ1VdD3DSEg&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiChnMq0bQfZb_yzgJgSZ7NlCGcgln0fbPico9fRxXnwMH5FTbXvGBk96q1eGpq4VTu6J2PuzftM2pUYwuX0Mu4JJ_plWkrzBuODFCdUiqp9mNFWK9Fuyp8VioPbkhPG6qw126MlntXFpEa1OZ5HggNseWhG7Z7t1qoPKkTXoXNczi3&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhwnOPV7DsfV_1KQYXQV_gDKUdxEk_w40_x_hNwusuyTZq0yTgeV2BqKTvFoPFUoibqBHwblH9pOHDJcjm5g9iHlDJdBj7WbD4Zbrthu0VmQ9llloXd3ldMDPFf-86lKoQMyzHckYOUpFSkVb3ORRerzXRIJB4KtWDrRGxNanaOD_oH&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg09SbpbeSTWjUgK5edTe6oF5jUzldDttvj1rlgcfsu45csgl6xD8yEL60lUOP_RckwBgEp57KDS7F0qiOfM4XBLg8idNERi1iBNERHxBYyh5cmGMADWaMnfcabI_oF7uRVGW08B9h67w71t6kNKB9WUfMXGAEoC9-0H8n6ASmhPlgy&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Galaxy%20Cafe"
    },
    {
      "name": "Sour Duck Market",
      "address": "1814 E Martin Luther King Jr Blvd, Austin, TX 78702, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 9:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://sourduckmarket.com/",
      "rating": 4.6,
      "num_of_ratings": 1555,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhOqkXmo-aUk92bH5iFOZVt0fdC8n2mHHwxZNA8RQ-MEdS6iBRLUIiT5ubklorJh-oEfM_lLagxlAuk4tgJoEJm7Q58gtIBclAmYPRXoA3A1_RH49709sfYmZaEQ5zoDYewxvqzi_wFhRvg97rJp6FanrCg1kJq7d3BNw6nEAkwKJBh&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi8_E3WFE5rTW9Zbh6Z_5S79SCNuHDMmFIl-7E2MLfiDERRFdaWcmpjQZ1BZDyYt9501YMHDcU_yvsw6MJVZ2afgZeeTWzM88hVjji1PYHc07zLkx44cqj0gaFCcTaivSoh2JCN23npkW_GGFAcOwBx7v1sVOSCZZ_3Gi6vShGFnyOC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhvqDN8psz_1_l3zfgUKLPU4ip9_xtWnfqsyX8vOdkHgue8FkCz9Y5U0o-ZmmZA0aR9p3YQrAJd2qFmahDLSUGdya67nAy7d2gj-Pc5uaA-TqiCd6cgVM7D3dKulrlfvZPNXB9PGP_i8VRVtGNTp8v2s29zwmhJzU6SVFnNicLSEpLu&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiz-dV_B31KwIZ3HnyDCvr8mICDfMx4esVMI-dz8-ZJ6Fi2zdxRaYteeaIuxmvhaPVwphj9VWhZO5G3kV6IX80O9abwDh7avZGf8CUffavZjnvXLFDb0WsnajmCY8ORF9iCvFitUWOTF0gr9o17GBJIAb9cotAVp28V75yFBIAJb3bJ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhCsHMtU06z6MOQ3oMWi8azUNCcsfoL7_AFRxiOlKE14t9Cs2Cl30EayVQ522W90pNq0YGO3zuCBsGvKOKf9YiwSKzGvcC3sH8eeR2daNBBCUmoJUZtIj890G9RS0FdxJdhWoFSHdsVsY24GpzXvIZ1_lbrsZhlvvwJYLB55oWHD8F9&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgtI-Lf-xcmz1Lu--UbyC7Nmgdwt-vArbHyur_FyMaLPPEsjJrNHiZQIarJ9KO-dIW5sXLUzCW-lNYdYi4Up90PeURdz6pMYbyzNuD6MWEWjlPTNlQZbIUwbzMJuuXE0_5lUDB2wg61faBakFYaoe0yWfMQ-63cOo9TyPdy0hO_HLmc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhXSqsonr33WV89GRKpyFyRravhI01ZTqzHGkyoPZjr6-d5A8N5aenCDKw8z7dkR7UWYCoPpkdtZeObZBkCEFWGeo0c3Q8U-V2-HFfsBl6CW1NLuJq_kEdthcOdZCgPPRTZZhZ2Ex-9og2k9tC97J5KAtEuxqLKmDFuXflx13RIe8lU&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjSevZz-9aBDR_KoTeC2lL69L9nPIp2Q2D26Biq8ePQvc67hANMWNo2_IlKVlykW3Yy5Cx7Yqv59C1gSB2BShGYXNV3KmEx_xtVgbpBdS_7yMlJoxXW_UVHZeV6mXSEA7Y3F6UE5xd7VSiQzD04NRRHnvd0Z3vNzqBWh0bzOeGbye3W&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiIT7b_jg4kTBSI81tD1So1S_liSNfxXReUcZYShBkW7l75NnEC1qsesxJSB7R924DpR3Pn2rAKRfi3qZFj-u27xfV7sI5uPla2qpkBmYRC_6Mw8kH6jQplEsLBgqIOOFOtJx7wq8117uMdoGblol6Ol5dwF2v39Ue9ZbVO6ZE4Vhs5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdau5N3AfBV6iv07tuUtbqScxM7oZzwbDpuNbFvW9ORYco8i5AoxgooAACJLO8eGEYZIy6yL1oy_MDgu-9jK20xF5uJYLgQt0xsiDmPjt0dAc5LMP-ukNlwHvaa9qHWjXd0BYaLIbArlWSq0Ib70S6oE8lEQEYtFaqMj0GYMmzVH07&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Sour%20Duck%20Market"
    },
    {
      "name": "Operation Turkey",
      "address": "4315 Guadalupe St #300, Austin, TX 78751, USA",
      "opening_hours": "N, /, A",
      "website": "http://operationturkey.com/",
      "rating": 3.8,
      "num_of_ratings": 9,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhqVNLZwhqIflgPJ6K1jZW-OKxUR1fVEX1Nj8GCHRxcozT0C0eQrNlwRgNZJgiCrznmdxX6BQ9j2GX93n1Il59LBTDwSVH1cZ8kQtVj2Bcf_ZIjSHu2HILSWBIz8IneRTY3oSBc6uZiC3ua1y9rjU0-IBHKr2SPApsb7WDm26sKjnUY&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgXp7aISZSo73XhyPV5GofMqAVOrxPKYN44bgmlHW2iZRDa5d245-8FGGQU8KCNz66zZv7qEP6TGhQyKwg5w0pMwCgNfX242KSEWbIjkKqj10zZE4j08W-Aw8HSMF_kDtW4joqEzA4tQM8vr1wrLmzAxbGecAWhzVAKdlm47ECgn0pD&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFizypnjXyKX0lWDpfpToRQHnkwpxigGwqjAyvZvVZgm6Swjnoj28cwFkph9cq1p1Ll-4ml16eizjBzBxNXzJZvbdwwGC-CFazOCxDknDSqTEdbQ-uqdosw38hnQ1AheWfkFpH2CjuzA4vYfnDGXoMSgkSebGh-SYKA6zzO2LnsSi1dO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhayLLvlZDKaWDebiNYXVUjRKYtNjBNch-0z5c5NfM2ySDIeHAwll4RvQu7kAJViufJjEmBTVEdzae2NrYcXNIcfEdlPBnBU1C1HIqo7G0BGELjukKJ8VsTNqVoFvjAKgajvibUFFfsYYzYQ8aKeEg5j8TQb9uQnycOvNK6vhSYJS6V&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg6gVChX5uWbiFcBrEhneVjrev7ooOouH-x4K9mJXrmdxcuLJrnUbKzEBmNuCtMk3awT1osJ6KpkPG9ye9j3L8BV7OTKpe4skY_gH8zK5FI8ld7gkSYENFop3F0xtWH3zOsmVH0_l55LjknAIXoRvoC1H_MpkeXCkleCafd3Ebo6M0M&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhiMDH-albU46a2YQnjnjSs130OyFklkspKLFpOsa2mh0M-fI_ekBYYd-nMDyx7FMbN6mcaV3w88oB88pRjhASulczDsOeZqetZqqB6ZxHF-4GZJEQ8Q52AGCKj7tXMzyF2NFxHVNzp4gkrzUPciu8d_LBvSKjyNlBwamcNzKVYjorN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj59Mmu0BbD43PdFfIkms-XfLSLVTOpW3JvDza8PQi6izFrTIgxwYGpBCe1hv6Qrf0jwigklIxYyV6X8JZwyI9gLHmZkfiKCMQvGwh55wM7dubs_1RkOnpXUG2di8hAkvPIPqjCOx3RsSW90jYcJX4e0SZdvewyVZRMRD5J2QLikQy_&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Operation%20Turkey"
    },
    {
      "name": "Hippie PicNik",
      "address": "1106 E 11th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: Closed, Tuesday: 12:00\u2009\u2013\u20093:00\u202fPM, 7:00\u2009\u2013\u200910:00\u202fPM, Wednesday: 12:00\u2009\u2013\u20093:00\u202fPM, 7:00\u2009\u2013\u200910:00\u202fPM, Thursday: 12:00\u2009\u2013\u20093:00\u202fPM, 7:00\u2009\u2013\u200910:00\u202fPM, Friday: 12:00\u2009\u2013\u20093:00\u202fPM, 7:00\u2009\u2013\u200910:00\u202fPM, Saturday: 12:00\u2009\u2013\u20093:00\u202fPM, 7:00\u2009\u2013\u200910:00\u202fPM, Sunday: Closed",
      "website": "http://www.hippiepicnik.com/",
      "rating": 4.9,
      "num_of_ratings": 108,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiUZsJ2A6yVd-kqzlID0NqgzAONf-ZcqLAb7nHmU3RDGQk4fnZ7iVY0Lps5QSKNY3vqn4EE8U1C1vMTEjk_m9gI5avzYdbkHdW9IluXCwi_2mFqhb52rZse79iz6VwAMLu3OET8C2NpeHZrgsM7aQty9HoRSxP8R5GMj1soaMILl1mn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiAAmpffkH-Bo9mOiUxm9i1jq2gic1Lr-_Yg8XNT8hfAsZxKwIURTzMMijgUS_KTHpqgMgCFHG125jfSceQcWqTEXVbn-Z-HgNKPgV6VQkmvGrA4gvBn6Iu9DINYvmcRT-aHjmBxX8jqaSiThm5XdjL_EYQ4kwXOn17XE921LUcbzEL&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiSDZTW_dynWFkXmO3pk0HnD2aml84T762bX09QktSn7V6MpaH2t8mId6QCMU8gND-mVu80pjNsI4jRe5CQoSjfLdzwwMfsd_1ApAOnr-jPrI5iftYXMvGnlhxSxw9TtQgYAC5TXqS4jewF8N9g_nPG3Y8zkmAMiao2-t8VBXmMDbkf&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiLI04mFWi-cfIxYwv88t-X1RRGK1cKXRtRNzuUxXCwAXqpv1x0-fLOfOjxThpGuM7MPCklfFfpTrIqbm76_-hUum3HTPGWEjv5bPk_OOjvLFtqzjMgjpYSG04OpgOvIhysvVd2x-0bFF8u56e-1oGeWy_-G3G0JJnqH9KWMX-mbed_&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjiCIbzkWBaXUnK8K54Aaq4-8wJAHjzaOtlC7vlQ9qVUU_Ki5yW9X3tv2y52rGarDIc6HneGynJq6vW3ZbFNV3nD6ujgerj3g0GAmH0TI8G8CVQ4RTk3qBqbMHzkpGcBbiO1Jc2wIDqV_pKddnPX6jIkvVxagVeNz2mLkd4smb50uA-&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjwtO8DgQQd7Ukvbvq-Pl8g4ICvRSTB5XmU-hqdrqO1Cs4UvZSwy8S_Cgk2qu3VxR1kTMKN8amnwXsXOC3cXyZj72rdO3f7t8n8b8BCvZ52LE4UI-7vZLbQ6GMRskQCCE5sFmFYYxRngnjy2mJCxQFt8H16eSVt5hWFcYWX0h0pVmAO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg6eSXKbBanzq0Wpw24J8JXW0Ytr10Xb67S6qwpgWyn2mFDEWGiz4Aak1Atz_dm8U8RMW61JOT9wRzUIRdhzjLP-6PcJpO8aQR7Sqa6F6mJhdCi6IjcKE7ONWzzPmL9Ulpapygqa4mHYPrZh6A2OvMAe3ISaCFRcvdmaSnIKKAheBc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgtNLYtoP48OtpI4nK68RiTFMYag4PVLSH-yUHxjjQGsB8-3DwIloJAi35KxmTXN3pG77kR4_q1d2zb-CoHdNLcagvgugYqFmh9oYypHkameP0bl5isDtpNy6OvPHYOWTnUe2HDPjlJwil8yXRvZphVPapRNAdoIX_cFjaZg-TvMqw7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhhcV7LFl3ysnj9ulukKvFO1cgoUU3n6lCNYJBPndazg948d0ZjJQlbupNPWoqJDhYKtIduRe6Yf29CgaAoWJVRCVwsGKiHRJdBGO5u-SNWCPaonavYxTfehKlLxd2yXCJmoZ82rMZOBtYFWpl-GKkznK9WCdabDpUrDUJIcYzDVbX6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiatjaz6-1y7X4VSxKZiB06atiAWsqr9mk5gaA0eI5RYS0iJEUw5ov1XjJYp-l4f5YXFoaIungzGPFsvF5zWHdhRUMaLaV5Glah5S2YXUAmuBSem0qYXvA56ceSR_EVhubgyO-d61ngc7MpKKFGqWaWItc7z0Z2obWPcbe8MssN_kNE&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Hippie%20PicNik"
    },
    {
      "name": "Kom\u00e9: Sushi Kitchen",
      "address": "5301 Airport Blvd #100, Austin, TX 78751, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 5:00\u2009\u2013\u200910:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 5:00\u2009\u2013\u200910:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 5:00\u2009\u2013\u200910:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20092:00\u202fPM, 5:00\u2009\u2013\u200910:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM",
      "website": "http://www.kome-austin.com/",
      "rating": 4.5,
      "num_of_ratings": 2279,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgpXXKSgTvFHtOSslvNYrMhkI-ndh7Yyc33060dOFyx1desvGUbitH4scAOmFoLNXle-u3ilRfzHpoiSSLyBr5r5fgfPFOTPPjRMMEfXnK3115WpLrh4cCI2DGV6sDj5ZMnlgz_i8yOADzl4nOW3aNcLQqOwEGHtWftwrUdVRJI9mTI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFidxPZZZcuBpDEjn-f-2aIHcSZgxQETsrWuaaNEaRBk4iTvVELMtrNfLyUrc6HR8voD7m-B5dbDPtQm28Nof4UrGsgobzUut_63IJYh28Gt7rXBpMgsL5TWypdTJl7pNX1v5C3O599vPSptdPnI_N6Ok0TzhnZgKfo8e02xyO8ldLFL&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjuMrR2-iwK_9KeIkxNLvxC1cwacnT46DyYC203HkIyDBZWJu2ytEncYNFZcAl5pzBna04WEByfcqR-6Xkq0Mu76KCUTyuMvzqde6UIAWHoS6jJONC_lHCDHoUiuZQsFHGv3z65OgO--3ZKtJjY3N0r81CDZOL2sdaWuowRYia91Y9j&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg4asYHuaSKeqJUlHteIi7IOiBKfGjge_fRH6ZYs6Ulse5zcp6jKpxfj69F9xWKy-8EVp31FIaMiLUXpwDZuBSztMw1B90wGoSPS03kIFGlTYvhK6B60l48nxFDEWBHPfqmotzyGXhkWacyfMMCBOiHC8z5vxdzJtPKNXsiAaHcalg8&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiWP_307kBjt6pLgEQp1dZPrW85w2iNGA81PWQkwfZU_PrE9-kUl7S0hsyaMhHkbykYwxofAbuevghvpVBWs5OwtF0n539aAI7BkEYhcgYC7_Uplb5zHArl2bW1efVTPl58c8eDVaCdEsUklZ4psfg2CijxPHJ8qrjaBQxi00i6D70j&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjU3iGgz_XgjOJVONiyQchzVE2unhKKnxWnBZk-a_AQMwKzmmEn7TAmsDlLzgZtKAoGp9b3U7bKJwfZrzZU_uwgiZiTIcTkHWEoB5vQcb4ILsm4PuvyfCh-kBsFLhdMnZU8DkOI75wuNe4Mu_3qskQF9ODjg0cLh6JXc_iPcompzyyo&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFipYz_zfiQntyAyeQwW8FBOGCHPZht2wWA8NROwRpB2_VsTF4bymsiPXUJNSNsOkZpOs0ejWvkD2_xKUwGe7baPejY8667c6uTdAdL7AKrWmCseNTNZ5xtXFjb_DRYT-NRHb1kO0fqMc9ovUwag3iERTnfxfduxMIrFJpn52jxFHK3i&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFghza7tGr-AEp3Ex7IBM1NjwY0y7vQPaFJhuBYZ_e7g8t_Ge7t_VhQ1VyT8K1tN94erZIy9jFevSE45ITKnzvGTfVIeQzVoS08nGekNhUWEUt6SaBMCiqZd-aBrOIEmt7BcV1uBuTh26TRQzUnoWP4ZNhm4WZWt32jaSfd55xzpOMjs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFia8StRBJ8P7C2VcFN3fuAj5pSqDSn63TRD4BjbcImuPhPABs6pDxz6gKi0FVSjgcJVfo-katGeiDXp3ARLqvhhLQCzr3uZz2PZ3NTOIpZfjJogze7zFZcE59G7yHNOubrX_z7Z75o2tIe7vV4SYG23FQZugi_g-OnlxKBHYQP-MPFw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjG6PKCcAPzfsiD5vlLf2pe6KVDraWqXWA7oJaB7iVSGNjP-tAAfRFdd-uyP9RLWEQHHVkKYPsc6GLDe2GMObrI4-tFuhQRaoO3ZfNGAcpcc7TeBe8xyRRoYP2thrmkCUdSRiMRxdXCQXQ2eGuUNzbAnVhfFp6Ho4cX2c5QrBeiYA_3&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Kom%C3%A9%3A%20Sushi%20Kitchen"
    },
    {
      "name": "Mandala Kitchen & Bar",
      "address": "1100 S Lamar Blvd #1125, Austin, TX 78704, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20093:00\u202fPM, 5:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Tuesday: 11:00\u202fAM\u2009\u2013\u20093:00\u202fPM, 5:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Wednesday: 11:00\u202fAM\u2009\u2013\u20093:00\u202fPM, 5:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Thursday: 11:00\u202fAM\u2009\u2013\u20093:00\u202fPM, 5:00\u202fPM\u2009\u2013\u20091:00\u202fAM, Friday: 2:00\u2009\u2013\u20093:00\u202fPM, 5:00\u202fPM\u2009\u2013\u20091:00\u202fAM, Saturday: 12:00\u202fPM\u2009\u2013\u20091:00\u202fAM, Sunday: 3:00\u202fPM\u2009\u2013\u200912:00\u202fAM",
      "website": "http://mandalakitchenandbar.com/",
      "rating": 4.2,
      "num_of_ratings": 309,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgCnihU2RtohtkN7oYrZBikNLDB1kS9xVC_h1AY-WtKZ3K0t2M7jo6ARFYY7CmpT3DTguN2djk0i_TeLrHAkGErAU0m3pEcUpq11JZmYoNrtFkTrHSKKAVv-VHNo6iSesD8D2zJSqnwq5bt73c1KLqTJFm_T2eMosQKyhYZzW9JP6JM&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFikexujkxvhd9sp6RDgPrmkNWGAgkdj9R9lUoLOuX8VYLmgsQRcBaXez-XuNpndASXV-MwaFzqPBEwEK3-AugDI8pKe0cDzk60HkhtUta5MqOdHmsJUvsopMabaaaue0dJEAFWspYZpEsNtgSySXz84_KaQDDIfsLrj27Mxca-hxaHi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh75mNXOkbuEiZ1WKWemRGmQuYVKCF55mlREkQkylr5nor6x_EUEciVdluiRjp7XTAsQwV0UQd_pivvL3rVuw_mDlgHK_9LV-NphmSNp7b2hc9eb1F3argYkATAV312uU0qsjFgn98vNr4MvA-hjoHV60t8VvUQSvXhusL6vnT-RP4k&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhUqkfEjQslOJxjdWlOaQdKyv9mSK732MODJ1tOp0X3EMdNfl64uXzJaLs4xkNCVGmv3S1uhCcZXhlE9Y-HtKgxM8xixrN7vPh5rMbn4LTGZw0WJBWxPdf8xdSZZ-ZOV-3DhbderE-VQDdObatn8CyJW7vT64A51h5kENvJ1dbaXuIO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFivhq_RqhK1DuFo0DVJJky8Seuz078UXPMHxIlx0leUw7tJfR-iey_jQhXWGXZvVrKuFfvH74tuZX0zVHTYkfZsvAoLzRGywh9oOMO5VXIOAR4mq6MEf8rnUCAg_L0QnQOE67yXLmKK-XzNHXGC0HzbsZBb250npuKjzg6QGrIXZnmN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi1gSrwM5sCIWLaBRzrnc4C49OvmhzPn8Ot4kckR1SyEXHJTzIDGhNcFCYLAyQ0-9gDBfLoXSXK9htpac4bzOgRKQ1WoPfov5vbpHHAj2bfpqJbiiMIIUR8M5JInMQyU6lSYO_1A7YkSfXJOeWvZltslaGHL4RVyHV2gBvXTbMB0Xit&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFim3Wf294W_qygbgs3xWSgeZPn1WPCyo6rzyG-zSl41WZiXH0Qab4eGLHfAZyo1HFJGMM0UPIzVCxnXu3148ZQ_g-dU7FbPYz7LkRLy92uIf58HYVXGg82QuS3IB1zqir_Pdifq4il7hYqHcQNQjveb4iP0GcSmE1nBIA8rDO1NQIsk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhmj6H60vcgZLIYaJBLvJlsRjWS1leIrdqJ4aJko4EMUv4Yc3UFCLuH7sGmJfE8fs3dt6DqLfPr2O8ga1uDZB0zFIxPGpr3rUNfl_ufhhO2gLXsjidumnQpuh0APW2Z3xA1jjP9vKPCwtD_K8Q_h1D80JQ3D44U_-dmsgl3nHY9EmOG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi96B4PWqlczph2KJN3MpJQSYHfD9h0VEBBv2STE40kLk-A_mFMKLSS3X3P9E4M6q0enyS_qUOfan-BZmctuxlDmtA9v4TwcGxoj4YcgIG_fxhyRk9kPLwfNVpyWRufjqAfohEy0X7bzL6RVs801f9E1MZ-AZrGazXlvNeNuOH7gHTf&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiwhJKMwMmlTm7WFgBitcp73WEdtAUy53y1jHLK3jFe2Juc5eStxXnXWLFhbd6wshnu5wgeDPt6FKwnzyY8yE7abxPV5Jpn6amYQilhmmVmYWmVoBKtd7YGU1lPheJE8flGTJIZYvETdHwoaUkBSHpqIP3w7uqjJitQqdhSnygGWXcE&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Mandala%20Kitchen%20%26%20Bar"
    },
    {
      "name": "The Lighthouse Cuisine & Kitchen",
      "address": "8650 Spicewood Springs Rd #133B, Austin, TX 78759, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u20096:00\u202fPM, Sunday: Closed",
      "website": "http://www.thelighthousecuisineaustin.com/",
      "rating": 4.6,
      "num_of_ratings": 22,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiougAOyv6wEQAkT39J6uzgASclrWCcWXiNG4_wCBPDfJ21Ae78ZHyYnt_ZAn4d-8uxOdC155FLk8IfZw0oz-sWLEmoTDqDUhH9boDYTAT2dmnJ-MJLeEBu7j3kYrmAypRIIas6XeqYsQqDv-VTojCYZ6vrBjUwgiFZjTriv1sVP2Xk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhDFFpV1pmDCwIlB5_vSsGoTjplvUM4noKBavU-_ppa-CBE1y1pNaoqQHD85W3Pj-QfE_PSp8--AoglxnoB_psyVHdy9TJtwjGgzCWNIQU7QY2VKEPgwkqU8eJlfL-CfbaiQUPFlPMk5xvgCKkFdtDcb5RlXvJcX0yYP_HrS99nzRJd&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgqehAvMhbMuKVwST8ctN66c089yZFsvCGBO2mNNn3gsImBZ5R2NKjANRyPjtnr7LMmGmLd1mBLEO-kep-hzu9mU4DSQZpFLiAMBDDAao4JR-BPBTnh9F8DKmzucBkjaOW-mCdxlYoUf9WOxFMDNivEScYm1s7nSBy3iTdG7mMC-ZHu&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgtwpziTPxtSxJiH-bgLeaZPg4lTEIrxK3uPG42024I8mwByxxMuzFJ9qKiiEHTGzxUbEtVTutqlNFLWy6UfJVT_7o80sQHC7wkltusGTA2ZRvbTACEifl0p_XfDRQ--3lopd_BJxf77LV5EJeWKHUPH2ZPAOp_FONDf3qkma3K7Nay&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjVk35vtLdPB2C1hCnW8Gch-RmlOT-3asrvU7BEGFgMlVSaC1CDSzYdj7tnemW9UAHkrusMuQVN3OUYwnpGzAPfhYWV3_MBcYl-3XdycTQuRpmBDSeh6jcwzT_XOtIOcfYG9298rSzvxQgO0Og_MHW7Nj7GKuSaqIU_gfDkacU5Oosy&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhO-GvRzMvZ2DGgEbHXM82zVx7gbttLlVWLDuNeu5dmjZMCS6HJgJtd0UddXZz-bQeghuA_xLXISINhklnMG4rX9e5wyJXwk-Rb_th9b2OILZJMoy7cr2EPHxK829c091O537WeTHRVvjZHoTAnSlLtvOboK8KoYhlyrMtRnLrgOF8P&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgxjNEQEpfjEzNbU0MuiXhoBKryccMxewwvQXHIS78hYd0c3ZcV-zroUSRC-K_KY3h9RALsxjb_ui8s5cIbqBFCfaPe1TqHENeZv0frO44wST4tPwNQqLgLDteWiRuGBYpyLlkMqAGtVChhE4zpTxJEuU3S_tp3ckM504_exjuyq7IK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj388QAO98eZ6gi0yQ868V4do0kPDe-ChH4LnTnF8oSM47M8vynrMBKLmvG6jPvkimNxpfguX52h-e2iK8SxaErreq7F6i2ICuKSxEERY2pBta5d23kPvtSUZ4cvSIltv3Ymq-xhsb7uVUEXx3BYM5o0Z6k_HSD3I0zOUMOAgzVru_0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhQ13Q7QEMkzYvcrT5C614u0aG1suWtWlS_MWI5TDholkCjJzqyBIlVGNRQxIw8LVXHnNpYqyHMbKCIg2n0tZzNORKKs3Iy16t40J8hzYxOc7ZFotIe3jADAQNfMgnJnr7pukbd_f7QozjMyD7zVORJz-qtXF7HOnfHdWf218-pDYvb&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgs29VD3cj8yJLorcRbES-JxO5C-acY4nLCTQSVfLGprS475ZnUIf4TMt6KShuDm9Cje_ZaYFb-jqqXV3qrGW1Ah5DUCYufXwsaxf7yzX_ztpfL84yYaXp4w9mlvwbXF7Dh_SAih979xT3GJ9Ua1o4mVTbqegLgoQIGV4Rp7ggGFTwJ&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=The%20Lighthouse%20Cuisine%20%26%20Kitchen"
    },
    {
      "name": "Carpenters Hall",
      "address": "400 Josephine St, Austin, TX 78704, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u200911:00\u202fPM, Saturday: 10:00\u202fAM\u2009\u2013\u200911:00\u202fPM, Sunday: 10:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://www.carpenterhotel.com/",
      "rating": 4.3,
      "num_of_ratings": 262,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg7m7YbKs2B-JXY4XQGJbk7lhDQ8eI8cw5oBjkJ2FDOKZPPZbwpnDtl-rC92XPEe0Gp1sET8eDe9aYX-qKldZgKzdT3Q6PlpxL5nCH28fwvBDRHZQr7kBo2xr8t6JWnrUxjt1yR_6UXbdyYzVks_nCTiiSDq0l1LnMEPlW41K-MDPv2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjpNcB1-Qj6tjo9mVgBYsS_EXXg-fduj8JY6Wd2jKA7hDUOx1yX9xkYF-my8D3Vuq-N61IHx1PtHGZHTGQc1yKIEeSmZtr0TM3wwI5BgAAPqsp2erm7kKwYlvhNLn7xxyD0hKt4gY0K1qkIU5v_CZ1qgEA1cA79i58WTaiKyTH8YLnQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiIZINHi2SakFkr7SLArBC0mPsr6JD63DQzWb4UFHwIs90lNWkts78Ksru8_-XEoaM2i6-YWP77IJbclMLDobo3n83cPzGe7Uw5Lqncnv8_s1S9xqn6Yvr7Stl-tjOwGXNWolSGauK5EfwVRoUF45XTOV4tv4cgIxOfTIAvyffkljFz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh5jqRxVci5J2RKGTo6N0yAeCECCmYeTS_owH6r7-IveHFanisodLjnnAEhvSnhY39izcroHVae6tiMNFxzu6gGO12uhdmKFC-BKdTzTl5HsXeGL1JnjtVK9iu0pgXEUigVhqQ3LWSys16h0vst68Dv-5IJkHlotD7wxjxhvX915goG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgkWUCerec6XPstqo-__Bs1A9vf1gvv8quJafWprhV5KnTH3htDWB5S-s4oNnBcrxzl4TXLg-8GU6IzxzqeXlriZ20PkIqpCe5BQ9UzU_EIv_-UOlPgY0SDojEaBpZN6ebPD2dJ3Yz5u913B8enqhfRwXY1bBiJ2NEzJtZNWH5a7mcv&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhXmKQbhaJTmQIfmXVA-OZQEv2iRs4gnX6CSMKfbZd_iojKQm_9_oyAHmHRxKAHHj7dcP5FI-CFWB5AeM4lUmGSNi_aNtmQuq_sPbiw4kv252dNG08mj8g1dbVmo01gsVRViiHqV2_Fi4k7y-cMaWtt6Aogno9aTwZZc67vAD7jtYdc&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjQkegTqvd4wlfexyaC2s8XnTsIJ3gz19fDpJp6sd7U-7AZW5xqIN2mhGgM1HaEN56zKnp1sxVeyf23VgBadJ-Lv1ZJSS5O85hd5y7qkFcknVrF90-SCdscdA-gnWDffln3hj47dvNunEus48E13VZk99yhzsUoSoyFxdGWIl90EIhN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgaKjnbtuveqrUY5ZwniOLRsRfFMV82hRQqiIF-7nx8fI2gC_8s91RfObfPdYHj-r5Qo6cLATXFc1DaD5ROWBLE_k3uc-7DZS-bl-J_479zcN99FYu-Fa0nieSibJP_FySfBPDnQ3BMzp-zw_lDDJzDO3OBbIZpApnkb10JFI2bX5ey&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjcu8JlZ6DdnA2fWm1CCbVAjPyDKAWKA0A_0HjFDUal4ETqlBJ_QqZWctIpgmBKGSYI0mFbeh3xjO2prYChpCYkiB51f6q9gngjp515KFmFQEp28LaJSxwQCe18Nvl_N9ozyrUMGxAWr3dTL_vBwu0Do4RgS1ajRW1pZXz4fAXMh-Hl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdsFabbzE4WhmQ8Df7s8KxWwzjd4ts_e3K9lLV0W9ypPFVeii6NkGKigDAKKjFQ2HgEUsgomfWYJyq2a1cijXCL5xAB3fB5uKglctiSeOYg5J-zjV-PRUGENyWc-qfQc3tK9wlU7X1Yo4UKMe3WOKXuEgBX78-rHrZYRPgMYib3YMb&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Carpenters%20Hall"
    },
    {
      "name": "The Well",
      "address": "440 W 2nd St, Austin, TX 78701, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20099:30\u202fPM, Saturday: 9:00\u202fAM\u2009\u2013\u20099:30\u202fPM, Sunday: 9:00\u202fAM\u2009\u2013\u20098:30\u202fPM",
      "website": "https://www.eatwellatx.com/",
      "rating": 4.4,
      "num_of_ratings": 467,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFizIxnp8wcitt6NRtBOgal-UHZ7jXNr0bXvMeVKQg7VvejbT5zARXrTrFxkzhPgKfWUTysg_CiwzPvx6x9l7f2eCEoRGSez1PvD7TIRvPYdmO0gGwNaz6vNaU5waMtWGpyRhAmund367c5Hq6gZRbJRonPIjZOkhGbyCvodA4F-t1-6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjaCtBXq6GPjNanPXRQrCvIMtB3AG_oMd-_fHxRz_sUXpfvhC3bZynM744VcXazlsFWNz2NaujiKg5-ZR9Ak3j5iQOKnEXbZPWioG14ySVRrzyb03Y0llcxvLG4OJon9GUJxex7I8vlA9AF07OVtQtoZw0EM6O1209K9HLPa_hCQHka&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg74Wwnv8szWXqkoThemos7D3YKHG96CUDjKy6NTWxctwu7LhxG7ecAvRRUsve70HdXY6ocxMi3kycMhcn7l_IrJkHiWXnR3Bwwn47TqP9DUIhfKemurPO4gplFl2WPHUUDQDP6-Qbbo2x5MvM9VlKe4Ak8yxKYI2UWiGHOGkmX8V49&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhtGseGdflW5Q9qdiLJnDxwzVfhAoeRvtvvzC44xdB6V65WDtP3K5zVZjqrKdivrXo5gdQjptvFgJhp6QBhkwjr_23kbSfTxE3rEF4lCE7UFe8_C0SiHy6ZOQk_K3pHse-kqJdPrM_qV-u9mmqgRGxbuPgBXvEfaNJt6-H02ECnt7wm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhRB-xRAnf4PVVMzS8Jd2wDnhbHcUO-l0OMVE9ZzhtRGyNpT5BlcKjJ0SFrZBjNsAWN1LNgDyqgW5HhxNs4glYZmM_JXKWlJwaz3XGR0oAc2_Gi7d-2LKIs4LLxlw_5HHFhG7o5amZqUcS2XBH_JsgGJh7MGKx58rmC7r48No00_GuG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiC5OJ7aAVdiGblKYKkobIyQZqMoT8Sdxf6NTn4By7ZPZYKCUdtBtrVHBLrIYZjcevcN-mt8A9f6Xdz5H40WTo9pbXcujxyO2q08Zp5mZ54OvgVTciOWb8_UoiDUA89AjUcqfz_bq4VcO_f88B5JtU6x_9H6ODsjOFrwOWwxw0hrfYi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg3ZjZnw80FbFn7gXDbUg_o9S3woglyIccla4AKZRxW8RMTLbl19jngvkWiUf4aL-SBTy2_cwEY__J33dWDknQOXi0hdFG9v-mOOFjvOX_aQE4m0NiT_6OzMep78OXolQqHzjZSlvDUFnDi-UdEZMA1yiAXP_DfvJCA4CXKsWzRkSuO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh1X3QQsZwxvbkzeMgLdYyvi4JokpCUd2MHc1mC7gJyUnTICAGeQm54s4nx82nb5h9JUBTVcidrjTtUw90tEQLZNNQLGChqi0JgGVgJtVf0eswU3sqvqowIq9cKyBD8jBLUVgMgBTdRA4mxtxGAMeNzchRB9J2RdVKqYKmS_4OS960N&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh3a1o63n3cETW6VFSoX-sGoalf3WJ4LPRs3RuoUqBMipgaAizx7mOMGRZHFlDmxkrp2VwXdlgDjsnjIAggfzTDhjlI_jZaQ02Lr3aNv8aQjbI-y3_AsVXdcloVB4ArY3-CtSHnyMfn5cA_4QpfpyGR-zcB_rl2eqaLG6VHW4UiNFpb&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg2RWVZTq2qN5yQTN2d0beG-l1FV8JEVwvI9XQkfwXuEeWeVgs0DXKtH6SZ6PdDgkwtEhfvTU01wET9QdyNn406NVn-rTk-0_ysfEHmhQM35jHDJ3MSFURtC2lbBVA0oTBa2CcQ8noHOIEcKGcEFWp0isafXCTqoY6w4G5leLDRMQks&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=The%20Well"
    },
    {
      "name": "Bistro Vonish",
      "address": "701 E 53rd St, Austin, TX 78751, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 3:00\u2009\u2013\u20099:00\u202fPM, Thursday: 3:00\u2009\u2013\u20099:00\u202fPM, Friday: 3:00\u2009\u2013\u20099:00\u202fPM, Saturday: 3:00\u2009\u2013\u20099:00\u202fPM, Sunday: 10:00\u202fAM\u2009\u2013\u20092:00\u202fPM",
      "website": "http://bistrovonish.square.site/",
      "rating": 4.7,
      "num_of_ratings": 329,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgXh5oyBiNSjm46TR2rgTgxDY5ba7iGu35eqGOUE3glE5BT71GKps3dXggeqzqYR8wX7cDjMrHB_SzzivDPrUK7O29kP0g1h-DzMCsnL6KoIXrACJOc3vh2-YM3Px5y933CC-v4JozSgXfAXsNqJmUhuyTIEzJf0NZrX8tTSx2z3Srs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhTBQsgnJ9Xnrz0XHUxvEd1Os3oS_msw0zCFFirAstmpoxs01YwyAzfjgpV8P2S60RvpKX0s9kZzcepA7wGEtzEFN1czoc6wpBfDGeM4mjyMtCrI52xAQ82EM7ITTx4N6Z9ex_GPsQbxJ0rTnqP1dhAtRcjwVcgpLHe1kgsOO5YL0aT&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgmASz2clp27qNFyhmivStTpJT3Tw6M5pci--nr8dKRdIq5bkNBNKPVhmRJNsVwYl2XQBgklib_azN3JzfV1H9h0lJYF9a6mFDAN-kSP7Untk4NMmJjWaQcajy_9bexVb9uF7N7RoE6Hoo9kv7JWuKXchohRo-1opNSnh8WytGKLa3E&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjT9Ef3RKmED1K8gy5gc4vOWQxKWM4rXrHxe7nNBlWN6gSQMAZfuZw6SkVD19AGrZniR8gEgEd5ugqL1mC_FQ_20wloj4hSOszG4x4a_NoYfoz52gnDTyp7-L9HHiRuFIWvojXmf-HVZZQVsBhapScHXVgX1FyV8TBTNbH5X3Ski7AD&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhRYHdPko7wZ5PGSba0NxsgUqeARSiKUUEtnnb7qMu-GzccrGtipcTXJ9-kPtBbMCal39UIFRaqBReLH8HGhiRVkkWFWBRcbWeegg2kQvmBabjDpgRA_iRY0LM2CpQKTmlgsIUNfKF6b_KdZzX6svNlZ5n9rfQgS3HnCN-djH5A4nEH&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjuHTS1DT_aFDKvh6Yah_FPsn3c2lDEU5issHCThF7gvDyBFp9Apu5RfQ_9IWaIhWvjOa_WfQqgSv8IGJ_5blg_GC-udyovwDxCCGSft73hRP1-LT624-Xnt69n45raqZX_-cQIVVsqTOCTMFsNLXAhNtFh3-h1x-jyrcDrq2uU9E4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFioZ3dw1X4NcEk9QqWqSrnLI9_jO-c-lzFx3WAU4nluks3dMBuxTTJ0uYH5BdrJXi_xMu1tM7Vc2AiEjsYGOUR4rpHAvVsguQCGVIsdumDcwyf2BQbjpXoL_WKSiVtodjVLY2b8orhLdfMzQghY29GTMw2-8D1gn0cS_aBNV69q0Oy6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiGLN25dZJJ4uY2ok7k6p7ug1wBjaDqTsCbEflO9GXT2ALD9QxvKjt0Nusm40nXkscfECBUBiV5LE8wZQQnl2kk5QH2LNyoV5Lvvn-qBAfnzSAQAYhHSTg_swKwuPTFitm8yvF9XCToeJ2tFcEX347xdH5LXjzYHxzu-OBMr1RRFJIF&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg4-we9i5SWZKAfiW0P6Z00b4eBdIDS7WZRKUNPsd9yMNBJKr4wuKuCR5S_7bn5xGVzIF3XbPv99upEfgfD6cIkj8P7rwF4CVzMoBrRBFUygow5YywCSWeX-U0Gc7zwRR4WyiIpcEAhzJr_msEYpCChCf57haV-a8cq55L0LcygZ2qi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiJw0DiY9QOJDcRJhrSZTwGs3Zp1NEoyR4zWBv5j35_yBDQoiZRI6zf3s50oZI-zguU6m8T7qHFLnSdVqhTp1IsOk1QcCFl5ZwewGh5rsTYUCzmq4eW2FioVU39UHp-YRpA41yUvxX7jt_6hpecgvBhUU0fRxfjp1ivrlBBmE0wTVIl&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Bistro%20Vonish"
    },
    {
      "name": "KAPATAD Kitchen and Cafe",
      "address": "3742 Far W Blvd Suite 113, Austin, TX 78731, USA",
      "opening_hours": "Monday: Closed, Tuesday: 4:30\u2009\u2013\u20098:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20092:30\u202fPM, 4:30\u2009\u2013\u20098:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20092:30\u202fPM, 4:30\u2009\u2013\u20098:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u20092:30\u202fPM, 4:30\u2009\u2013\u20098:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u20098:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u20098:00\u202fPM",
      "website": "https://kapatad.com/",
      "rating": 4.5,
      "num_of_ratings": 544,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVFtd_-aToShkMcy08ekzzxDWN5e7DOmAMesZSNev0VCAFmh-ubExizPCuc4zEobabiaJt8bGDWafmpxd8ioQa-Evt-PaiRV1LHj5bq5fnYiHy4iIMTzwkzvixmhkWiwJXcvcDpOBNma4YtWXtkFgUWz5954LLqliINr_cuOI2xcBl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjF8WnNALBmbnTQEEEd7O5TsY1i4B2KxxwjEph5NDsoPMjpwsl5hMIdK6SCwLB2HUyp7DYCeUwLfuJ5XzrPJ6uvHLreYR9xI16G3mnb4MnWHA7joM8-z-Dwn-kAnockPGe9gXE_2wpEI9CPBA29klvffNnQ3ybblB_NBH1tpQq-9_Y&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjsfZc3ZeKjnl-IVX-sPu-m0MY55ArWRZIhXz1g-3zL7olQtfib0Jx1jTmYUuiNRh3UFGH0FgiSpc1VBTQk83Hrm6opUZh2gOlXQXC8Jfg3EIhATk_NdMQTKR9vwOYFZ3-W161HXn3i9lkpRsy_KYGQ_cqe8q7IlySf92knHSCKlvx9&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj9E4wx8EXoZk8-MtdW-wdScnPaEFGa8IQSeS_h8UDPvdPfege9f54D7-3QrUP7FqC9pGWrSKctHzVmeP1iFqRydAKqMLEqRBHMc3QKwHL1Oh_4Dm4H3rzdE0UGRLLomLvchrclmGqQrsyIX3nrMUrmF2NHKISk1Rm-Rcg6Mb-jEgSV&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjy_LKATHC2SfPF8htOcjkxELWR7Z6Ejmh77EbBwQaPTAcKYHpVGiZu8xwJkclGuENMsfwD8mmlqHDEdh615ZxeLDwAo-5-8YmZeFPJIwIdeE0w59ufqRGTUSf3nRB6zARPoKFRqJGLVdNiQKOlJ_ixb6OnbGHTeUzOibuGMpTAifk5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg0OcTyaNpD2IvgzHGwNO3kVDI0uMCpYi9CL_qXbFs3YSN4nbbmPNaiXRdjUD9LlcfKyJhR1BTU253roz1m9hxpzXxJ9TpgkpqhIlgiP4zofjTEvhRkMoSjQnoxFLuCwef8qoyXtF85cmG9CBG78FZlkaPyTV02c4mQ9-olCiQx3tnn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiRJG3qI7Ks1ijOl6pg62heII4kP6U5fykZ37W0sKZUUG84DO5dNn1TPH1VXI5FmkbX3e-TZ8PUlXrXTlTO_JAFElsB2hJbBQDzGxDUnV_mB_ifyuW56PBqFFZXj0rRP6zxhMyRoCEHkC4VznevnaCEHqo7AIuQnFYbz6S_O27-XT80&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgN7H9Ng3zHXQodHu5dQMmua2dU6Nl9yovSs4rLcKfXYOzSK4hTJOijCoWkQepH-p3f3ne1zcfaSMnCYqIxxPoeNkZfQvvSLCooIrrGIIUZT6FpgTBVPAyO-zscc6NaMQ8ifSvl_Jhyb6ybplH8qW0iR4n4C1OEfckuToRJXavUlBft&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjCxnibuQcIzkY8T5tQZ6fbrlnkCSqLMfTAiwONtRk2jB-0Cfl2R9ZHsjkfTO8VfuDRfXAEx-9iwP2krPWwX0s8t_IA7mdQvsYiyvRTxE4CmZUivZwG2LFnwLateWA-8J_w7MNXjP28SZTUXI6_PAoLVXDHRy9EKJEmJjvg9pcKW8aG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh7EgHg7qoxbjetBeb98G-RQmimq6TdV_5b7ppzSnSwPVADeE8dVaZlFqfYJWhM4dTe7kR4DMDIS4BiDIfwnZpqW1ksVCh-UIJW6A4knw-21OJcNilDnBysCzcBvWarsG-114hooNhZgz0nDX6gNpDjjcvTEGcf7np_3HYTUUO-xnxS&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=KAPATAD%20Kitchen%20and%20Cafe"
    },
    {
      "name": "Sammie's",
      "address": "807 W 6th St, Austin, TX 78703, USA",
      "opening_hours": "Monday: 5:00\u2009\u2013\u200910:00\u202fPM, Tuesday: 5:00\u2009\u2013\u200911:00\u202fPM, Wednesday: 5:00\u2009\u2013\u200911:00\u202fPM, Thursday: 5:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Friday: 12:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Saturday: 5:00\u202fPM\u2009\u2013\u200912:00\u202fAM, Sunday: 5:00\u2009\u2013\u200910:00\u202fPM",
      "website": "http://sammiesitalian.com/",
      "rating": 4.3,
      "num_of_ratings": 375,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhTjUI4_RNFE6vJjoU-MkpPmSuxlbgOOptqnzQ9UL5IvKBqCyIUngEiKQzFYATXIQDNxolBrknqOxw8rxS9jNpSqXG5os2iWdrAOYBvDcUuA6vowGjpGI9gHZ0js8UdmKOpWFSeGXyRUWRZcHRDH_dADMF6I69IhK7XkF3WBSmuwHPQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh9wdBCIKi_o-8cRVy5NECdiysOVC93WLXD-vIlznybUpeUy61x-X_IqnHNDuufAspkV5_2u4j3UyfbwOxK-8gx0xKU2FasPSZfvOpTG0RpDMz7QCnJBnM26lWzQ1TZyYQqnvqHAXaDl2mI4V5m2abZTiRNKMPxS2p7ZkCNe31HAhW8&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFixTVLuRHJVl9KGn8qHHFJ81e4u5yI9igGo4n6WjYBa9HOLqJW4q_QoRvhPedGQSRCVRFRmoEHzAe8QmHHHWvTInvQNpIg__LSU5OFud0E1AZvmMd9yAmZ8pexW1yQJf4IMxi8HjaHBj7_eFOIBIQOUrXJa-Q8jREuCKNIPx4nKyKJW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiuAR94HZ1LdyG1r179rH3qkGcWLfcePHyMziKREHegBXVtkugh1kml9I1iUu6yfHJuJw-rMPlNMEEcqag5iGzbH99rBVRkJV2LEb0upzWdXyW8EStph0tff1yLLpmWQdw0hXcgR13Q1Mal9lgxLLzcEG7YWy6g1A-0psIMjBx1hM-f&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhLC-XrRiS5dzsjbONj1WFx4uZuqhXQTK7--kHR3Z6ioE-yfdM3Bg7wQNBmrGpw20wJMSmzXcRarF4kk0OHlyPN76fJ0QieB_JUatAdUHFta60bdQH6_ggJU0uG0zAMRH7DPzntZhX39OHZigV93PT_SswD85sYkjFY6pW4OTWr_zxK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgXo8hgda7209JkABPbHlq5TRotGfFLC2XjoG3_VJV9AKPSoKMiTkJGC_Vd-GRaqn3xSl7_csxzudx8wtEtwa2I8djMdSV9iVn4jXBJek1YI3RPmC0rikJLe_GDY57H1nQ0Q7bqoNryAxKQz-MBexveMxfVs-w91S9ASQOV5lNel_jz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg9lwLUaVCDqrF4KJa8naPDOfrilt-L1zp5EdCaOlH9G-uamMSNJbiW3w_UGhLDE0jCSmtYtl_2E0_u77ZKiTHfT79CHqiGMtCGWQXOw7Xm6ofuhkxhU17iHQXdAp6-h5-SDtUklGBdnQBvib0nKllVLpvLuPA4r9DeMKkQe-0p3K6G&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFidt0xu5igGB7O_V3CRjNvEqxDxg00CXYXR-9x9EUK-KQsmbqr_jRohsRBhEHrXDafZVuIFJ41XVahKsa1wMywY1nwuuQzNIluqkh7kJ8qL7d-LvlZyqK1weAONRome8dJCYtOmlSrtU9l6mADR2B3KNSQaklQ_jBQAYmePZU0NrT8_&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgK69d-fsvZRhbQayxWfQmWAayBeReuseKsJZFwN7wqspmzRMU1bHD0M70WL2t6cBFeOxkU366cia1Weh4sSm6dXQQkeQWsG3YMAfhPJS8pyh8LgQbakpad-BsY5vp8e6yrO_kNdLAuwsDEetmZotzIQNoYmb-RmroKP5xQLG3CebNJ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjJNUtirkWWKk3ZPEqUkWW0eHHNCIZhemYqaZG9Meg2mUbmfZiITLxkyYQ7hFdPzTtjcrP1FsFxLth83JFEXCeav3aEzYDfZCy2jbsleIGlz0sam26MLrP1pxKvMzjr7xbr_KHNeZ_VKVFy5r0NKsS5hN9rYrCKMEXrVTsalLIBca6M&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Sammie%27s"
    },
    {
      "name": "ATXFOODCO.",
      "address": "517 S Lamar Blvd LOT 1, Austin, TX 78704, USA",
      "opening_hours": "Monday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Tuesday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Wednesday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Thursday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Friday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Saturday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Sunday: 10:00\u202fAM\u2009\u2013\u20094:00\u202fPM",
      "website": "http://atxfoodco.com/",
      "rating": 4.8,
      "num_of_ratings": 400,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgaE1s6g-6KeJm5tvZFpNgcdJ5hIn86DpexjUSYCCvH_-Vvlpk5z0NQ3HHeK0UiC3Z6s-wFuYE6xmVDwSCtcrWjOIrjvyd79g9099bQwGesrZQeNGCj-CO-Ps2YJ8i8hN3R7Ok1YSg4zW0ZQ17lYQTYmsRJuRc_VTxKuajBoKD3z0m0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhdLX8WMlwx_FMKC2BqLnngAv7WOgEDs6L-e-UjURKfZS55xacHy0C7xVfJNpsqS-R7pMXg4Z635mQrWW0Q0aCKPXeDCPGqkHWPSt1pSlLI_P_Fs1SXaKqrl6brPBtGefl7UuZ6CF3P_l8lwwaDUTzpANXxgosagUIZZ87OFMHHttlq&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgDIm6XRbRyDSRdrAffmcmHn1VLGSpUq0FzekqY06ylQppCfS5NgJ0TirVV48ogwzDODRGyS1sFFwnuWe4oJFz0Oa0qR2vQg8xJuY1d-ueNkunlbLHGvtAdrFPQuqn6Im8S65wiK1FHWw1KGSfmHGr3qitpNc34sWqvXgZg8iH8Uisn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiSPBuxq2RcX1cxy7WZsn4qpgCUIKFlraWzVzXXHMUxutaOzmiW7Gg3tj5_F_6qf6-JwK1vMIU4GhjBL6pV_XPGN0NieCofQz5tDtPuyLA2_Le29ceH84tXPnYvmOgQek2jS-1Jdzk7EBxA8eF7WLQkw7-GHBSvoNVZHZC1i9Cd_A2L&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgGEbtY7Pr4Y4xlsxc54ogfxoZ8vCadipSSHFT2mXfgBXepbTgIh3YTM7XXIH8Umlw6RpgVtxx029GAY-bxW9Ge5huAnorGeg_EhMgppw5_TPayWXYegcZVefkD4EVXqYwHqFsAwz0D3gi3NpR0EZmw1QbWXcxD1qEa-GlY8W8fiIx5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj2xyFqdilF1ixCvPf4o1Mbw-7TkIOrJqoQxsmTboY2WEJcIIWFJ-Fo3iPenxvD95eAruwZstWZZ2TOfE6wxI54LfGwKZ7O8otnSgYKQUwJRXSXeo1LAG0GJeUroubyJgV24Xn_DSbJmS93QMf5Pp76SywWaza8kfA1QTDetJ-99CkG&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFipIYwhzrET35X_LQ05wDaLsAXH8McLdKGp_9qtV8Yk-CoF3X3yY3kx69VzCPtNpuzz596ZWneZWXi5eA9YQYlhRpl8KP9gv-2Tri3vgDQDKxIM2KkjaWBihyRbM4NIR_JYUSjt02iPJoxHtq7AvfVU9v2kycwTOSBweVsHcOMq0xne&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiHC-kJ7Yf79NhS84jaHRVUvtlQGPTllfGCWNz9GG2UDyyF-i3XviGZooJoUNVNw6XBy1022e30KPNduleIfiWhOkWsvjplfF4VLnOuEABLccSyO1pBNWTASjMgmxzk-tksb6YzHTctsQVzVoaMqahM4yU6FsUInvlcNq2GmoO-wtDz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhC_9JaawDRoF8aLruxnYDQDQzMII8xd1yqvRTAxDVOcCakpBV6xmNmhgVjQJm5iwc6noAElP2YAbyLTsIOnQXdYmpOzmQQn1_ucs282uYfZyMaHPljWZER5Eh8PnWok9hb-nWYi-6abU0LquWZupD-XXFONcgm5rkB0b2sJItrRw_N&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjVNz_YBOZnLfWfoT7nI696uAvpivm79itwpmqt2CLGLgQLvnIWozMtxSL7pPBj24N9KLB57rbSwl7P5-qwAWRJfV6cRwvGL9Que6hyFU92WvSpgCgiuAkMjklimlS45cPmQQ08X5o8R4oKMym9N2A4KpPxaDQZNLOet7bCQOGleHJd&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=ATXFOODCO."
    },
    {
      "name": "Cucina on 35th",
      "address": "1608 W 35th St, Austin, TX 78703, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Sunday: 11:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://www.cucina35.com/",
      "rating": 4.4,
      "num_of_ratings": 409,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj8u02uck2eZF6Ll4phJOhi8PwEsA0QaNN7P1-I8wfsbGHmpkXAZeerljFqQShGJiqn5a0bI1CMIXPpFb8fHPqt5iTtjjJLWIfhICySp61BwyCTzQ8LulaG2ShO2dW2gVAEVwzcoWWEhsWJqq42mHmuDK9MSEafxl7uPVmdEKtVii8m&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgJq0cbmskG2rFZBbmfEuBL-APggL6A501EPVxWvT-vFD8jJyM4nmqwyzb6-IDbZiwFbgH3xmPveUhZm18ch6XMBAxgUAbnNIu5Mejbind9ICtpd1uUpobPJuTgHcl3zsf7iXKUDpZOZhEHDL5nVSJCwVB_Jvk3MZHvS-K7FXru9_-N&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh15Wb6XHcDTFeyRJUrakcRVDgF4pW3j8Mpqv2moBcnNbxE5zUNCZ_bb-6_hxpuWFb-HuFFF0puyFzYx1DH8dsNvKIYTsod8SVOGEJVWaaprLM6SjXhSDtdyYTdCubi52BCXt3lE5Yu3xP9qjQOGD49JMjUZZfnglPn1j1HUHT-cEvx&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjWc7k_uaSv1BvRByIYK_nYVTfN1Ogl0xxptMtVngi0m38nZn_0HbTJSgKzuI3wFATq_BUSpDGtqVOzb3iA09lJut4cu5lkTxVkN5E4VhiJg_jNewNPY_1aKBDi9-xnl8O-mUzPp_hZuvIYBi9FEMj2wsMZDxPVY9PXZQPeK6m_jxfC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgPLXiW3c79LXtSATTDOR4VH3Tyzjc1xT8Mi2XdeZj71-Sp9ynpGfAnO6vzh_Z28AbuBkX_pftnfXRl8Panja-4oHr7AjALb0bNKdMlhNgckjCq-Xt35ImWpghkFir9ypSMYh7pP_z2yL3sMhS_R8kuUjkf2B3d8yTiHasDVgkO5ezO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg2EIk2aiEWrTMTMwKIOdKfReiG9mt-5hfNM8GoGaCl5VHbUT9eIholT--WwvPZxOuxArmi8nlpA6_iO8ZAcMm9sfteHhaGahmKKEmIbOUMZeufGmSHfLYgVDKcXex8K76TDgYuHPxzpJJlVSDghI0dNXdsYbPY_TFWIh_XdwC4juw7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi7i4iULnZy_FmebytbuvfwBNvHD5eyD1erhwSTPn91G2CJ9Yn17XS8i3Uo9xEJrRWVZEZWPtViCa2YqFkfMLNEAGos-kGz7_rTIykCV5ucyRDe7fEGAiYzOJ4MPmGN32pxKLrXUqnuv4AQC8Xa-PBEVT7pN9tBtzl5wWv12kU50_ZT&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjVke9iSp5o2PxE9UcVzrNvVyF0odztC69goXHww1sZtT5GhVmMfGSEq9XtADYARcXaXQnIQvE9Lq-6fIffVYILeClZKwSl5cpEXsDnNSg8U4Lh4PVdEdGds1dOYjSsIMIR8mDT8xyieljSKwV8B5XETkHVzlU7-FWFXHI1nhsbQI3G&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiOXW7QN6QFu19EQLNGrMlvxbxiJZ-YaOdGN3IvX5znbrFm86TJeFBUdjTFMNbQFsRQxm37lYbh8mq3YCxYRajS1Xc-4wfLtD-0fSgyJLsbF-n8hEshcwelw0jzDWlHskRW-AAYUksuo3SaeW-Au3D0O0CcOYbVfZWv358zTrbP9tO7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjOncQKpRVFcjRTnTe3-e-8GYGLlTPSfXzSFuG7tuib-iOcc12I6d6a-Ymv0pxbwATX4A2QrcyZ_cXXdC8bvYrW-ZnN47q7IIiN1yghHsEJgiFtxokUmkdBW6EN9qaIzc4SdQDtpXJ9Q04a8bRUFSWxQmY9rccORby27n_2Dafyc6_V&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Cucina%20on%2035th"
    },
    {
      "name": "Phoenician Resto Cafe",
      "address": "2909 Guadalupe St, Austin, TX 78705, USA",
      "opening_hours": "Monday: 11:00\u202fAM\u2009\u2013\u20099:45\u202fPM, Tuesday: 11:00\u202fAM\u2009\u2013\u20099:45\u202fPM, Wednesday: 11:00\u202fAM\u2009\u2013\u20099:45\u202fPM, Thursday: 11:00\u202fAM\u2009\u2013\u20099:45\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u20099:45\u202fPM, Saturday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 12:00\u2009\u2013\u20098:45\u202fPM",
      "website": "http://phoenicianrestocafe.net/",
      "rating": 4.6,
      "num_of_ratings": 859,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiy1UUV5OCjtrqBL8jYykJDBvFiokC1LwNuCUR3_gYo7xvb4Y--P_2Z9ip4bKQnenV8SEnIj6R_ncg3UZApzPK0OdCmySKOpl6e89monOHzTnPCzbnQO6IrWLYsd_DrelIY5OMB9VixuAGoC_7JD4q1_FS8mQlCHbvDgqb1PLsr4hEI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFghPGrefQ3DqHqUwUBSKWFfZI0oEJoqNTva51Ss4Q97Yg8XZOXnmn0rBTd3y9YrDc8lSYw-sR94VF0uEus33pmn1nLbyxgM6EBHi-wqEmb9WBuqskW20vYnyeY1eeV4ZZILtBa2tCF2H8AgMEaZBwykJGf2BTl_9V4nvECEaQQmVizs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjuz-awPrGzsgKsCAAJzLUd6t1uQxc69vELWhhIDsPUjjAXKjxfxgTdlnxf_rQ9quZboINJ09nKrn7LeYwwL7teAfhcmDpQ6DPnoF2iWcjhOt-B9miJf6mfpjsoLM-hJjD_eIbv7Ro-i8LYjcJtdYWHY5Xn4HtYL2hj5P03MLJ64ZS0&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgZpZ8BVkmoJAeAfllbjz6ZaphF3_BixAtvGhwe1RgVw7jHXjn05Lb2Mqa7xxeaB4Gyvc_dMWFShljtwL7srx-p3JmiBmD1hnuwiGar5opd84oHXkvyvW7Mb9h7OYzbDWvKpiqNviGxJS9WIKo2vymz5sllui4arSMWPJSN60lBANPD&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhMlXD2TNetDJYRJ9CRKorbDA7DMzWUkNHzAI94UytyitrTKNVoa1PedxG7B5-n_9GB3gsMz6AiB0e6H3HrHyKYEzNOriAjWBmk7-pTjm655w9Lw7nbi9-VDMwuzErnfnCviu5OXq4EYvDEIO7FBzXV6Wnjx78IH7nn8g_TCxSPiitm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh9v3QdlG0Wf_lwK2ztTLxevmq60u-drfgxDzbdyxnD8S2PY9TxLNJtzKeXz_JQGS36TOGZt2D4F3agTeSlcfWeXWtVn51HfPrSkBK96sZNSZtvyOfUKbAS1leHL6wOQm1vbuv2oRBATbeoHtsMJfoW2Lc3mJQlRVR7V-UhaavJFU9W&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjYcZ0f7y0r23yDc5Rby8AvwAJaZYgKiX2-Z26ngT7l2o-Lpmt3yeATxLI8xgJh1zYqjbBjpoAILfqjqCbWBOYB7vmpuwzolKiBV5nmnn5yDs3XqRFf2JkGd6hZg87XcV8QvmhQX4zaZUFn1zH5fpsBtC2KRe9c--EZjhrNTqE-iOEN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgnQ2aZvaCmlDzHYE_fZuN_K3B_QnNjy31CfU4wnN5QNn08yT8cKNrG7ytQwC9HGvmmPmJH6AtSI6StJ6Mr4sQ-_TCNILocO0GDTRs4q35FxkHBWsLIEf7CHk3T_Sv5opliOOE2dfryPURAMif_TUmQTufU-s5raARfrB7Bxgs95pFs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiuH9voga933cSxBUb8r8nk8996pYPtE9dt0Sr8Nc73GoTemzVoZrRC37v0ZBv-HUgkRIWmMFsmnYhBmPoel7vmEfVaWoytCK6XM1TdOgPeD21DrfTDQyZiyx8w5NevrBv9OeTxecNEHgnZ6snEFioPpNBgIgSyqC2w14sDLAD2D8Qm&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgxmpg7QxGDVRGeZInv9HLr-FOO3L3kRWJa2FXNHinoJ5O_r7mj2HGCGmNM07Ddt5fPjskoZXy4z3WkUC8AtOVK-vM_lviotCnbOjARjRPMLutGvPN4uJflaVMk8d7O1AiJei6LmxOWL3BP8DzHeh--sVuDyxuDM6TtYDnYaaoDoeMi&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Phoenician%20Resto%20Cafe"
    },
    {
      "name": "Kerbey Lane Cafe - Mueller",
      "address": "2200 Aldrich St STE 100, Austin, TX 78723, USA",
      "opening_hours": "Monday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Tuesday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Wednesday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Thursday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Friday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM",
      "website": "http://kerbeylanecafe.com/",
      "rating": 4.1,
      "num_of_ratings": 1400,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiEuqd89uldkFj4KPdapYgEL5xLZuutmuE2WO0Dams7Qj2gUncU0mKT-kUAMdtNXNHkzGILr0ostXzFV1psLo975W5-hYMf14hzIJUsW9_NR7E-zD00ciHvPLnsacGIj94kNi9Y6l5oE1R01HV1tMrVCdMkzDkcUIi-y7OD6qNAD1yv&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjvpe7EBmV3YwRaEclMydeG46tA06l8yvBoSyqvQ9reg0zevZ1j_IcDSNetyWvbTno7Sx5nAi5vbq7ZwvldG_cTAfF3_8sDsD8_CSwpL0OCmlDTjzCTQNpivfxyQBNXVokBjDulCA4UwGFBBeJejd8CilpIBzpAgMH5U09lg7aXJTRC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg5AiNAchaankrdbn6wB3Vd7EUeqtk5WFz_P4KZdjiWCUrGWgbjOKa7bVhd5TVHkodsu6f_PDE4p8CYyv7Thoi8QFodlEaUgy6u6-SOKXrubT2eSEJlaHjEbu3LWB8CiMt6WXx-dY97_FD3NF70TmOVIIL2OKk2lnWHiwN7mvgLRmRC&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhm3gAA_rnkznnZYe5Sr_BuY55aSxzNr657MpXQkh6hf_E8gd41TiSrRX56UY_XGpsr2Hk9PtyT4yy4Xoy6C_A-Daiuz4FrqUKxaNPxHZR5DgYD78CoyqFOC7QfAaQqadScLFuY_I1eQlWnbLys-ZNk646xyVWTXEykD6OwTolj0piB&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh_xbCuu8O_DYYcq4kTT-pfGgd0ouJ0hiS6FxS5KoGkszYp7ftvj68eshbbuFTRdxycrwW9bWhr-8_gexmVwrx3EnsauWDYAxBfRxMUEjTC6q8YWSpkkhVAAifEYTwxo5ExXpnE1Kfu3X7WgFGA2rIBXg1aHUbJBw9XUK7f00pL754B&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh14ul1fwhLVPIJXIOYHhNy6I5qD1yjwcnOINLsDsQmpPM3sJHS0z1JbBKdxi2xmasF9MjUIRvCV7x0vnIbYFC83aNMyTlWLG9JMmu4zakvNYToRD6E458HKrW2ZnwL1tAsk7x258dHBn3M69PWiRfKbyTUuSUgdoRvulbSmlH9ykUN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjTRMpu-QwHSYltrWMaKE_sfafgpZS_UQlhCFK7zkZ2bvmd1hyJuryV-nA3vnosdtx7iTEtqeAn5kcZBz4vsCk78umMTPcB-GDv9fOYo70weXREDsVL2m02BtyhXgVuJV1mi6smeniyS9t2CgesCFeNaP51auFXTI8tE2_Hh4mDY4jr&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhY25FtrcasmxKHI4iaL_udWHWaAGSjUaS8RJ0dhsnGmbEc5e_qTlKqSnCiTV2hicgUefy2VZZg1VjCyH8r3bLyplandDt48UinKcIf7-5KmI0hWXgWCpvoSPnOmrhHXaimlIK16J7k3Pr1MbP10O5znG86abxcb2Or_jYfBTDgp_RK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFghjaHjQsV3jvgYdkX55knBoDN7uucU_P_8dVJuCWJGVDbI8JdaC0o_Zsl_Shkj8q78gVQ4zce9-KKL5gD2cjAwW6ygQGOFMsAT6rD4XrbQ9PYeJXra1r76RtGbXQ-7IAy7t1rkycMauHCrqHbqFe1x5EjpklrMzflEGZ9_BK_XPKIw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiz6qxd1AWD8r29bBYSjXYBXYV-vJSs1F4vOeJOTV_ueCiJQKkcAgaBnyc8l_MDYlyYQfLG_ox9_RILaZMnRY-So0c1R4xgvTwtgjhqjGYL71ON9E9U3Qtz1Gj2X9NTFyxkOHQ1K1_ns6CkCAZPzeSGmK8FItpZmDUq7Zc6-Z-OUG5M&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Kerbey%20Lane%20Cafe%20-%20Mueller"
    },
    {
      "name": "Sawyer & Co",
      "address": "4827 E Cesar Chavez St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 7:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Tuesday: 7:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Wednesday: 7:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Thursday: 7:00\u202fAM\u2009\u2013\u20099:00\u202fPM, Friday: 7:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 9:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 9:00\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "https://www.sawyerand.co/",
      "rating": 4.4,
      "num_of_ratings": 2371,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFikE4KJzajW0fHSdmNffw9SG2PzqShc01K8sMplSVEbZ1q5EJkOhX0Iui55aKMCBmU57eUY6TJMHTQaYUNyH5DGkBK3-uconnBN601LFdfDCJZMe2Ks0r6Fglwa5cJG_NDN2TToY27cZt0E59ya_VCTYmaBcD5-1P-KW-t8bc8zZGn4&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiDRLdqznjMgcTAGMHY60KSGzJ_gvo2ztkIUsxOZsOdJDxVHJ7X-Ris_ddvr_VmES9LEIgKZvmLIUfbdcLp6wT2HrtSF0YXpupJbTkM2jGU90iuj540ONybxoBYCbV8p7T1vbxaQ2QoLbK2VHlyf_xCUEgOAhTxWq3yOVXMxIlXtSZn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVg8R3DwWTRusop4jx33v1SoZ8HiBQ1nNwmg8NeQUwVbKu34HPwHUPYTJ5NHXoUzcRFKEi6u13apRh3uQPovI0LlHz2HX5nG5t6tPjjfeM5bUam24fljapia1RnOOHZxkaSHlUOsRNp8RimubRNj5q-6IkmvLxE2bG6XKaUEdz6EHn&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgMVFbraPMJa8eZiQ-5s5q9ELdMyuR5BRAVeLxl04UN8zir8eo6DVn7KVjU105-7FGJegWX4PN6UGPQcV3HSwseh2URrTbG59hdSmyaOPQ7msQ3LvKO2gw7hml39L1DkCdN_2sQOw2yBdp2RN9_KjR6khCavR1dy1fnaqbIxvjwCY5G&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg1EooaUWrDSzhcFqc9zwBhPexJDH0kzIUAYdkdwFJPVzy-O6zOvZttdZtmo--3EjbIGMDeREetLBAxGY2Wb06tTamxI_AnogaY9VajLrwEMLXep2nYb1JCakPbH015JE-5x6ue3N1wRCny1LIiuI4H1Y4UlLLGbJ_h8CCyA3mma5XO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhpOQyh2jjBb7fFbrjYQp6dXwM0prf9-0InxyaExevMhpjppO3FSYEwkktSqxQEcQtN_xlbbrwv3AslbYuWEJDxC4L21KD4FlZSEhDI8Er31G4f-vUwl1hfnwh8iIC04AZXS2g6bj00wdwn6aXcogw1QIIR5EaeveLBbBNfexOqxKoQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiMsRgHZsiod6wf9gzHY23vvM4cLAH-ErLnUg0s8ufSG7v-2H6Dc6iXjgVIULjR-6IwggPBPLgynvdGAz1ARyRXoegmU4SYUwcotl6uFBa9G7Bv9Z4Tr7VThlFpEgBKnel6mA_jUimzm9VQWymqORWKsEdguQ6dMiD-GdpMtAulLnzE&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi7eP6sSf8889wK2ns_c7G7DWiueh4SYR5B79_GYBe3jb6JdhX72Sw3ZMTSpxwCXNcWv0dZxEiBsFqhopLgpbTTPPObhkRa-o5NknwlKIMJpr3zkwqP_be8F6OmJXqr6oOqv_z3beASxJ-zKncsd6qROJBlQAfDSfOZzXSHM1lSKWF7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFifxsNL_DX-V6swbjynsySptUnfH2r1mBeXHsea7n7acws5EInxx1qjb9dWL_RLs1An-Z8x167nUHkn7GRC_YzHQgZms_376gyp4vzjUgxWAD9f6njDPX6s0uUDQhMnQLEw1QGVRi4sSVkbS9zXsQp__e7QBBtyGI5ka6CCinGksfuW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj05iaZ_-qDKMF5wQBPNemS_y5U7Xn0LNlSwaS8wH4KeRBkQZcdjhCFU_NiuDJcWMCaa_JnDygyXZqtGAoaI3vd8m8XzNP3D4CPROxh1OwA5O7WA8wnACQo0ixJkoCwpXMz29odR-6unGrp3iwuQ7GVqPR0VojpsFzjm5R6ebDezLtL&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Sawyer%20%26%20Co"
    },
    {
      "name": "District Kitchen + Cocktails",
      "address": "5900 W Slaughter Ln D, Austin, TX 78749, USA",
      "opening_hours": "Monday: 3:30\u2009\u2013\u20099:00\u202fPM, Tuesday: 3:30\u2009\u2013\u20099:00\u202fPM, Wednesday: 3:30\u2009\u2013\u20099:00\u202fPM, Thursday: 3:30\u2009\u2013\u20099:00\u202fPM, Friday: 11:00\u202fAM\u2009\u2013\u200910:00\u202fPM, Saturday: 10:30\u202fAM\u2009\u2013\u200910:00\u202fPM, Sunday: 10:30\u202fAM\u2009\u2013\u20099:00\u202fPM",
      "website": "http://districtaustin.com/",
      "rating": 4.4,
      "num_of_ratings": 1035,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjbTl_UEaXXzPX1aFd-W2kBBStzxeahBUOQcNOab9peYD8tLVBhBqJ6cYRI0LdQhQ2AIP1_SStp5k9VtBGQxK7IBPyTIQcKxUCbb6sN4l8UF_bgnhWPk-k7dMttSvMkAdeMT2DTY8NhnjxKcavSFMJk_QfPETb1ekviL16HWDGmUYfw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhjDbzCvK5PGwbuxkherI7lPcuaatAL_71Vc1sLkkA34dC7ILSjvomRWJf7H0mrBnZbM-O5r9RxLnOSiWXNHFOtXYcZhirWrbf1ChRHxnLrmEytpGaAw4pYaXtIJYoeIqiklXSeTCjcglz3arXVbsEd-kCRzxlLKa_CMy5Jd1CzeUZj&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjGVcDiT_etou_4VCmHE3TSJJfjotKiKGp0nF8w0L8cHkOoSDQ_3ZlhTOTROvz4xy7vnkejBDwmr0l7vfxEGFWe0LQkPsI3rutTIRBOQh9Nudx6PmAgUZm-v1kT3fMtbgNLzLPTJENTgLw8s6-K1RQZ63nYQ9WGFgZP7KuyoaAuiFFE&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhBllz-FRSCNsPEYiKle4wZtAkkWE1MIQQR7RDRYyf5dUonf07okqgz3_zAVv9qhFPNel33gmzEqkXw7qEP1-1paQAKwJJ2RHkC8HUaRMhl_rAz1qcbODoqWeLxnyUoaOPVwNkoRNtMprZA0YTS6J-ZpigK2jNeFFy-NUZMdpctLaED&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi08_gj2zOhWgLsWnGmSxHXh4RQxmv8v9h6xy8AEqomFr_qgMAxfthWej9SxVa-hP8Z5K6k8EDIqTREDwQb5CyuKSOFUB5Q7dffbQmX8hh-QnTGLURCR--Oevt_POExISLw6RlBdF7noCo-nCds4i-8uroWgVDLdH595oUXgAFKbBBw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgcU61193KHGYf7dnulhEUoQfrDlPUPs0zbf3h3x2Rx67UukQk1TQDdMhINuDaIRkneZctavYgpI3VwG0qSDI4qVGcj5FEHObqLaQo_H1TI4e3xg5h3n4_hlXeASbjVV99zlSdxY5B_OlUHO_IOWRAIPE7_3g7LQSasphYHiZXk0Avq&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiGuboTJAroy1Q9GtUtpb1aufFQdIB56v_PDBax-SxZyeyo7sw_G2Gki6NvUYkSZU30Ewas6UXxqfTEcooIpSMFJA6kVJPq_74WC2tib_-66IYiTAh624pHxoL8jUID9ieAGFk_T0l870Z7cBEOaDYjePYOMZUhmbE4RNaNDGXTREFK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgEkRyo1TyIAa33oNH-Gizq1MPTiOZaAWVfJIYlodQ6DkA7M1ZlDXbi-qOLwBec3FUrxAVVt96XII2N7DzBywebIrm88p-BVPh1aanFpvgZKbdTc4pmw8m8NHirxD97At-iCGpm3fOwAatg39Ic9cTdgtoymb0PMn3EVlZLXYay2qvz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj_93rw1-EaihqhY6B93WvNjtAPWVhn3Un7P4OI86GG9NK7lSIawwsQ5k3JBncFvsrSrc2BsjG7DWzPw5DRODi3swa4fcKu-x6R2HqvFq8BtZWtDDQ4H78Xx-rjS4sB3DnUzx0a8Ntb84nuEv2vDSsV-q-qmCm3b-BYi-l-gf573tyE&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVqGG5ckX1ZXkrAyMmzMfwjA4OkJh55iB7UIlLfJMbVqV-p6i9XNVMJjAgh6Yq6q-E1e9a_8ImUSzJc-ThxB-t381qx_7dGchcGuCCmtlg4S3T5-CvAb7TAUa61uylRJeXZdr2SSGtu3BXOBRwJ1ApKg6kPrrCZwoeuv-tNhn_Wazx&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=District%20Kitchen%20%2B%20Cocktails"
    },
    {
      "name": "HAM BONZ Soul Food",
      "address": "5610 N Interstate Hwy 35, Austin, TX 78751, USA",
      "opening_hours": "N, /, A",
      "website": "",
      "rating": 4.1,
      "num_of_ratings": 15,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgsNbPPHUqd7LEuNgLnwxiZjJEuOYMuPRoxVfPaok91nGli1BSupH9RKkZflW-lWEJPwxPwVyKwGrDCvHtV_P-p9Wh28XybwN9_ZrrttAltJWOau6rQHJMAhWr0sVhxybcgVj8AiWsDQCXnfQ5adklCvIVJSdJ-UH5-tiLDxOp2yFaK&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgX1SbHd76uI43Fww-zY2ihgb3xls6rZkUWP9Ks5IWZ__1vfUCqD23lCw4OB1_ClPd8PFu7VGrbNnSjpEvgqwgAzlweFLkx3c9aFWcBNTtiUrbJd77tET8ZQpjAFUtbWOUikewpZXium0f9IPXv_oxjenegucFtK_WTUMn2ClqRPCFo&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=HAM%20BONZ%20Soul%20Food"
    },
    {
      "name": "Casa Marianella",
      "address": "821 Gunter St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://www.casamarianella.org/",
      "rating": 4.6,
      "num_of_ratings": 183,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhMMwnO2muddNiypVhzidEQ-LSB5rCdPUfKVMnhN9dt1SpC-0VjCuQ-ooYcZzfd4dC07gyLrrKakpmYiINkcsJGCbaNlOnpfrRB_hgww7rW0ctFKeZRNtXXDejsMRxQ_p2TDU-X2V_F4hURsmKs57Yq8byyO4D9GT2JtsVJwCwPg5Iq&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFirm0mB9WLsV_tUSnuODnvCOvl0ovBVmyDgr5ncZP4dq136v4jipcuBbp0GRJJqAVc2ZAiLrUvkFfw7tKcB32EMLdIcBc6Xp5fgb3CJ50F3PmtySGbk1lvkZ6edDLLvi5De9ZAcx-6uOFANtgGPSIWYUFW1TTzkSHHh0RuInHnl3Y2x&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhStbYGP8A1sS7AgdvlM3q9VNoc86WeO47iwDWxhTv0oQVb_EsJU58aZzuH8Evc718bM3fairx-WliB9oqSzu4ltC18ZkOXOaKGiqQouQQAhPnW0yavb_qtMT2rcat9Apjpf1zN6891ttTsuYCyYm9oE05jqKS-0rO8I2GsblSc3wCe&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhui2r4XgjCkzRG6uyIMwSW-Q-gMzFRDd3UuZ5xW3Mm3vbKsu-s4vGX-QeBvX4A1meIZiBqYbwM_XA9Ex03wzd2vHDatY1wf0m0UCHxsrPT5EgfxUk7FUPQZxYIXnmlDueLkeEtLnuG8ZlzcFNtHXKjvl-1_2Jz5tyQ9V50FyplneFp&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiTG5ENsWihKSsqPni1iaz0FgkDIw036T4Dbl5VToQ5FuhDBI8uyrcpIq2WGDVkbzT2Io6Agi72jFO3R6xKI9vjUiXeecC_ocl_yQOXmkNUV1oye2prrDG6a-UoQsxRRQHuBwF9leXmGt9ND9tA_6aZFuam8zezZ9lGIid3AHshCHgA&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh-i6khOUJKZLk4jDxmUErOLdFDVvBybeXD45m8e_E6FzgojDmpFW5t71ytk3nfpbbyc6x0IRhETIn4lNW6_n0G8ekude8bV-TeCJWtviLq_0QDR-WtPyDuwWdvGhJWxKPEpwlZdARSB5Jwft_VkzJIhndmMrRolAxg_R9xCxqBW5Jt&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiaSV60U166QI97qLlaeEjFHsgD9-iP05Ih7wmH6lBGQI6PDDB8KArK_yHvo7NJtxY7vsxHTrGKny8PDACwrh5ePhF0_kpU6KL3yQtxiG34pJm_Stfyvksr-487PXeg72JhGlSOSVj1AzL9hWOV_jkP0mSlwNT13vvUwoS-YKzOEb6U&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgHcX3jDGMcI_j0Yvx2Zjx13r1pIWzNwknBI6w0LYjAZHRbR2Lp6EB5LyR-mAXjt1mQgKNjbMnANCcmFSmbW4WD3wxSnl5e2bWmJa5B22KnPsadUgj5axwBzLOHV23_wMeKUWBqvnshS8UPsEo2FKlUGsVrcJELwYsZj4W4IyiDmHgw&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgxCVw5PPMQb-WlQYn71sVa3BmHiz-qbbkC4XGnhIz9SUxJsOsdhZ2Txc-2Z8y88qK8L3i7hycadk-xR4FvriSkz31EEoFUBQXtKs4ruAbnpC8d4eIdALJEAn1CKBy8uPLgZ3Oj7a5k8y-IHoAvgVFSoirENljv6dwPRQzvvnxLG8zl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjFXrayF4rBYjCLKPfTwaORsTYabf0MUAExu0AKcS-FZO_QlriKDGZ_E9oCv39ItBUVAS7P2kxzL_sb0Cu8gQCJAPfY5t21TLoVgpOMdMivd12Lj_Axn-YpvD7qdNwkuoELAerK9t_dur-no31si95WyquXtSWTgOV3_SGITWPR271b&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Casa%20Marianella"
    },
    {
      "name": "Green Doors",
      "address": "1503 S I-35 Frontage Rd, Austin, TX 78741, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Wednesday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Thursday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Friday: 9:00\u202fAM\u2009\u2013\u20095:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "http://www.greendoors.org/",
      "rating": 4.2,
      "num_of_ratings": 19,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgVK3Kj4iu00UIpRxL4009n2QQc-UeF86ESSxSXSAMcLVQOeolCSXKTkHd7PwWaZig-n9eIX1eCPmquyZ25x63L6gYKT_LWGjGjODkcmjGuZWOqqwMVcVr-xIaLg0_487_1-vnbreET3o4cazErXGTHuhrmwaSghI6bZpYf2oQHHCHI&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjlmPJR_e6KlLAloZOW5fhK6arzRDG72xVTw0ndAW3ZDUIwamVn9PlpJA_fqA9JvaR-Ae_ug1YqD93dSoKcxMgfJx6-Iani_muTy7SNV9KpC92nUB-bkLI3x7x7mdnx7f9rudF1tAdYTlwrsQdn3Ny1QKD5eIlLx7FKkq-gbrDmmqeA&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh8NIDR98xCtudEzgrWxGH8-r4S5lSYrpc-rvXnUJPth7oMt2G8QT_oM2H_Qhr-ooElTxCbehbEq6umtLhQfYcMcGtqNtNJXuccYwmKaFywrs2IV4Bvo5eZKBfeCtkj9frrUJuQDTSlL0ZLgUrcXtrI4vMUCU5whruyzITv2y5Z4Dfe&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFia1ubbSg93wuZNInHORY5PMDdArlRvz8I5XzRCg86D1F0AuALBdYl7GB8uZqxR0yA0Znjs7BuqhgtkUT276FYpWXBVK9DDRhxh3apHS6UHEH28fCPFctxMA7kYzj8FuAzS-rqcxspXYRCuo7fANIvaiNrm2Tn47Hh_nllznuY3d-2u&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Green%20Doors"
    },
    {
      "name": "Assistance League of Austin",
      "address": "4901 Burnet Rd, Austin, TX 78756, USA",
      "opening_hours": "Monday: Closed, Tuesday: Closed, Wednesday: 10:00\u202fAM\u2009\u2013\u20093:00\u202fPM, Thursday: 10:00\u202fAM\u2009\u2013\u20093:00\u202fPM, Friday: 10:00\u202fAM\u2009\u2013\u20093:00\u202fPM, Saturday: 10:00\u202fAM\u2009\u2013\u20093:00\u202fPM, Sunday: Closed",
      "website": "https://www.assistanceleague.org/austin/thrift-shop/",
      "rating": 4.7,
      "num_of_ratings": 79,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFje-4emXL4HoDs2eo8NDrjW9IPOnEH2ZdgKNI3i0UQ7054bfCmpqlFIhJdrKdJHbG2QJ4gxAoQyVLoxaVBB6pkihk8evn04IJxVY0B06DWZJg7tB0y6Tt-rBTLvqImx2M1UvHNNSTxJuCH_XXZgucMcej591rmcSa1UX-uTITe-Hehk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi97n36y_O9i13V0KzHxvdlP0bh751L0w7Nlj9DzWs3L4Trn_v4VHoi4OlaZ4l8Nug3aUvvpSgm2aBF5gryhjhIIchCVvlJChPDhdxTAXmY1chXcr7TD_V6BHk64mKslHbmwHtC1lWMxyb9jJAlyHmgPkYbmOcwaDFKQRWnvdzGXroO&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhZ1xH9SuQcUDXHPJTTVub2Nc5orsgQF6NZK1a1x-0Nt-Xie1WvqOva9c7LwDsWJXfHGH3MYPmsP0aYK3BS37cNC4RcHLudI87hyCtWcBdV0xT8ostRO97BrLb1V_4DAbvAJKXcUAPjWl-C5LZ5X-jbDioV4Ybk6NFINUSpQeZcyji2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjHdnf-syjkiRkHPeGy50gK1GFROttURUzathI-9iu65fStQbKZpSdjhQAGHqjMPQxfDlfRPdQwHT1MY--8gYNKPQ3_VCXleZk55-coRI3Vyda730jHHlz7ltqNvHdZOIwp-jJEEOn7XMEcdTv7-6p_g6CDCU-ULPkOUNt6JlIjTeCQ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj7GbncBvebHlJS6OYoG-KNJsOw_pvWgvyuhhUm-JC-h92nMjXbE3iPUr5BFwr8y4DKCF-bR2c_nm29A2WwuprjLi0Y8KZ7gX7UPBaY9wuRugF2bR9HPJldVE5Y9eZwOVvBRY6qvCXxHWmVz2XetlHtym0z1GEVwUbebMafkVlEVR52&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhXpkV8yqisltbuw9P7XL6WN2CFNK9D30Ge-rPZpxXPOdR4Av8Ta5ClXd8ANiuE2wO1dfNQXdduNEvN204UH3-iN3Mh3UKVixoyBjgVTYI0b1ZLJPextKzHWgfEk2SjSYPRuYtgefuKzO53gi62yByYx4o2XgRzhYQFLUh6oTaUrPv6&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiqw63JL5lBL6NIiNDuQAKxuSAx53Xx-Ltur3CmsmNomy2XgF9JbVW-uU-e0wpU2a7nL2JboM4qNjGwwWRWkCjTvn1B4y95DesAhgKu0GH3aALeYc6TAk-JN4mb6rMYU1smdDGOZJHsZCtaoHV8U1AivlcIwaVr0VA6c8DGO_hiOK6v&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFif_pHhtRXxAnwvG6lySBO-p72XZUQOBWU4uYsdXeCoa7GsJ2p4zbwWhigpsz_HOOQJpudQKsubZN0_W4st-eeJk9Y6-490M1AhZdgJg38IovrCctMHS0YTtVv3Il9yn2LXtug1CHCcpdhGKmkrMHocXIsX10-uHN_sh9iIMAUeIUov&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjFIuoXmUcIaf1YJ40VLBJNerxguWV9Nd76zzcdGoNGplNbGagqfinT2u1QtgXbYk3PeGog2FDE54y-8Pn2ssnhO66Xpit2kGtVYuBBYYUTbzVKpPfTvijB-4MauU5FoJ_kScz_x90udcx8y5jF-bPKYehMiB1hhxy1KAjdHqpUkleJ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjxsPdO-YUrZnNRkALQyMaqAKlS90NRTSvysYfuWRNaeqdS1kZDTjOGfRx-dzD6gAIqR1Ts0aMYPoXTMZPDsFXoY6CfCmiindELb64Aws70gkhEfQzdi5MoPqvAeK0I-TQFBPceQpC9JtZBFJcxQJzOiYygch7T5iOWxp5sGezBmjOb&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Assistance%20League%20of%20Austin"
    },
    {
      "name": "Front Steps",
      "address": "4507 N Interstate Hwy 35, Austin, TX 78722, USA",
      "opening_hours": "Monday: 8:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Tuesday: 8:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Wednesday: 8:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Thursday: 8:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Friday: 8:00\u202fAM\u2009\u2013\u20094:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://frontsteps.org/",
      "rating": 3.0,
      "num_of_ratings": 4,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhvEzGqp5NBJY6Ylwr4r2hSeYKYod8I6NaLT5HpLJK2-pGoeGngTYh-b6TS7ydi3ewhz-aNoUIx-CVrBErjwWkv3HAwziz_OXjj1hMEeHJlwpYx1Kkuv9hbqsvPKOVXVAlal9xDNEWjzs7syCPWONXbN8Jba4SVClPRJZxHK6vxrwUZ&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Front%20Steps"
    },
    {
      "name": "College Houses Cooperatives",
      "address": "1906 Pearl St, Austin, TX 78705, USA",
      "opening_hours": "Monday: 12:00\u2009\u2013\u20096:00\u202fPM, Tuesday: 12:00\u2009\u2013\u20096:00\u202fPM, Wednesday: 12:00\u2009\u2013\u20096:00\u202fPM, Thursday: 12:00\u2009\u2013\u20096:00\u202fPM, Friday: 12:00\u2009\u2013\u20096:00\u202fPM, Saturday: Closed, Sunday: Closed",
      "website": "https://collegehouses.org/",
      "rating": 3.9,
      "num_of_ratings": 27,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhrt00nhYrrmV4rMOn2DwqAISWyeLvyGS-3I_I7_ZWJp_PBye1bPihE2tjKv3NW1j-AKtRkM9ZOr7ixLRanjGVZzHRBPHs2ziQRJUTv16fqqqkOShJ-q0l1QwetmpXkJXT7KLyJPTSUklou2MGzL2g1qAVIvWZP_FV57OXlqlSE27hg&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFiKNpfu_3TkIL3A0hlEaE1y0GQcmeNB-RltQ43-3nP4bnl6HaoljQQSJP_SM9442QGIPlItwo__ct8IG7vtuJWGXOpYpvxws5Fx5lWLcW6pcJWo3GCY2ZNhesO5L0B8dG6hjpujCnZf_XZu59FSXDIO7huCTR4q63CMgJEc3NnPTNyz&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgT_iPr9159WjevfR7JYW9e6pkLubR7-MWJ7F-6CPyo3mrlE4jC5tfdS0qpISWZH36oDOea6o_ysTThEjFJqXPK5SO2XOPKgqOUbypqXNq--lGJFWivLp0UVlfLmlyEnxBvJwBcHa7pqOlwEE3KAzJ3c8_bMaMHns45q6D-i6bJy5tx&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh5BZwgsPMG2TmFqViKMzmbPx0K_G8TLKpXXVuzR6Ke_2i7aaF7-xOAhh_M01HvzqqEmfsk_pZ7pN9FGeQeLzxvbGQJrSz4EBWXO7lHYmO7IM861mXqRpYSAQewlhQ3fT6tlIDepyeMygFFyma_kIYrhQiC4LWEGHJY5Y5Ng33cby-C&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhblJpvj5zWimVrfI_GoWKG-YPeIXBxLi-E4lRGaQNq8GF-NYZV0IMR9bCd1-zH7ZnlyIKygmP8kuUlU7oQhB6C5DqAjlbzwBHNqxQh1Ia6inGL6u7MLYGk65sBOttLrS3TLPP0VcfBwWWRk9GSZul7Z2RamIMIG_sNd_tRSHIothnN&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFg9C2TRALG0cUKm9yzCvNp5yKZrkJylLc6-xSlZLUhxkxECsg3pkpYu_gWQlV581RLPfc6IDz-Sa3P_11vBCCTZH92IL1WNZ0Yqt1SDUUVnXxJSrT7Euzjp2HvDHlWwsJ7ykPEMs-Db7v0SRkCn9jDkfgEtgv90cEx4jegrwPdhvbu7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjapwGtE9Ew5uB9lWC6fKgN_4pHRSEIbI9-PSjUixESJSmF3cZE5kp4HiyaGK4Mkl6imPVqoWc0EvZOv2ZOCKax_Eb8TsBZjfmGdlzsenDlwkIVS-YguC15A8qSFW_gEcFdaSUZKRrpzGg2uZAZjYKjeVfdOss7ajAO8kz3l4WTuWcS&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgeiKUxuMuY4Zb1wdPQ1vKmPEMQnpLeGkrlPkzywAfEyJYj2pCQLplKqomQXC4ptMe-Os43jzSHjdZKq6lwElQvIJoIEfpTIEXFrEMyYXwUcwNoonHtRHZ50c7KSxKdTJYPxDULC2PQiZVDVmUhISfwaD8fPfr4UZNNZDskcJQiLtkM&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjS2mAL0fcSngfxbEbCv0RrqmDlA_Uo53fPsjuSwdNwAVDNoZf7fFYJLEBmWoRuVaKsaiaeIioKLP0da913NU2jNo_tdZIsrnr6SC8TXsDXxt0CVklCOF3JWPiNTXcX79mux-qZCWg6Em1m0wcCYIFDW4NPSv7qew60GJjyylKSpsLk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi53SLpFL0aCnUWRW4ZzkX8wlwvLoMYhGUE-lxmZjuw4EX_0Ip1O2rvbBmhqhsS1_h-ZdfJ7F5lraiCyyEHWQwBlFq0a05KxocnaonpKhhuhra7b3d5PeXFE3D-pU285GJvHHxnFAt-iK2pnga5ofIFRWUhOIggtjVPVPvVmeAs7Xx0&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=College%20Houses%20Cooperatives"
    },
    {
      "name": "Industry - Eastside",
      "address": "1211 E 5th St. Ste 150, Austin, TX 78702, USA",
      "opening_hours": "Monday: 9:00\u202fAM\u2009\u2013\u200911:00\u202fPM, Tuesday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fAM, Wednesday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fAM, Thursday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fAM, Friday: 9:00\u202fAM\u2009\u2013\u20091:00\u202fAM, Saturday: 10:00\u202fAM\u2009\u2013\u20091:00\u202fAM, Sunday: 10:00\u202fAM\u2009\u2013\u200911:00\u202fPM",
      "website": "https://www.industryaustintx.com/",
      "rating": 4.2,
      "num_of_ratings": 290,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgpdQYrBeuBeb-SFjVW2mFkknM-CnNwiyc84ayM5kJAQIYXgdOTg8uCBx1I1sUzf2sJDdxAXp9A2IAvVC_Otj5TBEhwcoJUXmt5IFezCFa5enru0kg2ZB4LP03btE__Y3wuBM04vuK1GDXaJ2w3tqzrLr3Lvb8JaTPdh7N-3ATT478k&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgCyuL8KUUb_KIy1fO22Lr7hApZWtCpTl-0-4G0whpLxSRMCl0QfKp3keo5FweyjFtoqSjEM3YkB5hMs0nmKu-5lIXKqqF2oOGrZEddwNnl95q3ztM8r4Du7ULy-kqvYsrO4akcnj1XtYokxhbYiFkWm6l1XJGgdRJOMOK6Sy-Pueu8&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjL2bP243QSjuJX34mGI6Yi94stxbb1H12bZUims9H3xRv7uqxom0j4itYFWvo27uuSHhHxEHOeHcp1VYh_UvcvP0NUNhfBW8JGF6BSfJ6VIAoOl45CbXQC2mnrzlwKM7N8asm-I92mtcK1QSk-Yqznc5CM6b1D68MxTJzHFxDzVXgZ&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjfzULrOoKMjeCvinvGp8E-CtrWtgDab2MtAqCUWfL-vxRpopDdI0SXO4B57KG_kjRZELkmKQWSaikip6qmsoWjpnFRUJ7NpDn76Aar-dHAQxb--oEQcfCyX2R6Im6leYIh7fRmZ6_4L_oZyUfDMUBl2PG5w_pUvpQNKuvrfMcO_CXp&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgYTHxf9nrbzI_btJKDPKxVSJt7FcYAaCkVFe_yrv4alEhcZdcPRjh2GpgINibPn8Znuz-fHZ2K2pNrKjUqAHCiERkUvqq1PJY90HveRgzolLvNoU9gl5KKwYLAArb96F91BuyXXp8AEO9h06CYF2FgO-2x-xrfcenNHHKKbdiKZSnl&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjSPnyg5OHU3bunLYgm5DkAOdjJo8aUUOD2ddIddCT7-QEXNfsIxJ2RvttM3Uzz7mprvzB7PlFa3dW-SysbscRz7bGaMK6EIr8boSRVxj7VJ4r-6M2ZoKJk9L7eSoeNBIWMDugCv_ImxD_7_BBoh5TrzP49ycd0S1_BOyKhPgjXcQKs&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj876LYnyNP_CEB0EL9iBE9HfKwhcvBg1SX9zLkcEsc7wOMDdGcmrxZk0OzYuykeZlHub28XIx8hPkDdDqyW6JZnC8pS7msSlnqL_VKulsMe73XHWLhkeWAMIMaAApT2cRk2Wk6mizRGCCkqTEeasqphUCBmWcYFUj0ffdkuVDjDheW&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhAtOoTRrhe5ROlZP9im61vUhZfqOjm3TNMDYAVXcSDMd1dnl9m_Erpr0_SnxEaaGggWx_R4NwQ6CaqNjMeQJrczTlBr3XoTCgF340fuZJJYuenxvQUyt6b6b2ILUnPIdNPZM4ApjdT2gblHtHsvPbYtimCQ68J_z7ei53lHZsre5cq&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhuNv9Ye0_ijkmWGc-zrmBSio9UpX1yW0XSr9i0MtJZDQmc44OOAxn5ZI5H501MKlz4gV3s3xKa6CbACQ0sJXFFl04pFEUQLOUxmerlSieeFmg-kIkE6lmD2IQ5V0z-ruJwx63rhRtWtc1dDVXRv1BjUvO-WNdZmhQF9whn_gWnWij5&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFi6_wL4sUaNLkGqWocgHCTda2Pn6RF5Mf0OO5ougVRzMT29e3LOrFLmTowo5KbKdfYE_qnnjvCk2dZuPtG4Do-5JyhiuomDdLHngvklBCYjH1JuDTuEMCPD0v5pOCqtlTa4H-7MRg2yhl5txAHrUyLWObTwYXkx0JIhkTgDI4aRc_p_&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Industry%20-%20Eastside"
    },
    {
      "name": "Treasure City Thrift",
      "address": "2142 E 7th St, Austin, TX 78702, USA",
      "opening_hours": "Monday: 1:00\u2009\u2013\u20097:00\u202fPM, Tuesday: 1:00\u2009\u2013\u20097:00\u202fPM, Wednesday: 1:00\u2009\u2013\u20097:00\u202fPM, Thursday: 1:00\u2009\u2013\u20097:00\u202fPM, Friday: 1:00\u2009\u2013\u20097:00\u202fPM, Saturday: 1:00\u2009\u2013\u20097:00\u202fPM, Sunday: 1:00\u2009\u2013\u20097:00\u202fPM",
      "website": "http://treasurecitythrift.org/",
      "rating": 4.3,
      "num_of_ratings": 188,
      "photo_urls": [
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgHMdkdXVu-1ubE9SapA9vI6iOGCiVCcmWUrLK7mijQTqbkBHWAf780Tc_DHbD0W0tOaVBmw3i7CVtaJqgfst4wkwA-tpqfCTNdpo91_f-7oX0yDNm51BeEo3bE_SCR_wtTXK6ygOxUMHBxLshJ4cCRDxUnTZWYs4SmRDhkS9Jmobcq&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFh1uz1xagme--VY8jGK2Pov0iE14uRikVQ_1E8lbJ1EmGDsQYFJRIAJjmC4UwBYcvNjDpdXHy9W6aGsmcx3ynHc0hl947pCufLHJx--Z6G5hqmTAtdoj95PxdiyDWRrApetp7H13dITw9M1UfiGpSWjm9Txyj47Drs-fMAh0IAwsSAk&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjcy1vTq7qVvC6ICAKPVWx6mMKeEc088K7XwmfnpRLrxx_Mej-jL3Jv3RTeVU-NW1_rzft80MOIftNNC2OevKb9d6jyXGO5_HTthmBPb9TFpntrq2Ftwpg0ATd1snvUFPiaC-KsM9RbnitbJZ3CxW1bDFhsfioqzJMMd-uDNsOG-m6l&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhmSdry9dM0o6zafjd-_scUHC9460VL1OQlAqMY-QEFzQv4R597dtLrcg8B7a16ONLm6Oj9m1SIQnD2AgajRBvVbJyhJ41QArN50stwCD2clBm3ANG3azA3EKsphJnDnnTi72cTHaNUBl3XPxhSse6BAGXY-ffQY0WgDpBofBNq4heg&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFhj9xM9w1hwi5HgM9MyEqyzGU_uH4liaRVAm6gp-gt4vAKO2zcfhGSpQJYL8FO5a3uzrYI_2Uh9YJs3BJENtNmRCom-FJhw04kcl87hd1Ru0GKmWIPxEDR5o6qFrjX_IjDYw60OJ2ShF1QqQnr5c_EOLh-y8ZLqg-QGK7iH2clqCoR7&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFgXkhd2IsqL_0Pr1P6a2OS3iewbd1JJmdLsrdCXqt3daOCe49d6aZjL-uSo2QuNSW_ayiJUtGXwhQc5DAInSpE8SNJSqSispr2p2pCNSRIGRY7QqZG7WG3ZHYQssO0c_ToeveEhhyAtz_GKjGiaPuQzXk5vr__fYuFz-9Uq_hepso_o&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFguc4wrULEhfQlT8Y8N29NzmYh1Ms0WYSMwP3xHU8OTJot2bTVpRHYJFX03lMSU-uH5MErU8ZzyaDubrNSNFKyaR8ReoQXnh_zYJqXKcCsAFGmA253HazKURYZp7rUSet3vQUMBLWP2VE1kCtsdcxqi0MVdhE4Yl8_JLlljcCG-JZsS&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjgSOs3h73LeT3N-vxAxiAW6MUxJOL4qHCc4EUYvC2JvsbcNY5eOdRo_JD16RZevttdSz49YEEzDq9nkstmFwAzitQvFF02iZZ2TPTkqdHEVpCvcgYI5bhVIwf1JRU5bJadyDqtXsMgjdY3W1jH_AD8yddLKRG2hvNG3-la-BgK2JGi&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFjM_XUyQN2eHL534n6tEia4GeA69xTAFf0s4jLWZY-pUcmVcTNNK0stmMZODfEb-YZh8B4mkG7zr5ac-g5Ky2DiTk2uNj-XHxWqhbOgziyCOBs5n961D6-WPjxyyWGhQtSWj4pVChlGkpcEEeMMsdn1x7N-rQv7ohFS61IFvKQ9WQv2&maxheight=200",
        "https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDoGuXljL4WXyzJFG5-6kZMe5Yt8VCksOE&photoreference=AWU5eFj8vlqyAD6gbYCmRoaGI-rmrcj1OeTiHIB9E3cfj6ShRO8hdZmc-2G8C-Dgmd2F8hEl0-auR6X1Y55Mbb9etre_-l7p9JPfiZJI6v0Z5TQ2Jxf2gg5YdZsFtAtWliTqOtYAUqPGPsj5bD1GNMtZ7veVsjEVQoFPu1c_uNHgtkb1B2_y&maxheight=200"
      ],
      "map_urls": "https://www.google.com/maps/search/?api=1&query=Treasure%20City%20Thrift"
    }
  ];