export const housing_data = [
    {
      "name": "Taos Cooperative",
      "address": "2612 Guadalupe St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.collegehouses.org/listings/taos",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29178211,-97.74158045&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2612%20Guadalupe%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Super Co-op (Laurel, Halstead, Nueces)",
      "address": "1905 Nueces Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://collegehouses.org/listings/nueces/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28319931,-97.74389648&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1905%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Pearl Street Cooperative",
      "address": "2000 Pearl St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.collegehouses.org/listings/pearl",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28400513,-97.74714366&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2000%20Pearl%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Block on Leon",
      "address": "2510 Leon Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.theblockwestcampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29030037,-97.74949646&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2510%20Leon%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Broadstone at the Lake",
      "address": "422 W Riverside Drive, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.259685,-97.748747&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=422%20W%20Riverside%20Drive%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Opsis Cooperative",
      "address": "1906 Pearl St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.collegehouses.org/listings/opsis",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28380734,-97.74702367&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1906%20Pearl%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "2401 Longview",
      "address": "2401 Longview St, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://2401longview.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28879929,-97.75050354&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2401%20Longview%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Pathways at Chalmers Courts-East",
      "address": "300 Chicon Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/residents/rad/chalmers/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.258853,-97.725872&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=300%20Chicon%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Alma Apartments",
      "address": "9220 N Interstate Highway 35, Austin, Texas 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.almaaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35812098,-97.68985208&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9220%20N%20Interstate%20Highway%2035%2C%20Austin%2C%20Texas%2078753"
    },
    {
      "name": "South Shore Disctrict Apartments",
      "address": "1333 Shore District Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.southshoredistrict.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24329948,-97.72720337&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1333%20Shore%20District%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Aspire at Onion Creek",
      "address": "2333 Cascades Avenue, Austin, TX 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.aspireonioncreek.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.1318918,-97.79457465&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2333%20Cascades%20Avenue%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "Block on 25th East",
      "address": "702 W 25th Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/the-block",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28957953,-97.74515909&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=702%20W%2025th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Woolridge Hall",
      "address": "2400 Nueces Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.2400nuecesapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28840065,-97.74340057&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2400%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Karnes House (fka University Garden Apartments)",
      "address": "2222 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://quartersoncampus.com/karnes-house/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28685336,-97.74501122&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2222%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Aspen Heights",
      "address": "805 Nueces Street, Austin, TX 78701",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.ahpliving.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27182,-97.747636&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=805%20Nueces%20Street%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "River Valley Apartments",
      "address": "810 E Slaughter Lane, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.rivervalleyapt.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.15999985,-97.77619934&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=810%20E%20Slaughter%20Lane%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Booker T. Washington Terraces",
      "address": "905 Bedford St, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/booker-t-washington-terraces/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27169991,-97.70770264&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=905%20Bedford%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Nine at Rio",
      "address": "2100 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.the9rio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.284272,-97.745293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2100%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Bainbridge Villas",
      "address": "3603 Southridge Drive, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.bainbridgevillas.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.234944,-97.776279&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3603%20Southridge%20Drive%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Stony Creek Apartments",
      "address": "4911 Manchaca Road, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.rentstonycreek.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22179985,-97.79260254&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4911%20Manchaca%20Road%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Yellowstone Apartments",
      "address": "1215 E 52nd Street, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://yellowstoneap.startlogic.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30859947,-97.70659637&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1215%20E%2052nd%20Street%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "The Villages at Fiskville",
      "address": "10127 Middle Fiskville Road, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "villagesatfiskville.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36454971,-97.68115895&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10127%20Middle%20Fiskville%20Road%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Spring Valley Townhomes",
      "address": "2402 E WILLIAM CANNON DR, Austin, TX 78744",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18555,-97.75675&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2402%20E%20WILLIAM%20CANNON%20DR%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Villas on Sixth",
      "address": "2011 E 6th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.villasonsixthapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2603843,-97.71979463&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2011%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Moontower",
      "address": "2204 San Antonio Street, Austin, TX N/A",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285646,-97.743134&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2204%20San%20Antonio%20Street%2C%20Austin%2C%20TX%20N/A"
    },
    {
      "name": "Village Christian Apartments",
      "address": "7925 Rockwood lane, Austin, TX 78757",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.villagechristianaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36039925,-97.7358017&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7925%20Rockwood%20lane%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "Villas on Sixth",
      "address": "2011 E 6th St, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.villasonsixthapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26070023,-97.72019958&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2011%20E%206th%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Aldrich 51",
      "address": "2604 Aldrich Street, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://aldrich51.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30439949,-97.70189667&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2604%20Aldrich%20Street%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "NORTHGATE WEST APARTMENTS",
      "address": "12405 Turtleback Ln, Austin, TX 78727-5247",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.424033,-97.726339&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12405%20Turtleback%20Ln%2C%20Austin%2C%20TX%2078727-5247"
    },
    {
      "name": "Lucero Apartments (fka Oak Creek Village)",
      "address": "2301 Durwood Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.luceroapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24223993,-97.75692734&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2301%20Durwood%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Block at Pearl - North",
      "address": "901 W 22nd Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/the-block",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28606359,-97.74663415&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=901%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Camden Lamar Heights",
      "address": "5400 N Lamar Boulevard, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.camdenliving.com/austin-tx-apartments/camden-lamar-heights",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32250023,-97.72899628&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5400%20N%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Wildflower Terrace",
      "address": "3801 Berkman Drive, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "dmawildflower.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29043615,-97.6984166&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3801%20Berkman%20Drive%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "The Heights on Congress (fka South Congress Village)",
      "address": "2703 S Congress Avenue, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23469746,-97.75600118&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2703%20S%20Congress%20Avenue%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Commons at Manor",
      "address": "12219 US-290, Manor, TX 78653",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "commonsatmanor.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.34732716,-97.53650804&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12219%20US-290%2C%20Manor%2C%20TX%2078653"
    },
    {
      "name": "1412 Perez Street",
      "address": "1412 Perez Street, Austin, TX 78721",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.281273,-97.692141&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1412%20Perez%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "PATHWAYS AT SALINA APARTMENTS (RAD)",
      "address": "1143 Salina St, Austin, TX 78702-2768",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.269064,-97.720063&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1143%20Salina%20St%2C%20Austin%2C%20TX%2078702-2768"
    },
    {
      "name": "Greyshire Village-UNDER CONSTRUCTION",
      "address": "3700 Payload Pass, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.223162,-97.755208&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3700%20Payload%20Pass%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "FLORESVILLE SENIOR HOUSING",
      "address": "504 River Oaks Dr, Austin, TX 78748-3896",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.146722,-97.814029&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=504%20River%20Oaks%20Dr%2C%20Austin%2C%20TX%2078748-3896"
    },
    {
      "name": "Candela Apartments",
      "address": "1614 E 6th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://arnoldaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26300049,-97.72530365&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1614%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Hometowne at Picadilly",
      "address": "500 Grand Avenue Parkway, Pflugerville, TX 78660",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "hometowneatpicadilly.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46466177,-97.64624908&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=500%20Grand%20Avenue%20Parkway%2C%20Pflugerville%2C%20TX%2078660"
    },
    {
      "name": "Bluebonnet Studios",
      "address": "2301 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/bluebonnet-studios/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24690056,-97.77570343&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2301%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "LaMadrid Apartments",
      "address": "11320 Manchaca Road, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.lamadridapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.16590014,-97.82835765&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11320%20Manchaca%20Road%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Regents at 24th",
      "address": "2401 San Gabriel Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.cwsapartments.com/regents-west-at-24th-austin-tx/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28842124,-97.74745612&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2401%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Ruckus on Rio",
      "address": "620 W 24th Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://ruckuswestcampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.288072,-97.743941&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=620%20W%2024th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "SEA RAD (RAD)",
      "address": "502 E Highland Mall Blvd, Austin, TX 78752-3722",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32828,-97.709515&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=502%20E%20Highland%20Mall%20Blvd%2C%20Austin%2C%20TX%2078752-3722"
    },
    {
      "name": "The Plaza at Windsor Hills",
      "address": "9601 Middle Fiskville Road, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.theplazaatwindsorhills.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35940944,-97.68500093&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9601%20Middle%20Fiskville%20Road%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "CityView at the Park",
      "address": "2000 Woodward St., Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.cityviewatthepark.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21809959,-97.74310303&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2000%20Woodward%20St.%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "26 West (fka Jefferson at West Campus)",
      "address": "600 W 26th Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "26West@americancampus.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29111022,-97.74346368&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=600%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "SafePlace 14 Unit Expansion",
      "address": "Undisclosed, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.safeaustin.org/safeplace/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.235751999773335,-97.7251190003692&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=Undisclosed%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Pathways at Gaston Place",
      "address": "1941 Gaston Place, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/gaston-place/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31220055,-97.68890381&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1941%20Gaston%20Place%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Villas on 26th",
      "address": "800 W 26th Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.villason26.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29100037,-97.74549866&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=800%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Union on San Antonio - UNDER CONSTRUCTION",
      "address": "1911 San Antonio St, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2829349,-97.7429329&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1911%20San%20Antonio%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Northwind Apartments - IN DEVELOPMENT",
      "address": "11122, 11204, 11208, and 11216 Cameron Road, Austin, TX 78754",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35592951,-97.636469&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11122%2C%2011204%2C%2011208%2C%20and%2011216%20Cameron%20Road%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "SouthPark Ranch",
      "address": "9401 S 1st Street, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.southparkranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.16990556,-97.80088439&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9401%20S%201st%20Street%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Union on 24th - UNDER CONSTRUCTION",
      "address": "701 W 24th St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28765288,-97.7451376&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=701%20W%2024th%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Quarters at Grayson",
      "address": "714 W 22nd Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://quartersoncampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28550747,-97.74630853&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=714%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Lodge at Merrilltown",
      "address": "14745 Merrilltown Drive, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "thelodgeatmerrilltownapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.4496327,-97.69329049&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=14745%20Merrilltown%20Drive%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Lakeside Apartments",
      "address": "85 Trinity Street, Austin, TX 78701",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/lakeside-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26129913,-97.74079895&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=85%20Trinity%20Street%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "1113 Myrtle Street",
      "address": "1113 Myrtle Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.270039,-97.727636&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1113%20Myrtle%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "900 South 1st Condos",
      "address": "900 S 1st St., Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.highrises.com/austin/the-900-condos/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2548008,-97.75229645&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=900%20S%201st%20St.%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Raintree Apartments",
      "address": "8806 Redfield Lane, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36289978,-97.71230316&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8806%20Redfield%20Lane%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Rebekah Baines Johnson Center",
      "address": "21 Waller Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "http://www.rbjseniorhousing.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2534008,-97.73320007&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=21%20Waller%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Coronado Hills Apartments",
      "address": "1438 Coronado Hills Drive, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32839966,-97.68849945&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1438%20Coronado%20Hills%20Drive%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "The Boulevard",
      "address": "1201 Grove Blvd, Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "http://www.prakpropertymanagement.com/austin.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.234256,-97.703974&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1201%20Grove%20Blvd%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Thurmond Heights",
      "address": "8426 Goldfinch Ct, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "hacanet.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35440063,-97.70829773&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8426%20Goldfinch%20Ct%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "The Jordan at Mueller",
      "address": "2724 Philomena St., Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://foundcom.org/housing/future-communities/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.297534,-97.694652&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2724%20Philomena%20St.%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Pathways at Meadowbrook Apartments",
      "address": "1201 W Live Oak St, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/meadowbrook-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24620056,-97.76599884&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1201%20W%20Live%20Oak%20St%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Pointe at Ben White",
      "address": "6934 E Ben White Blvd., Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://thepointeatbenwhite.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.215839,-97.703566&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6934%20E%20Ben%20White%20Blvd.%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Parmer Place Apartments",
      "address": "12101 Dessau Rd, Austin, TX 78751",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "liveatparmerplace.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.38120079,-97.64920044&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12101%20Dessau%20Rd%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Rosemont at Oak Valley",
      "address": "2800 Collins Creek Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.224565,-97.731191&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2800%20Collins%20Creek%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Vanitas Urban Flats",
      "address": "6103 Manor Rd, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.vanitasflats.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30699921,-97.67829895&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6103%20Manor%20Rd%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "MOSAIC HOUSING CORP XXIII",
      "address": "2404 Roehampton Dr, Austin, Texas 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.196794,-97.817146&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2404%20Roehampton%20Dr%2C%20Austin%2C%20Texas%2078745"
    },
    {
      "name": "Manchaca Road Apartments",
      "address": "3810 Manchaca Rd, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/manchaca-rd.-apts..html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23500061,-97.78549957&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3810%20Manchaca%20Rd%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Mosaic at Mueller",
      "address": "4600 Mueller Boulevard, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.mosaicaustin.com/?utm_knock=w",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.298876,-97.707922&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4600%20Mueller%20Boulevard%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "SOCO 121",
      "address": "121 Woodward St, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.apartments.com/soco-121-austin-tx/l67nveg/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.229317,-97.7599&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=121%20Woodward%20St%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Pathways at Bouldin Oaks",
      "address": "1203 Cumberland Rd, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "http://www.merrittcommunities.com/creeksidevillas/buda-tx-apartments.asp",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24029922,-97.76850128&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1203%20Cumberland%20Rd%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heights on Congress Apartments",
      "address": "2703 S Congress Ave, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.heightsoncongress.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23520088,-97.75610352&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2703%20S%20Congress%20Ave%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Villas on San Gabriel II",
      "address": "2414 San Gabriel Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://villasatsangabriel.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28910065,-97.74810028&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2414%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Harris Branch Senior Apartments",
      "address": "12433 Dessau Rd, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.harrisbranchseniors.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.389594,-97.646383&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12433%20Dessau%20Rd%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "Eberhart Place",
      "address": "808 Eberhart Ln, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.nationalchurchresidences.org/communities/tx/austin/eberhart-place",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20269966,-97.78759766&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=808%20Eberhart%20Ln%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Bridge at Goodnight Ranch",
      "address": "9005 Alderman Dr, Austin, TX 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatgoodnightranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.15313,-97.76074&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9005%20Alderman%20Dr%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "SoCo Park Apartments",
      "address": "7201 S Congress Avenue, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.circlesapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18604996,-97.78070255&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7201%20S%20Congress%20Avenue%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Allegre Point",
      "address": "1833 Cheddar Loop Rd, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.allegrepoint.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.44610023,-97.67169952&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1833%20Cheddar%20Loop%20Rd%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Cambridge Villas",
      "address": "15711 Dessau Road, Pflugerville, TX 78660",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.cambridgevillasseniorpflugerville.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.43037855,-97.6205375&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=15711%20Dessau%20Road%2C%20Pflugerville%2C%20TX%2078660"
    },
    {
      "name": "Guadalupe Court",
      "address": "6607 Guadalupe St., Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/guadalupe-court.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.332558,-97.717407&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6607%20Guadalupe%20St.%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "East 12th Street Apartments",
      "address": "3005 East 12th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/east-12th-st.-apts..html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27599907,-97.70300293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3005%20East%2012th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Avesta- South Shore",
      "address": "2005 Willow Creek Dr., Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "avestasouthshore.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23189926,-97.72840118&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2005%20Willow%20Creek%20Dr.%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Lupine Terrace Apartments",
      "address": "1137 Gunter Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.lupineterraceapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26878576,-97.69419966&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1137%20Gunter%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Bellingham Park Apartment Homes",
      "address": "6816 Boyce Lane, Austin, TX 78653",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.bellinghamparktx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35903838,-97.60413122&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6816%20Boyce%20Lane%2C%20Austin%2C%20TX%2078653"
    },
    {
      "name": "Travis Park Apartments",
      "address": "1100 E Oltorf Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "st-residential.com/2022/10/14/travis-park-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23667415,-97.74527125&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1100%20E%20Oltorf%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Santora Villas",
      "address": "1705 Frontier Valley Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.santoravillas.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22492389,-97.69476214&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1705%20Frontier%20Valley%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Pathways at Manchaca II",
      "address": "6113 Buffalo Pass, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/manchaca-ii/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21209908,-97.80560303&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6113%20Buffalo%20Pass%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Hillside Place",
      "address": "4821 E Riverside Dr, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.hillsideplaceaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23060036,-97.71829987&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4821%20E%20Riverside%20Dr%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Palms on Lamar (fka Malibu Apartments)",
      "address": "8600 N Lamar Boulevard, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "thepalmsonlamarapts.com/index.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35502489,-97.70486896&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8600%20N%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Heritage Pointe",
      "address": "1950 Webberville Road, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.heritagepointeapartments.net",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28438975,-97.6720286&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1950%20Webberville%20Road%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "M Station",
      "address": "2906 E Martin Luther King Jr Boulevard, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/m-station-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.283536,-97.7082219&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2906%20E%20Martin%20Luther%20King%20Jr%20Boulevard%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Kinney Avenue Apartments",
      "address": "1703 Kinney Ave, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/kinney-ave-apts..html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25259972,-97.76740265&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1703%20Kinney%20Ave%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Gateway Apartments",
      "address": "505 Swanee Dr., Austin, TX 78752",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "http://doublekaye.com/gateway.php",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33499908,-97.71520233&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=505%20Swanee%20Dr.%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Texas Shoal Creek",
      "address": "2502 Leon Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.texanshoalcreek.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28989983,-97.74949646&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2502%20Leon%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Twenty15",
      "address": "2015 Cedar Bend Drive, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.twenty15apts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.40953423,-97.69966465&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2015%20Cedar%20Bend%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Capital Studios",
      "address": "309 E 11th Street, Austin, TX 78701",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/capital-studios/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27185899,-97.7380688&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=309%20E%2011th%20Street%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "The Oaks at Ben White",
      "address": "6936 E Ben White Blvd, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "False",
      "website": "theoaksatbenwhite.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21536,-97.70284&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6936%20E%20Ben%20White%20Blvd%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Trails at the Park",
      "address": "815 W Slaughter Lane, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/trails-at-the-park-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17351687,-97.80652342&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=815%20W%20Slaughter%20Lane%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Villages at Fiskville",
      "address": "10127 Middle Fiskville Road, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://villagesatfiskville.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.364962,-97.681102&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10127%20Middle%20Fiskville%20Road%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Limestone Ridge Senior Living Apartments",
      "address": "7011 McKinney Falls Parkway, Austin, Texas 78744",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.16493702,-97.72181353&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7011%20McKinney%20Falls%20Parkway%2C%20Austin%2C%20Texas%2078744"
    },
    {
      "name": "Arrowhead Park Apartments",
      "address": "605 Masterson Pass, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.arrowheadparkapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36920259,-97.69097793&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=605%20Masterson%20Pass%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Urbana West (Goodnight  Ranch)",
      "address": "9004 Alderman Drive, Austin, TX 78747",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.152715,-97.762104&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9004%20Alderman%20Drive%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "Menchaca Commons",
      "address": "12040 Menchaca Rd, Austin, TX 78748",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://menchacacommons.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.142917,-97.833567&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12040%20Menchaca%20Rd%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Trove Eastside",
      "address": "2201 Montopolis Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.troveeastside.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22205632,-97.70402161&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2201%20Montopolis%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Huntington Meadows",
      "address": "7000 Decker Lane, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.hmeadows.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29689541,-97.62555084&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7000%20Decker%20Lane%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Hedge Apartments",
      "address": "8300 N IH-35, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.hedgeaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.346,-97.69627&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8300%20N%20IH-35%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Elm Ridge Apartments",
      "address": "1190 Airport Boulevard, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "elmridge-apts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27527321,-97.70104177&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1190%20Airport%20Boulevard%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Haywood",
      "address": "600 E. FM1626, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "thehaywoodapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.14312,-97.80057&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=600%20E.%20FM1626%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "1201 Fairbanks",
      "address": "1201 Fairbanks, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.alpsmgmt.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.329522,-97.69333&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1201%20Fairbanks%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Lowell at Mueller",
      "address": "1200 Broadmoor Dr, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "thelowellatmueller.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31106958,-97.70530097&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1200%20Broadmoor%20Dr%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "MOSAIC HOUSING CORPORATION XI",
      "address": "3015 Jubilee Trail, Austin, Texas 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.lowincomehousing.us/det/78748-1203-mosaic_housing_corporation_xi#google_vignette",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.177716,-97.842431&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3015%20Jubilee%20Trail%2C%20Austin%2C%20Texas%2078748"
    },
    {
      "name": "Grove Place",
      "address": "1881 Grove Boulevard, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.groveplaceapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22878739,-97.70661498&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1881%20Grove%20Boulevard%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "1004 Lydia Street",
      "address": "1004 Lydia Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.268574,-97.72722&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1004%20Lydia%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Bridge at Granada",
      "address": "414 E Wonsley Drive, Austin, Texas 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatgranada.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.344798,-97.700556&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=414%20E%20Wonsley%20Drive%2C%20Austin%2C%20Texas%2078753"
    },
    {
      "name": "Texan Tower",
      "address": "2505 San Gabriel Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.texantower.com/index.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2901088,-97.74739903&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2505%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Lark Austin",
      "address": "2100 Nueces Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://larkaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.284159,-97.744347&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2100%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "McKinney Falls Apartments",
      "address": "6625 McKinney Falls Parkway, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.mckinneyfallsapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.16995633,-97.72227421&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6625%20McKinney%20Falls%20Parkway%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Barstow Apartments",
      "address": "11701 Metric Blvd, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "False",
      "website": "thebarstowatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39595631,-97.70515107&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11701%20Metric%20Blvd%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Southwest Trails Apartments",
      "address": "8405 Old Bee Caves Road, Austin, TX 78735",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/southwest-trails-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25170019,-97.88751904&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8405%20Old%20Bee%20Caves%20Road%2C%20Austin%2C%20TX%2078735"
    },
    {
      "name": "21 Rio",
      "address": "2101 Rio Grande Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2842162,-97.74474277&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2101%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Legacy Ranch at Dessau East",
      "address": "13527 Harrisglenn Drive, Pflugerville, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.legacyranchatdessaueast.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.406667,-97.6408&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=13527%20Harrisglenn%20Drive%2C%20Pflugerville%2C%20TX%2078753"
    },
    {
      "name": "Highland Heights",
      "address": "803 Tirado Street, Austin, TX 78752",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.323349,-97.708987&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=803%20Tirado%20Street%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Arbor Park",
      "address": "6306 McNeil Drive, Austin, TX 78729",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.4413,-97.750817&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6306%20McNeil%20Drive%2C%20Austin%2C%20TX%2078729"
    },
    {
      "name": "Twenty Two 15 (fka Quarters at Bandera House)",
      "address": "2215 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "2215west.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28645112,-97.74461305&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2215%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Texan 26th",
      "address": "1009 W 26th Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.texan26.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29058927,-97.7481753&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1009%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1801 E 6th Street",
      "address": "1801 E 6th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.1801east6th.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.261905,-97.723296&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1801%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Block on Pearl South",
      "address": "900 W 22nd Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/the-block#contact-us",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28562987,-97.74670947&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=900%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Park at Estancia",
      "address": "820 Camino Vaquero Parkway, Austin, TX 78652",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.parkatestancia.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.123941,-97.804756&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=820%20Camino%20Vaquero%20Parkway%2C%20Austin%2C%20TX%2078652"
    },
    {
      "name": "Block on 23rd",
      "address": "2222 Pearl Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.americancampus.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28706135,-97.74671373&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2222%20Pearl%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1003 Lydia Street",
      "address": "1003 Lydia Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26823,-97.72764&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1003%20Lydia%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Texan West Campus",
      "address": "2616 Salado Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://americancampus.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.292272,-97.745467&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2616%20Salado%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Silver Springs Apartments",
      "address": "12151 N Interstate Hwy 35, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.silverspringsaustinapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39429806,-97.67009256&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12151%20N%20Interstate%20Hwy%2035%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Pointe on Rio",
      "address": "1901 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://pointeonrio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28285304,-97.74492906&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1901%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Lakeway Apartment Homes",
      "address": "FM 620 at Storm Drive, Austin, TX 78734",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.386633,-97.941733&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=FM%20620%20at%20Storm%20Drive%2C%20Austin%2C%20TX%2078734"
    },
    {
      "name": "East Grove Condominiums",
      "address": "1811 Webberville Road, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.eastgrovecondo.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28235,-97.67624&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1811%20Webberville%20Road%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Franklin Park Apartments",
      "address": "4509 E St. Elmo Rd, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.franklin-park.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.205069,-97.737872&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4509%20E%20St.%20Elmo%20Rd%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "West Gate Ridge",
      "address": "8700 West Gate Boulevard, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.pedcorliving.com/apartments/west-gate-ridge",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19319866,-97.83206995&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8700%20West%20Gate%20Boulevard%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Rosemont at Oak Valley (fka Pleasant Valley Villas)",
      "address": "2800 Collins Creek Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.rosemontaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22472895,-97.73127473&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2800%20Collins%20Creek%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Signature 1909",
      "address": "1909 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.signature1909.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.283589,-97.744783&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1909%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "2708 San Pedro St",
      "address": "2708 San Pedro St, Austin, Texas 78705",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://gcc01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fpost.craigslist.org%2Fmanage%2F7251250805%3Faction%3Ddisplay%26go%3Ddisplay&data=04%7C01%7CZachary.Stern%40austintexas.gov%7Cbcd1f3e6b54144f0eeb508d8ed3bcd56%7C5c5e19f6a6ab4b45b1d0be4608a9a",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2931349,-97.7461735&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2708%20San%20Pedro%20St%2C%20Austin%2C%20Texas%2078705"
    },
    {
      "name": "Quarters at Sterling",
      "address": "709 W 22nd Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://quartersoncampus.com/sterling-house/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28498621,-97.7459644&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=709%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Solomon",
      "address": "1414 E 51st St, Austin, Texas 78723",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "solomonatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.306398,-97.703812&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1414%20E%2051st%20St%2C%20Austin%2C%20Texas%2078723"
    },
    {
      "name": "Haus 5350",
      "address": "5350 Burnet Road, Austin, Texas 78756",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "haus5350.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.327593,-97.74045&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5350%20Burnet%20Road%2C%20Austin%2C%20Texas%2078756"
    },
    {
      "name": "Grand Marc at 26th",
      "address": "510 W 26th Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.grandmarcaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29099868,-97.74257135&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=510%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Millenium Youth Entertainment Center",
      "address": "91 Rainey Street, Austin, TX 78721",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26020539,-97.7379292&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=91%20Rainey%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Villas of Cordoba",
      "address": "5901 East Stassney Lane, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19009972,-97.73269653&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5901%20East%20Stassney%20Lane%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Bridge at Asher",
      "address": "10505 South IH35, Austin, Texas 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatasher.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.145744,-97.790795&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10505%20South%20IH35%2C%20Austin%2C%20Texas%2078747"
    },
    {
      "name": "21st Street Cooperative",
      "address": "707 W 21st St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.collegehouses.org/listings/21st",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28385273,-97.74586956&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=707%20W%2021st%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Springdale Estates",
      "address": "1050 Springdale Road, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.springdaleestatesaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27079964,-97.69329834&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1050%20Springdale%20Road%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Inspire on 22nd",
      "address": "2200 Nueces Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://liveatinspireatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285401,-97.744188&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2200%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Terrace at Walnut Creek",
      "address": "8712 Old Manor Road, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.terracewalnutcreek.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32709344,-97.63873504&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8712%20Old%20Manor%20Road%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Park at Summers Grove",
      "address": "2900 Century Park Blvd, Austin, TX 78727",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.parkatsummersgrove.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.434858,-97.694401&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2900%20Century%20Park%20Blvd%2C%20Austin%2C%20TX%2078727"
    },
    {
      "name": "Argosy at Crestview",
      "address": "1003 Justin Ln, Austin, TX 78757",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.argosyatcrestview.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33723393,-97.72234318&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1003%20Justin%20Ln%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "The Beckett",
      "address": "14011 FM 969, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.beckettaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24551039,-97.58735999&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=14011%20FM%20969%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Guadalupe I & II",
      "address": "7102 Guadalupe St, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/guadalupe-court.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33703289,-97.71532571&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7102%20Guadalupe%20St%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Trails at Vintage Creek",
      "address": "7224 Northeast Drive, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://foundcom.org/housing/our-austin-communities/trails-at-the-vintage-creek-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32110023,-97.68070221&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7224%20Northeast%20Drive%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "MAA South Lamar",
      "address": "1500 South Lamar Blvd, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.maasouthlamar.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.252823,-97.764453&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1500%20South%20Lamar%20Blvd%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Quail Park Village",
      "address": "9920 Quail Boulevard, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.quailparkvillage.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.37267006,-97.70021124&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9920%20Quail%20Boulevard%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Quarters at Nueces House",
      "address": "2300 Nueces Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://quartersoncampus.com/nueces-house/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28679177,-97.74390448&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2300%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Block on 28th",
      "address": "701 W 28th Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29327736,-97.74463321&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=701%20W%2028th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Retreat at North Bluff",
      "address": "6212 Crow Lane, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.retreatatnorthbluff.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19759941,-97.77249908&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6212%20Crow%20Lane%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Callaway House Austin",
      "address": "505 W 22nd Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/the-callaway-house-austin#specials",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28494112,-97.74342091&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=505%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Quarters at Cameron",
      "address": "2707 Rio Grande Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://quartersoncampus.com/cameron-house/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29239274,-97.74391417&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2707%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "E6 Apartments",
      "address": "2400 E 6th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.livee6.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25979996,-97.71600342&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2400%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Lakeline Station",
      "address": "13635 Rutledge Spur, Austin, TX 78717",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/lakeline-station-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.47654766,-97.78084061&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=13635%20Rutledge%20Spur%2C%20Austin%2C%20TX%2078717"
    },
    {
      "name": "Vista Bella Apartments",
      "address": "21101 Boggy Ford Road, Lago Vista, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.vistabellaapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.43224,-98.01179&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=21101%20Boggy%20Ford%20Road%2C%20Lago%20Vista%2C%20TX%2078745"
    },
    {
      "name": "Pflugerville Meadows",
      "address": "201 Meadow Lane, Pflugerville, TX 78660",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.pflugervillemeadows.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.44347,-97.627163&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=201%20Meadow%20Lane%2C%20Pflugerville%2C%20TX%2078660"
    },
    {
      "name": "Spring Terrace",
      "address": "7101 N Interstate Highway 35, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/spring-terrace/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33185202,-97.70296341&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7101%20N%20Interstate%20Highway%2035%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Acclaim at South Congress",
      "address": "701 Woodward Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "acclaimatsouthcongress.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22410638,-97.75348714&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=701%20Woodward%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "The Rail at MLK",
      "address": "2921 E. 17th St, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://therailatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.279159,-97.709818&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2921%20E.%2017th%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Timbers Apartments",
      "address": "1034 Clayton Lane, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "chavezfoundation.org/housing-economic-development-fund/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31991483,-97.70361857&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1034%20Clayton%20Lane%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Village at Collinwood",
      "address": "1001 Collinwood W Drive, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.villageatcollinwood.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36997681,-97.67479835&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1001%20Collinwood%20W%20Drive%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Pathways at North Loop Apartments",
      "address": "2300 W N Loop, Austin, TX 78756",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/north-loop-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32629967,-97.74250031&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2300%20W%20N%20Loop%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "Galileo (Home Ownership)",
      "address": "910 W 25th Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "austinresidence.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28963999,-97.74734934&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=910%20W%2025th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Creekview Apartment Homes",
      "address": "5001 Crainway Drive, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.creekviewaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31973074,-97.65822388&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5001%20Crainway%20Drive%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "The Reserve at Walnut Creek (fka Springdale Apartments)",
      "address": "8038 Exchange Drive, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.reserveatwalnutcreek.com/?utm_knock=gmb",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33340853,-97.66407696&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8038%20Exchange%20Drive%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "Skyline Terrace",
      "address": "1212 W Ben White Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/skyline-terrace/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22865012,-97.77799536&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1212%20W%20Ben%20White%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Mount Carmel Village",
      "address": "2504 New York Drive, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.mymountcarmelapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27441819,-97.71383798&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2504%20New%20York%20Drive%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Art on Brattons Edge",
      "address": "15405 Long Vista Drive, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.artatbrattonsedge.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.45420074,-97.67910004&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=15405%20Long%20Vista%20Drive%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "21 Rio",
      "address": "2101 Rio Grande St., Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.21rio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28429985,-97.74469757&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2101%20Rio%20Grande%20St.%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Springdale Gardens Apartments",
      "address": "1054 Springdale Rd, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27355084,-97.69234843&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1054%20Springdale%20Rd%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Marshall Apartments",
      "address": "1157 Salina Street & 1401 E 12th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.marshall-apartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27061405,-97.71974505&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1157%20Salina%20Street%20%26%201401%20E%2012th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Windy Ridge",
      "address": "10910 N Fm 620, Austin, TX 78726",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.windyridgeapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.44970006,-97.83061106&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10910%20N%20Fm%20620%2C%20Austin%2C%20TX%2078726"
    },
    {
      "name": "Spring Valley Apartments",
      "address": "2302 E William Cannon Dr, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.springvalley-apartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18770027,-97.7602005&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2302%20E%20William%20Cannon%20Dr%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Western Trails",
      "address": "2422 Western Trails Boulevard, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.greystonerents.com/communities/western-trails/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23016262,-97.79644071&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2422%20Western%20Trails%20Boulevard%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Runnymede Apartments",
      "address": "1101 Rutland Dr., Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "n/a",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36689949,-97.70269775&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1101%20Rutland%20Dr.%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Bella Vista",
      "address": "2501 Anken Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.bellavistaaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22965748,-97.72777044&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2501%20Anken%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "HillTop",
      "address": "2400 San Gabriel Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.hilltopatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28844878,-97.74801303&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2400%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Heights on Parmer Apartment Homes",
      "address": "1500 E Parmer Lane, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.pedcorliving.com/apartments/the-heights-on-parmer",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39075505,-97.65138976&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1500%20E%20Parmer%20Lane%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Douglas Landing",
      "address": "2347 Douglas Street, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.douglaslanding.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22855202,-97.73199281&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2347%20Douglas%20Street%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Walnut Creek Apartments",
      "address": "6409 Springdale Road, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "walnutcreekaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30752404,-97.66833436&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6409%20Springdale%20Road%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Stoneridge Apartments",
      "address": "16701 N Heatherwilde Boulevard, Pflugerville, TX 78660",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.stoneridgeapartmentliving.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.4535199,-97.63593158&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=16701%20N%20Heatherwilde%20Boulevard%2C%20Pflugerville%2C%20TX%2078660"
    },
    {
      "name": "The Bridge at Harris Ridge",
      "address": "1501 E Howard Lane, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.bridgeharrisridge.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.40380847,-97.64236205&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1501%20E%20Howard%20Lane%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Homestead Oaks",
      "address": "3226 W Slaughter Lane, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://foundcom.org/housing/our-austin-communities/homestead-oaks-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.1833992,-97.84410095&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3226%20W%20Slaughter%20Lane%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Riverside Townhomes",
      "address": "6118 Fairway Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.riversidetownhomesaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22949982,-97.70240021&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6118%20Fairway%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Cardinal Point Apartments",
      "address": "11015 Four Points Drive, Austin, TX 78730",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://foundcom.org/housing/our-austin-communities/cardinal-point/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39889908,-97.84539795&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11015%20Four%20Points%20Drive%2C%20Austin%2C%20TX%2078730"
    },
    {
      "name": "Spring Hollow Apartments",
      "address": "4803 Loyola Lane, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30832612,-97.66664245&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4803%20Loyola%20Lane%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "The Abali",
      "address": "4611 N I-35, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.theabali.com/#!",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.304665,-97.71283&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4611%20N%20I-35%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Pathways at Goodrich Place",
      "address": "2126 Goodrich Avenue, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "pathwaysatgoodrich.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.248739,-97.775849&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2126%20Goodrich%20Avenue%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Pecan Springs Commons - Multifamily",
      "address": "5800 Sweeney Circle, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30489922,-97.68000031&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5800%20Sweeney%20Circle%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "AMLI - South Shore",
      "address": "1620 E Riverside Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.amli.com/apartments/austin/downtown-austin-apartments/amli-south-shore?switch_code=94414&utm_source=Smart%2FAffordable%20Housing%20Program&utm_medium=Web&utm_campaign=South%20Shore%20AustinTX.gov",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2451992,-97.73020172&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1620%20E%20Riverside%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Torre Apartments",
      "address": "2020 Nueces Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://torreatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28461,-97.74167&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2020%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Oaks on Lamar (fka Santa Maria Village)",
      "address": "8071 N Lamar Boulevard, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.oaksonlamar.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.34876941,-97.71101908&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8071%20N%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "The Waters at Willow Run Apartments",
      "address": "15515 FM 1325, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.watersatwillowrun.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46190071,-97.69110107&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=15515%20FM%201325%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Urban East",
      "address": "6400 E Riverside Drive, Austin, Texas 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "urbaneastatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.225127,-97.69323&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6400%20E%20Riverside%20Drive%2C%20Austin%2C%20Texas%2078741"
    },
    {
      "name": "Travis Flats",
      "address": "5310 Helen St, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://dmatravisflats.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.314886,-97.713625&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5310%20Helen%20St%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Moontower",
      "address": "2204 San Antionio Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://moontoweratx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285646,-97.743134&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2204%20San%20Antionio%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Nexus East",
      "address": "720 Airport Boulevard, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.nexuseastaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.252779,-97.693218&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=720%20Airport%20Boulevard%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "AHA! at Briarcliff",
      "address": "1915 Briarcliff Blvd, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://ahaustin.org/aha-at-briarcliff/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.313769,-97.688991&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1915%20Briarcliff%20Blvd%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "AMLI Eastside",
      "address": "1000 San Marcos Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.amli.com/apartments/austin/downtown-austin-apartments/amli-eastside",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31386,-97.68888&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1000%20San%20Marcos%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Nightingale at Goodnight Ranch",
      "address": "5900 Charles Merle Drive, Austin, TX 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://dmanightingale.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.15640068,-97.75409698&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5900%20Charles%20Merle%20Drive%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "The Quincy",
      "address": "91 Red River St, Austin, TX 78701",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://quincyatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.260518,-97.739027&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=91%20Red%20River%20St%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "The Nine at West Campus",
      "address": "2518 Leon Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://theninewestcampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.290996,-97.749342&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2518%20Leon%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Platform Apartments Phase II",
      "address": "2823 E Martin Luther King Jr Blvd, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.platformapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.279807,-97.710691&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2823%20E%20Martin%20Luther%20King%20Jr%20Blvd%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Pathways at Chalmers Court - South",
      "address": "1638 E 2nd Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://chalmerscourtssouth.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.258942,-97.726723&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1638%20E%202nd%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Pathways at Manchaca Village",
      "address": "3628 Manchaca Rd, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/event-location/manchaca-village/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23740005,-97.78520203&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3628%20Manchaca%20Rd%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "AMLI - South Shore Phase II",
      "address": "1620 E Riverside Dr, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.amli.com/apartments/austin/downtown-austin-apartments/amli-south-shore",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.245229,-97.730198&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1620%20E%20Riverside%20Dr%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Aura on Lamar",
      "address": "5629 N Lamar Blvd, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.auraonlamaraustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32528,-97.726352&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5629%20N%20Lamar%20Blvd%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Block on Rio Grande",
      "address": "2819 Rio Grande Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.theblockwestcampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29509926,-97.7437973&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2819%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "St Johns West Apartments",
      "address": "601 W St Johns Avenue, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.stjohnswest.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.337701,-97.714785&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=601%20W%20St%20Johns%20Avenue%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Legacy on Rio",
      "address": "2614 Rio Grande, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://legacyonrio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29174,-97.74479&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2614%20Rio%20Grande%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Fivetwo at Highland - South",
      "address": "109 Jacob Fontaine Lane, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://fivetwoapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.324758,-97.714668&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=109%20Jacob%20Fontaine%20Lane%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Fort Branch at Truman's Landing",
      "address": "5800 Techni Center Drive, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.livefortbranchattrumanslanding.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27917069,-97.67143319&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5800%20Techni%20Center%20Drive%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "AMLI on Aldrich",
      "address": "2401 Aldrich Street, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.amli.com/aldrich?switch_code=94414&utm_source=Smart%2FAffordable%20Housing%20Program&utm_medium=Web&utm_campaign=Aldrich%20AustinTX.gov [gcc01.safelinks.protection.outlook.com]",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30240059,-97.70200348&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2401%20Aldrich%20Street%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "AMLI Branch Park",
      "address": "1911 Philomena St, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.amli.com/apartments/austin/mueller-apartments/amli-branch-park?switch_code=94414&utm_source=Smart%2FAffordable+Housing+Program&utm_medium=web&utm_campaign=branchparkSMARTHousing",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.299939,-97.70338&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1911%20Philomena%20St%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Elan Parkside Apartments",
      "address": "609 Clayton Lane, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32369995,-97.71350098&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=609%20Clayton%20Lane%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Terrace at Oak Springs",
      "address": "3000 Oak Springs Drive, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://integralcare.org/en/home/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27352219,-97.70056603&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3000%20Oak%20Springs%20Drive%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Aria Grand",
      "address": "1800 S IH 35, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.ariagrandapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.238642,-97.738699&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1800%20S%20IH%2035%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heights on Parmer Phase II",
      "address": "1500 E Parmer Lane, Austin, TX 78753",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.390823,-97.650793&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1500%20E%20Parmer%20Lane%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "AMLI at Mueller",
      "address": "1900 Simond Avenue, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.amli.com/mueller?switch_code=94414&utm_source=Smart%2FAffordable%20Housing%20Program&utm_medium=Web&utm_campaign=Mueller%20AustinTX.gov [gcc01.safelinks.protection.outlook.com]",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29719925,-97.70469666&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1900%20Simond%20Avenue%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Westchester Woods Apartments",
      "address": "19600 N Heatherwilde Boulevard, Pflugerville, TX 78660",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.westchesterwoodsapt.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.48953486,-97.61092034&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=19600%20N%20Heatherwilde%20Boulevard%2C%20Pflugerville%2C%20TX%2078660"
    },
    {
      "name": "Paradise Oaks Apartments",
      "address": "1500 Faro Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.paradiseoaksapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2342547,-97.71376885&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1500%20Faro%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Harris Branch Apartments",
      "address": "12435 Dessau Rd, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://harrisbranchseniors.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.38899994,-97.64489746&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12435%20Dessau%20Rd%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "Park Place at Loyola Apartments",
      "address": "6200 Loyola Lane, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.parkplaceloyolaapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30211754,-97.65078234&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6200%20Loyola%20Lane%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Riverside Meadows Apartments",
      "address": "1601 Montopolis Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "n/a",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22699928,-97.70059967&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1601%20Montopolis%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "1209 East Apartments",
      "address": "1209 E 52nd Street, Austin, TX 78723",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30879974,-97.7071991&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1209%20E%2052nd%20Street%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Solaris Aparments",
      "address": "1601 Royal Crest Drive, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.thesolarisaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23916839,-97.72929091&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1601%20Royal%20Crest%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Ruth R. Schulze House",
      "address": "915 W 22nd Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2852993,-97.74790192&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=915%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "William Cannon Apartment Homes",
      "address": "2112 E William Cannon Drive, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.pedcorliving.com/apartments/william-cannon",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18862145,-97.7613958&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2112%20E%20William%20Cannon%20Drive%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Colorado Creek Apartments",
      "address": "11700 Dionda Lane, Del Valle, TX 78617",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.coloradocreektx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.205648,-97.633937&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11700%20Dionda%20Lane%2C%20Del%20Valle%2C%20TX%2078617"
    },
    {
      "name": "Eryngo Hills (FKA Rosemont at Hidden Creek)",
      "address": "9345 US-290, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.eryngohills.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32719213,-97.64284977&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9345%20US-290%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "ION Austin",
      "address": "2100 San Antonio Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://ion-austin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28418536,-97.74312525&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2100%20San%20Antonio%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Windsor South Lamar",
      "address": "809 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.windsorsola.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25720024,-97.75990295&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=809%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "70 Rainey Street",
      "address": "70 Rainey Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://70rainey.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25839996,-97.73919678&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=70%20Rainey%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Runnymede Apartments",
      "address": "1101 Rutland Drive, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.runnymedeapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36717992,-97.702696&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1101%20Rutland%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Sierra Vista",
      "address": "4320 S Congress Avenue, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "foundcom.org/housing/our-austin-communities/sierra-vista-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21947094,-97.76724329&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4320%20S%20Congress%20Avenue%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Woodway Square Apartments",
      "address": "1700 Teri Road, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "www.woodwaysquareaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20538764,-97.75499698&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1700%20Teri%20Road%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Eagles Landing",
      "address": "8000 Decker Lane, Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.eagleslandingapartments.net",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30824469,-97.6222696&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8000%20Decker%20Lane%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Forest Park",
      "address": "1088 Park Plaza Drive, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "forestparkaustinapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.34895772,-97.6839517&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1088%20Park%20Plaza%20Drive%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "The Paddock at Norwood",
      "address": "1044 Norwood Park Boulevard, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "paddockatnorwood.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33714974,-97.69207873&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1044%20Norwood%20Park%20Boulevard%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Green Doors",
      "address": "1503 S IH35, Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24270058,-97.73490143&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1503%20S%20IH35%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "5006 West Wind Trail",
      "address": "5006 West Wind Trail, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22649956,-97.80449677&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5006%20West%20Wind%20Trail%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "1202 E 7th",
      "address": "1202 E 7th, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26571,-97.729669&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1202%20E%207th%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Arnold",
      "address": "1621 E 6th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://arnoldaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26239967,-97.72619629&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1621%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Carols House",
      "address": "1805 Heatherglen Lane, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "http://www.austintexas.gov/department/carols-house",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39520073,-97.7016983&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1805%20Heatherglen%20Lane%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "21 Pearl",
      "address": "911 W 21st Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://21pearlwestcampus.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2845993,-97.74720001&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=911%20W%2021st%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1002 Navasota",
      "address": "1002 Navasota Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.267916,-97.725661&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1002%20Navasota%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Daffodil Apartments",
      "address": "6009 Daffodil Drive, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/daffodil-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21109962,-97.7173996&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6009%20Daffodil%20Drive%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Buckingham Place Duplexes",
      "address": "743 Yarsa Blvd., Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.foundcom.org/housing/our-austin-communities/buckingham-place-duplexes/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17429924,-97.80470276&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=743%20Yarsa%20Blvd.%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Skyloft",
      "address": "507 W 23rd Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://skyloftaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28639984,-97.74349976&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=507%20W%2023rd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1002 Wheeless Street",
      "address": "1002 Wheeless Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26799965,-97.72779846&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1002%20Wheeless%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The 704",
      "address": "3401 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.cwsapartments.com/apartments/tx/austin/the-704/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24060059,-97.78610229&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3401%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "SOL",
      "address": "1133 Altum Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25889969,-97.68360138&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1133%20Altum%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Heritage Heights",
      "address": "1608 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266574,-97.724124&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1608%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Parkwest Condos",
      "address": "10616 Mellow Meadows Drive, Austin, TX 78750",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46050072,-97.79969788&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10616%20Mellow%20Meadows%20Drive%2C%20Austin%2C%20TX%2078750"
    },
    {
      "name": "1905 E 9th Street",
      "address": "1905 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26390076,-97.72100067&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1905%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Works at Pleasant Valley",
      "address": "2800 Lyons Road, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.lifeworksaustin.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26460075,-97.70729828&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2800%20Lyons%20Road%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "1112 E 10th Street",
      "address": "1112 E 10th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26819992,-97.728302&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1112%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1625 E 11th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266657,-97.722927&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1625%20E%2011th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Garden Terrace Phase III-UNDER CONSTRUCTION",
      "address": "1015 W William Cannon Drive, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://foundcom.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.198406,-97.792919&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1015%20W%20William%20Cannon%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Creeks Edge Apartments",
      "address": "1124 Rutland Drive, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.creeksedgeaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36940002,-97.70339966&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1124%20Rutland%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "1009 E 10th Street",
      "address": "1009 E 10th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26840019,-97.73059845&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1009%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1626 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266016,-97.723066&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1626%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "South 4th Street Apartments",
      "address": "2402 South 4th St, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24290085,-97.76380157&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2402%20South%204th%20St%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Manor House",
      "address": "5905 Manor Road, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.voa.org/housing_properties/manor-house-apartments",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30669975,-97.67939758&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5905%20Manor%20Road%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Accessible Housing Austin! - SF",
      "address": "7009 Thannas Way, Austin, TX 78744",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "http://www.ahaustin.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17770004,-97.75610352&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7009%20Thannas%20Way%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Lakeside Engineering - Duplexes",
      "address": "1123 Walton Lane, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25919914,-97.68730164&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1123%20Walton%20Lane%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Overture Mueller Apartments",
      "address": "4818 Berkman Drive, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.liveoverture.com/communities/austin-mueller/modern-living/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30170059,-97.70089722&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4818%20Berkman%20Drive%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Edgecreek Condos",
      "address": "12166 Metric Boulevard, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.40690041,-97.69960022&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12166%20Metric%20Boulevard%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Tree",
      "address": "3715 S 1st Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://liveattree.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22879982,-97.76699829&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3715%20S%201st%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heritage Heights",
      "address": "1625 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2657,-97.723292&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1625%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Nine at Rio",
      "address": "2100 Rio Grande St, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.liveatninerio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.284272,-97.745293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2100%20Rio%20Grande%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Heritage Heights",
      "address": "1609 E 11th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.267028,-97.72386&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1609%20E%2011th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "2412 Bryan Street",
      "address": "2412 Bryan Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26709938,-97.71640015&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2412%20Bryan%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Groves South Lamar Apartments",
      "address": "3607 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.greystar.com/properties/austin-tx/groves-south-lamar-apartments?sc_lang=en",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23889923,-97.78780365&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3607%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Eastside Station (fka Saltillo Station)",
      "address": "1700 E 4th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://eastsidestationapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26110077,-97.72530365&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1700%20E%204th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Lyons Gardens",
      "address": "2720 Lyons Road, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://lyonsgardens.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26449966,-97.70909882&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2720%20Lyons%20Road%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "4008 Brookview (ADU)",
      "address": "4008 Brookview Road, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2942009,-97.70999908&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4008%20Brookview%20Road%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Bridge at Sterling Springs",
      "address": "2809 W. William Cannon Dr, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "bridgeatsterlingsprings.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.207417,-97.81761&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2809%20W.%20William%20Cannon%20Dr%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "SOL",
      "address": "1129 Altum Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25860023,-97.68340302&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1129%20Altum%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Avon at 22nd",
      "address": "911 W 22nd Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.avonat22nd.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285403,-97.747712&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=911%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Glen Oaks Corner",
      "address": "908 Neal Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26849937,-97.71019745&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=908%20Neal%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Estancia Villas",
      "address": "1200 Estancia Parkway, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.estanciavillas.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.12290001,-97.80609894&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1200%20Estancia%20Parkway%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "904 Lydia",
      "address": "904 Lydia Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.267299649999998,-97.72850037&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=904%20Lydia%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "5406 Village Trail",
      "address": "5406 Village Trail, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19440079,-97.74620056&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5406%20Village%20Trail%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "1705 Rosewood Avenue",
      "address": "1705 Rosewood Avenue, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.269317,-97.720605&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1705%20Rosewood%20Avenue%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "FLORA Apartments",
      "address": "5406 Middle Fiskville Road, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.floraaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31669998,-97.71499634&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5406%20Middle%20Fiskville%20Road%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Marq on Burnet",
      "address": "6701 Burnet Road, Austin, TX 78757",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.cwsapartments.com/apartments/tx/austin/marq-on-burnet/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.34219933,-97.73799896&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6701%20Burnet%20Road%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "Heritage Heights",
      "address": "1623 E 11th Steet, Austin, TX 78723",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266717,-97.723054&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1623%20E%2011th%20Steet%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "2106 Chicon Street",
      "address": "2106 Chicon Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28240013,-97.7220993&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2106%20Chicon%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Indie Apartments",
      "address": "1630 E 6th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://indieapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26269913,-97.72450256&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1630%20E%206th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Inspire on 22nd",
      "address": "2200 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285401,-97.744188&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2200%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "AHFC - SF",
      "address": "2008 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.264686,-97.71899&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2008%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Accessible Housing Austin! - SF",
      "address": "3705 Tamil Street, Austin, TX 78749",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "http://www.ahaustin.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2112999,-97.83599854&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3705%20Tamil%20Street%2C%20Austin%2C%20TX%2078749"
    },
    {
      "name": "Anderson Village",
      "address": "3101 E 12th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/anderson-village.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27639961,-97.70220184&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3101%20E%2012th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Guadalupe Saldana - Duplex",
      "address": "1208 Paul Theresa Saldana Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26930046,-97.70300293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1208%20Paul%20Theresa%20Saldana%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Bridge at Sterling Village",
      "address": "10401 N Lamar, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.bridgeatsterlingvillage.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.371727,-97.690003&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10401%20N%20Lamar%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "AHFC - SF",
      "address": "7212 Providence Avenue, Austin, TX 78752",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.330556,-97.696935&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7212%20Providence%20Avenue%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "4910 West Wind Trail",
      "address": "4910 W Wind Trail, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22719955,-97.80419922&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4910%20W%20Wind%20Trail%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Carson Creek Duplexes",
      "address": "9605 Carson Creek Blvd, Del Valle, TX 78617",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hatctx.com/carson-creek/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.224063,-97.666296&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9605%20Carson%20Creek%20Blvd%2C%20Del%20Valle%2C%20TX%2078617"
    },
    {
      "name": "7314 Meador Avenue",
      "address": "7314 Meador Avenue, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33139992,-97.69509888&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7314%20Meador%20Avenue%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "The Plaza at Windsor Hills",
      "address": "9601 Middle Fiskville Road, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.theplazaatwindsorhills.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35940944,-97.68500093&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9601%20Middle%20Fiskville%20Road%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Heritage Heights",
      "address": "1611 E 11th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266957,-97.7237&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1611%20E%2011th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Glen Oaks Corner",
      "address": "912 Neal Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26869965,-97.71029663&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=912%20Neal%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Summit Oaks",
      "address": "11607 Sierra Nevada, Austin, TX 78759",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.hatctx.com/summit-oaks/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.42029953,-97.75460052&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11607%20Sierra%20Nevada%2C%20Austin%2C%20TX%2078759"
    },
    {
      "name": "Guadalupe Saldana Net Zero Subdivision",
      "address": "2717 Goodwin Ave, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/?page_id=93",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26959991,-97.70290375&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2717%20Goodwin%20Ave%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "2107 Salina Street",
      "address": "2107 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28210068,-97.72250366&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2107%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "809 San Marcos Street",
      "address": "809 San Marcos Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.267299649999998,-97.73130035&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=809%20San%20Marcos%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Park at Wells Branch Apartments",
      "address": "1915 Wells Branch Parkway, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "parkatwellsbranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.43370056,-97.68060303&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1915%20Wells%20Branch%20Parkway%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "4401 South Congress Apartments-UNDER CONSTRUCTION",
      "address": "4401 South Congress Avenue, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.217094,-97.766904&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4401%20South%20Congress%20Avenue%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "SOL",
      "address": "5916 Lux Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25919914,-97.68419647&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5916%20Lux%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "7EAST",
      "address": "2025 E 7th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.7eastaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26169968,-97.72000122&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2025%20E%207th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "2009 Salina Street",
      "address": "2009 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28120041,-97.72229767&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2009%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Garden Terrace Phase I",
      "address": "1015 W William Cannon Drive, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://foundcom.org/housing/our-austin-communities/garden-terrace/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19864089,-97.79342653&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1015%20W%20William%20Cannon%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "AC Autograph Hotel",
      "address": "1901 San Antonio Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.282237,-97.743144&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1901%20San%20Antonio%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Heritage Heights",
      "address": "1621 E 11th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266767,-97.723149&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1621%20E%2011th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Blackland CDC",
      "address": "1902 E 22nd St, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "www.blacklandcdc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28300095,-97.72149658&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1902%20E%2022nd%20St%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Sierra Ridge",
      "address": "201 W St Elmo Rd, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/sierra-ridge-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21820068,-97.76899719&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=201%20W%20St%20Elmo%20Rd%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Rosewood Courts",
      "address": "2001 Rosewood Ave, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/rosewood-courts/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26889992,-97.72000122&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2001%20Rosewood%20Ave%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Lamar Union Apartments",
      "address": "1100 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.lamarunion.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25589943,-97.76290131&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1100%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Pecan Hill Apartments",
      "address": "13000 Hymeadow Drive, Austin, TX 78729",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "http://www.accessiblespace.org/pecan-hills-apartments",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46209908,-97.78949738&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=13000%20Hymeadow%20Drive%2C%20Austin%2C%20TX%2078729"
    },
    {
      "name": "Post South Lamar 2",
      "address": "1414 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.maac.com/texas/austin/post-south-lamar",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25449944,-97.76260376&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1414%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Bridge at Sweetwater",
      "address": "2323 Wells Branch Parkway, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatsweetwater.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.439585,-97.687227&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2323%20Wells%20Branch%20Parkway%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Heritage Heights",
      "address": "1610 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266482,-97.723987&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1610%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "1100 E 10th Street",
      "address": "1100 E 10th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26849937,-97.72930145&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1100%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "2113 Salina Street",
      "address": "2113 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28249931,-97.72260284&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2113%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "2502 Nueces Street",
      "address": "2502 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.289642,-97.743338&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2502%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Corazon",
      "address": "1000 E 5th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.corazonatxapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26399994,-97.73210144&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1000%20E%205th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Lakeside Engineering - Duplexes",
      "address": "1125 Walton Lane, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25939941,-97.68730164&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1125%20Walton%20Lane%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "2320 Santa Rita Street",
      "address": "2320 Santa Rita Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25709915,-97.71730042&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2320%20Santa%20Rita%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "North Shore Apartments",
      "address": "110 San Antonio Street, Austin, TX 78701",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.northshoreaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26530075,-97.74949646&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=110%20San%20Antonio%20Street%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "Texan North Campus (fka Uptown Lofts)",
      "address": "5117 N Lamar Boulevard, Austin, TX 78751",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.texanproperties.net/tnc/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31920052,-97.73000336&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5117%20N%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "The Willows",
      "address": "1332 Lamar Square Dr, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "https://www.maryleefoundation.org/programs-and-services/affordable-housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25440025,-97.76509857&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1332%20Lamar%20Square%20Dr%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "SOL",
      "address": "5908 Ventus Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25860023,-97.68479919&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5908%20Ventus%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Ebenezer Village",
      "address": "1015 East 10th St, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "http://www.ebc3austin.org/about-us/ebenezer-village/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26840019,-97.7303009&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1015%20East%2010th%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "2401 Longview",
      "address": "2401 Longview Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28868985,-97.75048641&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2401%20Longview%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Residences at The Domain",
      "address": "11400 Domain Drive, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.udr.com/austin-apartments/north-burnet/residences-at-the-domain/contact-us/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.40069962,-97.72509766&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11400%20Domain%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "South Shore Disctrict Apartments (South Shore PUD)",
      "address": "1333 Shore District Drive, Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.243505,-97.727183&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1333%20Shore%20District%20Drive%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Bridge at Volente",
      "address": "11908 Anderson Mill Rd, Austin, TX 78726",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatvolente.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.456741,-97.826936&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11908%20Anderson%20Mill%20Rd%2C%20Austin%2C%20TX%2078726"
    },
    {
      "name": "Stonewood Village",
      "address": "4558 Avenue A, Austin, TX 78751",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "512living.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31240082,-97.73120117&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4558%20Avenue%20A%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Guadalupe Neighborhood Development Corporation - SF",
      "address": "809 E 9th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26810074,-97.73290253&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=809%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1602 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265855,-97.724932&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1602%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Mueller 52",
      "address": "1300 E 52nd St., Austin, TX 78723",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30879974,-97.70549774&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1300%20E%2052nd%20St.%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Guadalupe Saldana - Duplex",
      "address": "1212 Paul Theresa Saldana Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26959991,-97.70290375&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1212%20Paul%20Theresa%20Saldana%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1603 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266283,-97.724708&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1603%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "702 Plumpton Drive",
      "address": "702 Plumpton Drive, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19499969,-97.790802&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=702%20Plumpton%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "The Villages at The Domain",
      "address": "11011 Domain Drive, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.villagesdomain.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39620018,-97.72579956&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11011%20Domain%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Austin Childrens Shelter",
      "address": "4800 Manor Road, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.safeaustin.org/austinchildrensshelter/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29640007,-97.69000244&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4800%20Manor%20Road%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Bell South Lamar",
      "address": "2717 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bellapartmentliving.com/tx/austin/bell-south-lamar/index.aspx",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2432003,-97.78119659&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2717%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Garden Terrace Expansion (aka Garden Terrace, Phase II)",
      "address": "1015 W William Cannon Drive, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19864089,-97.79342653&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1015%20W%20William%20Cannon%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Bridge at Turtle Creek",
      "address": "6020 South First St, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatturtlecreek.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20316,-97.78461&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6020%20South%20First%20St%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "LifeWorks Transitional Living Project",
      "address": "3710 S 2nd Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.lifeworksaustin.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.230219,-97.770575&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3710%20S%202nd%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Bridge at South Point",
      "address": "6808 South IH 35, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatsouthpoint.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.187981,-97.773445&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6808%20South%20IH%2035%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Enfield Gardens",
      "address": "2300 Enfield Road, Austin, Texas 78726",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2880687,-97.767135&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2300%20Enfield%20Road%2C%20Austin%2C%20Texas%2078726"
    },
    {
      "name": "Legacy Apartments",
      "address": "1342 Lamar Square Drive, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "https://www.maryleefoundation.org/programs-and-services/affordable-housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25370026,-97.76409912&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1342%20Lamar%20Square%20Drive%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "2112 E 8th Street",
      "address": "2112 E 8th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26280022,-97.71820068&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2112%20E%208th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Ivy Condos - Acq of 10 Units (2/5/13)",
      "address": "3204 Manchaca Road, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24027574,-97.78266876&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3204%20Manchaca%20Road%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heritage Heights",
      "address": "1627 E 11th Street, Austin, TX 78723",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266657,-97.722927&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1627%20E%2011th%20Street%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Stassney Apartments",
      "address": "5600 Nancy Dr., Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/stassney-apts..html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20859909,-97.7845993&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5600%20Nancy%20Dr.%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Leisure Time Village",
      "address": "1920 Gaston Place Dr, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.hacanet.org/location/leisure-time-village/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.313344999999998,-97.689629&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1920%20Gaston%20Place%20Dr%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "SOL",
      "address": "5921 Ventus Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25810051,-97.68470001&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5921%20Ventus%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "1109 Spearson Lane",
      "address": "1109 Spearson Lane, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20140076,-97.79419708&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1109%20Spearson%20Lane%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "907 Spence Street",
      "address": "907 Spence Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25869942,-97.7358017&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=907%20Spence%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Seville on 4th Street",
      "address": "1401 E 4th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26160049,-97.72930145&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1401%20E%204th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Corner",
      "address": "2504 San Gabriel Street, Austin, TX 78705",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://utcorner.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29039955,-97.74790192&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2504%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "GNDC Alley Flats",
      "address": "2808 Gonzales Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.260303,-97.708853&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2808%20Gonzales%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Lexington Hills Apartments",
      "address": "2430 Cromwell Circle, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "lexingtonhillsaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.226708,-97.720812&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2430%20Cromwell%20Circle%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "2106 Chestnut Avenue",
      "address": "2106 Chestnut Avenue, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.283231,-97.717821&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2106%20Chestnut%20Avenue%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "44 East-UNDER CONSTRUCTION",
      "address": "44 East Avenue, Austin, TX 78701",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.44eastaveaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.255834,-97.739021&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=44%20East%20Avenue%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "2004 Chicon Street",
      "address": "2004 Chicon Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28109932,-97.72180176&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2004%20Chicon%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "2608 Salado Multifamily",
      "address": "2608 Salado Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29161949,-97.74552828&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2608%20Salado%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Metropolis Apartments",
      "address": "2200 Pleasant Valley Rd, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "metropolisapartmentsaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2317009,-97.72640228&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2200%20Pleasant%20Valley%20Rd%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Santa Rita Courts",
      "address": "2341 Corta St, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/santa-rita-courts/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2560997,-97.71720123&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2341%20Corta%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "The Guthrie II",
      "address": "3218 Gonzales Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://theguthrieaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25830078,-97.70459747&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3218%20Gonzales%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "1804 Martin Luther King Jr Boulevard",
      "address": "1804 Martin Luther King Jr Boulevard, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28000069,-97.72180176&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1804%20Martin%20Luther%20King%20Jr%20Boulevard%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Bridge at Tech Ridge",
      "address": "12800 Center Lake Dr, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeattechridge.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.409242,-97.668016&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12800%20Center%20Lake%20Dr%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Pecan Springs Commons",
      "address": "5807 Sweeney Circle, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30509949,-97.67900085&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5807%20Sweeney%20Circle%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Crest at Pearl",
      "address": "2323 San Antonio Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.americancampus.com/student-apartments/tx/austin/crest-at-pearl",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28732,-97.742444&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2323%20San%20Antonio%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "2203 Salina Street Rehab",
      "address": "2203 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28300095,-97.72270203&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2203%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Crossroads Apartments",
      "address": "8801 McCann Drive, Austin, TX 78757",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://foundcom.org/housing/our-austin-communities/crossroads-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.37240028,-97.72869873&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8801%20McCann%20Drive%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "Bexley at Whitestone",
      "address": "9826 N Lake Creek Parkway, Austin, TX 78717",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "bexleywhitestone.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.47220039,-97.79250336&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9826%20N%20Lake%20Creek%20Parkway%2C%20Austin%2C%20TX%2078717"
    },
    {
      "name": "2503 E 9th Steet",
      "address": "2503 E 9th Steet, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26469994,-97.71430206&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2503%20E%209th%20Steet%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Block on 25th West",
      "address": "2501 Pearl Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28958455,-97.7462254&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2501%20Pearl%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Texan at Pearl",
      "address": "2515 Pearl Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29064945,-97.74625073&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2515%20Pearl%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "ADU",
      "address": "5413 Duval Street, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.31830025,-97.71700287&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5413%20Duval%20Street%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "Timbers",
      "address": "1034 Clayton Ln, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3192997,-97.70359802&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1034%20Clayton%20Ln%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "2407 S 4th Street",
      "address": "2407 S 4th Street, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24259949,-97.76319885&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2407%20S%204th%20Street%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heritage Heights",
      "address": "1608 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265298,-97.724707&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1608%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1607 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266121,-97.724503&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1607%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1605 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266166,-97.724673&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1605%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Glen Oaks Corner",
      "address": "916 Neal Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26880074,-97.71050262&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=916%20Neal%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Austin Travis County MHMR",
      "address": "403 E 15th Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.integralcare.org/en/home/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27549934,-97.73560333&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=403%20E%2015th%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Manor Town Apartments",
      "address": "200 W Carrie Manor St, Manor, TX 78653",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hatctx.com/manor-town/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.339698,-97.559283&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=200%20W%20Carrie%20Manor%20St%2C%20Manor%2C%20TX%2078653"
    },
    {
      "name": "4902 West Wind Trail",
      "address": "4902 West Wind Trail, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2276001,-97.80329895&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4902%20West%20Wind%20Trail%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Alexander Oaks Apartments",
      "address": "6119 Valiant Circle, Austin, TX 78749",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hatctx.com/alexander-oaks/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.23390007,-97.85160065&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6119%20Valiant%20Circle%2C%20Austin%2C%20TX%2078749"
    },
    {
      "name": "2505 Village Trail Circle",
      "address": "2505 Village Trail Circle, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19479942,-97.7460022&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2505%20Village%20Trail%20Circle%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "10th Street Alley Flat",
      "address": "1817 W 10th Street, Austin, TX 78703",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.clarksvillecdc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28129959,-97.76509857&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1817%20W%2010th%20Street%2C%20Austin%2C%20TX%2078703"
    },
    {
      "name": "Volume",
      "address": "2604 Manor Rd., Austin, TX 78722",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "volumeapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28549957,-97.71489716&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2604%20Manor%20Rd.%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Guadalupe Saldana - Duplex",
      "address": "1220 Paul Theresa Saldana Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27020073,-97.70249939&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1220%20Paul%20Theresa%20Saldana%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1609 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265297,-97.724708&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1609%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "King Fisher Creek Apartments",
      "address": "4601 E Saint Elmo, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.greystonerents.com/KingfisherCreek.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20429993,-97.73750305&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4601%20E%20Saint%20Elmo%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "The Flagship",
      "address": "1312 Lamar Square, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.maryleefoundation.org/programs-and-services/affordable-housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2548008,-97.76319885&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1312%20Lamar%20Square%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Saint Louise House II",
      "address": "Undisclosed, Austin, TX Undisclosed",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://saintlouisehouse.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21100044,-97.80400085&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=Undisclosed%2C%20Austin%2C%20TX%20Undisclosed"
    },
    {
      "name": "La Vista de Guadalupe",
      "address": "813 E 8th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26709938,-97.73290253&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=813%20E%208th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "1803 E 20th Street",
      "address": "1803 E 20th Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28050041,-97.72200012&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1803%20E%2020th%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "2110 Salina Street",
      "address": "2110 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28240013,-97.72309875&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2110%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Live Oak Trails",
      "address": "8500 US Highway 71 W, Austin, TX 78735",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/live-oak-trails-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24869919,-97.89029694&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8500%20US%20Highway%2071%20W%2C%20Austin%2C%20TX%2078735"
    },
    {
      "name": "Avon at 22nd",
      "address": "911 W 22nd Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285403,-97.747712&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=911%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1007 Waller Street",
      "address": "1007 Waller Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26869965,-97.72930145&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1007%20Waller%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "GNDC Alley Flats",
      "address": "2808 Gonzales Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.260303,-97.708853&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2808%20Gonzales%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Elysium Grand",
      "address": "3300 Oak Creek Drive, Austin, Texas 78727",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.elysiumgrandapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.427826,-97.704833&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3300%20Oak%20Creek%20Drive%2C%20Austin%2C%20Texas%2078727"
    },
    {
      "name": "Hawthorne at the District",
      "address": "2239 Cromwell Circle, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.hawthorneatthedistrict.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22789955,-97.71829987&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2239%20Cromwell%20Circle%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "700 E 11th Street-UNDER CONSTRUCTION",
      "address": "700 E 11th Street, Austin, TX 78701",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.271082,-97.733918&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=700%20E%2011th%20Street%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "Glen Oaks Corner",
      "address": "904 Neal Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26849937,-97.70999908&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=904%20Neal%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Vintage",
      "address": "904 W 22nd Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28561668,-97.74701685&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=904%20W%2022nd%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Rosement at Oak Valley",
      "address": "2800 Collins Creek Dr, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22459984,-97.73120117&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2800%20Collins%20Creek%20Dr%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Rio West Student Living",
      "address": "2704 Rio Grande Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29296103,-97.74454388&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2704%20Rio%20Grande%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1116 Harvard Street",
      "address": "1116 Harvard Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26720047,-97.71630096&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1116%20Harvard%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Villas on San Gabriel II",
      "address": "2414 San Gabriel Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.288843579999998,-97.74789193&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2414%20San%20Gabriel%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Camden Rainey Street",
      "address": "91 Rainey Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.camdenliving.com/austin-tx-apartments/camden-rainey-street",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26020539,-97.7379292&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=91%20Rainey%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Axis West Campus",
      "address": "2505 Longview Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.axiswestcampus.com/index",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29019928,-97.75050354&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2505%20Longview%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Garden Terrace Phase I",
      "address": "1015 W William Cannon Drive, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19864089,-97.79342653&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1015%20W%20William%20Cannon%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "St. James Apartments",
      "address": "5514 Roosevelt Avenue, Austin, TX 78756",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "http://burlingtonventures.com/saint-james-5514-roosevelt/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32699966,-97.73290253&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5514%20Roosevelt%20Avenue%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "Modera Domain-UNDER CONSTRUCTION",
      "address": "2618 Kramer Lane, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.395266,-97.718565&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2618%20Kramer%20Lane%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Hotel Allandale",
      "address": "7685 Northcross Drive, Austin, TX 78757",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "http://www.theallandale.com/index.php",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35409927,-97.73529816&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7685%20Northcross%20Drive%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "Ellora Apartments",
      "address": "908 W 21st Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28498076,-97.74746589&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=908%20W%2021st%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Bent Tree Apartments",
      "address": "8405 Bent Tree Road, Austin, TX 78759",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.benttreeaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.372794,-97.743142&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8405%20Bent%20Tree%20Road%2C%20Austin%2C%20TX%2078759"
    },
    {
      "name": "303 San Saba Street",
      "address": "303 San Saba Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25600052,-97.71430206&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=303%20San%20Saba%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Heritage Heights",
      "address": "1607 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265309,-97.724877&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1607%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Austin Travis County MHMR",
      "address": "6222 N Lamar Boulevard, Austin, TX 78757",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.integralcare.org/en/home/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3314991,-97.72399902&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6222%20N%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "2111 Salina Street",
      "address": "2111 Salina Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28240013,-97.72260284&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2111%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Mary Lee Charles Place",
      "address": "1345 Lamar Square Drive, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.maryleefoundation.org/programs-and-services/affordable-housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25419998,-97.76409912&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1345%20Lamar%20Square%20Drive%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Heritage Heights",
      "address": "1600 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265715,-97.725015&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1600%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Oak Springs Villas",
      "address": "3001 Oak Springs Drive, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "https://www.voa.org/housing_properties/oak-springs-villas",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27269936,-97.70140076&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3001%20Oak%20Springs%20Drive%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Accessible Housing Austin! - SF",
      "address": "7624 Cayenne Lane, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "http://www.ahaustin.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22410011,-97.69129944&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7624%20Cayenne%20Lane%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "HPI-Domain Office",
      "address": "10721 Domain Drive, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39543,-97.726734&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10721%20Domain%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Jeremiah Program Moody Campus",
      "address": "1200 Paul Theresa Saldana Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://jeremiahprogram.org/austin/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.27000046,-97.70290375&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1200%20Paul%20Theresa%20Saldana%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "ACDDC - Alley Flat",
      "address": "1406 Ruth Avenue, Austin, TX 78757",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.338875,-97.728433&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1406%20Ruth%20Avenue%2C%20Austin%2C%20TX%2078757"
    },
    {
      "name": "Thornton Flats",
      "address": "2501 Thornton Road, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.thorntonflats.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24349976,-97.77269745&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2501%20Thornton%20Road%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "1700 E Martin Luther King Jr Boulevard",
      "address": "1700 E Martin Luther King Jr Boulevard, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.279744,-97.723068&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1700%20E%20Martin%20Luther%20King%20Jr%20Boulevard%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Benjamin Todd Apartments",
      "address": "1507 West 39 1/2 St, Austin, TX 78756",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/benjamin-todd-apts..html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30739975,-97.74479675&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1507%20West%2039%201/2%20St%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "5611 Teri Road",
      "address": "5611 Teri Road, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19269943,-97.73390198&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5611%20Teri%20Road%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Heritage Heights",
      "address": "1624 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266176,-97.723095&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1624%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "9215 Kempler Drive",
      "address": "9215 Kempler Drive, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://ahaustin.org/aha-accessible-housing-austin/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.1807003,-97.82150269&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9215%20Kempler%20Drive%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Treaty Oaks",
      "address": "3700 Manchaca Road, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.austintexas.gov/department/treaty-oaks-apartments",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2364006,-97.78450012&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3700%20Manchaca%20Road%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "AVIA at 26th",
      "address": "1010 W 26th Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.291287,-97.748907&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1010%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Villas on Nueces",
      "address": "2207 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285871,-97.743575&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2207%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Saint Louise House",
      "address": "Undisclosed, Austin, TX Undisclosed",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://saintlouisehouse.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24189949,-97.78540039&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=Undisclosed%2C%20Austin%2C%20TX%20Undisclosed"
    },
    {
      "name": "Quarters at Montgomery",
      "address": "2700 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2920704,-97.74320662&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2700%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Heritage Heights",
      "address": "1607 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266118,-97.724505&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1607%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Cobblestone Court",
      "address": "2101 Davis Lane, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "https://www.nationalchurchresidences.org/communities/tx/austin/cobblestone-court",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18560028,-97.8184967&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2101%20Davis%20Lane%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Chalmers Courts",
      "address": "300 Chicon St, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/chalmers-courts/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26000023,-97.72450256&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=300%20Chicon%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Bridge at Center Ridge",
      "address": "701 Center Ridge Dr, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatcenterridge.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.412638,-97.668456&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=701%20Center%20Ridge%20Dr%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "2014 Covered Wagon Pass",
      "address": "2014 Covered Wagon Pass, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19319916,-97.75869751&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2014%20Covered%20Wagon%20Pass%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "SOL",
      "address": "5929 Lux Street, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25860023,-97.68419647&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5929%20Lux%20Street%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Regents at 26th",
      "address": "900 W 26th Street, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.cwsapartments.com/regents-west-at-26th-austin-tx/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.29120064,-97.74649811&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=900%20W%2026th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Texas Alpha House",
      "address": "2501 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28932076,-97.74277481&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2501%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1803 E 22nd Steet",
      "address": "1803 E 22nd Steet, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2826004,-97.72250366&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1803%20E%2022nd%20Steet%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Radius on Grove",
      "address": "2301 Grove Boulevard, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.radiusongrove.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22299957,-97.7071991&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2301%20Grove%20Boulevard%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Guadalupe Saldana - Duplex",
      "address": "1216 Paul Theresa Saldana Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26989937,-97.70269775&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1216%20Paul%20Theresa%20Saldana%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Eastern Oaks",
      "address": "4922 Nuckols Crossing, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hatctx.com/eastern-oaks/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19630051,-97.73829651&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4922%20Nuckols%20Crossing%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Heritage Heights",
      "address": "1619 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.265931,-97.72366&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1619%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Villas on Nueces-UNDER CONSTRUCTION",
      "address": "2207 Nueces Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://www.villasonnueces.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285871,-97.743575&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2207%20Nueces%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Plaza Saltillo-UNDER CONSTRUCTION",
      "address": "1011 E 5th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.263744,-97.732588&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1011%20E%205th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Cornerstone",
      "address": "1322 Lamar Square Drive, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "N/A",
      "waitlist": "True",
      "website": "https://www.maryleefoundation.org/programs-and-services/affordable-housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2553997,-97.76450348&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1322%20Lamar%20Square%20Drive%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "2102 Chicon Street",
      "address": "2102 Chicon Street, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.blacklandcdc.org/housing/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28210068,-97.72200012&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2102%20Chicon%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Texan Shoal Creek",
      "address": "2502 Leon Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28985058,-97.74945961&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2502%20Leon%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "2008 East 10th Street Unit B-UNDER CONSTRUCTION",
      "address": "2008 E. 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.264686,-97.718991&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2008%20E.%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Bridge at Terracina",
      "address": "8100 N Mopac Expressway, Austin, TX 78759",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.bridgeatterracina.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.365698,-97.744578&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8100%20N%20Mopac%20Expressway%2C%20Austin%2C%20TX%2078759"
    },
    {
      "name": "Heritage Heights",
      "address": "1622 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266259,-97.723201&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1622%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "4810 West Wind Trail",
      "address": "4810 West Wind Trail, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22789955,-97.8030014&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4810%20West%20Wind%20Trail%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Ivy Condos",
      "address": "3204 Manchaca Road, Austin, TX 78704",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "http://www.easterseals.com/centraltx/our-programs/adult-services/community-housing-services/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24020004,-97.78359985&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3204%20Manchaca%20Road%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "West Koenig Flats",
      "address": "5608 Avenue F, Austin, TX 78751",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.westkoenigaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3220005,-97.71920013&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5608%20Avenue%20F%2C%20Austin%2C%20TX%2078751"
    },
    {
      "name": "2109 Salina Street",
      "address": "2109 Salina Street, Austin, TX 78722",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28246,-97.722169&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2109%20Salina%20Street%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Accessible Housing Austin! - SF",
      "address": "9407 Kempler Drive, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://www.ahaustin.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17819977,-97.82219696&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9407%20Kempler%20Drive%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Burnet Flats",
      "address": "5453 Burnet Road, Austin, TX 78756",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.burnetflats.com/?utm_source=GoogleLocalListing&utm_medium=organic",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32999992,-97.73930359&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5453%20Burnet%20Road%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "Wilshire West (The Josephine)",
      "address": "4411 Airport Boulevard, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://theleafgroup.com/tlg/pages/Our%20Properties/josephine.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30089951,-97.71299744&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4411%20Airport%20Boulevard%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "Heritage Heights",
      "address": "1605 E 9th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26536,-97.724973&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1605%20E%209th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Cherry Creek Duplexes",
      "address": "5510-B Fernview Road, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://foundcom.org/housing/our-austin-communities/cherry-creek-duplexes/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21859932,-97.80339813&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5510-B%20Fernview%20Road%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "St. Georges Court",
      "address": "1443 Coronado Hills Dr, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://stgeorgescourt.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32769966,-97.68939972&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1443%20Coronado%20Hills%20Dr%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Twenty Four Zero One",
      "address": "620 W 24th Street, Austin, TX 78705",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.288072,-97.743941&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=620%20W%2024th%20Street%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "1211 Inks Avenue",
      "address": "1211 Inks Avenue, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26589966,-97.72789764&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1211%20Inks%20Avenue%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "4904 West Wind Trail",
      "address": "4904 West Wind Trail, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://frameworkscdc.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22739983,-97.80339813&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4904%20West%20Wind%20Trail%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Preserve at Wells Branch",
      "address": "1773 Wells Branch Pkwy., Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.preserveatwellsbranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.434368,-97.67293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1773%20Wells%20Branch%20Pkwy.%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Bridge at Northwest Hills",
      "address": "3600 Greystone Dr, Austin, TX 78731",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.bridgeatnorthwesthills.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.358816,-97.75162&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3600%20Greystone%20Dr%2C%20Austin%2C%20TX%2078731"
    },
    {
      "name": "Heritage Heights",
      "address": "1604 E 10th Street, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.266652,-97.724337&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1604%20E%2010th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "7605 Elderberry Drive",
      "address": "7605 Elderberry Drive, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.greendoors.org/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18569946,-97.78949738&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7605%20Elderberry%20Drive%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "1141 Shady Lane Mixed Use",
      "address": "1141 Shady Lane, Austin, TX 78721",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26259995,-97.69120026&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1141%20Shady%20Lane%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "110 Chicon Street",
      "address": "110 Chicon Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.guadalupendc.org",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25799942,-97.72460175&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=110%20Chicon%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Franklin Gardens",
      "address": "3522 E Martin Luther King Jr Blvd., Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "http://www.prakpropertymanagement.com/franklin-gardens.html",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28389931,-97.69709778000001&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3522%20E%20Martin%20Luther%20King%20Jr%20Blvd.%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Tricia & David Ciccocioppo (Homeowners) -Alley Flat",
      "address": "1608 Cedar Avenue, Austin, TX 78702",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.278448,-97.714054&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1608%20Cedar%20Avenue%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Gibson Flats",
      "address": "1219 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://gibsonflats.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2541008,-97.76200104&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1219%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Woodway Village",
      "address": "4600 Nuckols Crossing, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.woodwayvillage.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.200071,-97.735362&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4600%20Nuckols%20Crossing%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "IBM 45 Multifamily-Flatiron Domain",
      "address": "10727 Domain Drive, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://flatirondomain.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39369965,-97.7256012&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10727%20Domain%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "SAMdorosa",
      "address": "6700 Manchaca Road, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "http://b-austin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.20639992,-97.80709839&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6700%20Manchaca%20Road%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Villas del Sol",
      "address": "1711 Rutland Drive, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "n/a",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.37509918,-97.71240234&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1711%20Rutland%20Drive%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Wildhorse Flats",
      "address": "10525 Wildhorse Ranch Trl, Manor, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "wildhorseflats.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.33089,-97.282419&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10525%20Wildhorse%20Ranch%20Trl%2C%20Manor%2C%20TX%2078724"
    },
    {
      "name": "Sync at Mueller",
      "address": "4646 Mueller Boulevard, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.syncatmueller.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.299753,-97.707009&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4646%20Mueller%20Boulevard%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Fivetwo at Highland - North",
      "address": "110 Jacob Fontaine Lane, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://fivetwoapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.324758,-97.714668&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=110%20Jacob%20Fontaine%20Lane%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Crosstown",
      "address": "6507 East Riverside Drive, Austin, Texas 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.crosstownatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2235169,-97.7016586&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6507%20East%20Riverside%20Drive%2C%20Austin%2C%20Texas%2078741"
    },
    {
      "name": "Broadstone East End",
      "address": "5400 Jain Lane, Austin, Texas 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.broadstoneeastend.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.263033,-97.687228&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5400%20Jain%20Lane%2C%20Austin%2C%20Texas%2078721"
    },
    {
      "name": "The Johnny",
      "address": "613 W. St. John's Ave, Austin, Texas 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "thejohnnyatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3381246,-97.7160511&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=613%20W.%20St.%20John%27s%20Ave%2C%20Austin%2C%20Texas%2078752"
    },
    {
      "name": "Medina Highlands",
      "address": "212 W. Highland Mall Blvd, Austin, TX 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://medinahighlandsaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32886064,-97.71712042&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=212%20W.%20Highland%20Mall%20Blvd%2C%20Austin%2C%20TX%2078752"
    },
    {
      "name": "Lenox 7th",
      "address": "4910 East 7th Street, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://lenox7th.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25529,-97.70055&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4910%20East%207th%20Street%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Northshore",
      "address": "110 San Antonio St, Austin, Texas 78701",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "northshoreaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2654892,-97.7494043&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=110%20San%20Antonio%20St%2C%20Austin%2C%20Texas%2078701"
    },
    {
      "name": "Zilker Studios",
      "address": "1508 S Lamar Blvd, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/zilker-studios/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25215,-97.76502&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1508%20S%20Lamar%20Blvd%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "The Reserve at Springdale",
      "address": "5605 Springdale Road, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.reserveatspringdaleapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.299055,-97.672951&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5605%20Springdale%20Road%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Bridge at Cameron",
      "address": "9201 Cameron Road, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://bridgeatcameronapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.348406,-97.674431&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9201%20Cameron%20Road%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "Preserve At Rolling Oaks",
      "address": "15450 FM 1325, Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.preserverollingoaks.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46724,-999.99999999&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=15450%20FM%201325%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "Villages at Ben White",
      "address": "7000 E Ben White, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://villagesatbenwhite.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.217468,-97.70192&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7000%20E%20Ben%20White%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Pathways at Shadowbend Ridge",
      "address": "6328 Shadow Bend, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/shadowbend-ridge/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.19809914,-97.78019714&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6328%20Shadow%20Bend%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Pathways at Georgian Manor",
      "address": "110 Bolles Cir, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "True",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/georgian-manor/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35000038,-97.70320129&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=110%20Bolles%20Cir%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "The Aspect",
      "address": "4900 E. Oltorf, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.theaspectaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22441,-97.72114&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4900%20E.%20Oltorf%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Bridge At Indigo",
      "address": "10800 Lakeline Blvd., Austin, TX 78717",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatindigo.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.48345,-97.79374&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10800%20Lakeline%20Blvd.%2C%20Austin%2C%20TX%2078717"
    },
    {
      "name": "Laurel Creek",
      "address": "11704 N. Lamar, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://foundcom.org/housing/our-austin-communities/laurel-creek/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.386674,-97.684527&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=11704%20N.%20Lamar%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Fifteen15 South Lamar",
      "address": "1515 South Lamar Blvd, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.fifteen15southlamarapartments.com/fifteen-15-south-lamar-austin-tx/contact",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.25154,-97.76444&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1515%20South%20Lamar%20Blvd%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Waterloo Terrace",
      "address": "12190 N Mopac Expressway, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://foundcom.org/housing/our-austin-communities/waterloo-terrace/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.413996,-97.708737&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12190%20N%20Mopac%20Expressway%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Vi Collina",
      "address": "2401 E Oltorf Street, Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22853,-97.7296&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2401%20E%20Oltorf%20Street%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "The Works II at Pleasant Valley",
      "address": "835 N Pleasant Valley Road, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "http://theworksapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26472,-97.7072793&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=835%20N%20Pleasant%20Valley%20Road%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Cambrian East Riverside",
      "address": "1806 Clubview Avenue, Austin, TX 78741",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://prosperahcs.org/portfolio-posts/cambrian_east_riverside/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22718053,-97.70590877&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1806%20Clubview%20Avenue%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Roosevelt Gardens",
      "address": "5606 Roosevelt Avenue, Austin, TX 78756",
      "students_only": "N/A",
      "community_disabled": "True",
      "waitlist": "N/A",
      "website": "https://www.projecttransitions.org/housing",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32791,-97.73246&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5606%20Roosevelt%20Avenue%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "Villas on Rio",
      "address": "2111 Rio Grande St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://villasonrio.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.285037,-97.744711&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2111%20Rio%20Grande%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Zoey Apartments",
      "address": "5700 E Riverside Dr, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://livezoey.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22839,-97.708942&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5700%20E%20Riverside%20Dr%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Talavera Lofts",
      "address": "413 Navasota St, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://dmatalavera.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26244,-97.728944&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=413%20Navasota%20St%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Los Portales de Lena Guerrero",
      "address": "5225 Jain Ln, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "liveatlosportales.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26289677,-97.68721512&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5225%20Jain%20Ln%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "The Bowen",
      "address": "3000 Gracie Kiltz Ln, Austin, TX 78758",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://thebowen.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.395315,-97.7251&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3000%20Gracie%20Kiltz%20Ln%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Tip Top Rentals",
      "address": "2010 West Loop #D, Austin, Texas 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.372783599999998,-97.7209074&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2010%20West%20Loop%20%23D%2C%20Austin%2C%20Texas%2078758"
    },
    {
      "name": "Yugo Austin Waterloo",
      "address": "2400 Seton Ave, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.yugoaustinwaterloo.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28805,-97.74411&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2400%20Seton%20Ave%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Starlight",
      "address": "2901 Manor Rd, Austin, TX 78722",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.starlightatx.com/starlight-austin-tx/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28555,-97.71006&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2901%20Manor%20Rd%2C%20Austin%2C%20TX%2078722"
    },
    {
      "name": "The Mark at Austin",
      "address": "812 W 23rd St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.themarkatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.28786,-97.7463&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=812%20W%2023rd%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "The Standard at Austin",
      "address": "715 W 23rd St, Austin, TX 78705",
      "students_only": "True",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://thestandardaustin.landmark-properties.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.286992,-97.745828&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=715%20W%2023rd%20St%2C%20Austin%2C%20TX%2078705"
    },
    {
      "name": "Bridge at Loyola Lofts",
      "address": "6400 Loyola Ln., Austin, TX 78724",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeloyola.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.30329,-97.64853&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6400%20Loyola%20Ln.%2C%20Austin%2C%20TX%2078724"
    },
    {
      "name": "Oaks on North Plaza",
      "address": "9125 North Plaza, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.oaksonnorthplaza.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35409927,-97.68460083&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9125%20North%20Plaza%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Bell Lakeshore",
      "address": "2515 Elmont Drive, Austin, Texas 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.belllakeshore.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.2390265,-97.7233012&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2515%20Elmont%20Drive%2C%20Austin%2C%20Texas%2078741"
    },
    {
      "name": "Urban Oaks",
      "address": "6725 Circle S Road, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.urbanoaksapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.189959,-97.776293&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6725%20Circle%20S%20Road%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Bridge at Canyon Creek",
      "address": "9009 North FM 620, Austin, TX 78726",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatcanyoncreek.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.433230000000002,-97.84056&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9009%20North%20FM%20620%2C%20Austin%2C%20TX%2078726"
    },
    {
      "name": "Bridge at Heritage Woods",
      "address": "12205 N. Lamar Blvd., Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatheritagewoods.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3997,-97.67921&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12205%20N.%20Lamar%20Blvd.%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Pathways at Northgate Apts",
      "address": "9120 Northgate Blvd, Austin, TX 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/northgate-apartments/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.37120056,-97.71649933&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9120%20Northgate%20Blvd%2C%20Austin%2C%20TX%2078758"
    },
    {
      "name": "Commons at Goodnight",
      "address": "2022 E. Slaughter Ln, Austin, TX 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://commonsatgoodnightapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.152689,-97.763732&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2022%20E.%20Slaughter%20Ln%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "Bridge At Davenport Place",
      "address": "3301 Dessau Road, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.bridgeatdavenportplace.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.4027,-97.63892&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3301%20Dessau%20Road%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "The Bond",
      "address": "10300 Metropolitan Drive, Austin, Texas 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://thebondatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.384375,-97.717953&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10300%20Metropolitan%20Drive%2C%20Austin%2C%20Texas%2078758"
    },
    {
      "name": "44 South",
      "address": "4411 S Congress Avenue, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.cwsapartments.com/apartments/tx/austin/44-south/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.21660279,-97.76756656&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4411%20S%20Congress%20Avenue%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Crossroad Commons",
      "address": "8407 E. Parmer Ln, Manor, TX 78653",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.crossroadcommonsapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.35107,-97.59148&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8407%20E.%20Parmer%20Ln%2C%20Manor%2C%20TX%2078653"
    },
    {
      "name": "James on South First",
      "address": "8800 South 1st Street, Austin, Texas 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.jamesonsouthfirstapts.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17566942,-97.79954729&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8800%20South%201st%20Street%2C%20Austin%2C%20Texas%2078748"
    },
    {
      "name": "The Royce at 8100",
      "address": "8100 Anderson Mill, Austin, TX 78729",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.theroyce8100apts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.46264,-97.75691&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8100%20Anderson%20Mill%2C%20Austin%2C%20TX%2078729"
    },
    {
      "name": "Montecito Apartments",
      "address": "3111 Parker Lne, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.montecitoapartmentsaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22394,-97.74193&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=3111%20Parker%20Lne%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Arbors at Tallwood",
      "address": "8810 Tallwood Drive, Austin, TX 78759",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.arborsattallwood.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.37846,-97.74274&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8810%20Tallwood%20Drive%2C%20Austin%2C%20TX%2078759"
    },
    {
      "name": "Revolve ATX",
      "address": "112 Will Davis Drive, Austin, Texas 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://revolveatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.328063,-97.713864&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=112%20Will%20Davis%20Drive%2C%20Austin%2C%20Texas%2078752"
    },
    {
      "name": "Melrose Trail",
      "address": "13005 Heinemann Drive, Austin, TX 78727",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.melrosetrailapts.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.442,-97.7439&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=13005%20Heinemann%20Drive%2C%20Austin%2C%20TX%2078727"
    },
    {
      "name": "Agave at South Congress",
      "address": "625 E. Stassney Lane, Austin, TX 78745",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "agavesouthcongress.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.200764,-97.769295&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=625%20E.%20Stassney%20Lane%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Lucent Apartments",
      "address": "12201 Dessau Rd., Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.lucentapartmentsatx.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3857,-97.64899&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12201%20Dessau%20Rd.%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "The Studio at Thinkeast",
      "address": "1143 Shady Lane, Austin, TX 78721",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "www.thinkeastapartments.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.26192691,-97.68889586&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1143%20Shady%20Lane%2C%20Austin%2C%20TX%2078721"
    },
    {
      "name": "Bridge at Ribelin Ranch",
      "address": "9900 McNeil Dr, Austin, TX 78750",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "bridgeatribelinranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39568,-97.83227&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9900%20McNeil%20Dr%2C%20Austin%2C%20TX%2078750"
    },
    {
      "name": "Bridge at Southpark Meadows",
      "address": "715 W. Slaughter Ln, Austin, TX 78748",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatsouthparkmeadows.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17464,-97.80295&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=715%20W.%20Slaughter%20Ln%2C%20Austin%2C%20TX%2078748"
    },
    {
      "name": "Goodrich Place",
      "address": "2126 Goodrich Ave, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://www.hacanet.org/location/goodrich-place/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.24830055,-97.77390289&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2126%20Goodrich%20Ave%2C%20Austin%2C%20TX%2078704"
    },
    {
      "name": "Bridge At Monarch Bluffs",
      "address": "8515 South IH 35, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatmonarchbluffs.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17212,-97.78217&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8515%20South%20IH%2035%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Bridge at Harris Ridge",
      "address": "1501 E Howard Ln, Austin, TX 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "bridgeharrisridge.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.40419,-97.642227&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1501%20E%20Howard%20Ln%2C%20Austin%2C%20TX%2078754"
    },
    {
      "name": "Alexan Braker Pointe",
      "address": "10801 N. Mopac Hwy Building 4, Austin, Texas 78759",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://alexanbrakerpointe.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.39796,-97.748965&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=10801%20N.%20Mopac%20Hwy%20Building%204%2C%20Austin%2C%20Texas%2078759"
    },
    {
      "name": "Timbercreek",
      "address": "614 S. 1st Street, Austin, Texas 78704",
      "students_only": "N/A",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "timbercreekaustin.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.256634,-97.746071&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=614%20S.%201st%20Street%2C%20Austin%2C%20Texas%2078704"
    },
    {
      "name": "Arbors at Creekside",
      "address": "1026 Clayton Lane, Austin, TX 78723",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.nationalchurchresidences.org/communities/arbors-at-creekside/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.32028858,-97.7040865&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1026%20Clayton%20Lane%2C%20Austin%2C%20TX%2078723"
    },
    {
      "name": "Moonlight Gardens",
      "address": "5204 Charles Merle Dr., Austin, TX 78747",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://moonlightaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.15717,-97.7611&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5204%20Charles%20Merle%20Dr.%2C%20Austin%2C%20TX%2078747"
    },
    {
      "name": "The Martingale",
      "address": "8104 S. Congress, Austin, TX 78745",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "https://themartingaleaustin.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17806,-97.78558&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=8104%20S.%20Congress%2C%20Austin%2C%20TX%2078745"
    },
    {
      "name": "Bridge At Steiner Ranch",
      "address": "4800 Steiner Ranch Blvd., Austin, TX 78732",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatsteinerranch.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.38424,-97.87358&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4800%20Steiner%20Ranch%20Blvd.%2C%20Austin%2C%20TX%2078732"
    },
    {
      "name": "Estates at Norwood",
      "address": "916 Norwood Park, Austin, TX 78753",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "True",
      "website": "https://estatesatnorwood.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.34235,-97.69413&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=916%20Norwood%20Park%2C%20Austin%2C%20TX%2078753"
    },
    {
      "name": "Bridge at Balcones",
      "address": "12215 Hunters Chase Dr., Austin, TX 78729",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeatbalcones.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.4414,-97.77373&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=12215%20Hunters%20Chase%20Dr.%2C%20Austin%2C%20TX%2078729"
    },
    {
      "name": "Bridge At Canyon View",
      "address": "4506 E. William Cannon, Austin, TX 78744",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://bridgeatcanyonview.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.18603,-97.75677&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4506%20E.%20William%20Cannon%2C%20Austin%2C%20TX%2078744"
    },
    {
      "name": "Bridge At Henley",
      "address": "6107 E. Riverside, Austin, TX 78741",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.bridgeathenley.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.22531,-97.70479&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6107%20E.%20Riverside%2C%20Austin%2C%20TX%2078741"
    },
    {
      "name": "Heritage Estates At Wells Branch",
      "address": "14011 Owen Tech Blvd., Austin, TX 78728",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.heritageestatesatwellsbranch.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.43223,-97.67215&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=14011%20Owen%20Tech%20Blvd.%2C%20Austin%2C%20TX%2078728"
    },
    {
      "name": "The Preserve Hyde Park",
      "address": "6000 N Lamar Blvd, Austin, Texas 78752",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.thepreservehydepark.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.3289014,-97.7247648&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=6000%20N%20Lamar%20Blvd%2C%20Austin%2C%20Texas%2078752"
    },
    {
      "name": "MLK Highline",
      "address": "2832 E Martin Luther King Jr Blvd, Austin, TX 78702",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.mlkhighline.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.332352000598306,-97.63097899935805&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=2832%20E%20Martin%20Luther%20King%20Jr%20Blvd%2C%20Austin%2C%20TX%2078702"
    },
    {
      "name": "Hanover Republic Square",
      "address": "303 W 5th St, Austin, TX 78701",
      "students_only": "N/A",
      "community_disabled": "N/A",
      "waitlist": "N/A",
      "website": "N/A",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.264792,-97.7494085&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=303%20W%205th%20St%2C%20Austin%2C%20TX%2078701"
    },
    {
      "name": "Bridge at Delco Flats",
      "address": "7601 Springdale Rd, Austin, Texas 78742",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.bridgeatdelcoflats.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.318951,-97.660904&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=7601%20Springdale%20Rd%2C%20Austin%2C%20Texas%2078742"
    },
    {
      "name": "Alara North Burnet",
      "address": "1735 Rutland Drive, Austin, Texas 78758",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://www.northburnetapartments.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.374041599999998,-97.7124453&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1735%20Rutland%20Drive%2C%20Austin%2C%20Texas%2078758"
    },
    {
      "name": "Bridge at Paloma",
      "address": "9911 Dessau Rd, Austin, Texas 78754",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "www.bridgeatpaloma.com",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.358326,-97.664105&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=9911%20Dessau%20Rd%2C%20Austin%2C%20Texas%2078754"
    },
    {
      "name": "AMLI 5350",
      "address": "5350 Burnet Road, Austin, TX 78756",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "https://www.amli.com/5350?switch_code=94414&utm_source=Smart%2FAffordable%20Housing%20Program&utm_medium=Web&utm_campaign=5350%20AustinTX.gov [gcc01.safelinks.protection.outlook.com]",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.392879500495795,-97.69457150098356&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=5350%20Burnet%20Road%2C%20Austin%2C%20TX%2078756"
    },
    {
      "name": "Korina at The Grove",
      "address": "4424 Jackson Ave, Austin, TX 78731",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "N/A",
      "website": "https://korinaatx.com/",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.36182899982829,-97.8372650000552&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=4424%20Jackson%20Ave%2C%20Austin%2C%20TX%2078731"
    },
    {
      "name": "Post South Lamar",
      "address": "1500 S Lamar Boulevard, Austin, TX 78704",
      "students_only": "False",
      "community_disabled": "False",
      "waitlist": "False",
      "website": "http://www.maac.com/texas/austin/post-south-lamar",
      "street_view_url": "https://maps.googleapis.com/maps/api/streetview?location=30.17030350016828,-97.80896549965996&key=AIzaSyADq7fsUquUY9JyrJIW9_36CUGP__Pgj90&size=200x200&fov=90",
      "google_maps_url": "https://www.google.com/maps/search/?api=1&query=1500%20S%20Lamar%20Boulevard%2C%20Austin%2C%20TX%2078704"
    }
  ];