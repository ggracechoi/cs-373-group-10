export const thrift_data = [
    {
      "id": "4VVADHT3enN6U2ifmWF8gQ",
      "name": "Monkies Vintage and Thrift",
      "address": "1904 Guadalupe St, Austin, TX 78705",
      "phone_number": "(512) 520-4595",
      "website": "https://www.yelp.com/biz/monkies-vintage-and-thrift-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 53,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/D6srGxW2KClhoKnrmIgT1Q/o.jpg"
    },
    {
      "id": "C2OJITOy6IY47_1yhKal2A",
      "name": "Austin Pets Alive! Thrift - Burnet",
      "address": "5801 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 373-3099",
      "website": "https://www.yelp.com/biz/austin-pets-alive-thrift-burnet-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.8,
      "num_of_reviews": 135,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NgoEgWfn9L-3TicvfVPvUw/o.jpg"
    },
    {
      "id": "kOKCwfvGjFwRLUiP0xqdNA",
      "name": "Uptown Cheapskate - Austin",
      "address": "3005 S Lamar Blvd, Austin, TX 78704",
      "phone_number": "(512) 462-4646",
      "website": "https://www.yelp.com/biz/uptown-cheapskate-austin-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 322,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/snpywoXOVMZOUVFWVmFuOA/o.jpg"
    },
    {
      "id": "QlnWnbaKOzmsJeFmUSNKbg",
      "name": "Charm School Vintage",
      "address": "1111 E 11th St, Austin, TX 78702",
      "phone_number": "(512) 524-0166",
      "website": "https://www.yelp.com/biz/charm-school-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.6,
      "num_of_reviews": 46,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/QI7sAzGbo1bVd59o62YA4A/o.jpg"
    },
    {
      "id": "34Auebr1CDgF9izA7pVITA",
      "name": "Blue Velvet",
      "address": "217 W North Loop Blvd, Austin, TX 78751",
      "phone_number": "(512) 452-2583",
      "website": "https://www.yelp.com/biz/blue-velvet-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.4,
      "num_of_reviews": 98,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/9gl5ufrjq6olDKht_INVWQ/o.jpg"
    },
    {
      "id": "hoebho-FX0VtX3iEwJqkgw",
      "name": "Treasure City Thrift",
      "address": "2142 E 7th St, Austin, TX 78702",
      "phone_number": "(512) 524-2820",
      "website": "https://www.yelp.com/biz/treasure-city-thrift-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 116,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/NJb1lPfjojtHtUc5jmeI5Q/o.jpg"
    },
    {
      "id": "EcAo4cV_oDWPm8ydsObScg",
      "name": "Stardust Vintage",
      "address": "2810 Manchaca Rd, Austin, TX 78704",
      "phone_number": "(512) 551-2678",
      "website": "https://www.yelp.com/biz/stardust-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.6,
      "num_of_reviews": 12,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/5upwGTfI2lWQUdThhADbAg/o.jpg"
    },
    {
      "id": "ncM3c6PRJnOA5p8-7o_L0Q",
      "name": "DoubleTake ATX",
      "address": "6318 Burnet Rd, Austin, TX 78757",
      "phone_number": "(512) 922-5844",
      "website": "https://www.yelp.com/biz/doubletake-atx-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 17,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/ifLaCfvHJ3IHH_FFmC6bMw/o.jpg"
    },
    {
      "id": "meUd6W42aSAQRO8kBC9TEQ",
      "name": "St. Vincent de Paul Thrift Store",
      "address": "901 W Braker Ln, Austin, TX 78758",
      "phone_number": "(512) 442-5652",
      "website": "https://www.yelp.com/biz/st-vincent-de-paul-thrift-store-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 84,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/BjDE0sM6_ctHvqxjXMoLUA/o.jpg"
    },
    {
      "id": "uZO8QCvvAUFiIeiqLsGR1g",
      "name": "Next To New",
      "address": "5435 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 459-1288",
      "website": "https://www.yelp.com/biz/next-to-new-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 55,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NsWl_C44obqL5b1yh1OlDQ/o.jpg"
    },
    {
      "id": "YQSUw4IwkPXPwwpK2Nut2Q",
      "name": "Top Drawer",
      "address": "5312 Airport Blvd North Loop 53rd, Austin, TX 78751",
      "phone_number": "(512) 454-5161",
      "website": "https://www.yelp.com/biz/top-drawer-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 91,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/N3KL8PiOTqD3-uXZTHBOGA/o.jpg"
    },
    {
      "id": "9mXdirsuNqglyEKoSdFzjQ",
      "name": "Brazos Trading",
      "address": "6539 N Lamar, Austin, TX 78752",
      "phone_number": "(512) 796-5312",
      "website": "https://www.yelp.com/biz/brazos-trading-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 24,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/0frQJxTYNJk-mtj8G-_MJA/o.jpg"
    },
    {
      "id": "HdprPVSQ8u8rJ5ZF6z-5Pg",
      "name": "Thrift Town",
      "address": "5700 Manchaca Rd, Austin, TX 78745",
      "phone_number": "(512) 442-7200",
      "website": "https://www.yelp.com/biz/thrift-town-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.8,
      "num_of_reviews": 112,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/68HPNH7MH0TBrM0CzHr7Jg/o.jpg"
    },
    {
      "id": "1V20c-Kyl3pV0v7APcLVKA",
      "name": "Treasures Thrift Store",
      "address": "1099 E Main St, Round Rock, TX 78664",
      "phone_number": "(512) 244-2431",
      "website": "https://www.yelp.com/biz/treasures-thrift-store-round-rock-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 10,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/r1cxUUu8OTYLbnfWAw2YZw/o.jpg"
    },
    {
      "id": "GHHVJvOPYVdPY9w-nFcK7g",
      "name": "Room Service Vintage",
      "address": "117 E North Loop Blvd, Austin, TX 78751",
      "phone_number": "(512) 451-1057",
      "website": "https://www.yelp.com/biz/room-service-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.5,
      "num_of_reviews": 167,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/flKeQKWYx3Q2Ph4DvLpwJw/o.jpg"
    },
    {
      "id": "yTqygAeYWKEtEWjBqwr2jg",
      "name": "Lo-Fi Vintage",
      "address": "604 W 29th St, Austin, TX 78705",
      "phone_number": "(512) 792-9663",
      "website": "https://www.yelp.com/biz/lo-fi-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.4,
      "num_of_reviews": 19,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/4x4zBgFD7N56kcjHRviXhA/o.jpg"
    },
    {
      "id": "CFVz-Q-ACSyo-1VmZ--oTg",
      "name": "Flamingos Vintage Pound",
      "address": "2915 Guadalupe St, Austin, TX 78705",
      "phone_number": "(512) 432-5043",
      "website": "https://www.yelp.com/biz/flamingos-vintage-pound-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 30,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/zm6PSXgWO2tGNsn4v_l20g/o.jpg"
    },
    {
      "id": "mOl-PajM_pzXSlX0grGgOw",
      "name": "Hope Family Thrift Store",
      "address": "13801 Burnet Rd, Austin, TX 78727",
      "phone_number": "(512) 467-4940",
      "website": "https://www.yelp.com/biz/hope-family-thrift-store-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.7,
      "num_of_reviews": 31,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/1PR-MtGHxBfV949xmHb-pw/o.jpg"
    },
    {
      "id": "1490M-kTpkPHw_wXWGZeFQ",
      "name": "Uncommon Objects",
      "address": "1602 Fortview, Austin, TX 78704",
      "phone_number": "(512) 442-4000",
      "website": "https://www.yelp.com/biz/uncommon-objects-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 402,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/9lMNC87IXlCRHANYFtBNKg/o.jpg"
    },
    {
      "id": "s1CDV6kvo-5w0C-YZl3GIg",
      "name": "Goodwill Central Texas - South Outlet",
      "address": "6505 Burleson Rd, Austin, TX 78744",
      "phone_number": "(512) 681-3301",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-south-outlet-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.4,
      "num_of_reviews": 136,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/gH3n7xgmt7SvT7zZiZlsRQ/o.jpg"
    },
    {
      "id": "y0pdA2r2H4_c9scomkDN5w",
      "name": "Austin Antique Mall",
      "address": "8822 McCann Dr, Austin, TX 78757",
      "phone_number": "(512) 459-5900",
      "website": "https://www.yelp.com/biz/austin-antique-mall-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 132,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/G-lbzvLfRlBz4Ancsvp6Ig/o.jpg"
    },
    {
      "id": "JhUw_3xRgmg9ljIfrb3TCA",
      "name": "United Apparel Liquidators",
      "address": "249 W 2nd St, Austin, TX 78701",
      "phone_number": "(512) 912-1111",
      "website": "https://www.yelp.com/biz/united-apparel-liquidators-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 37,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/x1DKKL9z1X8o5ZwR59ardQ/o.jpg"
    },
    {
      "id": "mxmcCVEhOE85OE8u5j_Cqg",
      "name": "Texas Thrift Store",
      "address": "5319 N Ih 35, Austin, TX 78723",
      "phone_number": "(512) 380-0025",
      "website": "https://www.yelp.com/biz/texas-thrift-store-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.3,
      "num_of_reviews": 114,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/ieh8ckCcfbSGw2MMWQKmRg/o.jpg"
    },
    {
      "id": "cZaWOwTrOLX2QeJu8ejvKw",
      "name": "Far Out Home Fittings",
      "address": "1500 W Ben White Blvd, Austin, TX 78704",
      "phone_number": "(512) 445-3213",
      "website": "https://www.yelp.com/biz/far-out-home-fittings-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 56,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/tMxIQqHbdaWD2wLm-Awi9Q/o.jpg"
    },
    {
      "id": "mKVRXBhc9eSN2kP57BKQXQ",
      "name": "Second Looks",
      "address": "4107 S Capital Of Texas Hwy, Austin, TX 78704",
      "phone_number": "(512) 442-9797",
      "website": "https://www.yelp.com/biz/second-looks-austin-3?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 23,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/SmhE_sp63cfgWh1o3vk35A/o.jpg"
    },
    {
      "id": "EFHnlEQ-PieRN2pYkAH9Iw",
      "name": "Kyle Flea Market",
      "address": "1119 N Old Hwy 81, Kyle, TX 78640",
      "phone_number": "(512) 656-5958",
      "website": "https://www.yelp.com/biz/kyle-flea-market-kyle?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.8,
      "num_of_reviews": 31,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/7H5-xT-UrJLI8AYxwxqJpA/o.jpg"
    },
    {
      "id": "ETkgst4qmzfotJOPpkmSIg",
      "name": "Goodwill Central Texas - Lamar Oaks",
      "address": "4001 S Lamar Blvd, Austin, TX 78704",
      "phone_number": "(512) 637-7529",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-lamar-oaks-austin-3?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 7,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/H0pYACtU-DSZ_qAP1kFUPA/o.jpg"
    },
    {
      "id": "wNVuJntptsN6Uxf9loDsqA",
      "name": "Just Between Us Consignment",
      "address": "13233 Pond Springs Rd, Austin, TX 78729",
      "phone_number": "(512) 250-0746",
      "website": "https://www.yelp.com/biz/just-between-us-consignment-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 38,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/wYIPRJivY7-lp3yAE_oBKw/o.jpg"
    },
    {
      "id": "35_dm2nHsY2ajDg23MzW5Q",
      "name": "Buffalo Exchange",
      "address": "2904 Guadalupe St, Austin, TX 78705",
      "phone_number": "(512) 480-9922",
      "website": "https://www.yelp.com/biz/buffalo-exchange-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.0,
      "num_of_reviews": 368,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/gt9khh5JWLpQ-jgMnphz-w/o.jpg"
    },
    {
      "id": "vgiC9J19VO_k9xY2rmzihw",
      "name": "Goodwill Central Texas - North Outlet",
      "address": "2300 Scarbrough Dr, Austin, TX 78728",
      "phone_number": "(512) 681-3322",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-north-outlet-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 27,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/83cU5-bHl9A8QZYwL6dDmg/o.jpg"
    },
    {
      "id": "vFdpfD6wuHZS830I5-oTrQ",
      "name": "Thrift Land",
      "address": "512 W Stassney Ln, Austin, TX 78745",
      "phone_number": "(512) 326-1510",
      "website": "https://www.yelp.com/biz/thrift-land-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.4,
      "num_of_reviews": 92,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/xdFSJidor4r46JyTzhNeOQ/o.jpg"
    },
    {
      "id": "TMh9t4DSyWyNouN7SNVpPg",
      "name": "Austin Country Flea Market",
      "address": "9500 Hwy 290 E, Austin, TX 78724",
      "phone_number": "(512) 928-4711",
      "website": "https://www.yelp.com/biz/austin-country-flea-market-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.3,
      "num_of_reviews": 59,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/dKWo54KFFmfNffizC7ijNQ/o.jpg"
    },
    {
      "id": "R3RKBAfmAAdFr2WSoUvHMg",
      "name": "Austin Pets Alive! Thrift - Oltorf",
      "address": "1409 W Oltorf St, Austin, TX 78704",
      "phone_number": "(512) 386-1997",
      "website": "https://www.yelp.com/biz/austin-pets-alive-thrift-oltorf-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.6,
      "num_of_reviews": 60,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/H8bcGfbERFacF_uLaqhuNw/o.jpg"
    },
    {
      "id": "j3U5o1A71J_quDf3OnjC0A",
      "name": "Goodwill Central Texas - 10th St. Boutique",
      "address": "914 N Lamar Blvd, Austin, TX 78703",
      "phone_number": "(512) 681-3330",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-10th-st-boutique-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.6,
      "num_of_reviews": 10,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/MbYaEC29BYTpqVQk00ck4Q/o.jpg"
    },
    {
      "id": "kpIDPWEVsV0q3NvlHVm_Ew",
      "name": "Goodwill Central Texas - Lake Austin",
      "address": "2411 Lake Austin Blvd, Austin, TX 78703",
      "phone_number": "(512) 478-6711",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-lake-austin-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.1,
      "num_of_reviews": 51,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/ZsOANDR_9pxJ2KrRTLZPPQ/o.jpg"
    },
    {
      "id": "qhDlzftvxkmw2Qd70eU1vQ",
      "name": "Secret Oktober",
      "address": "4411 Russell Dr, Austin, TX 78745",
      "phone_number": "(512) 897-4803",
      "website": "https://www.yelp.com/biz/secret-oktober-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.4,
      "num_of_reviews": 36,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/hH3T8VQeJ8wsGqNIoz3gEQ/o.jpg"
    },
    {
      "id": "66X3gaq2MkX14BfTubIrhQ",
      "name": "The Salvation Army Family Store & Donation Center",
      "address": "4216 S Congress Ave, Austin, TX 78745",
      "phone_number": "(512) 447-4044",
      "website": "https://www.yelp.com/biz/the-salvation-army-family-store-and-donation-center-austin-11?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 51,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/XZSEb7PdjSI4YkIKvND6aQ/o.jpg"
    },
    {
      "id": "wPvIWpnpHNVTLsLpGbgK6g",
      "name": "Feathers Boutique",
      "address": "1700B S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 912-9779",
      "website": "https://www.yelp.com/biz/feathers-boutique-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 89,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/38Shw4QSJ_YuO-E-UX5FuA/o.jpg"
    },
    {
      "id": "ysnR_iiBwub9o8IttXd7IQ",
      "name": "Pavement - Modern & Recycled Fashion",
      "address": "611 S Lamar Blvd, Austin, TX 78704",
      "phone_number": "(512) 551-3132",
      "website": "https://www.yelp.com/biz/pavement-modern-and-recycled-fashion-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.7,
      "num_of_reviews": 75,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/VBcFjwXJ_cIR8MkSHpUpNA/o.jpg"
    },
    {
      "id": "d0x-wheHlqLqTOZSG55ebw",
      "name": "It's New To Me",
      "address": "7719 Burnet Rd, Austin, TX 78757",
      "phone_number": "(512) 451-0388",
      "website": "https://www.yelp.com/biz/its-new-to-me-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 47,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/nUjmjI7W4CduVkVQiCStcA/o.jpg"
    },
    {
      "id": "YyqZbxqs673YEAQW0_uIKA",
      "name": "Big Bertha's Paradise",
      "address": "112 E N Loop Blvd, Austin, TX 78751",
      "phone_number": "(512) 444-5908",
      "website": "https://www.yelp.com/biz/big-berthas-paradise-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.2,
      "num_of_reviews": 58,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/rCSv4uz_-dyMxFiNbmSifw/o.jpg"
    },
    {
      "id": "oUiAIQU0GF9xnSyfyrx-CQ",
      "name": "Revival Vintage",
      "address": "5201 North Lamar Blvd, Austin, TX 78751",
      "phone_number": "(512) 524-2029",
      "website": "https://www.yelp.com/biz/revival-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.7,
      "num_of_reviews": 17,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/1bNLK5CfLQVa6TOcII8Z8w/o.jpg"
    },
    {
      "id": "PW8d327HbV5Yb_8pMmS3Hg",
      "name": "Lions Thrift Shop",
      "address": "108 N Main St, Elgin, TX 78621",
      "phone_number": "(512) 285-5525",
      "website": "https://www.yelp.com/biz/lions-thrift-shop-elgin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.5,
      "num_of_reviews": 6,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/XjbQvK98Diqdd_5eB7FlHA/o.jpg"
    },
    {
      "id": "bikIrr3UwQSpeNU6TqtEow",
      "name": "Goodwill Central Texas Boutique - Westbank",
      "address": "2814 Bee Cave Rd, Austin, TX 78746",
      "phone_number": "(512) 329-8771",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-boutique-westbank-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.2,
      "num_of_reviews": 27,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/_m8HtPn7I6qTk7jOg6e50A/o.jpg"
    },
    {
      "id": "LLEkwii2OWuTbg_byjjuZQ",
      "name": "Goodwill",
      "address": "12400 W Hwy 71, Bee Cave, TX 78738",
      "phone_number": "(512) 263-2379",
      "website": "https://www.yelp.com/biz/goodwill-bee-cave?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.5,
      "num_of_reviews": 6,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/EJUyB2f3007rAr6wtAILKg/o.jpg"
    },
    {
      "id": "nM3_I6lkQzp4JPAoqDTAHg",
      "name": "Wags Thrift Store",
      "address": "3411 Ranch Rd 620, Austin, TX 78734",
      "phone_number": "",
      "website": "https://www.yelp.com/biz/wags-thrift-store-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.6,
      "num_of_reviews": 5,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/he_pDvh0ks8ahYB4WRLUXA/o.jpg"
    },
    {
      "id": "PMI15_56jt9I3yelcpDAJg",
      "name": "Triple Z Threadz",
      "address": "1708 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 994-6878",
      "website": "https://www.yelp.com/biz/triple-z-threadz-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.5,
      "num_of_reviews": 26,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/SLcjbKEqgsxzUp5HZ5VLbA/o.jpg"
    },
    {
      "id": "yZO4agur4EpedZjf-G4XmQ",
      "name": "Savers",
      "address": "11101 Pecan Park Blvd, Cedar Park, TX 78613",
      "phone_number": "(512) 257-0359",
      "website": "https://www.yelp.com/biz/savers-cedar-park?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.7,
      "num_of_reviews": 57,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/xthPWlCatG7fWJXDzMp0sA/o.jpg"
    },
    {
      "id": "5O4o8aKv2Zimw7-YS3Nesw",
      "name": "Goodwill Central Texas - North Lamar",
      "address": "5555 N Lamar Blvd, Austin, TX 78751",
      "phone_number": "(512) 451-2306",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-north-lamar-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 71,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/Bekrh9sadf7UCwC9NL-OaA/o.jpg"
    },
    {
      "id": "4HiZqeTK--BVwcU8QtWXxw",
      "name": "Maya Star",
      "address": "1508 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 912-1475",
      "website": "https://www.yelp.com/biz/maya-star-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 96,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/hGVhCS3-KnQzP66zrapv2w/o.jpg"
    },
    {
      "id": "yMmkdG3SyBcC_JuSkHS-lQ",
      "name": "Atown",
      "address": "5502 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 323-2533",
      "website": "https://www.yelp.com/biz/atown-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 179,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/SjxEkb3sEjvKILxw9A0oOA/o.jpg"
    },
    {
      "id": "lASgNIlcA1JHUclObGtJlA",
      "name": "Thrift House",
      "address": "4901 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 458-2633",
      "website": "https://www.yelp.com/biz/thrift-house-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.4,
      "num_of_reviews": 5,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/wjzL5YQLNiDHIu-AdVLn7w/o.jpg"
    },
    {
      "id": "sR2HX6obmcFWG4lPV943Qg",
      "name": "Prototype Vintage Design",
      "address": "1700 1/2 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 447-7686",
      "website": "https://www.yelp.com/biz/prototype-vintage-design-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 39,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/TvmWVOHiHvRqAQ1oMbsaCQ/o.jpg"
    },
    {
      "id": "OjnW_lEwKmBbKNfre7cqgw",
      "name": "Austin Pets Alive - 51st",
      "address": "5102 Clarkson Ave, Austin, TX 78751",
      "phone_number": "(512) 906-0303",
      "website": "https://www.yelp.com/biz/austin-pets-alive-51st-austin-3?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 10,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/woMIMGQjrhpSwp9vDdoUMA/o.jpg"
    },
    {
      "id": "VNNku49hObKjH46orhMhlg",
      "name": "City Wide Garage Sale",
      "address": "900 Barton Springs Rd, Austin, TX 78704",
      "phone_number": "(512) 441-2828",
      "website": "https://www.yelp.com/biz/city-wide-garage-sale-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.3,
      "num_of_reviews": 31,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/94ELhkVU8m6YzCJ8kaMPVA/o.jpg"
    },
    {
      "id": "LL_5DuSrV5OpHOFZhZT8Mg",
      "name": "Round Rock Premium Outlets",
      "address": "4401 N Interstate 35, Round Rock, TX 78664",
      "phone_number": "(512) 863-6688",
      "website": "https://www.yelp.com/biz/round-rock-premium-outlets-round-rock?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.7,
      "num_of_reviews": 189,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/UDQOI933mouW6XheJvXbGw/o.jpg"
    },
    {
      "id": "7wE7UNImH89qAglbFJiKdQ",
      "name": "Lake Travis Thrift Shop",
      "address": "440 Medical Pkwy, Lakeway, TX 78738",
      "phone_number": "(512) 263-0314",
      "website": "https://www.yelp.com/biz/lake-travis-thrift-shop-lakeway?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 32,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/jNQxJBEZ7d0z94hgRWwcVA/o.jpg"
    },
    {
      "id": "v6WF9DTV5ypiSuLL0krdrQ",
      "name": "Pavement Modern & Recycled Clothing",
      "address": "2932 Guadalupe St, Austin, TX 78705",
      "phone_number": "(512) 551-3132",
      "website": "https://www.yelp.com/biz/pavement-modern-and-recycled-clothing-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.6,
      "num_of_reviews": 27,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/vxu8wnbf1DU19_O93Fv00Q/o.jpg"
    },
    {
      "id": "FDNAWzsY_QHFZQAX_SiSeg",
      "name": "HCCM Thrift Store",
      "address": "1501 Leander Dr, Leander, TX 78641",
      "phone_number": "(512) 334-6464",
      "website": "https://www.yelp.com/biz/hccm-thrift-store-leander-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 5.0,
      "num_of_reviews": 1,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/EXhGXqqU0euwAlvCi4Atow/o.jpg"
    },
    {
      "id": "giEWKfP-KKs1nxuK0t0Shw",
      "name": "Styling by Sydney",
      "address": "None, Austin, TX 78705",
      "phone_number": "(512) 731-6053",
      "website": "https://www.yelp.com/biz/styling-by-sydney-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 5.0,
      "num_of_reviews": 7,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/pvZay-yZj91Eo62lu3GJ5A/o.jpg"
    },
    {
      "id": "G5RMlp3GOlAvcgDRp_KTjw",
      "name": "Home Consignment Center",
      "address": "12812 Shops Pkwy, Bee Cave, TX 78738",
      "phone_number": "(512) 263-4600",
      "website": "https://www.yelp.com/biz/home-consignment-center-bee-cave?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.2,
      "num_of_reviews": 46,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/GKhU4FE8IITS7dyJ3_ApAg/o.jpg"
    },
    {
      "id": "yjlsB4V_UWIEGksY96aCRQ",
      "name": "Assistance League of Austin",
      "address": "4901 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 458-2633",
      "website": "https://www.yelp.com/biz/assistance-league-of-austin-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 24,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/5y5qpOIHvg2j8a5MDieGfg/o.jpg"
    },
    {
      "id": "Bt9N2hxDldR6f0umFtYn0w",
      "name": "Home Consignment Center",
      "address": "10515 N Mopac Expy, Austin, TX 78759",
      "phone_number": "(512) 346-2900",
      "website": "https://www.yelp.com/biz/home-consignment-center-austin-3?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 84,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/0v4v5nqqh-Y_CkDptMICDg/o.jpg"
    },
    {
      "id": "6LuuPy41Bn9V5Hp8MvDUUg",
      "name": "The Caring Place",
      "address": "2000 Railroad Ave, Georgetown, TX 78627",
      "phone_number": "(512) 943-0700",
      "website": "https://www.yelp.com/biz/the-caring-place-georgetown?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.1,
      "num_of_reviews": 34,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/X3dWp3A7mOqz4EEf22G-ew/o.jpg"
    },
    {
      "id": "tKCcRNel7wDQwPizQr5Y-w",
      "name": "Goodwill Central Texas Boutique - Chimney Corners",
      "address": "3910 Far W Blvd, Austin, TX 78731",
      "phone_number": "(512) 795-6387",
      "website": "https://www.yelp.com/biz/goodwill-central-texas-boutique-chimney-corners-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.3,
      "num_of_reviews": 32,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/MLa91V4EUqpK8fUEBGrNpQ/o.jpg"
    },
    {
      "id": "MMkvQs5GUtKRxueV1e8tGA",
      "name": "Garment Modern + Vintage",
      "address": "701 S Lamar Blvd, Austin, TX 78704",
      "phone_number": "(512) 326-7670",
      "website": "https://www.yelp.com/biz/garment-modern-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 15,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/7xKq7QAvqKQFvK3rnxYaag/o.jpg"
    },
    {
      "id": "CzwJayyFqcsGoJDso6kIJw",
      "name": "Out of the Past Collectibles",
      "address": "5341 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 371-3550",
      "website": "https://www.yelp.com/biz/out-of-the-past-collectibles-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.2,
      "num_of_reviews": 74,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/S6km2iiBzCdbvfXxt_8VoQ/o.jpg"
    },
    {
      "id": "i_7yZsPHq4A8hyq0o9K0IA",
      "name": "MOSS Designer Consignment",
      "address": "705 South Lamar Blvd, Austin, TX 78704",
      "phone_number": "(512) 916-9961",
      "website": "https://www.yelp.com/biz/moss-designer-consignment-austin-4?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.2,
      "num_of_reviews": 72,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/60O7q-Gj5-YZ6Jep8yC2Bg/o.jpg"
    },
    {
      "id": "ppVWOC20Stkx8yCXtouXnA",
      "name": "Ermine Vintage",
      "address": "106 E N Loop Blvd, Austin, TX 78751",
      "phone_number": "",
      "website": "https://www.yelp.com/biz/ermine-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.5,
      "num_of_reviews": 24,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/JZHQb5K7RPgV8hIVgC4rWQ/o.jpg"
    },
    {
      "id": "I5-4QFuInqOjz_l0P6o-Ig",
      "name": "Assistance League of Georgetown Area",
      "address": "900 N Austin Ave, Georgetown, TX 78626",
      "phone_number": "(512) 864-2542",
      "website": "https://www.yelp.com/biz/assistance-league-of-georgetown-area-georgetown?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.8,
      "num_of_reviews": 5,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/opRBH0mgZrj59KIbViyxUg/o.jpg"
    },
    {
      "id": "K5M7eDkRXtSc_oPS5gryFw",
      "name": "Uptown Modern",
      "address": "5111 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 452-1200",
      "website": "https://www.yelp.com/biz/uptown-modern-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.2,
      "num_of_reviews": 61,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/4WEE90ZnoWEYBubYd4k9Lw/o.jpg"
    },
    {
      "id": "R9ur5tKF5NgJw_9Rw_yvZw",
      "name": "Goorin Bros.",
      "address": "1323 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 326-4287",
      "website": "https://www.yelp.com/biz/goorin-bros-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.4,
      "num_of_reviews": 72,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/ZZRi_hzyrVhSGDuBXHDixw/o.jpg"
    },
    {
      "id": "f76pxnlklz6f6MeT-xtqkw",
      "name": "Haute Exchange",
      "address": "400 West Hwy 290, Dripping Springs, TX 78620",
      "phone_number": "(512) 829-4430",
      "website": "https://www.yelp.com/biz/haute-exchange-dripping-springs?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 8,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/hsmXy6wXeLsk-EsI9Ytwsg/o.jpg"
    },
    {
      "id": "nBkCFgn_9HpIrAOmlvRMzg",
      "name": "The Salvation Army Family Store & Donation Center",
      "address": "601 W Louis Henna Blvd, Round Rock, TX 78664",
      "phone_number": "(512) 246-7292",
      "website": "https://www.yelp.com/biz/the-salvation-army-family-store-and-donation-center-round-rock-8?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.6,
      "num_of_reviews": 54,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/obUbSihYfHli--YWa0MeOA/o.jpg"
    },
    {
      "id": "_BGN-EMkdWog2Dpz_D-o6g",
      "name": "Tyler's",
      "address": "7434 N Lamar Blvd, Austin, TX 78752",
      "phone_number": "(512) 582-1252",
      "website": "https://www.yelp.com/biz/tylers-outlet-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.4,
      "num_of_reviews": 36,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/YNWlu7QiPVzu8yq6eN0f-Q/o.jpg"
    },
    {
      "id": "1ccrVcxg5OgG0Qn7aPqdAg",
      "name": "Say Hi",
      "address": "5249 Burnet Rd, Austin, TX 78756",
      "phone_number": "(512) 909-7446",
      "website": "https://www.yelp.com/biz/say-hi-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.4,
      "num_of_reviews": 17,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/U9sYORnYSU7hqUIHWH322Q/o.jpg"
    },
    {
      "id": "TVYUpxlPNae8i60eywEYPA",
      "name": "Thrift Center",
      "address": "4101 S 1st St, Austin, TX 78745",
      "phone_number": "(512) 804-2665",
      "website": "https://www.yelp.com/biz/thrift-center-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.2,
      "num_of_reviews": 35,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/Q9zuTWuNeG8MrDGMRPKguA/o.jpg"
    },
    {
      "id": "q8NmxaVXGpYtQjj4J025tg",
      "name": "Style Encore North Austin",
      "address": "2929 W Anderson Ln, Austin, TX 78757",
      "phone_number": "(512) 420-2226",
      "website": "https://www.yelp.com/biz/style-encore-north-austin-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.9,
      "num_of_reviews": 79,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/UqEIIcnYlZ-kxnN3VyFgyA/o.jpg"
    },
    {
      "id": "heWN7R97M_yyOlvzmavq3w",
      "name": "Yellow Bike Project",
      "address": "1216 Webberville Rd, Austin, TX 78721",
      "phone_number": "(512) 524-5299",
      "website": "https://www.yelp.com/biz/yellow-bike-project-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.7,
      "num_of_reviews": 27,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/WITmbq5cffE7nHPS7lxNDA/o.jpg"
    },
    {
      "id": "YJ9d9wrg4UfEAIp4PuZ5PQ",
      "name": "Leopard Lounge",
      "address": "2928 Guadalupe St, Austin, TX 78705",
      "phone_number": "(512) 551-2202",
      "website": "https://www.yelp.com/biz/leopard-lounge-austin-2?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 4,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/N47bD3egeGJfC9LyuYQccQ/o.jpg"
    },
    {
      "id": "S89MsDxSLTvQdVfu6ciEWA",
      "name": "Uptown Cheapskate - Round Rock",
      "address": "2601 S Ih 35 Frontage Rd, Round Rock, TX 78664",
      "phone_number": "(512) 520-8025",
      "website": "https://www.yelp.com/biz/uptown-cheapskate-round-rock-round-rock?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.9,
      "num_of_reviews": 62,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/6yY4rxzOLagHwglGf8u77g/o.jpg"
    },
    {
      "id": "L4DSa4z00nu49iR1CboDLQ",
      "name": "Plato's Closet",
      "address": "5400 Brodie Ln, Austin, TX 78745",
      "phone_number": "(512) 358-8888",
      "website": "https://www.yelp.com/biz/platos-closet-austin-3?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.6,
      "num_of_reviews": 421,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/pyopeBg4acCdyxlboQHjsg/o.jpg"
    },
    {
      "id": "I6Ibun5C1rZ2pfmfeWmTmg",
      "name": "Co-Star",
      "address": "1708 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 912-7970",
      "website": "https://www.yelp.com/biz/co-star-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.5,
      "num_of_reviews": 17,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/AWRKpsrvUIGAEIIYcZweWQ/o.jpg"
    },
    {
      "id": "D7oIZ9J65PT91DtXAuQbQw",
      "name": "Austin Creative Reuse",
      "address": "2005 Wheless Ln, Austin, TX 78723",
      "phone_number": "(512) 375-3041",
      "website": "https://www.yelp.com/biz/austin-creative-reuse-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.8,
      "num_of_reviews": 78,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/SQ8wFhPwUwa_g7DOztvyog/o.jpg"
    },
    {
      "id": "C01jqZGYDJAV8a0dmpAlag",
      "name": "Side Kitsch Vintage",
      "address": "6535 N Lamar Blvd, Austin, TX 78752",
      "phone_number": "(512) 660-5131",
      "website": "https://www.yelp.com/biz/side-kitsch-vintage-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 5.0,
      "num_of_reviews": 4,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/mMpEi_CvMEZ5iSDxInPkIg/o.jpg"
    },
    {
      "id": "e86MqejxsiztlZ-Ztany1w",
      "name": "The Salvation Army Family Store & Donation Center",
      "address": "8801-B Research Blvd, Austin, TX 78758",
      "phone_number": "(512) 836-2700",
      "website": "https://www.yelp.com/biz/the-salvation-army-family-store-and-donation-center-austin-10?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 2.6,
      "num_of_reviews": 31,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/d1V6qHxChgApXvTdW5n3AQ/o.jpg"
    },
    {
      "id": "ij4E6roRKm_6pgRTOKElUg",
      "name": "The Great Outdoors",
      "address": "2730 S Congress Ave, Austin, TX 78704",
      "phone_number": "(512) 448-2992",
      "website": "https://www.yelp.com/biz/the-great-outdoors-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.3,
      "num_of_reviews": 287,
      "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/w83zYq479xf4Jsh5XN7u6Q/o.jpg"
    },
    {
      "id": "u7pEDRDeG-r28UtmKEAqyw",
      "name": "Le Garage Sale",
      "address": "900 Barton Springs Rd, Austin, TX 78704",
      "phone_number": "",
      "website": "https://www.yelp.com/biz/le-garage-sale-austin?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.3,
      "num_of_reviews": 26,
      "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/krwm5MQY8G_JVknTu7xoKw/o.jpg"
    },
    {
      "id": "seBlTrXidu4bCq5mbOLDYw",
      "name": "Wimberley Market Days",
      "address": "601 Fm 2325, Wimberley, TX 78676",
      "phone_number": "(512) 847-2391",
      "website": "https://www.yelp.com/biz/wimberley-market-days-wimberley?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 3.7,
      "num_of_reviews": 21,
      "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/un9iEPmj-vXWLAYtdyjPLg/o.jpg"
    },
    {
      "id": "6dA57yqzBIW8QFWSrW7SYQ",
      "name": "Wimberley Village Thrift Store",
      "address": "450 River Rd, Wimberley, TX 78676",
      "phone_number": "(512) 847-5400",
      "website": "https://www.yelp.com/biz/wimberley-village-thrift-store-wimberley?adjust_creative=VY8YzJ-hNZlVKzV1admXTA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=VY8YzJ-hNZlVKzV1admXTA",
      "rating": 4.0,
      "num_of_reviews": 5,
      "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/4htDRaoA0QQB9JmMclxXAw/o.jpg"
    }
  ];