import { Visualization1, Visualization2, Visualization3 } from './VisualizationsPage';

const Visualizations = () => {
  return (
    <div style={{ width: '100%' }}>
      <div style={{ width: '100%' }}>
        <h5 style={{ margin: '50px 20px' }}>Total Counts of Each Model</h5>
        <Visualization1 />
      </div>
      <div style={{ width: '100%' }}>
        <h5 style={{ margin: '50px 20px' }}>Average Number of Ratings</h5>
        <Visualization2 />
      </div>
      <div style={{ width: '100%' }}>
        <h5 style={{ margin: '50px 20px' }}>Location Map</h5>
        <Visualization3 />
      </div>
      <div style={{ width: '100%' }}>
        <h5 style={{ margin: '50px 20px' }}>Our Self-Critiques</h5>
        <div style={{ marginBottom: '50px', paddingLeft: '20px', paddingRight: '20px' }}>
          <p><b>What did we do well?</b> We synthesized information from various sources to create a tool to help the low-income residents of Austin with some of the services that can best serve them. We worked well as a team, combining our knowledge and experiences to create a unit stronger than the sum of its parts.</p>
          <p><b>What did we learn?</b> We gained firsthand experience with maintaining a realistic medium-scale project with a team of developers. We learned about how today's websites are designed and built, and we gained a better appreciation of how data flows across the Internet.</p>
          <p><b>What did we teach each other?</b> Even though our team is just a small group of UTCS students, we found that each of our team members had unique previous experiences that helped each of us fulfill our specific responsibilities within the group. We learned a lot from each other, but arguably the most important thing we learned is the importance of specialization.</p>
          <p><b>What can we do better?</b> We could improve the user experience of our website by refining our design to be more user-friendly. In particular, things like smoother page changing, improved consistency in visual design, and more advanced search options would go a long way. On a larger scale, we could also integrate data from more sources or add more models to our website. It could be argued that expanding to promote services in other communities might be an improvement, but since each city can face very different challenges in this area, there is also a lot of worth in a city-specific tool like ours.</p>
          <p><b>What effect did the peer reviews have?</b> The peer reviews helped to guide our development process. As the developer of an application, it's possible that you become blind to or grow used to some of the less intuitive aspects of your application, so constructive feedback from a perspective with fresh eyes is invaluable.</p>
          <p><b>What puzzles us?</b> A significant portion of the low-income community does not have an Internet-connected device or a reliable source of electricity. How could we better serve those in the community who have limited access to technology?</p>
        </div>
      </div>
    </div>
  );
};

export default Visualizations;
