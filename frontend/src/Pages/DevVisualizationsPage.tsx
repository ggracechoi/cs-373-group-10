import React, { useEffect, useRef, useState } from "react";
import * as d3 from "d3";
import axios from "axios";

interface CountryData {
  name: string;
  refugees: number;
}

const RefugeeVisualization: React.FC = (): JSX.Element => {
  const svgRef = useRef<SVGSVGElement>(null);
  const [data, setData] = useState<CountryData[]>([]);

  interface Country {
    name: string;
    refugees: number;
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("https://api.supportsouthsudan.me/countries");
        const json = await response.json();

        const thresholdPercentage = 0.001;
        const maxY = d3.max(json.countries, (d: Country) => d.refugees);
        const threshold = maxY ? maxY * thresholdPercentage : 0;
        const filteredData = json.countries.filter(
          (country: CountryData) => country.refugees > threshold
        );
        setData(filteredData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (!data || data.length === 0 || !svgRef.current) return;

    const svg = d3.select(svgRef.current);

    const longestName = d3.max(data, (d) => d.name.length)!;
    const maxRefugees = d3.max(data, (d) => d.refugees)!;
    const margin = {
      top: 0,
      right: 50 + longestName * 4,
      bottom: 0,
      left: 150 + maxRefugees.toString().length * 8,
    };
    const width = 1000 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;

    const g = svg
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const y = d3
      .scaleBand()
      .domain(data.map((d) => d.name))
      .range([0, height])
      .padding(0.1);

    const x = d3
      .scaleLinear()
      .domain([0, d3.max(data, (d) => d.refugees)!])
      .range([0, width]);

    const colorScale = d3.scaleOrdinal(d3.schemePastel2);

    g.selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("y", (d) => y(d.name)!)
      .attr("x", 0)
      .attr("width", (d) => x(d.refugees)!)
      .attr("height", y.bandwidth())
      .style("fill", (_, i) => colorScale(i));

    g.selectAll(".text")
      .data(data)
      .enter()
      .append("text")
      .attr("class", "label")
      .attr("x", (d) =>
        x(d.refugees) > 60 ? x(d.refugees) - 5 : x(d.refugees) + 5
      )
      .attr("y", (d) => y(d.name)! + y.bandwidth() / 2)
      .attr("dy", "-0.2em")
      .style("font-size", "10px")
      .style("fill", "black")
      .style("font-weight", "bold")
      .style("text-anchor", (d) => (x(d.refugees) > 60 ? "end" : "start"))
      .text((d) => d.name);

    g.selectAll(".refugee-count")
      .data(data)
      .enter()
      .append("text")
      .attr("class", "refugee-count")
      .attr("x", (d) =>
        x(d.refugees) > 60 ? x(d.refugees) - 5 : x(d.refugees) + 5
      )
      .attr("y", (d) => y(d.name)! + y.bandwidth() + 5)
      .attr("dy", "-1.9em")
      .style("font-size", "10px")
      .style("fill", "black")
      .style("text-anchor", (d) => (x(d.refugees) > 60 ? "end" : "start"))
      .text((d) => `${d.refugees} refugees`);

    g.append("line")
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 0)
      .attr("y2", height)
      .style("stroke", "black")
      .style("stroke-width", "1px")
      .style("shape-rendering", "crispEdges");
  }, [data]);

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <svg ref={svgRef} width={1000} height={500}></svg> { }
    </div>
  );
};

const GlobalHQVisualization = () => {
  const ref = useRef();

  const countryNameMap = {
    "United Kingdom of Great Britain and Northern Ireland": "England",
    "United States of America": "USA",
  };

  const fetchOrganizations = async () => {
    let allOrgs: any[] = [];
    let page = 1;
    let total = 0;
    let limit = 50;
    do {
      const response = await axios.get(
        `https://api.supportsouthsudan.me/orgs?page=${page}&limit=${limit}`
      );
      allOrgs = allOrgs.concat(response.data.orgs);
      total = response.data._metadata.total;
      page++;
    } while (page <= Math.ceil(total / limit));
    return allOrgs;
  };

  useEffect(() => {
    const width = 1500;
    const height = 700;
    const svg = d3
      .select(ref.current)
      .attr("width", width)
      .attr("height", height)
      .style("background", "white");

    const projection = d3
      .geoMercator()
      .center([0, 20])
      .scale(300)
      .translate([width / 2, height / 2]);

    const jitter = 35;

    Promise.all([
      d3.json(
        "https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/world.geojson"
      ),
      fetchOrganizations(),
    ]).then(([dataGeo, orgs]) => {
      svg
        .append("g")
        .selectAll("path")
        .data(dataGeo.features)
        .enter()
        .append("path")
        .attr("fill", "#b8b8b8")
        .attr("d", d3.geoPath().projection(projection))
        .style("stroke", "none")
        .style("opacity", 0.3);

      const countryData = dataGeo.features.reduce((acc, feature) => {
        acc[feature.properties.name] = feature;
        return acc;
      }, {});

      orgs.forEach((org) => {
        const country = org.country[0];
        const mappedCountry = countryNameMap[country] || country;
        if (mappedCountry && countryData[mappedCountry]) {
          const coords = d3.geoCentroid(countryData[mappedCountry]);
          svg
            .append("circle")
            .attr(
              "cx",
              projection(coords)[0] + (Math.random() - 0.5) * 2 * jitter
            )
            .attr(
              "cy",
              projection(coords)[1] + (Math.random() - 0.5) * 2 * jitter
            )
            .attr("r", 5)
            .style("fill", "blue")
            .attr("stroke", "#FFF")
            .attr("stroke-width", 1.5)
            .append("title")
            .text(org.name);
        } else {
          console.error(
            "Country not found or not matched in GeoJSON:",
            country
          );
        }
      });
    });
  }, []);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <svg ref={ref}></svg>
    </div>
  );
};

const NewsTypeVisualization = () => {
  const ref = useRef(null);

  useEffect(() => {
    const fetchNewsTypes = async () => {
      try {
        const response = await axios.get(
          "https://api.supportsouthsudan.me/news"
        );
        if (response.data && response.data.news) {
          return response.data.news.reduce((acc, item) => {
            const type = item.Type;
            acc[type] = (acc[type] || 0) + 1;
            return acc;
          }, {});
        } else {
          console.error("No news data found");
          return {};
        }
      } catch (error) {
        console.error("Error fetching news data:", error);
        return {};
      }
    };

    fetchNewsTypes().then((data) => {
      if (!ref.current) return;

      const width = 500;
      const height = 450;
      const radius = 200;
      const svg = d3
        .select(ref.current)
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", `translate(${width / 2}, ${height / 2})`);

      const color = d3
        .scaleOrdinal()
        .domain(Object.keys(data))
        .range(d3.schemePastel2);

      const pie = d3.pie().value((d) => d.value);
      const data_ready = pie(
        Object.entries(data).map(([key, value]) => ({ key, value: +value }))
      );

      const arc = d3.arc()
        .innerRadius(100)
        .outerRadius(radius);

      const outerArc = d3.arc()
        .innerRadius(radius * 1.1) // Adjust this value to move the labels further out
        .outerRadius(radius * 1.1);

      const slices = svg
        .selectAll("allSlices")
        .data(data_ready)
        .enter()
        .append("g")
        .attr("class", "slice");

      slices
        .append("path")
        .attr("d", arc)
        .attr("fill", (d) => color(d.data.key))
        .attr("stroke", "black")
        .style("stroke-width", "2px")
        .style("opacity", 0.7);

      // Add hidden labels within group elements
      slices
        .append("text")
        .text((d) => `${d.data.key}: ${d.data.value}`)
        .attr("transform", (d) => `translate(${arc.centroid(d)})`)
        .style("text-anchor", "middle")
        .style("alignment-baseline", "middle")
        .style("font-size", "12px")
        .style("visibility", "hidden"); // Initially hide the text

      // Add hover interaction
      slices
        .on("mouseover", function (d, i) {
          d3.select(this).select("text").style("visibility", "visible");
        })
        .on("mouseout", function (d, i) {
          d3.select(this).select("text").style("visibility", "hidden");
        });
    });
  }, []);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "auto",
      }}
    >
      <svg ref={ref}></svg>
    </div>
  );
};
export { RefugeeVisualization, GlobalHQVisualization, NewsTypeVisualization };
