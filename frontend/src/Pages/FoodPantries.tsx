import { useEffect, useState } from "react";
import FoodPantriesCard from "../components/foodpantries/FoodPantriesCard";
import PageNav from "../components/shared/PageNav";
import "./../components/InstancesHeader.css";
import "./Pages.css";
import { useSearchParams } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function FoodPantries() {
  const [pageSize, _setPageSize] = useState(15);
  const [numPages, setNumPages] = useState(1);
  const [searchParams, setSearchParams] = useSearchParams();
  const [allPantries, setAllPantries] = useState<any[]>();

  const page = Number(searchParams.get("page"));

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function setPage(newPage: number) {
    setSearchParams((prevSearchParams) => {
      prevSearchParams.set("page", newPage.toString());
      return prevSearchParams;
    });
  }

  // Load data from API
  useEffect(() => {
    // Reformat params as needed
    let params = "";
    searchParams.forEach((val, key) => {
      if (val === "") return;
      if (key === "sort") {
        params += `${val}=true&`;
      } else {
        params += `${key}=${val}&`;
      }
    });
    params += "a"; // temporary fix for bug with query strings
    const q = searchParams.get("query");
    if (q === null || q === "") {
      // Call filter endpoint
      fetch(`https://api.atxassist.me/food-pantries/filtered?${params}`)
        .then((res) => res.json())
        .then((data) => {
          setAllPantries(data);
        });
    } else {
      fetch(`https://api.atxassist.me/food-pantries/search?${params}`)
        .then((res) => res.json())
        .then((data) => {
          setAllPantries(data);
        });
    }
  }, []);

  // Set number of pages when data is available
  useEffect(() => {
    if (allPantries === undefined) return;
    setNumPages(Math.ceil(allPantries.length / pageSize));
  }, [allPantries, pageSize]);

  // This is what's rendered before data from API is returned
  if (allPantries === undefined) {
    return <>Loading...</>;
  }

  // Select the items on this page
  const start: number = pageSize * (page - 1);
  const end: number = Math.min(start + pageSize, allPantries.length);
  const pageItems = allPantries.slice(start, end);

  const q = searchParams.get("query") || undefined;
  const sort = searchParams.get("sort") || undefined;

  return (
    <>
      <h1 className="instances-summary">Learn about all the food pantries available in the Austin area.</h1>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '10px' }}>
        {/* search bar */}
        <form className="form-inline my-2 my-lg-0" style={{ padding: "10px" }} action="" method="get">
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            name="query"
          />
          <button className="btn btn-outline-success my-2 my-sm-0" type="submit">
            Search
          </button>
        </form>

        {/* modal */}
        <Button variant="primary" onClick={handleShow}>
          Sort and Filter
        </Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Sort and Filter</Modal.Title>
        </Modal.Header>
        <form action="" method="get" id="searchform">
          <Modal.Body>
            <h5>Sort By</h5>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="sort"
                value="sortRatings"
                id="flexRadioDefault1"
                defaultChecked={sort === "sortRatings"}
              />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Number of Ratings
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="sort"
                value="sortAlphabetically"
                id="flexRadioDefault2"
                defaultChecked={sort === "sortAlphabetically"}
              />
              <label className="form-check-label" htmlFor="flexRadioDefault2">
                Alphabetical
              </label>
              <div style={{ height: '20px' }}></div>
            </div>
            <h5>Filter By</h5>
            <h6>Opening On</h6>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Sunday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Sunday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Sunday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Monday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Monday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Monday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Tuesday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Tuesday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Tuesday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Wednesday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Wednesday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Wednesday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Thursday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Thursday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Thursday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Friday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Friday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Friday
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                name="day"
                value="Saturday"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("day", "Saturday")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                Saturday
              </label>
              <div style={{ height: '20px' }}></div>
            </div>
            <h6>Minimum Rating</h6>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="rating"
                value="1"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("rating", "1")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                1 Star
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="rating"
                value="2"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("rating", "2")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                2 Stars
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="rating"
                value="3"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("rating", "3")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                3 Stars
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="rating"
                value="4"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("rating", "4")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                4 Stars
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="rating"
                value="5"
                id="flexCheckDefault"
                defaultChecked={searchParams.has("rating", "5")}
              />
              <label className="form-check-label" htmlFor="flexCheckDefault">
                5 Stars
              </label>
              <div style={{ height: '20px' }}></div>
            </div>
            <h6>Zip Code</h6>
            <div
              className="form-inline my-2 my-lg-0"
              style={{ padding: "10px" }}
            >
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                name="zipCode"
                defaultValue={searchParams.get("zipCode") || ""}
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit">
              Submit
            </Button>
            <Button variant="secondary" type="reset" href="./">
              Reset
            </Button>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </form>
      </Modal>

      <PageNav numPages={numPages} page={page} setPage={setPage} />
      <h1 className="instances-header">Showing items {start + 1}-{end} out of {allPantries.length} </h1>
      <h1 className="instances-header-2">ADDRESS, RATING, NUMBER OF RATINGS</h1>
      {pageItems.map((e, i) => {
        return <FoodPantriesCard pantry={e} key={i} query={q} />;
      })}
    </>
  );
}

export default FoodPantries;
