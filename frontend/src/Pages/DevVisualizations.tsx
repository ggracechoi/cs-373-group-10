import React from 'react';
import { RefugeeVisualization, GlobalHQVisualization, NewsTypeVisualization } from './DevVisualizationsPage';

const DevVisualizations: React.FC = () => {
    return (
        <div style={{ width: '100%'}}>
            <h5 style={{ margin: '50px 20px' }}>Refugee Statistics by Country</h5>
            <RefugeeVisualization />
            <h5 style={{ margin: '50px 20px' }}>Organization Headquarters</h5>
            <GlobalHQVisualization />
            <h5 style={{ margin: '50px 20px' }}>Type of News</h5>
            <NewsTypeVisualization />
            <h5 style={{ margin: '50px 20px' }}>Developer Team's Critiques</h5>
            <div style={{ marginBottom: '50px', paddingLeft: '20px', paddingRight: '20px' }}>
                <p><b>What did they do well?</b> SupportSouthSudan clearly shares its goal to educate about South Sudanese refugees, the supporting countries, related charities, and the latest news. The website is easy to use and organized, making it quick to find various types of information. This setup helps raise awareness about the challenges refugees face and the important work of charities. The straightforward design and easy navigation make it a great resource for anyone looking to support or learn about the South Sudanese refugee community.</p>
                <p><b>How effective was their RESTful API?</b> Their RESTful API efficiently fetches data. However, the limit of 10 items per request might be inconvenient for users needing more data at once, as it requires paging through results.</p>
                <p><b>How well did they implement your user stories?</b> The website has successfully implemented their user stories. Their updates show a careful effort to enhance user experience and site functionality, responding well to the feedback and needs identified in their user stories.</p>
                <p><b>What did we learn from their website?</b> From the website, we learn about the challenges and experiences of South Sudanese refugees. It highlights the welcoming countries and the active involvement of charities and organizations in providing support. The site also offers updated news articles, which help enhance understanding and encourage support for the refugee situation. The content aims to increase awareness and motivate the global community to assist South Sudanese refugees.</p>
                <p><b>What can they do better?</b> The website is functional and informative, but the design could be more consistent and visually appealing to better engage users. Additionally, improving the API to allow for more flexible data retrieval limits would enhance the user experience, especially for those needing more comprehensive data.</p>
                <p><b>What puzzles us about their website?</b> The strict API fetch limit on the website may not be immediately clear, leading to questions about the balance between performance and user convenience.</p>
            </div>
        </div>
    );
}

export default DevVisualizations;
