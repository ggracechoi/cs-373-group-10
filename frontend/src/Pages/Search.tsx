import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { ThriftStore } from "../types/ThriftStore";
import { AffordableHousing } from "../types/AffordableHousing";
import { FoodPantry } from "../types/FoodPantry";
import AffordableHousingCard from "../components/AffordableHousingCard";
import ThriftStoresCard from "../components/ThriftStoresCard";
import FoodPantriesCard from "../components/foodpantries/FoodPantriesCard";

function Search () {
  // Get search query
  const [searchParams, _setSearchParams] = useSearchParams();

  // Load from various search endpoints
  const [ thrifts, setThrifts ] = useState<ThriftStore[]>();
  const [ housing, setHousing ] = useState<AffordableHousing[]>();
  const [ pantries, setPantries ] = useState<FoodPantry[]>();

  // Loads thrift stores
  useEffect(() => {
    const query = searchParams.get("query");
    fetch(`https://api.atxassist.me/thrift-stores/search?query=${query}&a`)
      .then((res) => res.json())
      .then((json) => {
        setThrifts(json);
      });
  }, [searchParams]);

  // Loads affordable housing
  useEffect(() => {
    const query = searchParams.get("query");
    fetch(`https://api.atxassist.me/affordable-housing/search?query=${query}&a`)
      .then((res) => res.json())
      .then((json) => {
        setHousing(json);
      });
  }, [searchParams]);

  // Loads food pantries
  useEffect(() => {
    const query = searchParams.get("query");
    fetch(`https://api.atxassist.me/food-pantries/search?query=${query}&a`)
      .then((res) => res.json())
      .then((json) => {
        setPantries(json);
      });
  }, [searchParams]);

  // Render
  const q = searchParams.get("query") || undefined;
  return (
    <>
      <h3>Food Pantries:</h3>
      {pantries?.map((e, i) => {
        return <FoodPantriesCard pantry={e} key={i} query={q} />;
      })}
      <h3>Thrift Stores:</h3>
      {thrifts?.map((e, i) => {
        return <ThriftStoresCard store={e} key={i} query={q} />;
      })}
      <h3>Affordable Housing:</h3>
      {housing?.map((e, i) => {
        return <AffordableHousingCard housing={e} key={i} query={q} />;
      })}
    </>
  );
}

export default Search;
