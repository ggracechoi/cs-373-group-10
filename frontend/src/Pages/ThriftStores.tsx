import { useEffect, useState } from "react";
import ThriftStoresCard from "../components/ThriftStoresCard";
import "./../components/InstancesHeader.css";
import "./Pages.css";
import { useSearchParams } from "react-router-dom";
import PageNav from "../components/shared/PageNav";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function ThriftStores() {
  const [pageSize, _setPageSize] = useState(15);
  const [numPages, setNumPages] = useState(1);
  const [searchParams, setSearchParams] = useSearchParams();
  const [allStores, setAllStores] = useState<any[]>();

  const page = Number(searchParams.get("page"));

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function setPage(newPage: number) {
    setSearchParams(prevSearchParams => {
      prevSearchParams.set("page", newPage.toString());
      return prevSearchParams;
    });
  }

  // Load data from API
  useEffect(() => {
    // Reformat params as needed
    let params = "";
    searchParams.forEach((val, key) => {
      if (val === "") return;
      if (key === "sort") {
        params += `${val}=true&`;
      } else {
        params += `${key}=${val}&`;
      }
    });
    params += "a"; // temporary fix for bug with query strings
    const q = searchParams.get("query");
    if (q === null || q === "") {
      // Call filter endpoint
      fetch(`https://api.atxassist.me/thrift-stores/filtered?${params}`)
        .then((res) => res.json())
        .then((data) => {
          setAllStores(data);
        });
    } else {
      fetch(`https://api.atxassist.me/thrift-stores/search?${params}`)
        .then((res) => res.json())
        .then((data) => {
          setAllStores(data);
        });
    }
  }, []);

  // Set number of pages when data is available
  useEffect(() => {
    if (allStores === undefined) return;
    setNumPages(Math.ceil(allStores.length / pageSize))
  }, [allStores, pageSize]);

  // This is what's rendered before data from API is returned
  if (allStores === undefined) {
    return <>Loading...</>;
  }

  // Select the items on this page
  const start: number = pageSize * (page - 1);
  const end: number = Math.min(start + pageSize, allStores.length);
  const pageItems = allStores.slice(start, end);

  const q = searchParams.get("query") || undefined;
  const sort = searchParams.get("sort") || undefined;
  return (
    <>
      <h1 className="instances-summary">Learn about all the thrift stores available in the Austin area.</h1>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '10px' }}>
        {/* search bar */}
        <form className="form-inline my-2 my-lg-0" style={{ padding: '10px' }} method="get" action="">
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
            name="query"
          />
          <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

        {/* modal */}
        <Button variant="primary" onClick={handleShow} >
          Sort and Filter
        </Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Sort and Filter</Modal.Title>
        </Modal.Header>
        <form action="" method="get">
          <Modal.Body>
            <h5>Sort By</h5>
            <div className="form-check" >
              <input className="form-check-input" type="radio" name="sort" value="sortRating" id="flexRadioDefault1"
                defaultChecked={sort === "sortRating"} />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Ratings
              </label>
            </div>
            <div className="form-check" >
              <input className="form-check-input" type="radio" name="sort" value="sortNumRatings" id="flexRadioDefault1"
                defaultChecked={sort === "sortNumRatings"} />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Number of Ratings
              </label>
            </div>
            <div className="form-check" >
              <input className="form-check-input" type="radio" name="sort" value="sortAlphabetically" id="flexRadioDefault1"
                defaultChecked={sort === "sortAlphabetically"} />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Alphabetical
              </label>
              <div style={{ height: '20px' }}></div>
            </div>
            <h5>Filter By</h5>
            <h6>Zip Code</h6>
            <div className="form-inline my-2 my-lg-0" style={{ padding: "10px" }}>
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                name="zipCode"
                defaultValue={searchParams.get("zipCode") || ""}
              />
            </div>
            <div style={{ height: '20px' }}></div>
            <h6>Area Code</h6>
            <div className="form-inline my-2 my-lg-0" style={{ padding: "10px" }}>
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                name="areaCode"
                defaultValue={searchParams.get("areaCode") || ""}
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit">
              Submit
            </Button>
            <Button variant="secondary" type="reset" href="./">
              Reset
            </Button>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </form>
      </Modal>

      <PageNav numPages={numPages} page={page} setPage={setPage} />
      <h1 className="instances-header">Showing items {start + 1}-{end} out of {allStores.length}</h1>
      <h1 className="instances-header-2">ADDRESS, PHONE NUMBER, RATING, NUMBER OF RATINGS</h1>
      {pageItems.map((e, i) => {
        return <ThriftStoresCard store={e} key={i} query={q} />;
      })}
    </>
  );
}

export default ThriftStores;
