import { useState, useEffect } from "react";
import axios from 'axios';
import { Card, Col, Container, ListGroup, Row } from "react-bootstrap";

// TODO: find a better way to do this
import pfpGrace from "../assets/team/grace.png";
import pfpEshi from "../assets/team/eshi.png";
import pfpBen from "../assets/team/benjamin.png";
import pfpTvisha from "../assets/team/tvisha.png";
import pfpSaketh from "../assets/team/saketh.png";

// interface userInfo {
//   name: string;
//   username: string;
//   image: any;
//   role: string;
//   bio: string;
//   commitCount: number;
//   issues: number;
//   tests: number;
// }

const team = [
  {
    name: "Grace Choi",
    gitlab_username: "ggracechoi",
    image: pfpGrace,
    role: "Frontend",
    bio: "Interests: Hiking, swimming, music, and entrepreneurship",
    tests: 0,
  }, {
    name: "Eshi Kohli",
    gitlab_username: "eshi_kohli",
    image: pfpEshi,
    role: "Full stack",
    bio: "Interests: AI/ML, playing piano, Formula 1, The Weeknd",
    tests: 0,
  }, {
    name: "Tvisha Andharia",
    gitlab_username: "tvisha.andharia",
    image: pfpTvisha,
    role: "Full stack",
    bio: "Interests: Playing the flute, reading, and swimming",
    tests: 0,
  }, {
    name: "Saketh Ram Kotamraju",
    gitlab_username: "saketh.k1",
    image: pfpSaketh,
    role: "Backend",
    bio: "Interests: Basketball, Tennis, Poker, Startups",
    tests: 0,
  }, {
    name: "Benjamin Zimmerman",
    gitlab_username: "benjizim",
    image: pfpBen,
    role: "Frontend",
    bio: "Interests: Spurs basketball, rap & rock, and building robots",
    tests: 0,
  },
];

interface toolInfo {
  name: string;
  info: string;
}

const toolsUsed: toolInfo[] = [
  {
    name: "Python",
    info: "Python served as the backbone of our data processing and scraping efforts.",
  }, {
    name: "Vite",
    info: "Vite helped us rapidly develop and build our frontend code.",
  }, {
    name: "React",
    info: "React played a pivotal role in the frontend development of our website.",
  }, {
    name: "Bootstrap",
    info: "Bootstrap's set of pre-designed components, styles, and layouts helped with shaping the visual and responsive aspects of our website.",
  }, {
    name: "Postman",
    info: "Postman served as a tool for documenting our API.",
  }, {
    name: "VS Code",
    info: "Visual Studio Code was our preferred text editor for writing, debugging, and maintaining our codebase.",
  }, {
    name: "GitLab",
    info: "GitLab served as our version control and collaboration platform.",
  },
];

const names: { [key: string]: number } = {
  "Grace Choi": 0,
  "Eshi Kohli": 1,
  "Benjamin Zimmerman": 2,
  "Saketh Ram Kotamraju": 3,
  "Tvisha Andharia": 4
};

const client = axios.create({
  baseURL: 'https://gitlab.com/api/v4/projects/54606897',
});

// interface ContributorInfo {
//   name: string;
//   email: string;
//   commits: number;
// }

export default function About() {
  const [commitsState, updateCommits] = useState([0, 0, 0, 0, 0]);
  const [issuesState, updateIssues] = useState([0, 0, 0, 0, 0]);
  const [testsState, _updateTests] = useState([5, 6, 5, 5, 5]);

  useEffect(() => {
    let newCommits = [0, 0, 0, 0, 0];
    client.get("repository/contributors")
      .then((response) => {
        response.data.forEach((element: any) => {
          const { name, commits } = element;
          // console.log("Name from GitLab API:", name);
          team.forEach((member) => {
            // console.log("member names:", member.name);
            if (member.gitlab_username === 'eshi_kohli' && name === 'eshikohli') {
              newCommits[names['Eshi Kohli']] += commits;
            } else if (member.gitlab_username === 'tvisha.andharia' && name === 'TA2525') {
              newCommits[names['Tvisha Andharia']] += commits;
            } else if (member.name === name || member.gitlab_username === name) {
              newCommits[names[member.name]] += commits;
            }
          });
        });
        updateCommits(newCommits);
        console.log("Commit counts:", newCommits);
      });

    let promises = [];
    for (let i = 1; i < 10; i++) {
      promises.push(client.get(`issues?per_page=100&page=${i}`).then((response) => response.data));
    }

    let counts = [0, 0, 0, 0, 0];
    Promise.all(promises).then((pageArr) => {
      pageArr.forEach((page) => {
        page.forEach((issue: any) => {
          if (issue.closed_by !== null) {
            counts[names[issue.closed_by.name]]++;
          }
        });
      });
      updateIssues(counts);
    });
  }, []);

  return (
    <>
      <Container className="bg-white">
        <h1 className="text-center mt-5 mb-4">About ATX Assist</h1>
        <p className="text-left">ATX Assist is a dedicated platform committed to
          supporting the thriving community of Austin. Our mission is to empower
          and uplift the low-income residents by providing essential information
          on food pantries, thrift stores, and affordable housing options. We
          believe that access to these resources is fundamental to fostering a
          strong and resilient community. By offering a centralized hub of
          information, we aim to make it easier for individuals and families to
          navigate the available services in Austin, promoting a sense of
          stability and well-being.
        </p>

        <p className="text-left">Integrating data on local food pantries,
          thrift stores, and housing gives efficient and easy access to
          important life-supporting information for low-income families. For
          example, a family looking for affordable housing can see the closest
          food pantry and thrift store available. This combined information will
          aid families in deciding where to live and to reach any services they
          need in a minimum amount of time.
        </p>

        <h1 className="text-center mt-4 mb-4">Our Team</h1>

        <Row xs={2} md={3} className="g-4 justify-content-center">
          {team.map((member, i) => {
            return (
              <Col style={{ padding: '10px' }} key={i}>
                <Card className="h-100">
                  <Card.Img variant="top" src={member.image} />
                  <Card.Body>
                    <h5 className="card-title">{member.name}</h5>
                    <h6 className="card-subtitle mb-2">{member.role} | GitLab: @{member.gitlab_username}</h6>
                    <Card.Text>{member.bio}</Card.Text>
                  </Card.Body>
                  <ListGroup variant="flush">
                    <ListGroup.Item>Commits: {commitsState[names[member.name]]}</ListGroup.Item>
                    <ListGroup.Item>Issues: {issuesState[names[member.name]]}</ListGroup.Item>
                    <ListGroup.Item>Unit tests: {testsState[names[member.name]]}</ListGroup.Item>
                  </ListGroup>
                </Card>
              </Col>
            )
          })}
        </Row>

        <h1 className="text-center mt-4 mb-4">Data Sources</h1>

        <Row xs={1} md={3} className="g-4 mb-5">
          <Col>
            <Card className="h-100">
              <Card.Body>
                <Card.Title>Google Maps</Card.Title>
                <Card.Text>
                  We sourced information on food pantries through the Google Maps API.
                  The API allowed us to retrieve details such as addresses and operating
                  hours for food pantries in Austin.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>Yelp</Card.Title>
                <Card.Text>
                  Our information on thrift stores comes from the Yelp API.
                  By utilizing the Yelp API, we were able to compile a comprehensive
                  list of thrift stores in Austin, complete with details like accessibility,
                  ratings, and website URLs.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>City of Austin Open Data Portal</Card.Title>
                <Card.Text>
                  We gathered information on affordable housing from the City of Austin
                  Open Data Portal. Through web scraping techniques, we extracted
                  details such as available housing units, website URLs, and accessibility.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <h1 className="text-center mt-4 mb-4">Tools Used</h1>

        <Row xs={2} md={1} className="g-4 justify-content-center" >
          {toolsUsed.map((tool: toolInfo, i) => (
            <Col key={i}>
              <Card className="h-100">
                <Card.Body>
                  <Card.Title>{tool.name}</Card.Title>
                  <Card.Text>{tool.info}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>

        <h1 className="text-center mt-4 mb-4">Our Source Code</h1>
        <Container className="text-center mb-5" >
          <a href="https://gitlab.com/ggracechoi/cs373-group-10">GitLab Repo</a><br />
          <a href="https://documenter.getpostman.com/view/32820631/2sA2r545aP">
            Postman API
          </a>
          <div style={{ height: '20px' }}></div>
        </Container>
      </Container>
    </>
  );
}

