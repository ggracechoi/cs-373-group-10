import FoodPantryImage from "./../assets/foodPantryImage.png";
import ThriftStoreImage from "./../assets/thriftStoreImage.png";
import AffordableHousingImage from "./../assets/affordableHousingImage.png";
import "./Home.css";
import { useState } from 'react';
import { Toggle } from "../components/Toggle";

function Home() {
  const [isDark, setIsDark] = useState(false);
  return (
    <>
      <section className="jumbotron text-center">
        <div className="container">
          <h1 className="jumbotron-heading">
            Essential Living, Endless Possibilities
          </h1>
          <p className="description-text">
            As Austin sees a growing influx of residents, an increasing number
            of low-income communities are facing displacement due to
            gentrification. This has caused the cost of living to soar, putting
            many individuals and families in difficult positions. Our project
            seeks to establish a dedicated space for underserved communities to
            learn about essential resources. By connecting Austin residents to
            information about affordable food, clothes, and housing, we hope to
            make an impact in our local community.
          </p>
        </div>
      </section>
      <div className={isDark ? "album-dark" : "album"}>
        <Toggle
          handleChange={() => setIsDark(!isDark)}
          isChecked={isDark}
        />
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="card mb-4 box-shadow">
                <img
                  className="card-img-top"
                  src={FoodPantryImage}
                  alt="Card image cap"
                />
                <div className="card-body">
                    <h2>Food Pantries</h2>
                  <p className="card-text">
                    Get connected with food pantries near you! Obtain nutritious
                    meals for free or for a discounted price.
                  </p>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                      <a
                        role="button"
                        className="btn btn-sm btn-outline-secondary"
                        href="/foodpantries"
                      >
                        Learn More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card mb-4 box-shadow">
                <img
                  className="card-img-top"
                  src={ThriftStoreImage}
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h2>Thrift Stores</h2>
                  <p className="card-text">
                    Discover local thrift stores! Catch up to the latest trends
                    and styles without the high costs.
                  </p>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                    <a
                        role="button"
                        className="btn btn-sm btn-outline-secondary"
                        href="/thriftstores"
                      >
                        Learn More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card mb-4 box-shadow">
                <img
                  className="card-img-top"
                  src={AffordableHousingImage}
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h2>Affordable Housing</h2>
                  <p className="card-text">
                    Find your home in Austin, TX! Avoid the fees and find a home
                    that perfect suits your needs.
                  </p>
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                      <a
                        role="button"
                        className="btn btn-sm btn-outline-secondary"
                        href="/affordablehousing"
                      >
                        Learn More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
