.DEFAULT_GOAL := all
SHELL         :=  bash
COVERAGE := coverage

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3
else ifeq ($(shell uname -p), x86_64)
    BLACK         := black-3.11
    MYPY          := mypy-3.11
    PYDOC         := pydoc3.10
    PYLINT        := pylint-3.11
    PYTHON        := python3.11
else
    BLACK         := black
    MYPY          := mypy
    PYDOC         := pydoc
    PYLINT        := pylint
    PYTHON        := python
endif

# run docker
docker:
	docker run --rm -i -t -v $(PWD):/usr/python -w /usr/python gpdowning/python

# get git config
config:
	git config -l

# run unit tests
test:
	$(PYTHON) -m unittest discover -s backend -p 'test_*.py'

# get git log
atxassist.log.txt:
	git log > atxassist.log.txt

# run tests with coverage
test_coverage:
	$(COVERAGE) run -m unittest discover -s backend -p 'test_*.py'

# generate coverage report
coverage_report:
	$(COVERAGE) report -m

# clean coverage data
clean_coverage:
	$(COVERAGE) erase

# get git status
status:
	make --no-print-directory clean
	@echo
	git branch
	git remote -v
	git status

# download files from the CS 373 Group 10 code repo
pull:
	make --no-print-directory clean
	@echo
	git pull
	git status

# upload files to the CS 373 Group 10 code repo
push:
	make --no-print-directory clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	-git add atxassist.log.txt
	git add Makefile
	git add README.md
	git commit -m "another commit"
	git push
	git status

all:

# auto format the code
format:
	$(BLACK) ./backend/scrapers/affordable_housing.py
	$(BLACK) ./backend/scrapers/food_pantries.py
	$(BLACK) ./backend/scrapers/thrift_store.py
	$(BLACK) ./backend/add_to_tables.py
	$(BLACK) ./backend/association_tables_data.py
	$(BLACK) ./backend/server.py
	$(BLACK) ./backend/test_api.py
	$(BLACK) ./backend/search.py
	
# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml  \
    atxassist.log.txt

# check the existence of check files
check: $(C_FILES)

# remove temporary files
clean:
	rm -rf __pycache__
	rm -rf .mypy_cache

# remove temporary files and generated files
scrub:
	make --no-print-directory clean
	rm -f atxassist.log.txt

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(BLACK)
	@echo
	$(BLACK) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

	@echo
	which $(MYPY)
	@echo
	$(MYPY) --version

	@echo
	which pip
	@echo
	pip --version

	@echo
	which $(PYDOC)

	@echo
	which $(PYLINT)
	@echo
	$(PYLINT) --version | head -n 1

	@echo
	which $(PYTHON)
	@echo
	$(PYTHON) --version

	@echo
	which vim
	@echo
	vim --version | head -n 1
