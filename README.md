# CS 373 - ATXAssist, Group 10

### Website
www.atxassist.me

### Pecha Kucha
https://www.youtube.com/watch?v=TYfRkTpSOuk 

### Team Info
| Name | GitLab ID | Est. Completion Time (hrs) | Actual Completion Time (hrs) |
|---|---|---|---|
| Grace Choi | [@ggracechoi](https://gitlab.com/ggracechoi) | 70 | 65 |
| Eshi Kohli | [@eshi_kohli](https://gitlab.com/eshi_kohli) | 70 | 65 |
| Tvisha Andharia | [@tvisha.andharia]() | 65 | 60 |
| Benjamin Zimmerman | [@benjizim](https://gitlab.com/benjizim) | 80 | 70 |
| Saketh Kotamraju | [@saketh.k1](https://gitlab.com/saketh.k1) | 65 | 60 |

### Phase 1 Leader - Grace Choi
- Led daily Scrums
- Facilitated the division of roles and responsibilities 
- Helped team decide on a meeting time
- Regularly checked in with team about progress
- Reminded team about meetings
- Took questions to the TAs

### Phase 2 Leader - Eshi Kohli
- Frequently went through rubric to assess what needed to be done
- Regularly checked in with team about progress
- Picked up extra work
- Helped other members

### Phase 3 Leader - Benjamin Zimmerman
- Helped other members
- Learned new skills to complete work
- Gave updates about progress

### Phase 4 Leaders - Tvisha Andharia & Saketh Kotamraju
- Made sure work got done on time
- Successfully completed assigned tasks
- Stayed active in team meetings

### API Documentation
https://documenter.getpostman.com/view/32820631/2sA2r545aP

### Backend API 
https://api.atxassist.me/

### GitLab Pipelines
https://gitlab.com/ggracechoi/cs373-group-10/-/pipelines/1242390272

### Git SHA
77bf55d67edb14c617ebc096b4f5336a514e2d3c

<!-- # CS 373 Group 10 

**The Team:** 
- Grace Choi
- Eshi Kohli
- Tvisha Andharia
- Benjamin Zimmerman
- Saketh Kotamraju 

**Project Name:**
ATXAssist

**Project Description:** 
As Austin sees a growing influx of residents, an increasing number of low-income communities are facing displacement due to gentrification. This has caused the cost of living to soar, putting many individuals and families in difficult positions. Our project seeks to establish a dedicated space for underserved communities to learn about essential resources. By connecting Austin residents to information about affordable food, clothes, and housing, we hope to make an impact in our local community.

**URLs of Data Sources:** 
- RESTful API - [Yelp Fusion API](https://docs.developer.yelp.com/docs/fusion-intro)
- [Google Maps API](https://developers.google.com/maps)
- [Austin Food Pantries](https://www.findhelp.org/food/food-pantry--austin-tx)
- [Austin Thrift Stores](https://www.yelp.com/search?cflt=thrift_stores&find_loc=austin%2C+TX)
- [Austin Affordable Housing](https://data.austintexas.gov/widgets/4syj-z4ky?mobile_redirect=true)

**Models:** 
1. Food Pantries (790 instances)
    - Name - sortable
    - Location - sortable 
    - Last reviewed time - sortable
    - Cost - sortable
    - Language - sortable
    - Image of food pantry - media
    - Location of food pantry on a map - media
    - Website
    - Brief overview
2. Thrift Stores (90 instances)
    - Name - sortable
    - Location - sortable
    - Rating - sortable
    - Number of reviews - sortable
    - Accessibility - sortable
    - Image of thrift store - media
    - Location of thrift store on a map - media
    - Website
    - Brief overview
3. Affordable Housing (591 instances)
    - Name - sortable
    - Location - sortable
    - Students only - sortable
    - Disabled Community - sortable
    - Waitlist - sortable 
    - Image of affordable housing - media
    - Location of affordable housing on a map - media
    - Website
    - Brief overview

**Connection Between Models/Instances:** 
We will connect all of the models together by proximity (e.g. if you’re looking at an instance of a food pantry, we’ll display instances of thrift stores and affordable housing in the same area).

**Questions our project will answer:** 
- Where are places to get affordable food?
- Where are places to get affordable clothes?
- Where are places to get affordable housing? -->
